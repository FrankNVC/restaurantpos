/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Main.Application;
import Model.Restaurant;
import Utilities.JPagingPanel;
import Utilities.Localization;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Nguyen Minh Sang
 */
public class SearchAccountController extends AbstractAction implements ActionListener {

    private Restaurant r;

    /**
     *
     * @param r
     */
    public SearchAccountController(Restaurant r) {
        this.r = r;
    }

    /**
     * defines event when search account is clicked
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        //String[] column_names = {"Username", "FullName", "Gender", "Age", "Account type"};
        String[] column_names = {Localization.getResourceBundle().getString("AdminView.column_names.Username"), Localization.getResourceBundle().getString("AdminView.column_names.FullName"), Localization.getResourceBundle().getString("AdminView.column_names.Gender"), Localization.getResourceBundle().getString("AdminView.column_names.Age"), Localization.getResourceBundle().getString("AdminView.column_names.Account_type")};
        ArrayList<String[]> data = r.getAccountList(Application.av.getTxt_search().getText(), true, true, true);
        DefaultTableModel list_account_table_model = new DefaultTableModel(null, column_names) {
            @Override
            public Class<?> getColumnClass(int column) {
                return (column == 0) ? Integer.class : Object.class;
            }
        };

        for (int i = 0; i < data.size(); i++) {
            list_account_table_model.addRow(data.get(i));
        }

        if (Application.av.getPnl_list_account() != null) {
            Application.av.getPnl_center_account_grid_view().remove(Application.av.getPnl_list_account());
        }

        JPagingPanel pnl_list_account = new JPagingPanel(list_account_table_model);

        Application.av.getPnl_center_account_grid_view().add(pnl_list_account, BorderLayout.CENTER);
        Application.av.setPnl_list_account(pnl_list_account);

        pnl_list_account.getTable().setCellSelectionEnabled(false);
        pnl_list_account.getTable().setRowSelectionAllowed(true);
        pnl_list_account.getTable().getSelectionModel().addListSelectionListener(new SelectAccountController(r));

        Application.av.getBtn_delete_account().setEnabled(false);
        Application.av.getBtn_update_account().setEnabled(false);
        Application.av.getTxt_search().setText(null);
        Application.av.getTxt_search().requestFocus();
        Application.av.revalidate();
    }
}
