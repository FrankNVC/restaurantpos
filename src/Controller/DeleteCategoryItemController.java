/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Main.Application;
import Model.Restaurant;
import Utilities.Localization;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author chi linh
 */
public class DeleteCategoryItemController extends KeyAdapter implements ActionListener {

    private Restaurant r;

    /**
     *
     * @param r
     */
    public DeleteCategoryItemController(Restaurant r) {
        this.r = r;
    }

    /**
     * defines event when a category is deleted
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Application.cmv.deselectAllTable();
        JTree tree = Application.cmv.getTree();
        if (tree.getSelectionPaths() != null) {
            if (Application.cmv.getRdb_category().isSelected()) {
                int result;
                if (tree.getSelectionPaths().length > 1) {
                    result = r.showConfirmDialog( Localization.getResourceBundle().getString("DeleteCategoryItemController.categories"));
                } else {
                    result = r.showConfirmDialog(Localization.getResourceBundle().getString("DeleteCategoryItemController.category"));
                }
                if (result == JOptionPane.YES_OPTION) {
                    ArrayList<String> categories = new ArrayList<>();
                    for (int i = 0; i < tree.getSelectionPaths().length; i++) {
                        if (tree.getSelectionPaths()[i].getPathCount() == 2) {
                            categories.add((String) ((DefaultMutableTreeNode) tree.getSelectionPaths()[i].getLastPathComponent()).getUserObject());
                        } else {
                            categories.add((String) ((DefaultMutableTreeNode) tree.getSelectionPaths()[i].getParentPath().getLastPathComponent()).getUserObject());
                        }
                    }
                    JOptionPane.showMessageDialog(null, r.deleteCategory(categories), Localization.getResourceBundle().getString("DeleteCategoryItemController.message_1"), JOptionPane.INFORMATION_MESSAGE);
                    r.update();
                }
            } else {
                int result;
                if (tree.getSelectionPaths().length > 1) {
                    result = r.showConfirmDialog(Localization.getResourceBundle().getString("DeleteCategoryItemController.items"));
                } else {
                    result = r.showConfirmDialog(Localization.getResourceBundle().getString("DeleteCategoryItemController.item"));
                }
                if (result == JOptionPane.YES_OPTION) {
                    ArrayList<String> itemNames = new ArrayList<>();
                    for (int i = 0; i < tree.getSelectionPaths().length; i++) {
                        String selectedItem = (String) ((DefaultMutableTreeNode) tree.getSelectionPaths()[i].getLastPathComponent()).getUserObject();
                        String[] nameAndPrice = selectedItem.split("\\-");
                        itemNames.add(nameAndPrice[0].trim());
                    }
                    JOptionPane.showMessageDialog(null, r.deleteItem(itemNames), Localization.getResourceBundle().getString("DeleteCategoryItemController.message_2"), JOptionPane.INFORMATION_MESSAGE);
                    r.update();
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("DeleteCategoryItemController.chooseCategoryOrItem"));
        }
    }

    /**
     *
     * @param e
     */
    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_DELETE) {
            if (Application.cmv.getTree().getSelectionPaths() != null) {
                Application.cmv.getBtn_delete().doClick();
            }
        }
    }
}
