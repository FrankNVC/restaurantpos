package View;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import Controller.ChangePasswordController;
import Controller.CreateAccountController;
import Controller.CreateCategoryItemController;
import Controller.CreateCustomerController;
import Controller.CreateDiscountController;
import Controller.CustomerViewController;
import Controller.DeleteAccountController;
import Controller.DeleteCategoryItemController;
import Controller.DeleteOrderItemController;
import Controller.DeleteTableController;
import Controller.EditAccountController;
import Controller.EditCategoryItemController;
import Controller.EditDiscountController;
import Controller.EditOrderItemController;
import Controller.EditTableController;
import Controller.ExitController;
import Controller.MergeTableController;
import Controller.PaymentController;
import Controller.PrintOrderController;
import Controller.RefreshOrderController;
import Controller.ReportController;
import Controller.SwitchTableController;
import Controller.UnmergeTableController;
import Controller.ViewAllBillsController;
import Controller.ViewOrderController;
import Main.Application;
import Model.Account;
import Model.Restaurant;
import Utilities.Localization;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Observable;
import java.util.Observer;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

/**
 *
 * @author Na
 */
public class TheMenuBar extends JMenuBar implements Observer {

    private Restaurant r;
    private JMenu menu_file = new JMenu(Localization.getResourceBundle().getString("menu_file"));
    private JMenuItem file_change_password = new JMenuItem(Localization.getResourceBundle().getString("file_change_password"));
    private JMenuItem file_exit = new JMenuItem(Localization.getResourceBundle().getString("file_exit"));
    private JMenu menu_help = new JMenu(Localization.getResourceBundle().getString("menu_help"));
    private JMenuItem help_user_guide = new JMenuItem(Localization.getResourceBundle().getString("help_user_guide"));
    private JMenuItem help_about = new JMenuItem(Localization.getResourceBundle().getString("help_about"));
    private JMenu menu_discount = new JMenu(Localization.getResourceBundle().getString("menu_discount"));
    private JMenuItem menu_create_discount = new JMenuItem(Localization.getResourceBundle().getString("menu_create_discount"));
    private JMenuItem menu_edit_discount = new JMenuItem(Localization.getResourceBundle().getString("menu_edit_discount"));
    private JMenu menu_account = new JMenu(Localization.getResourceBundle().getString("menu_account"));
    private JMenuItem menu_create_account = new JMenuItem(Localization.getResourceBundle().getString("menu_create_account"));
    private JMenuItem menu_edit_account = new JMenuItem(Localization.getResourceBundle().getString("menu_edit_account"));
    private JMenuItem menu_delete_account = new JMenuItem(Localization.getResourceBundle().getString("menu_delete_account"));
    private JMenu menu_restaurantMenu = new JMenu(Localization.getResourceBundle().getString("menu_restaurantMenu"));
    private JMenuItem menu_create = new JMenuItem(Localization.getResourceBundle().getString("menu_create"));
    private JMenuItem menu_edit = new JMenuItem(Localization.getResourceBundle().getString("menu_edit"));
    private JMenuItem menu_delete = new JMenuItem(Localization.getResourceBundle().getString("menu_delete"));
    private JMenu menu_table = new JMenu(Localization.getResourceBundle().getString("menu_table"));
    private JMenuItem menu_edit_table = new JMenuItem(Localization.getResourceBundle().getString("menu_edit_table"));
    private JMenuItem menu_delete_table = new JMenuItem(Localization.getResourceBundle().getString("menu_delete_table"));
    private JMenuItem menu_merge_table = new JMenuItem(Localization.getResourceBundle().getString("menu_merge_table"));
    private JMenuItem menu_unmerge_table = new JMenuItem(Localization.getResourceBundle().getString("menu_unmerge_table"));
    private JMenuItem menu_switch_table = new JMenuItem(Localization.getResourceBundle().getString("menu_switch_table"));
    private JMenu menu_order_item = new JMenu(Localization.getResourceBundle().getString("menu_order_item"));
    private JMenuItem menu_refresh_order = new JMenuItem(Localization.getResourceBundle().getString("menu_refresh_order"));
    private JMenuItem menu_edit_order = new JMenuItem(Localization.getResourceBundle().getString("menu_edit_order"));
    private JMenuItem menu_delete_order = new JMenuItem(Localization.getResourceBundle().getString("menu_delete_order"));
    private JMenuItem menu_view_order = new JMenuItem(Localization.getResourceBundle().getString("menu_view_order"));
    private JMenuItem menu_print_order = new JMenuItem(Localization.getResourceBundle().getString("menu_print_order"));
    private JMenuItem menu_view_payment = new JMenuItem(Localization.getResourceBundle().getString("menu_view_payment"));
    private JMenu menu_report = new JMenu(Localization.getResourceBundle().getString("menu_report"));
    private JMenuItem menu_view_bills = new JMenuItem(Localization.getResourceBundle().getString("menu_view_bills"));
    private JMenuItem menu_create_report = new JMenuItem(Localization.getResourceBundle().getString("menu_create_report"));
    private JMenu menu_customer = new JMenu(Localization.getResourceBundle().getString("menu_customer"));
    private JMenuItem menu_customer_view = new JMenuItem(Localization.getResourceBundle().getString("menu_customer_view"));
    private JMenuItem menu_customer_create = new JMenuItem(Localization.getResourceBundle().getString("menu_customer_create"));
    ///////////////////////////////////////////////////////////////////////////////////////////
    private JMenu menu_language = new JMenu(Localization.getResourceBundle().getString("menu_language"));
    private JMenuItem language_english = new JMenuItem(Localization.getResourceBundle().getString("language_english"));
    private JMenuItem language_vietnamese = new JMenuItem(Localization.getResourceBundle().getString("language_vietnamese"));

    //////////////////////////////////////////////////////////////////////////////////////////
    public TheMenuBar(final Restaurant r, final JFrame mainFrame) {

        this.r = r;
        menu_file.setMnemonic('F');
        file_change_password.setMnemonic('C');
        file_exit.setMnemonic('E');

        menu_discount.setMnemonic('D');
        menu_create_discount.setMnemonic('C');
        menu_edit_discount.setMnemonic('E');

        menu_restaurantMenu.setMnemonic('R');
        menu_create.setMnemonic('C');
        menu_edit.setMnemonic('E');
        menu_delete.setMnemonic('D');

        menu_table.setMnemonic('T');
        menu_edit_table.setMnemonic('E');
        menu_delete_table.setMnemonic('D');
        menu_merge_table.setMnemonic('M');
        menu_unmerge_table.setMnemonic('U');
        menu_switch_table.setMnemonic('S');

        menu_order_item.setMnemonic('O');
        menu_view_order.setMnemonic('V');
        menu_refresh_order.setMnemonic('R');
        menu_edit_order.setMnemonic('E');
        menu_delete_order.setMnemonic('D');
        menu_print_order.setMnemonic('P');
        menu_view_payment.setMnemonic('A');

        menu_report.setMnemonic('E');
        menu_create_report.setMnemonic('G');

        menu_help.setMnemonic('H');
        help_about.setMnemonic('A');
        help_user_guide.setMnemonic('U');

        menu_account.setMnemonic('A');
        menu_create_account.setMnemonic('C');
        menu_edit_account.setMnemonic('E');
        menu_delete_account.setMnemonic('D');

        KeyStroke alt_f4 = KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_MASK);
        file_exit.setAccelerator(alt_f4);

        if (Application.currentUser != null) {
            menu_file.add(file_change_password);
            menu_file.addSeparator();
            file_change_password.addActionListener(new ChangePasswordController());
        }

        menu_file.add(file_exit);

        KeyStroke f1 = KeyStroke.getKeyStroke(KeyEvent.VK_F1, InputEvent.BUTTON1_MASK);
        help_user_guide.setAccelerator(f1);

        menu_help.add(help_user_guide);

        KeyStroke f3 = KeyStroke.getKeyStroke(KeyEvent.VK_F3, InputEvent.BUTTON1_MASK);
        help_about.setAccelerator(f3);

        menu_help.add(help_about);

        //////////////////////////////////////////////////////////////////////////////////////////
        KeyStroke le = KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.BUTTON1_MASK);
        language_english.setAccelerator(le);

        menu_language.add(language_english);

        KeyStroke lv = KeyStroke.getKeyStroke(KeyEvent.VK_L, InputEvent.BUTTON1_MASK);
        language_vietnamese.setAccelerator(lv);

        menu_language.add(language_vietnamese);

        if (Localization.getCOUNTRY().equals("US")) {
            language_english.setEnabled(false);
            language_vietnamese.setEnabled(true);
        } else {
            language_english.setEnabled(true);
            language_vietnamese.setEnabled(false);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////
        this.add(menu_file);

        if (Application.currentUser != null && Application.currentUser.getType() != Account.AccountType.ADMINISTRATOR) {

            if (Application.currentUser.getType() == Account.AccountType.MANAGER) {
                menu_create_discount.addActionListener(new CreateDiscountController());
                menu_edit_discount.addActionListener(new EditDiscountController(r));
                menu_discount.add(menu_create_discount);
                menu_discount.add(menu_edit_discount);
                this.add(menu_discount);

                menu_create.addActionListener(new CreateCategoryItemController());
                menu_edit.addActionListener(new EditCategoryItemController(r));
                menu_delete.addActionListener(new DeleteCategoryItemController(r));

                menu_restaurantMenu.add(menu_create);
                menu_restaurantMenu.add(menu_edit);
                menu_restaurantMenu.add(menu_delete);

                this.add(menu_restaurantMenu);
            }
            menu_edit_table.addActionListener(new EditTableController(r));
            menu_delete_table.addActionListener(new DeleteTableController(r));
            menu_merge_table.addActionListener(new MergeTableController(r));
            menu_unmerge_table.addActionListener(new UnmergeTableController(r));
            menu_switch_table.addActionListener(new SwitchTableController(r));
            menu_table.add(menu_edit_table);
            menu_table.add(menu_delete_table);
            menu_table.addSeparator();
            menu_table.add(menu_merge_table);
            menu_table.add(menu_unmerge_table);
            menu_table.add(menu_switch_table);
            this.add(menu_table);

            menu_refresh_order.addActionListener(new RefreshOrderController(r));
            menu_edit_order.addActionListener(new EditOrderItemController(r));
            menu_delete_order.addActionListener(new DeleteOrderItemController(r));
            menu_view_order.addActionListener(new ViewOrderController());
            menu_print_order.addActionListener(new PrintOrderController());
            menu_view_payment.addActionListener(new PaymentController(r));
            menu_order_item.add(menu_view_order);
            menu_order_item.add(menu_refresh_order);
            menu_order_item.add(menu_edit_order);
            menu_order_item.add(menu_delete_order);
            menu_order_item.add(menu_print_order);
            menu_order_item.addSeparator();
            menu_order_item.add(menu_view_payment);
            this.add(menu_order_item);

            if (Application.currentUser.getType() == Account.AccountType.MANAGER) {
                menu_view_bills.addActionListener(new ViewAllBillsController(r));
                menu_create_report.addActionListener(new ReportController());
                menu_report.add(menu_view_bills);
                menu_report.add(menu_create_report);
                this.add(menu_report);

                menu_customer_view.addActionListener(new CustomerViewController(r));
                menu_customer_create.addActionListener(new CreateCustomerController());
                menu_customer.add(menu_customer_view);
                menu_customer.add(menu_customer_create);
                this.add(menu_customer);
            }
        }
        if (Application.currentUser != null && Application.currentUser.getType() == Account.AccountType.ADMINISTRATOR) {

            menu_create_account.addActionListener(new CreateAccountController());
            menu_edit_account.addActionListener(new EditAccountController(r));
            menu_delete_account.addActionListener(new DeleteAccountController(r));
            menu_account.add(menu_create_account);
            menu_account.add(menu_edit_account);
            menu_account.add(menu_delete_account);
            this.add(menu_account);
        }

        file_exit.addActionListener(new ExitController(mainFrame));
        help_about.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ImageIcon icon_logo = new ImageIcon(this.getClass().getResource("/Images/banner.png"));
                JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("help_about.showMessageDialog.message"), Localization.getResourceBundle().getString("help_about.showMessageDialog.title"), JOptionPane.OK_OPTION, icon_logo);
            }
        });

        help_user_guide.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
                if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
                    try {
                        desktop.browse(this.getClass().getResource("/UserGuide/administrator.html").toURI());
                    } catch (URISyntaxException | IOException ex) {
                        System.out.println(ex);
                    }
                }
            }
        });
        this.add(menu_help);

        language_english.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                changeLanguage("en", "US", mainFrame);
            }
        });

        language_vietnamese.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                changeLanguage("vi", "VN", mainFrame);
            }
        });
        this.add(menu_language);
    }

    public JMenu getMenu_account() {
        return menu_account;
    }

    public JMenuItem getMenu_create_account() {
        return menu_create_account;
    }

    public JMenuItem getMenu_create() {
        return menu_create;
    }

    public JMenuItem getMenu_create_discount() {
        return menu_create_discount;
    }

    public JMenuItem getMenu_create_report() {
        return menu_create_report;
    }

    public JMenuItem getMenu_delete() {
        return menu_delete;
    }

    public JMenuItem getMenu_delete_account() {
        return menu_delete_account;
    }

    public JMenuItem getMenu_delete_order() {
        return menu_delete_order;
    }

    public JMenuItem getMenu_delete_table() {
        return menu_delete_table;
    }

    public JMenu getMenu_discount() {
        return menu_discount;
    }

    public JMenuItem getMenu_edit() {
        return menu_edit;
    }

    public JMenuItem getMenu_edit_account() {
        return menu_edit_account;
    }

    public JMenuItem getMenu_edit_discount() {
        return menu_edit_discount;
    }

    public JMenuItem getMenu_edit_order() {
        return menu_edit_order;
    }

    public JMenuItem getMenu_edit_table() {
        return menu_edit_table;
    }

    public JMenu getMenu_file() {
        return menu_file;
    }

    public JMenu getMenu_help() {
        return menu_help;
    }

    public JMenuItem getMenu_merge_table() {
        return menu_merge_table;
    }

    public JMenu getMenu_order_item() {
        return menu_order_item;
    }

    public JMenuItem getMenu_print_order() {
        return menu_print_order;
    }

    public JMenuItem getMenu_refresh_order() {
        return menu_refresh_order;
    }

    public JMenu getMenu_report() {
        return menu_report;
    }

    public JMenu getMenu_restaurantMenu() {
        return menu_restaurantMenu;
    }

    public JMenuItem getMenu_switch_table() {
        return menu_switch_table;
    }

    public JMenu getMenu_table() {
        return menu_table;
    }

    public JMenuItem getMenu_unmerge_table() {
        return menu_unmerge_table;
    }

    public JMenuItem getMenu_view_bills() {
        return menu_view_bills;
    }

    public JMenuItem getMenu_view_order() {
        return menu_view_order;
    }

    public JMenuItem getMenu_view_payment() {
        return menu_view_payment;
    }

    public void changeLanguage(String language, String country, JFrame mainFrame) {

        Localization.setLANGUAGE(language);
        Localization.setCOUNTRY(country);

        mainFrame.dispose();
        //TheMenuBar
        TheMenuBar menubar = new TheMenuBar(r, mainFrame);
        mainFrame.setJMenuBar(menubar);
        //LoginView
        Application.lv.setLbl_username(Localization.getResourceBundle().getString("lbl_username"));
        Application.lv.setLbl_password(Localization.getResourceBundle().getString("lbl_password"));
        Application.lv.setLbl_contact(Localization.getResourceBundle().getString("lbl_contact"));
        Application.lv.setIcon_login();
        Application.lv.setIcon_login_clicked();
        Application.lv.setBtn_login_ToolTipText(Localization.getResourceBundle().getString("btn_login.tooltip"));

        if (Application.lv.getLbl_error_username().getText().equalsIgnoreCase("Please enter your username!")) {
            Application.lv.getLbl_error_username().setText(Localization.getResourceBundle().getString("Restaurant.enterUsername"));
            Application.lv.getLbl_error_username().setForeground(Color.red);
            Application.lv.getLbl_error_password().setText("");
        } else if (Application.lv.getLbl_error_username().getText().equalsIgnoreCase("Nhập tài khoản!")) {
            Application.lv.getLbl_error_username().setText(Localization.getResourceBundle().getString("Restaurant.enterUsername"));
            Application.lv.getLbl_error_username().setForeground(Color.red);
            Application.lv.getLbl_error_password().setText("");
        } else if (Application.lv.getLbl_error_username().getText().equalsIgnoreCase("Username not found!")) {
            Application.lv.getLbl_error_username().setText(Localization.getResourceBundle().getString("Restaurant.usernameNotFound"));
            Application.lv.getLbl_error_username().setForeground(Color.red);
            Application.lv.getLbl_error_password().setText("");
        } else if (Application.lv.getLbl_error_username().getText().equalsIgnoreCase("Tài khoản này không tồn tại!")) {
            Application.lv.getLbl_error_username().setText(Localization.getResourceBundle().getString("Restaurant.usernameNotFound"));
            Application.lv.getLbl_error_username().setForeground(Color.red);
            Application.lv.getLbl_error_password().setText("");
        }

        if (Application.lv.getLbl_error_password().getText().equalsIgnoreCase("Please enter the password!")) {
            Application.lv.getLbl_error_password().setText(Localization.getResourceBundle().getString("Restaurant.enterPassword"));
            Application.lv.getLbl_error_password().setForeground(Color.red);
            Application.lv.getLbl_error_username().setText("");
        } else if (Application.lv.getLbl_error_password().getText().equalsIgnoreCase("Nhập mật khẩu!")) {
            Application.lv.getLbl_error_password().setText(Localization.getResourceBundle().getString("Restaurant.enterPassword"));
            Application.lv.getLbl_error_password().setForeground(Color.red);
            Application.lv.getLbl_error_username().setText("");
        } else if (Application.lv.getLbl_error_password().getText().equalsIgnoreCase("Incorrect password!")) {
            Application.lv.getLbl_error_password().setText(Localization.getResourceBundle().getString("Restaurant.incorrectPassword"));
            Application.lv.getLbl_error_password().setForeground(Color.red);
            Application.lv.getLbl_error_username().setText("");
        } else if (Application.lv.getLbl_error_password().getText().equalsIgnoreCase("Mật khẩu không đúng!")) {
            Application.lv.getLbl_error_password().setText(Localization.getResourceBundle().getString("Restaurant.incorrectPassword"));
            Application.lv.getLbl_error_password().setForeground(Color.red);
            Application.lv.getLbl_error_username().setText("");
        }

        //Application
        mainFrame.setTitle(Localization.getResourceBundle().getString("Application.frame.title"));
        //AdminView
        if (Application.currentUser != null) {
            Application.av.getPnl_user_status().getLbl_user_status().setText(Localization.getResourceBundle().getString("CurrentUserStatusPanel.lbl_user_status") + Application.currentUser.getUsername());
            Application.av.getPnl_user_status().getBtn_logout().setToolTipText(Localization.getResourceBundle().getString("CurrentUserStatusPanel.btn_logout"));
            Application.av.getPnl_user_status().getBtn_user_image().setToolTipText(Localization.getResourceBundle().getString("CurrentUserStatusPanel.btn_user_image"));
            Application.cmv.getPnl_user_status().getLbl_user_status().setText(Localization.getResourceBundle().getString("CurrentUserStatusPanel.lbl_user_status") + Application.currentUser.getUsername());
            Application.cmv.getPnl_user_status().getBtn_logout().setToolTipText(Localization.getResourceBundle().getString("CurrentUserStatusPanel.btn_logout"));
            Application.cmv.getPnl_user_status().getBtn_user_image().setToolTipText(Localization.getResourceBundle().getString("CurrentUserStatusPanel.btn_user_image"));

        }
        Application.av_cusp.setIcon_logout();
        Application.av_cusp.setIcon_logout_clicked();
        Application.cmv_cusp.setIcon_logout();
        Application.cmv_cusp.setIcon_logout_clicked();

        Application.av.setBtn_search_tooltip(Localization.getResourceBundle().getString("AdminView.btn_search.tooltip"));
        Application.av.setBtn_create_account_tooltip(Localization.getResourceBundle().getString("AdminView.btn_create_account.tooltip"));
        Application.av.setBtn_delete_account_tooltip(Localization.getResourceBundle().getString("AdminView.btn_delete_account.tooltip"));
        Application.av.setBtn_update_account_tooltip(Localization.getResourceBundle().getString("AdminView.btn_update_account.tooltip"));
        Application.av.setCb_admin(Localization.getResourceBundle().getString("AdminView.cb_admin"));
        Application.av.setCb_manager(Localization.getResourceBundle().getString("AdminView.cb_manager"));
        Application.av.setCb_cashier(Localization.getResourceBundle().getString("AdminView.cb_cashier"));
        Application.av.setBorder_west_search(Localization.getResourceBundle().getString("AdminView.border_west_search"));
        Application.av.setBorder_west_detail(Localization.getResourceBundle().getString("AdminView.border_west_detail"));
        Application.av.setBorder_center_account(Localization.getResourceBundle().getString("AdminView.border_center_account"));
        Application.av.setFields(Localization.getResourceBundle().getString("AdminView.fields.Username"), Localization.getResourceBundle().getString("AdminView.fields.Account_type"), Localization.getResourceBundle().getString("AdminView.fields.Full_name"), Localization.getResourceBundle().getString("AdminView.fields.Gender"), Localization.getResourceBundle().getString("AdminView.fields.Social_security_number"), Localization.getResourceBundle().getString("AdminView.fields.Date_of_birth"), Localization.getResourceBundle().getString("AdminView.fields.Phone_number"), Localization.getResourceBundle().getString("AdminView.fields.Email"), Localization.getResourceBundle().getString("AdminView.fields.Address"));
        Application.av.reload();

        //CashierManagerView
        Application.cmv.setLbl_total(Localization.getResourceBundle().getString("CashierManagerView.lbl_total"));
        Application.cmv.setBtn_print(Localization.getResourceBundle().getString("CashierManagerView.btn_print"));
        Application.cmv.setBtn_payment(Localization.getResourceBundle().getString("CashierManagerView.btn_payment"));
        Application.cmv.setBtn_edit_order(Localization.getResourceBundle().getString("CashierManagerView.btn_edit_order"));
        Application.cmv.setBtn_refresh_order(Localization.getResourceBundle().getString("CashierManagerView.btn_refresh_order"));
        Application.cmv.setBtn_delete_order(Localization.getResourceBundle().getString("CashierManagerView.btn_delete_order"));
        Application.cmv.setBtn_view_order(Localization.getResourceBundle().getString("CashierManagerView.btn_view_order"));
        Application.cmv.setBtn_delete_table(Localization.getResourceBundle().getString("CashierManagerView.btn_delete_table"));
        Application.cmv.setBtn_merge(Localization.getResourceBundle().getString("CashierManagerView.btn_merge"));
        Application.cmv.setBtn_unmerge(Localization.getResourceBundle().getString("CashierManagerView.btn_unmerge"));
        Application.cmv.setBtn_switch(Localization.getResourceBundle().getString("CashierManagerView.btn_switch"));
        Application.cmv.setBtn_edit_table(Localization.getResourceBundle().getString("CashierManagerView.btn_edit_table"));
        Application.cmv.setRdb_vacant(Localization.getResourceBundle().getString("CashierManagerView.rdb_vacant"));
        Application.cmv.setRdb_occupied(Localization.getResourceBundle().getString("CashierManagerView.rdb_occupied"));
        Application.cmv.setRdb_reserved(Localization.getResourceBundle().getString("CashierManagerView.rdb_reserved"));
        Application.cmv.setRdb_category(Localization.getResourceBundle().getString("CashierManagerView.rdb_category"));
        Application.cmv.setRdb_item(Localization.getResourceBundle().getString("CashierManagerView.rdb_item"));
        Application.cmv.setBtn_create(Localization.getResourceBundle().getString("CashierManagerView.btn_create"));
        Application.cmv.setBtn_delete(Localization.getResourceBundle().getString("CashierManagerView.btn_delete"));
        Application.cmv.setBtn_edit(Localization.getResourceBundle().getString("CashierManagerView.btn_edit"));
        Application.cmv.setBtn_oval_table(Localization.getResourceBundle().getString("CashierManagerView.btn_oval_table"));
        Application.cmv.setBtn_rectangle_table(Localization.getResourceBundle().getString("CashierManagerView.btn_rectangle_table"));
        Application.cmv.setLbl_remaining(Localization.getResourceBundle().getString("CashierManagerView.lbl_remaining"));
        Application.cmv.setCb_multi_table(Localization.getResourceBundle().getString("CashierManagerView.cb_multi_table"));

        Application.cmv.setBorder_west(Localization.getResourceBundle().getString("CashierManagerView.border_west"));
        Application.cmv.setBorder_center_map(Localization.getResourceBundle().getString("CashierManagerView.border_center_map"));
        Application.cmv.setBorder_east(Localization.getResourceBundle().getString("CashierManagerView.border_east"));
        //CashierManagerView.addTable.locationOutOfMap = This location is out of map!                           dialog
        //CashierManagerView.moveSingleTable.locationNotAvailable_1 = This location is not available!           dialog
        //CashierManagerView.moveSingleTable.locationNotAvailable_2 = This location is not available!           dialog
        //CashierManagerView.moveSingleTable.locationNotAvailable_3 = This location is not available!           dialog
        //CashierManagerView.moveMergedTable.locationNotAvailable_1 = This location is not available!           dialog
        //CashierManagerView.moveMergedTable.locationNotAvailable_2 = This location is not available!           dialog
        //CashierManagerView.moveMergedTable.locationNotAvailable_3 = This location is not available!           dialog
        //CashierManagerView.updateOrderItemList.no = No                                                        already
        //CashierManagerView.updateOrderItemList.name = Name                                                    already
        //CashierManagerView.updateOrderItemList.quantity = Quantity                                            already
        //CashierManagerView.updateOrderItemList.note = Note                                                    already
        //CashierManagerView.updateOrderItemList.status = Status                                                already
        //CashierManagerView.updateOrderItemList.price = Price                                                  already
        //CashierManagerView.importData.tableOverLimit_1 = The number of table is over limit!                   dialog
        //CashierManagerView.importData.tableOverLimit_2 = The number of table is over limit!                   dialog
        //CashierManagerView.importData.options.confirm = Confirm                                               dialog message
        //CashierManagerView.importData.options.cancel = Cancel                                                 dialog title
        //CashierManagerView.importData.orderItemInformation = Order item information                           dialog
        //CashierManagerView.importData.manyCharacters = Word has too many characters!                          ceoif.getTxt_note().setText
        //CashierManagerView.importData.quantityCannotBe0 = Quantity cannot be 0                                ceoif.getLbl_quantity_error().setText
        //CashierManagerView.importData_2.tableOverLimit_1 = The number of table is over limit!                 dialog
        //CashierManagerView.importData_2.tableOverLimit_2 = The number of table is over limit!                 dialog
        Application.cmv.updateOrderItemList();

        Application.cmv.updateTotal();                              // private changed to public
        Application.cmv.updateMenuList(Application.cmv.getTop());   // private changed to public
        Application.cmv.updateOrderItemList();

// = Print order
// = Payment
// = Edit order
// = Refresh order
// = Delete order
// = View order
// = Delete table
// = Merge Table
// = Unmerge Table
// = Switch Table
// = Edit Table
//= Vacant
// = Occupied
// = Reserved
// = Category
// = Item
//= Create
// = Delete
// = Edit
// = Oval Table
// = Rectangle Table
// = Remaining: 
// = Multi Table
//CashierManagerView.top = Menu
// = Menu
// = Restaurant Map
// = Order Detail
        mainFrame.setVisible(true);
    }

    @Override
    public void update(Observable o, Object arg) {
        this.validate();
    }
}
