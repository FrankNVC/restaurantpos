/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Main.Application;
import Model.Restaurant;
import Model.Table;
import Utilities.Localization;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author Frank NgVietCuong
 */
public class RefreshOrderController implements ActionListener {

    private Restaurant r;
    /**
     *
     * @param r
     */
    public RefreshOrderController(Restaurant r) {
        this.r = r;
    }

    /**
     * defines events when an ordered is refreshed
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (Application.cmv.getCurrent_tables().size() == 1) {
            if(!Application.cmv.getCurrent_tables().get(0).getO().getOrderItems().isEmpty()) {
                String options[] = {Localization.getResourceBundle().getString("RefreshOrderController.options.Confirm"), Localization.getResourceBundle().getString("RefreshOrderController.options.Cancel")};
                int result = JOptionPane.showOptionDialog(null, Localization.getResourceBundle().getString("RefreshOrderController.refreshOrder.message"), Localization.getResourceBundle().getString("RefreshOrderController.refreshOrder.title"), JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, null);
                if (result == JOptionPane.YES_OPTION) {
                    Table table = Application.cmv.getCurrent_tables().get(0);
                    r.refreshOrder(table);
                    Application.cmv.updateTable(table);
                    Application.cmv.updateOrderItemList();
                    r.update();
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("RefreshOrderController.chooseTable"));
        }
    }
}
