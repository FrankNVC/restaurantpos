/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Main.Application;
import Model.Order;
import Utilities.Localization;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author Nguyen Minh Sang
 */
public class PrintOrderController implements ActionListener {

    /**
     * defines events when print order button is clicked
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (Application.cmv.getCurrent_tables().size() == 1) {
            Order o = Application.cmv.getCurrent_tables().get(0).getO();
            o.printOrder();
        } else {
            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("PrintOrderController.chooseTable"));
        }
    }
}