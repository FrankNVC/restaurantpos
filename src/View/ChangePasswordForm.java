/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.JDialogKeyListener;
import Utilities.Localization;
import Utilities.SpringUtilities;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SpringLayout;

/**
 * this is the form for changing password of a user account
 *
 * @author Pham
 */
public class ChangePasswordForm extends JPanel {

    private JLabel lbl_username = new JLabel(Localization.getResourceBundle().getString("ChangePasswordForm.lbl_username"));
    private JLabel lbl_old_password = new JLabel(Localization.getResourceBundle().getString("ChangePasswordForm.lbl_old_password"));
    private JLabel lbl_new_password = new JLabel(Localization.getResourceBundle().getString("ChangePasswordForm.lbl_new_password"));
    private JLabel lbl_confirm_password = new JLabel(Localization.getResourceBundle().getString("ChangePasswordForm.lbl_confirm_password"));
    private JLabel lbl_account_type = new JLabel(Localization.getResourceBundle().getString("ChangePasswordForm.lbl_account_type"));
    private JLabel lbl_full_name = new JLabel(Localization.getResourceBundle().getString("ChangePasswordForm.lbl_full_name"));
    private JLabel lbl_gender = new JLabel(Localization.getResourceBundle().getString("ChangePasswordForm.lbl_gender"));
    private JLabel lbl_ssn = new JLabel(Localization.getResourceBundle().getString("ChangePasswordForm.lbl_ssn"));
    private JLabel lbl_dob = new JLabel(Localization.getResourceBundle().getString("ChangePasswordForm.lbl_dob"));
    private JLabel lbl_chosen_dob = new JLabel("", JLabel.TRAILING);
    private JLabel lbl_phone = new JLabel(Localization.getResourceBundle().getString("ChangePasswordForm.lbl_phone"));
    private JLabel lbl_email = new JLabel(Localization.getResourceBundle().getString("ChangePasswordForm.lbl_email"));
    private JLabel lbl_address = new JLabel(Localization.getResourceBundle().getString("ChangePasswordForm.lbl_address"));
    private JLabel lbl_old_password_error = new JLabel();
    private JLabel lbl_new_password_error = new JLabel();
    private JLabel lbl_confirm_password_error = new JLabel();
    private JTextField txt_username = new JTextField(15);
    private JPasswordField pwf_old_password = new JPasswordField(15);
    private JPasswordField pwf_new_password = new JPasswordField(15);
    private JPasswordField pwf_confirm_password = new JPasswordField(15);
    private JTextField txt_full_name = new JTextField(15);
    private JTextField txt_ssn = new JTextField(15);
    private JTextField txt_phone = new JTextField(15);
    private JTextField txt_email = new JTextField(15);
    private JTextArea txt_address = new JTextArea();
    private ButtonGroup group_account_type = new ButtonGroup();
    private ButtonGroup group_gender = new ButtonGroup();
    private JRadioButton rdb_admin = new JRadioButton(Localization.getResourceBundle().getString("ChangePasswordForm.rdb_admin"));
    private JRadioButton rdb_manager = new JRadioButton(Localization.getResourceBundle().getString("ChangePasswordForm.rdb_manager"));
    private JRadioButton rdb_cashier = new JRadioButton(Localization.getResourceBundle().getString("ChangePasswordForm.rdb_cashier"));
    private JRadioButton rdb_male = new JRadioButton(Localization.getResourceBundle().getString("ChangePasswordForm.rdb_male"));
    private JRadioButton rdb_female = new JRadioButton(Localization.getResourceBundle().getString("ChangePasswordForm.rdb_female"));
    private JPanel pnl_main = new JPanel(new SpringLayout());
    private JPanel pnl_account_type = new JPanel(new GridLayout(1, 3));
    private JPanel pnl_gender = new JPanel(new GridLayout(1, 2));
    private JOptionPane p;
    private JDialog d;
    private ImageIcon change_password_icon;

    /**
     *
     */
    public ChangePasswordForm() {
        this.setPreferredSize(new Dimension(450, 650));
        this.setMaximumSize(new Dimension(450, 650));
        this.setMinimumSize(new Dimension(450, 650));

        this.setBackground(Color.WHITE);
        pnl_main.setBackground(Color.WHITE);
        rdb_admin.setBackground(Color.WHITE);
        rdb_manager.setBackground(Color.WHITE);
        rdb_cashier.setBackground(Color.WHITE);
        rdb_male.setBackground(Color.WHITE);
        rdb_female.setBackground(Color.WHITE);

        pnl_main.setPreferredSize(new Dimension(350, 600));
        pnl_main.setMaximumSize(new Dimension(350, 600));
        pnl_main.setMinimumSize(new Dimension(350, 600));

        txt_username.setPreferredSize(new Dimension(180, 25));
        txt_username.setMaximumSize(new Dimension(180, 25));
        txt_username.setMinimumSize(new Dimension(180, 25));

        pwf_old_password.setPreferredSize(new Dimension(180, 25));
        pwf_old_password.setMaximumSize(new Dimension(180, 25));
        pwf_old_password.setMinimumSize(new Dimension(180, 25));

        pwf_new_password.setPreferredSize(new Dimension(180, 25));
        pwf_new_password.setMaximumSize(new Dimension(180, 25));
        pwf_new_password.setMinimumSize(new Dimension(180, 25));

        pwf_confirm_password.setPreferredSize(new Dimension(180, 25));
        pwf_confirm_password.setMaximumSize(new Dimension(180, 25));
        pwf_confirm_password.setMinimumSize(new Dimension(180, 25));

        pnl_account_type.setPreferredSize(new Dimension(250, 25));
        pnl_account_type.setMaximumSize(new Dimension(250, 25));
        pnl_account_type.setMinimumSize(new Dimension(250, 25));

        txt_full_name.setPreferredSize(new Dimension(180, 25));
        txt_full_name.setMaximumSize(new Dimension(180, 25));
        txt_full_name.setMinimumSize(new Dimension(180, 25));

        pnl_gender.setPreferredSize(new Dimension(180, 25));
        pnl_gender.setMaximumSize(new Dimension(180, 25));
        pnl_gender.setMinimumSize(new Dimension(180, 25));

        txt_ssn.setPreferredSize(new Dimension(180, 25));
        txt_ssn.setMaximumSize(new Dimension(180, 25));
        txt_ssn.setMinimumSize(new Dimension(180, 25));

        txt_phone.setPreferredSize(new Dimension(180, 25));
        txt_phone.setMaximumSize(new Dimension(180, 25));
        txt_phone.setMinimumSize(new Dimension(180, 25));

        txt_email.setPreferredSize(new Dimension(180, 25));
        txt_email.setMaximumSize(new Dimension(180, 25));
        txt_email.setMinimumSize(new Dimension(180, 25));

        txt_address.setBorder(BorderFactory.createLineBorder(Color.black));
        txt_address.setPreferredSize(new Dimension(180, 75));
        txt_address.setMaximumSize(new Dimension(180, 75));
        txt_address.setMinimumSize(new Dimension(180, 75));

        pnl_main.add(lbl_username);
        pnl_main.add(txt_username);

        pnl_main.add(lbl_old_password);
        pnl_main.add(pwf_old_password);
        pnl_main.add(lbl_old_password_error);

        pnl_main.add(lbl_new_password);
        pnl_main.add(pwf_new_password);
        pnl_main.add(lbl_new_password_error);

        pnl_main.add(lbl_confirm_password);
        pnl_main.add(pwf_confirm_password);
        pnl_main.add(lbl_confirm_password_error);

        pnl_main.add(lbl_account_type);
        pnl_main.add(pnl_account_type);

        pnl_account_type.add(rdb_admin);
        pnl_account_type.add(rdb_manager);
        pnl_account_type.add(rdb_cashier);
        group_account_type.add(rdb_admin);
        group_account_type.add(rdb_manager);
        group_account_type.add(rdb_cashier);

        pnl_main.add(lbl_full_name);
        pnl_main.add(txt_full_name);

        pnl_main.add(lbl_gender);
        pnl_main.add(pnl_gender);

        pnl_gender.add(rdb_male);
        pnl_gender.add(rdb_female);
        group_gender.add(rdb_male);
        group_gender.add(rdb_female);

        pnl_main.add(lbl_ssn);
        pnl_main.add(txt_ssn);

        pnl_main.add(lbl_dob);
        pnl_main.add(lbl_chosen_dob);

        pnl_main.add(lbl_phone);
        pnl_main.add(txt_phone);

        pnl_main.add(lbl_email);
        pnl_main.add(txt_email);

        pnl_main.add(lbl_address);
        pnl_main.add(txt_address);

        SpringUtilities.makeCompactGrid(pnl_main, 27, 1, 20, 10, 130, 3);

        pnl_main.setOpaque(true);

        txt_username.setEditable(false);
        txt_username.setFocusable(false);
        rdb_admin.setEnabled(false);
        rdb_admin.setFocusable(false);
        rdb_manager.setEnabled(false);
        rdb_manager.setFocusable(false);
        rdb_cashier.setEnabled(false);
        rdb_cashier.setFocusable(false);
        rdb_male.setEnabled(false);
        rdb_male.setFocusable(false);
        rdb_female.setEnabled(false);
        rdb_female.setFocusable(false);
        txt_full_name.setEditable(false);
        txt_full_name.setFocusable(false);
        txt_ssn.setEditable(false);
        txt_ssn.setFocusable(false);
        txt_phone.setEditable(false);
        txt_phone.setFocusable(false);
        txt_email.setEditable(false);
        txt_email.setFocusable(false);
        txt_address.setEnabled(false);
        txt_address.setFocusable(false);

        this.add(pnl_main);
    }

    /**
     *
     * @returna
     */
    public JTextField getTxt_username() {
        return txt_username;
    }

    /**
     *
     * @param username
     */
    public void setTxt_username(String username) {
        txt_username.setText(username);
    }

    /**
     *
     * @return
     */
    public JPasswordField getPwf_old_password() {
        return pwf_old_password;
    }

    /**
     *
     * @return
     */
    public JPasswordField getPwf_new_password() {
        return pwf_new_password;
    }

    /**
     *
     * @return
     */
    public JPasswordField getPwf_confirm_password() {
        return pwf_confirm_password;
    }

    /**
     *
     * @return
     */
    public JLabel getLbl_old_password_error() {
        return lbl_old_password_error;
    }

    /**
     *
     * @return
     */
    public JLabel getLbl_new_password_error() {
        return lbl_new_password_error;
    }

    /**
     *
     * @return
     */
    public JLabel getLbl_confirm_password_error() {
        return lbl_confirm_password_error;
    }

    /**
     *
     * @return
     */
    public JTextField getTxt_full_name() {
        return txt_full_name;
    }

    /**
     *
     * @return
     */
    public JTextField getTxt_ssn() {
        return txt_ssn;
    }

    /**
     *
     * @return
     */
    public JTextField getTxt_phone() {
        return txt_phone;
    }

    /**
     *
     * @return
     */
    public JTextField getTxt_email() {
        return txt_email;
    }

    /**
     *
     * @return
     */
    public JTextArea getTxt_address() {
        return txt_address;
    }

    /**
     *
     * @return
     */
    public JRadioButton getRdb_admin() {
        return rdb_admin;
    }

    /**
     *
     * @return
     */
    public JRadioButton getRdb_manager() {
        return rdb_manager;
    }

    /**
     *
     * @return
     */
    public JRadioButton getRdb_cashier() {
        return rdb_cashier;
    }

    /**
     *
     * @return
     */
    public JRadioButton getRdb_male() {
        return rdb_male;
    }

    /**
     *
     * @return
     */
    public JRadioButton getRdb_female() {
        return rdb_female;
    }

    /**
     *
     * @return
     */
    public JLabel getLbl_chosen_dob() {
        return lbl_chosen_dob;
    }

    public JDialog getD() {
        return d;
    }

    public JOptionPane getP() {
        return p;
    }

    public void createDialog(ImageIcon change_password_icon) {
        String[] options = {Localization.getResourceBundle().getString("ChangePasswordForm.options.Confirm"), Localization.getResourceBundle().getString("ChangePasswordForm..options.Cancel")};
        p = new JOptionPane(this, JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION, change_password_icon, options, null);
        d = p.createDialog(Localization.getResourceBundle().getString("ChangePasswordForm.changePassword"));
        this.getPwf_old_password().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getPwf_old_password().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getPwf_new_password().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getPwf_new_password().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getPwf_confirm_password().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getPwf_confirm_password().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getPwf_old_password().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getPwf_old_password().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getPwf_new_password().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getPwf_new_password().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getPwf_confirm_password().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getPwf_confirm_password().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
    }

}
