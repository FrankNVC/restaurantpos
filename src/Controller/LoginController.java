package Controller;

import Main.Application;
import Model.Account.AccountType;
import Model.Restaurant;
import Utilities.Localization;
import View.TheMenuBar;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Nguyen Minh Sang
 */
public class LoginController extends AbstractAction implements ActionListener {

    private Restaurant r;
    private JFrame mainFrame;

    /**
     *
     * @param r
     * @param mainFrame
     */
    public LoginController(Restaurant r, JFrame mainFrame) {
        this.r = r;
        this.mainFrame = mainFrame;
    }

    /**
     * defines events when login button is clicked
     *
     * @param mainFrame
     */
    public void updateFrame(JFrame mainFrame) {
        this.mainFrame = mainFrame;
    }

    /**
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String username = Application.lv.getTxtUsername().getText().trim();
        String password = new String(Application.lv.getPwfPassword().getPassword());
        String message = r.logIn(username, password);
        if (message.toLowerCase().contains("username") || message.toLowerCase().contains("tài khoản")) {
            Application.lv.getLbl_error_username().setText(message);
            Application.lv.getLbl_error_username().setForeground(Color.red);
            Application.lv.getLbl_error_password().setText("");
        } else if (message.toLowerCase().contains("password") || message.toLowerCase().contains("mật khẩu")) {
            Application.lv.getLbl_error_password().setText(message);
            Application.lv.getLbl_error_password().setForeground(Color.red);
            Application.lv.getLbl_error_username().setText("");
        } else {
            logIn();
            Application.lv.getLbl_error_password().setText("");
            Application.lv.getLbl_error_username().setText("");
            Application.lv.getTxtUsername().setText("");
            Application.lv.getPwfPassword().setText("");
        }
        Application.lv.getPwfPassword().setText("");
    }

    /**
     * defines events when username and password are matched
     */
    public void logIn() {

        mainFrame.dispose();
        mainFrame = new JFrame();
        TheMenuBar menubar = new TheMenuBar(r, mainFrame);
        mainFrame.setJMenuBar(menubar);
        if (Application.currentUser.getType() == AccountType.ADMINISTRATOR) {
            Application.av.reload();
            Application.currentView = Application.av;
            mainFrame.add(Application.av, BorderLayout.CENTER);
            Application.av_cusp.reload(mainFrame);
        } else {
            Application.cmv.reload();
            Application.currentView = Application.cmv;
            mainFrame.add(Application.cmv, BorderLayout.CENTER);
            Application.cmv_cusp.reload(mainFrame);
        }
        mainFrame.setResizable(false);
        mainFrame.pack();
        mainFrame.setTitle(Localization.getResourceBundle().getString("LoginController.frame.title"));
        mainFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                String options[] = {Localization.getResourceBundle().getString("LoginController.options.Confirm"), Localization.getResourceBundle().getString("LoginController.options.Cancel")};
                int result = JOptionPane.showOptionDialog(null, Localization.getResourceBundle().getString("LoginController.showConfirmDialog.message"), Localization.getResourceBundle().getString("LoginController.showConfirmDialog.title"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, null);
                System.out.println(result);
                if (result == JOptionPane.YES_OPTION) {
                    r.saveData();
                    mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    super.windowClosing(e);
                }
            }
        });
        mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);
    }
}