package Model;

import java.io.Serializable;

/**
 *
 * @author Nguyen Minh Sang
 */
public class Table implements Serializable, Comparable {

    /**
     * enumeration for table status
     */
    public enum TableStatus {

        RESERVED,
        VACANT,
        OCCUPIED
    }

    /**
     * enumeration for table shape
     */
    public enum TableShape {

        RECTANGLE,
        OVAL
    }

    private String customerName;
    private int tableNumber;
    private int cell_root_id;
    private int table_group_id;
    private int table_group_root_id;
    private TableStatus tableStatus;
    private TableShape tableShape;
    private Order o;

    /**
     * constructor for Table auto increment TABLE_NUMBER after creating new
     * Table
     *
     * @param m
     * @param tableShape
     */
    public Table(int table_number, TableShape tableShape) {
        this.tableNumber = table_number;
        this.tableStatus = TableStatus.VACANT;
        this.tableShape = tableShape;
        this.o = new Order();

        this.table_group_id = -1;
    }

    public void setTable_group_root_id(int table_group_root_id) {
        this.table_group_root_id = table_group_root_id;
    }

    public int getTable_group_root_id() {
        return table_group_root_id;
    }

    public void setTableShape(TableShape tableShape) {
        this.tableShape = tableShape;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(int tableNumber) {
        this.tableNumber = tableNumber;
    }

    public int getTableGroupId() {
        return table_group_id;
    }

    public void setTable_group_id(int table_group_id) {
        this.table_group_id = table_group_id;
    }

    public TableStatus getTableStatus() {
        return tableStatus;
    }

    public void setTableStatus(TableStatus tableStatus) {
        this.tableStatus = tableStatus;
    }

    public TableShape getTableShape() {
        return tableShape;
    }

    public Order getO() {
        return o;
    }

    public void setO(Order o) {
        this.o = o;
    }

    public int getCell_root_id() {
        return cell_root_id;
    }

    public void setCell_root_id(int cell_root_id) {
        this.cell_root_id = cell_root_id;
    }

    /**
     * compares 2 Table objects using tableNumber
     *
     * @param o
     * @return -1 when smaller
     * @return 0 when equal
     * @return 1 when greater
     */
    @Override
    public int compareTo(Object o) {
        if (tableNumber < ((Table) o).getTableNumber()) {
            return -1;
        } else if (tableNumber > ((Table) o).getTableNumber()) {
            return 1;
        } else {
            return 0;
        }
    }
}
