package Controller;

import Main.Application;
import Model.Restaurant;
import Model.Table;
import Utilities.Localization;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;

/**
 *
 * @author Nguyen Minh Sang
 */
public class DeleteOrderItemController extends KeyAdapter implements ActionListener {

    private Restaurant r;
    
    /**
     *
     * @param r
     */
    public DeleteOrderItemController(Restaurant r) {
        this.r = r;
    }

    /**
     * defines event when an order item is deleted
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        int result;
        if (Application.cmv.getCurrent_tables().size() == 1) {
            Table table = Application.cmv.getCurrent_tables().get(0);
            if (!table.getO().getOrderItems().isEmpty()) {
                if (Application.cmv.getTable_order_detail().getSelectedRows().length > 1) {
                    result = r.showConfirmDialog(Localization.getResourceBundle().getString("DeleteOrderItemController.orderItems.message"));
                } else {
                    result = r.showConfirmDialog(Localization.getResourceBundle().getString("DeleteOrderItemController.orderItem.message"));
                }
                if (result == JOptionPane.YES_OPTION) {
                    System.out.println(Application.cmv.getTable_order_detail().getSelectedRows().length);
                    for (int i = 0; i < Application.cmv.getTable_order_detail().getSelectedRows().length; i++) {
                        int orderItemID = Integer.parseInt((String) Application.cmv.getTable_order_detail().getValueAt(Application.cmv.getTable_order_detail().getSelectedRows()[i], 0));
                        r.deleteOrderItem(Application.cmv.getCurrent_tables(), orderItemID);
                        r.update();
                    }
                    Application.cmv.updateOrderItemList();
                    Application.cmv.updateTable(table);
                }
            } else {
                JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("DeleteOrderItemController.chooseOrderItem.message"), Localization.getResourceBundle().getString("DeleteOrderItemController.chooseOrderItem.title"), JOptionPane.INFORMATION_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("DeleteOrderItemController.chooseTable.message"), Localization.getResourceBundle().getString("DeleteOrderItemController.chooseTable.title"), JOptionPane.INFORMATION_MESSAGE);
        }
    }

    /**
     *
     * @param e
     */
    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_DELETE) {
            if (Application.cmv.getTable_order_detail().getSelectedRow() > -1) {
                Application.cmv.getBtn_delete_order().doClick();
            }
        }        
    }
}