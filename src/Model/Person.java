/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Mr Giang
 */
public abstract class Person implements Serializable {

    /**
     * Enumeration for Gender
     */
    public enum Gender {

        MALE,
        FEMALE
    }

    protected String fullName = "";
    protected Gender g;
    protected String ssn;
    protected Date dob = null;
    protected String address = "";
    protected String phoneNumber = "";
    protected String email = "";

    public String getFullName() {
        return fullName;
    }

    /**
     *
     * @param fullName
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Gender getG() {
        return g;
    }

    public void setG(Gender g) {
        this.g = g;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    /**
     *
     * @return String value of Gender
     */
    public String printGender() {
        if (g == Gender.FEMALE) {
            return "Female";
        } else {
            return "Male";
        }
    }

    public int getAge() {
        if (dob != null) {
            Calendar dobCalendar = Calendar.getInstance();
            dobCalendar.setTime(dob);
            Calendar today = Calendar.getInstance();
            int age = today.get(Calendar.YEAR) - dobCalendar.get(Calendar.YEAR);
            if (today.get(Calendar.MONTH) < dobCalendar.get(Calendar.MONTH)) {
                age--;
            } else if (today.get(Calendar.MONTH) == dobCalendar.get(Calendar.MONTH) && today.get(Calendar.DAY_OF_MONTH) < dobCalendar.get(Calendar.DAY_OF_MONTH)) {
                age--;
            }
            return age;
        } else {
            return 0;
        }
    }
}
