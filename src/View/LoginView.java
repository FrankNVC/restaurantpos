package View;

import Controller.LoginController;
import Model.Restaurant;
import Utilities.Localization;
import Utilities.REPOSButton;
import Utilities.SpringUtilities;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.Observable;
import java.util.Observer;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SpringLayout;

/**
 * this is the login view of the program
 * @author Pham Ba Phuoc
 */
public class LoginView extends JPanel implements Observer {

    private JTextField txt_username = new JTextField();
    private JPasswordField pwf_password = new JPasswordField();
    private ImageIcon icon_logo = new ImageIcon(this.getClass().getResource("/Images/logo.png"));
    private JLabel lbl_logo = new JLabel(icon_logo);
    private JLabel lbl_username = new JLabel(Localization.getResourceBundle().getString("lbl_username"), JLabel.LEFT);
    private JLabel lbl_password = new JLabel(Localization.getResourceBundle().getString("lbl_password"), JLabel.LEFT);
    private JLabel lbl_contact = new JLabel(Localization.getResourceBundle().getString("lbl_contact"));
    private JLabel lbl_username_error = new JLabel();
    private JLabel lbl_password_error = new JLabel();
    private JLabel lbl_error1 = new JLabel();
    private JLabel lbl_error2 = new JLabel();
    private ImageIcon icon_login = new ImageIcon(this.getClass().getResource(Localization.getResourceBundle().getString("icon_login")));
    private ImageIcon icon_login_clicked = new ImageIcon(this.getClass().getResource(Localization.getResourceBundle().getString("icon_login_clicked")));    
    private String tooltip = Localization.getResourceBundle().getString("btn_login.tooltip");
    private REPOSButton btn_login = new REPOSButton(icon_login, icon_login_clicked, tooltip);
    //status: logo image
    private JPanel pnl_main = new JPanel(new SpringLayout());
    private JPanel pnl_logo = new JPanel();
    //south: contact information
    private JPanel pnl_contact = new JPanel();
    private LoginController lc;

    public LoginView(Restaurant r, JFrame mainFrame) {        

        lc = new LoginController(r, mainFrame);

        this.setPreferredSize(new Dimension(1100, 690));

        this.setLayout(new BorderLayout());

        pnl_logo.add(lbl_logo);

//        username and password panel
        txt_username.setPreferredSize(new Dimension(200, 25));
        txt_username.setMaximumSize(new Dimension(200, 25));
        txt_username.setMinimumSize(new Dimension(200, 25));

        pwf_password.setPreferredSize(new Dimension(200, 25));
        pwf_password.setMaximumSize(new Dimension(200, 25));
        pwf_password.setMinimumSize(new Dimension(200, 25));

        pnl_main.add(lbl_username);
        pnl_main.add(txt_username);
        pnl_main.add(lbl_username_error);

        pnl_main.add(lbl_password);
        pnl_main.add(pwf_password);
        pnl_main.add(lbl_password_error);

        pnl_main.add(lbl_error1);
        pnl_main.add(btn_login);
        pnl_main.add(lbl_error2);

        btn_login.setPreferredSize(new Dimension(100, 65));
        btn_login.setMaximumSize(new Dimension(100, 65));

//        contact information panel
        pnl_contact.add(lbl_contact);

        pnl_main.setBackground(Color.WHITE);
        pnl_contact.setBackground(Color.WHITE);

        pnl_logo.setPreferredSize(new Dimension(this.getWidth(), 250));
        pnl_logo.setMaximumSize(new Dimension(this.getWidth(), 250));
        pnl_logo.setMinimumSize(new Dimension(this.getWidth(), 250));

        pnl_main.setPreferredSize(new Dimension(this.getWidth(), 350));
        pnl_main.setMaximumSize(new Dimension(this.getWidth(), 350));
        pnl_main.setMinimumSize(new Dimension(this.getWidth(), 350));

        pnl_contact.setPreferredSize(new Dimension(this.getWidth(), 90));
        pnl_contact.setMaximumSize(new Dimension(this.getWidth(), 90));
        pnl_contact.setMinimumSize(new Dimension(this.getWidth(), 90));

        SpringUtilities.makeCompactGrid(pnl_main, 3, 3, 390, 80, 7, 7);

        pnl_main.setOpaque(true);

        this.add(pnl_logo, BorderLayout.NORTH);
        this.add(pnl_main, BorderLayout.CENTER);
        this.add(pnl_contact, BorderLayout.SOUTH);

        this.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "login");
        this.getActionMap().put("login", lc);
        txt_username.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "login");
        txt_username.getActionMap().put("login", lc);
        pwf_password.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "login");
        pwf_password.getActionMap().put("login", lc);

        btn_login.addActionListener(lc);
        
        txt_username.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                txt_username.setSelectionStart(0);
                txt_username.setSelectionEnd(txt_username.getText().length());
            }

            @Override
            public void focusLost(FocusEvent e) {
            }
        });        
        
        pwf_password.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                pwf_password.setSelectionStart(0);
                pwf_password.setSelectionEnd(pwf_password.getPassword().length);
            }

            @Override
            public void focusLost(FocusEvent e) {
                txt_username.requestFocus();
            }
        });
        
    }
    
    public JLabel getLbl_username() {
        return lbl_username;
    }
    
    public JLabel getLbl_password() {
        return lbl_password;
    }

    public JTextField getTxtUsername() {
        return txt_username;
    }

    public JPasswordField getPwfPassword() {
        return pwf_password;
    }

    public JLabel getLbl_error_username() {
        return lbl_username_error;
    }

    public JLabel getLbl_error_password() {
        return lbl_password_error;
    }

    public REPOSButton getBtn_login() {
        return btn_login;
    }

    public LoginController getLc() {
        return lc;
    }
    
    public void setIcon_login() {
        ImageIcon image_icon = new ImageIcon(this.getClass().getResource(Localization.getResourceBundle().getString("icon_login")));
        this.icon_login.setImage(image_icon.getImage());
    }

    public void setIcon_login_clicked() {
        ImageIcon image_icon = new ImageIcon(this.getClass().getResource(Localization.getResourceBundle().getString("icon_login_clicked")));
        this.icon_login_clicked.setImage(image_icon.getImage());
    }
    
    public void setLbl_username(String lbl_username) {
        this.lbl_username.setText(lbl_username);
    }

    public void setLbl_password(String lbl_password) {
        this.lbl_password.setText(lbl_password);
    }

    public void setLbl_contact(String lbl_contact) {
        this.lbl_contact.setText(lbl_contact);
    }

    public void setBtn_login_ToolTipText(String tooltip) {
        btn_login.setToolTipText(tooltip);
    }
    
    

    @Override
    public void update(Observable o, Object o1) {
        this.validate();
    }
    
    public void addObs(Restaurant r) {
        r.addObserver(this);
    }
}
