package Model;

/**
 *
 * @author Nguyen Minh Sang
 */
public class Cashier extends Account {

    /**
     * constructor for Cashier auto increment of CASHIER_ID after creating new
     * Cashier
     *
     * @param m
     * @param g
     * @param ssn
     * @param fullName
     * @param address
     */
    public Cashier(int ID, Gender g, String ssn, String fullName, String address) {
        this.username = "c";
        this.username += ID;
        this.type = AccountType.CASHIER;
        this.g = g;
        this.ssn = ssn;
        this.fullName = fullName;
        this.address = address;
    }
}
