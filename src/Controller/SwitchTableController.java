/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Main.Application;
import Model.Restaurant;
import Model.Table;
import Model.Table.TableStatus;
import Utilities.Localization;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Frank NgVietCuong
 */
public class SwitchTableController implements ActionListener {

    private Restaurant r;

    /**
     *
     * @param r
     */
    public SwitchTableController(Restaurant r) {
        this.r = r;
    }

    /**
     * defines events when switch table button is clicked
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        ArrayList<Table> table = Application.cmv.getCurrent_tables();
        if (table.size() == 2) {
            if ((table.get(0).getTableStatus() == TableStatus.VACANT && table.get(1).getTableStatus() == TableStatus.OCCUPIED)
                    || (table.get(0).getTableStatus() == TableStatus.OCCUPIED && table.get(1).getTableStatus() == TableStatus.VACANT)) {
                r.switchTable(Application.cmv.getCurrent_tables());
                r.update();
                Application.cmv.deselectAllTable();
                Application.cmv.updateOrderItemList();
                Application.cmv.validate();

            } else {
                JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("SwitchTableController.invalidSelectedTables.message"), Localization.getResourceBundle().getString("SwitchTableController.invalidSelectedTables.title"), JOptionPane.INFORMATION_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("SwitchTableController.chooseTable.message"), Localization.getResourceBundle().getString("SwitchTableController.chooseTable.title"), JOptionPane.INFORMATION_MESSAGE);
        }

    }
}
