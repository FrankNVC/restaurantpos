package Model;

import java.awt.print.PrinterJob;
import java.io.*;
import java.util.*;
import javax.print.*;
import javax.print.attribute.*;
import javax.print.attribute.standard.Destination;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Nguyen Minh Sang
 */
public class Order implements Serializable {

    private Date orderDate;
    private int orderItemID = 1;
    private ArrayList<OrderItem> orderItems;
    private String discount;

    /**
     * constructor for Order initialize the Order Item array list
     */
    public Order() {
        this.orderDate = new Date();
        orderItems = new ArrayList<>();
    }

   public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public int getOrderItemID() {
        return orderItemID;
    }

    /**
     * mutator for orderItemID make sure that no ID is unused
     */
    public void setOrderItemID() {
        // initialize orderItemID begins at 1
        if (orderItems.isEmpty()) {
            orderItemID = 1;
        } else {
            for (int i = 1; i <= orderItems.size(); i++) {
                boolean missing = true;
                for (int j = 0; j < orderItems.size(); j++) {
                    if (orderItems.get(j).getOrderItemId() == i) {
                        missing = false;
                        break;
                    }
                }
                if (missing) {
                    orderItemID = i;
                    break;
                } else {
                    if (i == orderItems.size()) {
                        orderItemID = i + 1;
                    }
                }
            }
        }
    }
    
    public ArrayList<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(ArrayList<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String newDiscount) {
        this.discount = newDiscount;
    }

    /**
     * updates the order item in the orderItems array list
     * @param id
     * @param quantity
     * @param note
     * @param isDone
     */
    public void editOrderItem(int id, int quantity, String note, boolean isDone) {
        OrderItem oi = orderItems.get(id-1);
        oi.setQuantity(quantity);
        oi.setNote(note);
        oi.setIsDone(isDone);
    }

    /**
     * prints the order for the kitchen
     */
    public void printOrder() {
        File f = new File("Order.txt");
        f.setWritable(true);
        String document = "";
        for (int i = 0; i < orderItems.size(); i++) {
            document += "\n[ " + orderItems.get(i).getQuantity() + " ]\t[ " + orderItems.get(i).getItem().getItemName() + " ]\t[ " + orderItems.get(i).getNote() + " ]\t[ ";
            if (orderItems.get(i).getIsDone()) {
                document += "Done";
            } else {
                document += "Waiting";
            }
            document += " ]";
        }
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(f));
            bw.write(document);
            bw.close();
        } catch (IOException ex) {
            System.out.println(ex);
        }
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("Order.txt");
        } catch (FileNotFoundException ex) {
            System.out.println(ex);
        }
        DocFlavor df = DocFlavor.INPUT_STREAM.AUTOSENSE;
        Doc d = new SimpleDoc(fis, df, null);
        PrinterJob pj = PrinterJob.getPrinterJob();
        if (pj.printDialog()) {
            PrintService ps = pj.getPrintService();
            DocPrintJob dpj = ps.createPrintJob();
            if (pj.getPrintService().toString().equalsIgnoreCase("Win32 Printer : Microsoft XPS Document Writer")) {
                JFileChooser fc = new JFileChooser();
                fc.setAcceptAllFileFilterUsed(false);
                fc.setFileFilter(new FileNameExtensionFilter("Text (*.txt)", "txt"));
                fc.showSaveDialog(null);
                fc.setSelectedFile(new File(fc.getSelectedFile().getPath() + ".txt"));
                PrintRequestAttributeSet a = new HashPrintRequestAttributeSet();
                a.add(new Destination(fc.getSelectedFile().toURI()));
                try {
                    dpj.print(d, a);
                } catch (PrintException ex) {
                    System.out.println(ex);
                }
            } else {
                try {
                    dpj.print(d, null);
                } catch (PrintException ex) {
                    System.out.println(ex);
                }
            }
        }
        f.delete();
    }
}
