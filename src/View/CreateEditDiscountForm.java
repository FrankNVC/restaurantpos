/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.JDialogKeyListener;
import Main.Application;
import Utilities.JTextFieldLimit;
import Utilities.Localization;
import Utilities.SpringUtilities;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SpringLayout;

/**
 * this is the form for creating and editing discount
 *
 * @author Pham
 */
public class CreateEditDiscountForm extends JPanel {

    private JLabel lbl_old_discount = new JLabel(Localization.getResourceBundle().getString("CreateEditDiscountForm.lbl_old_discount"));
    private JLabel lbl_discount = new JLabel(Localization.getResourceBundle().getString("CreateEditDiscountForm.lbl_discount"));
    private JLabel lbl_discount_value = new JLabel(Localization.getResourceBundle().getString("CreateEditDiscountForm.lbl_discount_value"));
    private JLabel lbl_discount_error = new JLabel();
    private JLabel lbl_discount_value_error = new JLabel();
    private JLabel lbl_discount_percent = new JLabel("%");
    private JTextField txt_discount = new JTextField(15);
    private JTextField txt_discount_value = new JTextField(15);
    private JCheckBox chk_delete_discount = new JCheckBox(Localization.getResourceBundle().getString("CreateEditDiscountForm.chk_delete_discount"));

    private Vector vct_discount = new Vector();
    private DefaultComboBoxModel model = new DefaultComboBoxModel(vct_discount);
    private JComboBox cbx_discount = new JComboBox(model);

    private JPanel pnl_main = new JPanel(new SpringLayout());
    private JPanel pnl_dist = new JPanel(new BorderLayout());

    private JOptionPane p;
    private JDialog d;

    public CreateEditDiscountForm(boolean edit) {
        this.setPreferredSize(new Dimension(200, 200));
        this.setMaximumSize(new Dimension(200, 200));
        this.setMinimumSize(new Dimension(200, 200));

        pnl_main.setPreferredSize(new Dimension(150, 200));
        pnl_main.setMaximumSize(new Dimension(150, 200));
        pnl_main.setMinimumSize(new Dimension(150, 200));

        cbx_discount.setPreferredSize(new Dimension(150, 25));
        cbx_discount.setMaximumSize(new Dimension(150, 25));
        cbx_discount.setMinimumSize(new Dimension(150, 25));

        txt_discount.setPreferredSize(new Dimension(150, 25));
        txt_discount.setMaximumSize(new Dimension(150, 25));
        txt_discount.setMinimumSize(new Dimension(150, 25));

        pnl_dist.setPreferredSize(new Dimension(150, 25));
        pnl_dist.setMaximumSize(new Dimension(150, 25));
        pnl_dist.setMinimumSize(new Dimension(150, 25));

        pnl_dist.add(txt_discount_value, BorderLayout.CENTER);
        pnl_dist.add(lbl_discount_percent, BorderLayout.EAST);

        if (edit) {
            lbl_discount = new JLabel(Localization.getResourceBundle().getString("CreateEditDiscountForm.lbl_discount"));
            pnl_main.add(lbl_old_discount);
            pnl_main.add(cbx_discount);
        }

        pnl_main.add(lbl_discount);
        pnl_main.add(txt_discount);
        pnl_main.add(lbl_discount_error);

        if (edit) {
            pnl_main.add(chk_delete_discount);
        }
        pnl_main.add(lbl_discount_value);
        pnl_main.add(pnl_dist);
        pnl_main.add(lbl_discount_value_error);

        if (edit) {
            SpringUtilities.makeCompactGrid(pnl_main, 9, 1, 0, 0, 0, 3);
        } else {
            SpringUtilities.makeCompactGrid(pnl_main, 6, 1, 0, 20, 0, 5);
        }

        pnl_main.setOpaque(true);

        txt_discount_value.setDocument(new JTextFieldLimit(2, "^[\\d]+$", false));

        this.add(pnl_main);

        String options[] = {Localization.getResourceBundle().getString("CreateEditDiscountForm.options.Confirm"), Localization.getResourceBundle().getString("CreateEditDiscountForm.options.Cancel")};
        p = new JOptionPane(this, JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION, null, options, null);
        if (!edit) {
            d = p.createDialog(Localization.getResourceBundle().getString("CreateEditDiscountForm.createDiscount"));
        } else {
            d = p.createDialog(Localization.getResourceBundle().getString("CreateEditDiscountForm.editDiscount"));
        }
        this.getTxt_discount().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getTxt_discount().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getTxt_discount().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getTxt_discount().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getTxt_discount_value().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getTxt_discount_value().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getTxt_discount_value().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getTxt_discount_value().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));

        cbx_discount.addItemListener(new ItemListener(){

            @Override
            public void itemStateChanged(ItemEvent e) {
                txt_discount_value.setText(Integer.toString(Application.r.getDiscount().get(cbx_discount.getSelectedItem().toString())));
                txt_discount.setText(cbx_discount.getSelectedItem().toString());
            }
        });
    }

    public JTextField getTxt_discount() {
        return txt_discount;
    }

    public JTextField getTxt_discount_value() {
        return txt_discount_value;
    }

    public JComboBox getCbx_discount() {
        return cbx_discount;
    }

    public JCheckBox getChk_delete_discount() {
        return chk_delete_discount;
    }

    public JLabel getLbl_discount_error() {
        return lbl_discount_error;
    }

    public JLabel getLbl_discount_value_error() {
        return lbl_discount_value_error;
    }

    public Vector getVct_discount() {
        return vct_discount;
    }

    public JOptionPane getP() {
        return p;
    }

    public JDialog getD() {
        return d;
    }

}
