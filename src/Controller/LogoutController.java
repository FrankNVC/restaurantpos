/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Main.Application;
import Model.Restaurant;
import Utilities.Localization;
import View.TheMenuBar;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Nguyen Minh Sang
 */
public class LogoutController implements ActionListener {

    private Restaurant r;
    private JFrame mainFrame;

    /**
     *
     * @param r
     * @param mainFrame
     */
    public LogoutController(Restaurant r, JFrame mainFrame) {
        this.r = r;
        this.mainFrame = mainFrame;
    }

    /**
     * update the mainframe of the controller
     *
     * @param mainFrame is the frame using the controller
     */
    public void updateFrame(JFrame mainFrame) {
        this.mainFrame = mainFrame;
    }

    /**
     * defines events when logout button is clicked
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (r.logOut()) {
            mainFrame.dispose();
            mainFrame = new JFrame();
            Application.currentView = Application.lv;
            Application.lv.getLc().updateFrame(mainFrame);
            TheMenuBar menubar = new TheMenuBar(r, mainFrame);
            mainFrame.setJMenuBar(menubar);
            mainFrame.add(Application.lv, BorderLayout.CENTER);
            mainFrame.setResizable(false);
            mainFrame.pack();
            mainFrame.setTitle(Localization.getResourceBundle().getString("LogoutController.welcomeToRepos"));
            mainFrame.addWindowListener(new WindowAdapter() {
                @Override
                public void windowOpened(WindowEvent e) {
                    super.windowOpened(e);
                    Application.lv.getTxtUsername().requestFocus();
                }

                @Override
                public void windowClosing(WindowEvent e) {
                    String options[] = {Localization.getResourceBundle().getString("LogoutController.options.Confirm"), Localization.getResourceBundle().getString("LogoutController.options.Cancel")};
                    int result = JOptionPane.showOptionDialog(null, Localization.getResourceBundle().getString("LogoutController.showConfirmDialog.message"), Localization.getResourceBundle().getString("LogoutController.showConfirmDialog.title"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, null);
                    System.out.println(result);
                    if (result == JOptionPane.YES_OPTION) {
                        r.saveData();
                        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                        super.windowClosing(e);
                    }
                }
            });
            mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            mainFrame.setLocationRelativeTo(null);
            mainFrame.setVisible(true);
        }
    }
}
