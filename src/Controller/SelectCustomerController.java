/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Customer;
import Model.Restaurant;
import Utilities.Localization;
import View.CustomerView;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import javax.swing.DefaultListModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Mr Giang
 */
public class SelectCustomerController implements ListSelectionListener {

    private Restaurant r;
    CustomerView cv;

    public SelectCustomerController(Restaurant r, CustomerView cv) {
        this.r = r;
        this.cv = cv;
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (cv.getPnl_list_customer().getTable().getSelectedRows().length < 1) {
            cv.getBtn_delete_customer().setEnabled(false);
            cv.getBtn_update_customer().setEnabled(false);
            ((DefaultListModel) cv.getList_customer_detail_right().getModel()).clear();
        } else {
            cv.getBtn_delete_customer().setEnabled(true);
            if (cv.getPnl_list_customer().getTable().getSelectedRows().length == 1) {
                cv.getBtn_update_customer().setEnabled(true);
                Customer a = r.findCustomer((String) cv.getPnl_list_customer().getTable().getValueAt(cv.getPnl_list_customer().getTable().getSelectedRow(), 4));
                DefaultListModel data = new DefaultListModel();
                data.addElement(a.printType());
                data.addElement(a.getFullName());
                data.addElement(a.printGender());
                data.addElement(a.getSsn() + "");
                if (a.getDob() == null) {
                    data.addElement("N/A");
                } else {
                    DateFormat dateformat = DateFormat.getDateInstance(DateFormat.MEDIUM, new Locale(Localization.getLANGUAGE(), Localization.getCOUNTRY()));
                    data.addElement(dateformat.format(a.getDob()));
                }
                data.addElement(a.getPhoneNumber() + "");
                if (a.getEmail().equalsIgnoreCase("")) {
                    data.addElement("N/A");
                } else {
                    data.addElement(a.getEmail());
                }
                if (a.getAddress().equalsIgnoreCase("")) {
                    data.addElement("N/A");
                } else {
                    data.addElement(a.getAddress());
                }
                data.addElement(a.getLoyalty_points() + "");
                cv.getList_customer_detail_right().setModel(data);
            } else {
                cv.getBtn_update_customer().setEnabled(false);
                ((DefaultListModel) cv.getList_customer_detail_right().getModel()).clear();
            }
        }
        cv.revalidate();
    }
}