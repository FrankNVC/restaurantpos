package View;

import Controller.JDialogKeyListener;
import Controller.SelectPhoneNetworkController;
import Utilities.JTextFieldLimit;
import Utilities.Localization;
import Utilities.REPOSButton;
import Utilities.SpringUtilities;
import com.toedter.calendar.JCalendar;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SpringLayout;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 * this is the form for creating and editing information of a user account
 *
 * @author Pham
 */
public class CreateEditAccountForm extends JPanel {

    private JLabel lbl_username = new JLabel(Localization.getResourceBundle().getString("CreateEditAccountForm.lbl_username"));
    private JLabel lbl_password = new JLabel(Localization.getResourceBundle().getString("CreateEditAccountForm.lbl_password"));
    private JLabel lbl_confirm_password = new JLabel(Localization.getResourceBundle().getString("CreateEditAccountForm.lbl_confirm_password"));
    private JLabel lbl_account_type = new JLabel(Localization.getResourceBundle().getString("CreateEditAccountForm.lbl_account_type"));
    private JLabel lbl_full_name = new JLabel(Localization.getResourceBundle().getString("CreateEditAccountForm.lbl_full_name"));
    private JLabel lbl_gender = new JLabel(Localization.getResourceBundle().getString("CreateEditAccountForm.lbl_gender"));
    private JLabel lbl_ssn = new JLabel(Localization.getResourceBundle().getString("CreateEditAccountForm.lbl_ssn"));
    private JLabel lbl_dob = new JLabel(Localization.getResourceBundle().getString("CreateEditAccountForm.lbl_dob"));
    private JLabel lbl_chosen_dob = new JLabel(Localization.getResourceBundle().getString("CreateEditAccountForm.lbl_chosen_dob"), JLabel.LEFT);
    private JLabel lbl_phone = new JLabel(Localization.getResourceBundle().getString("CreateEditAccountForm.lbl_phone"));
    private JLabel lbl_email = new JLabel(Localization.getResourceBundle().getString("CreateEditAccountForm.lbl_email"));
    private JLabel lbl_address = new JLabel(Localization.getResourceBundle().getString("CreateEditAccountForm.lbl_address"));
    private JLabel lbl_password_error = new JLabel();
    private JLabel lbl_confirm_password_error = new JLabel();
    private JLabel lbl_account_type_error = new JLabel();
    private JLabel lbl_full_name_error = new JLabel();
    private JLabel lbl_gender_error = new JLabel();
    private JLabel lbl_ssn_error = new JLabel();
    private JLabel lbl_dob_error = new JLabel();
    private JLabel lbl_phone_error = new JLabel();
    private JLabel lbl_email_error = new JLabel();
    private JLabel lbl_address_error = new JLabel();
    private ImageIcon icon_calendar = new ImageIcon(this.getClass().getResource("/Images/calendar.png"));
    private ImageIcon icon_calendar_clicked = new ImageIcon(this.getClass().getResource("/Images/calendar_clicked.png"));
    private REPOSButton btn_show_calendar = new REPOSButton(icon_calendar, icon_calendar_clicked, Localization.getResourceBundle().getString("CreateEditAccountForm.btn_show_calendar"));
    private JTextField txt_username = new JTextField(15);
    private JPasswordField pwf_password = new JPasswordField(15);
    private JPasswordField pwf_confirm_password = new JPasswordField(15);
    private JTextField txt_full_name = new JTextField(15);
    private JTextField txt_ssn = new JTextField(15);
    private JTextField txt_phone1 = new JTextField();
    private JTextField txt_phone2 = new JTextField();
    private JTextField txt_email = new JTextField(15);
    private JTextArea txt_address = new JTextArea();
    private ButtonGroup group_account_type = new ButtonGroup();
    private ButtonGroup group_gender = new ButtonGroup();
    private JCalendar cal_dob = new JCalendar();
    private JRadioButton rdb_admin = new JRadioButton(Localization.getResourceBundle().getString("CreateEditAccountForm.rdb_admin"));
    private JRadioButton rdb_manager = new JRadioButton(Localization.getResourceBundle().getString("CreateEditAccountForm.rdb_manager"));
    private JRadioButton rdb_cashier = new JRadioButton(Localization.getResourceBundle().getString("CreateEditAccountForm.rdb_cashier"));
    private JRadioButton rdb_male = new JRadioButton(Localization.getResourceBundle().getString("CreateEditAccountForm.rdb_male"));
    private JRadioButton rdb_female = new JRadioButton(Localization.getResourceBundle().getString("CreateEditAccountForm.rdb_female"));
    private JCheckBox chk_reset_password = new JCheckBox(Localization.getResourceBundle().getString("CreateEditAccountForm.chk_reset_password"));
    private JPanel pnl_main = new JPanel(new SpringLayout());
    private JPanel pnl_account_type = new JPanel(new GridLayout(1, 3));
    private JPanel pnl_gender = new JPanel(new GridLayout(1, 2));
    private JPanel pnl_dob = new JPanel(new FlowLayout(FlowLayout.LEFT));
    private JPanel pnl_phone = new JPanel(new GridLayout(1, 4));
    private JScrollPane spane_address = new JScrollPane(txt_address);
    private Vector cbx_phone_code = new Vector();
    private DefaultComboBoxModel model_code = new DefaultComboBoxModel(cbx_phone_code);
    private JComboBox cbx_phone = new JComboBox(model_code);
    private Vector cbx_phone_network = new Vector();
    private DefaultComboBoxModel model_network = new DefaultComboBoxModel(cbx_phone_network);
    private JComboBox cbx_network = new JComboBox(model_network);
    private JOptionPane p;
    private JDialog d;

    public CreateEditAccountForm(boolean edit) {
        this.setPreferredSize(new Dimension(450, 650));
        this.setMaximumSize(new Dimension(450, 650));
        this.setMinimumSize(new Dimension(450, 650));

        rdb_admin.setBackground(Color.WHITE);
        rdb_manager.setBackground(Color.WHITE);
        rdb_cashier.setBackground(Color.WHITE);
        rdb_male.setBackground(Color.WHITE);
        rdb_female.setBackground(Color.WHITE);
        chk_reset_password.setBackground(Color.WHITE);

        pnl_main.setPreferredSize(new Dimension(350, 650));
        pnl_main.setMaximumSize(new Dimension(350, 650));
        pnl_main.setMinimumSize(new Dimension(350, 650));

        txt_username.setPreferredSize(new Dimension(180, 20));
        txt_username.setMaximumSize(new Dimension(180, 20));
        txt_username.setMinimumSize(new Dimension(180, 20));

        pwf_password.setPreferredSize(new Dimension(180, 20));
        pwf_password.setMaximumSize(new Dimension(180, 20));
        pwf_password.setMinimumSize(new Dimension(180, 20));

        pwf_confirm_password.setPreferredSize(new Dimension(180, 20));
        pwf_confirm_password.setMaximumSize(new Dimension(180, 20));
        pwf_confirm_password.setMinimumSize(new Dimension(180, 20));

        pnl_account_type.setPreferredSize(new Dimension(250, 30));
        pnl_account_type.setMaximumSize(new Dimension(250, 30));
        pnl_account_type.setMinimumSize(new Dimension(250, 30));

        txt_full_name.setPreferredSize(new Dimension(180, 20));
        txt_full_name.setMaximumSize(new Dimension(180, 20));
        txt_full_name.setMinimumSize(new Dimension(180, 20));

        pnl_gender.setPreferredSize(new Dimension(200, 30));
        pnl_gender.setMaximumSize(new Dimension(200, 30));
        pnl_gender.setMinimumSize(new Dimension(200, 30));

        txt_ssn.setPreferredSize(new Dimension(180, 20));
        txt_ssn.setMaximumSize(new Dimension(180, 20));
        txt_ssn.setMinimumSize(new Dimension(180, 20));

        pnl_phone.setPreferredSize(new Dimension(180, 30));
        pnl_phone.setMaximumSize(new Dimension(180, 30));
        pnl_phone.setMinimumSize(new Dimension(180, 30));

        cbx_network.setPreferredSize(new Dimension(100, 20));
        cbx_network.setMaximumSize(new Dimension(100, 20));
        cbx_network.setMinimumSize(new Dimension(100, 20));

        txt_email.setPreferredSize(new Dimension(180, 20));
        txt_email.setMaximumSize(new Dimension(180, 20));
        txt_email.setMinimumSize(new Dimension(180, 20));

        spane_address.setPreferredSize(new Dimension(180, 80));
        spane_address.setMaximumSize(new Dimension(180, 80));
        spane_address.setMinimumSize(new Dimension(180, 80));

        pnl_main.add(lbl_username);
        pnl_main.add(txt_username);

        pnl_main.add(lbl_password);
        pnl_main.add(pwf_password);
        pnl_main.add(lbl_password_error);

        pnl_main.add(lbl_confirm_password);
        pnl_main.add(pwf_confirm_password);
        pnl_main.add(lbl_confirm_password_error);

        pnl_main.add(lbl_account_type);
        pnl_main.add(pnl_account_type);
        pnl_main.add(lbl_account_type_error);

        pnl_account_type.add(rdb_admin);
        pnl_account_type.add(rdb_cashier);
        pnl_account_type.add(rdb_manager);
        group_account_type.add(rdb_admin);
        group_account_type.add(rdb_cashier);
        group_account_type.add(rdb_manager);

        pnl_main.add(lbl_full_name);
        pnl_main.add(txt_full_name);
        pnl_main.add(lbl_full_name_error);

        pnl_main.add(lbl_gender);
        pnl_main.add(pnl_gender);
        pnl_main.add(lbl_gender_error);

        pnl_gender.add(rdb_male);
        pnl_gender.add(rdb_female);
        group_gender.add(rdb_male);
        group_gender.add(rdb_female);

        pnl_main.add(lbl_ssn);
        pnl_main.add(txt_ssn);
        pnl_main.add(lbl_ssn_error);

        Calendar c = Calendar.getInstance();
        c.add(Calendar.YEAR, -18);
        cal_dob.setMaxSelectableDate(c.getTime());
        cal_dob.setDate(c.getTime());
        pnl_main.add(lbl_dob);
        
        btn_show_calendar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String[] options = {Localization.getResourceBundle().getString("CreateEditAccountForm.btn_show_calendar.options.Confirm"), Localization.getResourceBundle().getString("CreateEditAccountForm.btn_show_calendar.options.Cancel")};
                int result = JOptionPane.showOptionDialog(null, cal_dob, Localization.getResourceBundle().getString("CreateEditAccountForm.btn_show_calendar.showOptionDialog.title"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, null);
                if (result == 0) {
                    DateFormat dateformat = DateFormat.getDateInstance(DateFormat.MEDIUM, new Locale(Localization.getLANGUAGE(), Localization.getCOUNTRY()));
                    lbl_chosen_dob.setText(dateformat.format(cal_dob.getDate()));
                }
            }
        });

        lbl_chosen_dob.setPreferredSize(new Dimension(80, 20));

        pnl_dob.setPreferredSize(new Dimension(200, 35));
        pnl_dob.setMaximumSize(new Dimension(200, 35));

        pnl_dob.add(lbl_chosen_dob);
        pnl_dob.add(btn_show_calendar);

        pnl_main.add(pnl_dob);
        pnl_main.add(lbl_dob_error);

        String[] phone_network = {Localization.getResourceBundle().getString("CreateEditAccountForm.phone_network.chooseNetwork"), Localization.getResourceBundle().getString("CreateEditAccountForm.phone_network.local"), "MobiFone", "Viettel", "VinaPhone", "Vietnam Mobile", "Gmobile", "S-Fone"};
        cbx_phone_network.addAll(Arrays.asList(phone_network));

        pnl_main.add(lbl_phone);
        pnl_main.add(cbx_network);
        pnl_main.add(pnl_phone);
        pnl_phone.add(cbx_phone);
        pnl_phone.add(txt_phone1);
        pnl_phone.add(txt_phone2);
        pnl_main.add(lbl_phone_error);

        pnl_main.add(lbl_email);
        pnl_main.add(txt_email);
        pnl_main.add(lbl_email_error);

        pnl_main.add(lbl_address);
        txt_address.setLineWrap(true);
        txt_address.setBorder(BorderFactory.createLineBorder(Color.black));
        pnl_main.add(spane_address);
        pnl_main.add(lbl_address_error);
        pnl_main.add(chk_reset_password);

        SpringUtilities.makeCompactGrid(pnl_main, 34, 1, 20, 0, 130, 2);

        pnl_main.setOpaque(true);

        lbl_username.setEnabled(edit);
        lbl_username.setVisible(edit);
        txt_username.setEditable(false);
        txt_username.setFocusable(false);
        txt_username.setVisible(edit);
        lbl_password.setEnabled(edit);
        lbl_password.setVisible(edit);
        lbl_password_error.setEnabled(edit);
        lbl_password_error.setVisible(edit);
        lbl_confirm_password.setEnabled(edit);
        lbl_confirm_password.setVisible(edit);
        pwf_confirm_password.setEnabled(edit);
        pwf_confirm_password.setVisible(edit);
        pwf_password.setEnabled(edit);
        pwf_password.setVisible(edit);
        chk_reset_password.setVisible(edit);
        cbx_network.setSelectedIndex(0);
        cbx_phone.setEnabled(false);
        txt_phone1.setEnabled(false);
        txt_phone2.setEnabled(false);

        txt_full_name.setDocument(new JTextFieldLimit(50, "^[\\D]*$", false));
        txt_ssn.setDocument(new JTextFieldLimit(9, "^[\\d]*$", false));
        txt_phone1.setDocument(new JTextFieldLimit(4, "^[\\d]*$", false));
        txt_phone2.setDocument(new JTextFieldLimit(4, "^[\\d]*$", false));
        txt_address.setDocument(new JTextFieldLimit(60, "", false));
        txt_phone1.getDocument().addDocumentListener(new TextBoxPhoneListener());

        cbx_network.addItemListener(new SelectPhoneNetworkController(this, null));
        cbx_phone.addItemListener(new ComboBoxPhoneListener());

        chk_reset_password.addChangeListener(new ChangeListener() {

            @Override
            public void stateChanged(ChangeEvent e) {
                pwf_password.setEnabled(!chk_reset_password.isSelected());
                pwf_confirm_password.setEnabled(!chk_reset_password.isSelected());
            }
        });

        this.add(pnl_main);

        ImageIcon icon_create_edit_account = new ImageIcon(getClass().getResource("/Images/create_edit_account.png"));
        String[] options = {Localization.getResourceBundle().getString("CreateEditAccountForm.editAccount.Confirm"), Localization.getResourceBundle().getString("CreateEditAccountForm.editAccount.Cancel")};
        p = new JOptionPane(this, JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION, icon_create_edit_account, options, null);
        if(!edit)
        {
            d = p.createDialog(Localization.getResourceBundle().getString("CreateEditAccountForm.createDialog.create"));
        }
        else
        {
            d = p.createDialog(Localization.getResourceBundle().getString("CreateEditAccountForm.createDialog.edit"));
        }
        this.getTxt_address().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getTxt_address().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getTxt_address().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getTxt_address().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getTxt_email().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getTxt_email().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getTxt_email().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getTxt_email().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getTxt_full_name().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getTxt_full_name().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getTxt_full_name().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getTxt_full_name().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getTxt_phone1().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getTxt_phone1().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getTxt_phone1().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getTxt_phone1().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getTxt_phone2().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getTxt_phone2().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getTxt_phone2().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getTxt_phone2().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getTxt_ssn().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getTxt_ssn().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getTxt_ssn().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getTxt_ssn().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getCbx_network().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getCbx_network().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getCbx_network().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getCbx_network().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getCbx_phone().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getCbx_phone().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getCbx_phone().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getCbx_phone().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getChk_reset_password().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getChk_reset_password().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getChk_reset_password().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getChk_reset_password().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getPwf_confirm_password().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getPwf_confirm_password().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getPwf_confirm_password().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getPwf_confirm_password().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getPwf_confirm_password().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getPwf_confirm_password().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getPwf_confirm_password().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getPwf_confirm_password().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getRdb_admin().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getRdb_admin().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getRdb_admin().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getRdb_admin().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getRdb_cashier().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getRdb_cashier().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getRdb_cashier().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getRdb_cashier().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getRdb_female().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getRdb_female().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getRdb_female().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getRdb_female().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getRdb_male().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getRdb_male().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getRdb_male().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getRdb_male().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getRdb_manager().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getRdb_manager().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getRdb_manager().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getRdb_manager().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
    }

    public JRadioButton getRdb_admin() {
        return rdb_admin;
    }

    public JRadioButton getRdb_manager() {
        return rdb_manager;
    }

    public JRadioButton getRdb_cashier() {
        return rdb_cashier;
    }

    public JTextField getTxt_full_name() {
        return txt_full_name;
    }

    public JTextArea getTxt_address() {
        return txt_address;
    }

    public JCalendar getCal_dob() {
        return cal_dob;
    }

    public JLabel getLbl_account_type_error() {
        return lbl_account_type_error;
    }

    public JTextField getTxt_ssn() {
        return txt_ssn;
    }

    public JLabel getLbl_chosen_dob() {
        return lbl_chosen_dob;
    }

    public JLabel getLbl_ssn_error() {
        return lbl_ssn_error;
    }

    public void setRdb_admin() {
        rdb_admin.setSelected(true);
    }

    public void setRdb_manager() {
        rdb_manager.setSelected(true);
    }

    public void setRdb_cashier() {
        rdb_cashier.setSelected(true);
    }

    public JRadioButton getRdb_male() {
        return rdb_male;
    }

    public JRadioButton getRdb_female() {
        return rdb_female;
    }

    public JLabel getLbl_gender_error() {
        return lbl_gender_error;
    }

    public JLabel getLbl_phone_error() {
        return lbl_phone_error;
    }

    public JLabel getLbl_email_error() {
        return lbl_email_error;
    }

    public JTextField getTxt_email() {
        return txt_email;
    }

    public JComboBox getCbx_phone() {
        return cbx_phone;
    }

    public JComboBox getCbx_network() {
        return cbx_network;
    }

    public Vector getCbx_phone_code() {
        return cbx_phone_code;
    }

    public JTextField getTxt_phone1() {
        return txt_phone1;
    }

    public JTextField getTxt_phone2() {
        return txt_phone2;
    }

    public JTextField getTxt_username() {
        return txt_username;
    }

    public JCheckBox getChk_reset_password() {
        return chk_reset_password;
    }

    public JPasswordField getPwf_password() {
        return pwf_password;
    }

    public JPasswordField getPwf_confirm_password() {
        return pwf_confirm_password;
    }

    public JLabel getLbl_password_error() {
        return lbl_password_error;
    }

    public JLabel getLbl_confirm_password_error() {
        return lbl_confirm_password_error;
    }

    public JLabel getLbl_full_name_error() {
        return lbl_full_name_error;
    }

    public JLabel getLbl_address_error() {
        return lbl_address_error;
    }

    public JLabel getLbl_dob_error() {
        return lbl_dob_error;
    }

    public JOptionPane getP() {
        return p;
    }

    public JDialog getD() {
        return d;
    }

    class ComboBoxPhoneListener implements ItemListener {

        @Override
        public void itemStateChanged(ItemEvent e) {
            txt_phone1.requestFocus();
        }
    }

    class TextBoxPhoneListener implements DocumentListener {

        @Override
        public void insertUpdate(DocumentEvent e) {
            if (cbx_network.getSelectedIndex() == 1 && txt_phone1.getText().length() == 4) {
                txt_phone2.requestFocus();
            } else if (cbx_network.getSelectedIndex() > 1 && txt_phone1.getText().length() == 3) {
                txt_phone2.requestFocus();
            }
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            if (cbx_network.getSelectedIndex() == 1 && txt_phone1.getText().length() == 4) {
                txt_phone2.requestFocus();
            } else if (cbx_network.getSelectedIndex() > 1 && txt_phone1.getText().length() == 3) {
                txt_phone2.requestFocus();
            }
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            if (cbx_network.getSelectedIndex() == 1 && txt_phone1.getText().length() == 4) {
                txt_phone2.requestFocus();
            } else if (cbx_network.getSelectedIndex() > 1 && txt_phone1.getText().length() == 3) {
                txt_phone2.requestFocus();
            }
        }
    }
}

