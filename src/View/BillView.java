/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.SearchBillController;
import Controller.SelectBillController;
import Model.Restaurant;
import Utilities.JPagingPanel;
import Utilities.Localization;
import Utilities.REPOSButton;
import com.toedter.calendar.JCalendar;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import javax.swing.ImageIcon;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Frank NgVietCuong
 */
public class BillView extends JPanel implements Observer {

    private Restaurant r;
    private JPanel pnl_north = new JPanel(new BorderLayout());
    private JPanel pnl_north_cal = new JPanel(new GridLayout(1, 2));
    private JPanel pnl_bill_detail_view = new JPanel(new BorderLayout());
    private JPanel pnl_center = new JPanel(new GridLayout(1, 2));
    private JPanel pnl_bill_grid_view = new JPanel(new BorderLayout());
    private JCalendar cal_from = new JCalendar();
    private JCalendar cal_to = new JCalendar();
    private ImageIcon icon_search_normal = new ImageIcon(this.getClass().getResource("/Images/search.png"));
    private ImageIcon icon_search_clicked = new ImageIcon(this.getClass().getResource("/Images/search_clicked.png"));
    private REPOSButton btn_search = new REPOSButton(icon_search_normal, icon_search_clicked, "Search bills");
    private JEditorPane pnl_bill_detail = new JEditorPane();
    private JPagingPanel pnl_list_bill;
    private JScrollPane spane_west_bill_detail = new JScrollPane(pnl_bill_detail);
    String fullHTML = "";

    public BillView(Restaurant r) {
        this.r = r;

        // set window size
        this.setSize(800, 600);
        this.setLayout(new BorderLayout());


        pnl_north.setPreferredSize(new Dimension(800, 200));
        pnl_center.setPreferredSize(new Dimension(800, 400));

        this.add(pnl_north, BorderLayout.NORTH);
        this.add(pnl_center, BorderLayout.CENTER);

        pnl_bill_detail_view.setBackground(Color.WHITE);
        pnl_north_cal.setBackground(Color.WHITE);
        pnl_center.setBackground(Color.WHITE);
        pnl_bill_grid_view.setBackground(Color.WHITE);

        pnl_north_cal.add(cal_from);
        pnl_north_cal.add(cal_to);

        //search button
        TitledBorder border_north = new TitledBorder("Search");
        border_north.setTitleColor(Color.BLUE);
        pnl_north.setBorder(border_north);

        pnl_north.add(pnl_north_cal, BorderLayout.CENTER);
        pnl_north.add(btn_search, BorderLayout.EAST);

        // detailed view of bill
        TitledBorder border_detail_view = new TitledBorder(Localization.getResourceBundle().getString("BillView.borderDetailViewTitle"));
        border_detail_view.setTitleColor(Color.BLUE);
        pnl_bill_detail_view.setBorder(border_detail_view);
        pnl_bill_detail_view.add(spane_west_bill_detail);
        spane_west_bill_detail.setViewportView(pnl_bill_detail);
        pnl_bill_detail.setContentType("text/html");
        pnl_bill_detail.setEditable(false);

        //grid view of bills
        TitledBorder border_grid_view = new TitledBorder(Localization.getResourceBundle().getString("BillView.borderGridViewTitle"));
        border_grid_view.setTitleColor(Color.BLUE);
        pnl_bill_grid_view.setBorder(border_grid_view);

        pnl_center.add(pnl_bill_detail_view);
        pnl_center.add(pnl_bill_grid_view);

        btn_search.addActionListener(new SearchBillController(r, this));

        cal_from.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "search");
        cal_from.getActionMap().put("search", new SearchBillController(r, this));

    }

    public JPanel getPnl_center_bill_grid_view() {
        return pnl_bill_grid_view;
    }

    public JCalendar getCal_from() {
        return cal_from;
    }
    
    public JCalendar getCal_to() {
        return cal_to;
    }
    
    public REPOSButton getBtn_search() {
        return btn_search;
    }

    public JPagingPanel getPnl_list_bill() {
        return pnl_list_bill;
    }

    public void setPnl_list_bill(JPagingPanel pnl_list_bill) {
        this.pnl_list_bill = pnl_list_bill;
    }

    public void addObs() {
        r.addObserver(this);
    }

    public void reload() {

        String[] column_names = {Localization.getResourceBundle().getString("BillView.number"), Localization.getResourceBundle().getString("BillView.date"), Localization.getResourceBundle().getString("BillView.staff"), Localization.getResourceBundle().getString("BillView.table"), Localization.getResourceBundle().getString("BillView.total")};
        ArrayList<String[]> data = r.getBillList(null, null);
        DefaultTableModel list_bill_table_model = new DefaultTableModel(null, column_names) {
            @Override
            public Class<?> getColumnClass(int column) {
                return (column == 0) ? Integer.class : Object.class;
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (int i = 0; i < data.size(); i++) {
            list_bill_table_model.addRow(data.get(i));
        }

        if (pnl_list_bill != null) {
            pnl_bill_grid_view.remove(pnl_list_bill);
        }
        pnl_list_bill = new JPagingPanel(list_bill_table_model);

        pnl_list_bill.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        pnl_list_bill.getTable().getColumnModel().getColumn(0).setPreferredWidth(this.getWidth() * 35 / 1000);
        pnl_list_bill.getTable().getColumnModel().getColumn(1).setPreferredWidth(this.getWidth() * 135 / 1000);
        pnl_list_bill.getTable().getColumnModel().getColumn(2).setPreferredWidth(this.getWidth() * 145 / 1000);
        pnl_list_bill.getTable().getColumnModel().getColumn(3).setPreferredWidth(this.getWidth() * 45 / 1000);
        pnl_list_bill.getTable().getColumnModel().getColumn(4).setPreferredWidth(this.getWidth() * 101 / 1000);

        pnl_list_bill.getTable().setCellSelectionEnabled(false);
        pnl_list_bill.getTable().setRowSelectionAllowed(true);
        pnl_list_bill.getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        pnl_list_bill.getTable().getSelectionModel().addListSelectionListener(new SelectBillController(r, this));
        pnl_list_bill.getTable().revalidate();

        pnl_bill_grid_view.add(pnl_list_bill, BorderLayout.CENTER);

        validate();
    }

    public void billPanel(Calendar date, String staff, int tableNumber, int total, String discount, int discountValue, ArrayList<String[]> orderItems) {

        setFinalOrderList(orderItems);
        Date tempDate = date.getTime();
        DateFormat dateformat = DateFormat.getDateInstance(DateFormat.LONG, new Locale(Localization.getLANGUAGE(), Localization.getCOUNTRY()));

        String header = "<html><center><p style='font-size:12px; font-weight:bold;'>" + Localization.getResourceBundle().getString("BillView.billViewRestaurant") + "</p>"
                + "<p style='font-size:12px;'>" + Localization.getResourceBundle().getString("BillView.billViewAddress") + "<br/>"
                + Localization.getResourceBundle().getString("BillView.billViewTel") + "37761300</p></center>"
                + "<p style='text-align:right;font-size:12px;font-weight:bold;'>" + Localization.getResourceBundle().getString("BillView.billViewTable") + tableNumber + "</p>"
                + "<p style='font-size:12px;'>" + Localization.getResourceBundle().getString("BillView.billViewDate") + dateformat.format(tempDate) + "<br/>"
                + Localization.getResourceBundle().getString("BillView.billViewStaff") + staff + "</p>";

        String table = "<body><table width='100'>"
                + "<tr>"
                + "<td colspan='3'>____________________________________</td>"
                + "</tr>";

        int subTotal = 0;

        for (int i = 0; i < orderItems.size(); i++) {

            String[] item = orderItems.get(i);
            String name = item[0];
            int quantity = Integer.parseInt(item[1]);
            int price = Integer.parseInt(item[2]);
            subTotal += price * quantity;
            String sPrice = new DecimalFormat("###,###,###").format(price);
            if (sPrice.contains(",")) {
                sPrice = sPrice.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
            } else {
                sPrice = sPrice.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
            }

            String sAmount = new DecimalFormat("###,###,###").format(price * quantity);
            if (sAmount.contains(",")) {
                sAmount = sAmount.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
            } else {
                sAmount = sAmount.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
            }

            table += "<tr>"
                    + "<td colspan='3' style='font-size:8px;' >" + name + "</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td style='font-size:8px;'>" + quantity + " x</td>"
                    + "<td style='font-size:8px;'>" + sPrice + "</td>"
                    + "<td style='text-align:right;font-size:8px;'>= " + sAmount + Localization.getResourceBundle().getString("CashierManagerView.vnd") + "</td>"
                    + "</tr>";
        }
        int discountAmount = subTotal * discountValue / 120;
        String sub = new DecimalFormat("###,###,###").format(subTotal);
        if (sub.contains(",")) {
            sub = sub.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
        } else {
            sub = sub.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
        }

        String sDiscountAmount = new DecimalFormat("###,###,###").format(discountAmount);
        if (sDiscountAmount.contains(",")) {
            sDiscountAmount = sDiscountAmount.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
        } else {
            sDiscountAmount = sDiscountAmount.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
        }

        subTotal -= discountAmount;
        String sTotal = new DecimalFormat("###,###,###").format(total);
        if (sTotal.contains(",")) {
            sTotal = sTotal.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
        } else {
            sTotal = sTotal.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
        }

        table += "<tr>"
                + "<td colspan='3'>____________________________________</td>"
                + "</tr>"
                + "<tr style='font-size:8px;'>"
                + "<td>" + Localization.getResourceBundle().getString("BillView.billViewSubtotal") + "</td>" + "<td></td>" + "<td style='text-align:right;font-weight:bold;'>" + sub + Localization.getResourceBundle().getString("CashierManagerView.vnd") + "</td>"
                + "</tr>"
                + "<tr style='font-size:8px;'>"
                + "<td>" + Localization.getResourceBundle().getString("BillView.billViewDiscount") + "</td>" + "<td>" + discountValue + "%</td>" + "<td style='text-align:right;font-weight:bold;'>" + sDiscountAmount + Localization.getResourceBundle().getString("CashierManagerView.vnd") + "</td>"
                + "</tr>"
                + "<tr>"
                + "<td colspan='3'>-----------------------------------------------------</td>"
                + "</tr>"
                + "<tr style='font-size:8px;'>"
                + "<td>" + Localization.getResourceBundle().getString("BillView.billViewTotal") + "</td>" + "<td></td>" + "<td style='text-align:right;font-weight:bold;'>" + sTotal + Localization.getResourceBundle().getString("CashierManagerView.vnd") + "</td>"
                + "</tr>";

        table += "</table></body></html>";

        fullHTML = header + table;

        pnl_bill_detail.setText(fullHTML);
        pnl_bill_detail.revalidate();
    }

    public void setFinalOrderList(ArrayList<String[]> orderItem) {

        String[] item;
        String name;
        int quantity;

        for (int i = 0; i < orderItem.size(); i++) {
            item = orderItem.get(i);
            name = item[0];
            quantity = Integer.parseInt(item[2]);
            for (int j = i + 1; j < orderItem.size(); j++) {
                if (name.equals(orderItem.get(j)[0])) {
                    quantity += Integer.parseInt(orderItem.get(j)[2]);
                    orderItem.remove(j);
                }
            }
            item[2] = Integer.toString(quantity);
        }
    }

    @Override
    public void update(Observable o, Object o1) {
        reload();
    }
}