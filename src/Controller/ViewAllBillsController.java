/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Restaurant;
import View.BillView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author Frank NgVietCuong
 */
public class ViewAllBillsController implements ActionListener {
    
    private Restaurant r;

    public ViewAllBillsController(Restaurant r) {
        this.r = r;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        BillView bv = new BillView(r);
        bv.reload();
        JOptionPane.showMessageDialog(null, bv);
    }
    
}
