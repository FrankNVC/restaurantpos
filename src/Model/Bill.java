/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author Frank NgVietCuong
 */
public class Bill implements Serializable {

    private int id;
    private Calendar date;
    private String staff;
    private int tableNumber;
    private int total;
    private String discount;
    private int discountValue;

    private ArrayList<String[]> orderItems = new ArrayList<>();

    public Bill(int ID, Calendar date, String staff, int tableNumber, int total, String discount, int discountValue, ArrayList<String[]> orderItems) {
        this.date = date;
        this.staff = staff;
        this.tableNumber = tableNumber;
        this.total = total;
        this.discount = discount;
        this.discountValue = discountValue;
        this.orderItems = orderItems;
        this.id = ID;
    }

    public Calendar getDate() {
        return date;
    }

    public String getDiscount() {
        return discount;
    }

    public int getDiscountValue() {
        return discountValue;
    }

    public ArrayList<String[]> getOrderItems() {
        return orderItems;
    }

    public int getTotal() {
        return total;
    }

    public int getId() {
        return id;
    }

    public String getStaff() {
        return staff;
    }

    public int getTableNumber() {
        return tableNumber;
    }

    @Override
    public String toString() {
        String s = "ID= " + id + " Date= " + date.getTime().toString() + " Staff= " + staff;

        for (int i = 0; i < orderItems.size(); i++) {
            s = s + " Item= " + orderItems.get(i)[0] + " Price=" + orderItems.get(i)[1] + " Quantity=" + orderItems.get(i)[2] + " Category=" + orderItems.get(i)[3];
        }
        return s;
    }
}
