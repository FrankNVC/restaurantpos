package Controller;

import Main.Application;
import Model.Restaurant;
import Utilities.Localization;
import Utilities.StringUtil;
import View.CreateEditCategoryForm;
import View.CreateEditItemForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;

/**
 *
 * @author chi linh
 */
public class CreateCategoryItemController implements ActionListener {

    /**
     * defines event when a category item is created
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Restaurant r = Application.r;
        Application.cmv.deselectAllTable();
        boolean created = false;
        if (Application.cmv.getRdb_category().isSelected()) {

            // Initialise the create category form
            CreateEditCategoryForm ccf = new CreateEditCategoryForm(false);

            Object result;
            do {
                // Process the create category form
                ccf.getD().setVisible(true);
                result = ccf.getP().getValue();

                if (result == ccf.getP().getOptions()[0]) {
                    String category = StringUtil.TrimSpace(ccf.getTxt_category().getText());
                    if (category.equals("")) {
                        r.displayError(ccf.getLbl_category_error(), Localization.getResourceBundle().getString("CreateCategoryItemController.categoryRequired"));
                    } else if (r.validateCategory(category)) {
                        ccf.getLbl_category_error().setText(null);

                        created = true;
                        r.createCategory(category);
                        JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("CreateCategoryItemController.createCategorySuccessfully.message") + "\"" + category + "\"!", Localization.getResourceBundle().getString("CreateCategoryItemController.createCategorySuccessfully.title"), JOptionPane.INFORMATION_MESSAGE);
                        r.update();
                    } else {
                        r.displayError(ccf.getLbl_category_error(), Localization.getResourceBundle().getString("CreateCategoryItemController.categoryAlreadyExit"));
                    }
                }
            } while (!created && result == ccf.getP().getOptions()[0]);
        } else {

            // Initialise the create item form
            String category = "";
            if (Application.cmv.getTree().getSelectionPath() == null || Application.cmv.getTree().getSelectionPath().getPathCount() >= 2) {
                if (Application.cmv.getTree().getSelectionPath() != null) {
                    if (Application.cmv.getTree().getSelectionPath().getPathCount() == 3) {
                        category = (String) ((DefaultMutableTreeNode) Application.cmv.getTree().getSelectionPath().getParentPath().getLastPathComponent()).getUserObject();
                    } else {
                        category = (String) ((DefaultMutableTreeNode) Application.cmv.getTree().getSelectionPath().getLastPathComponent()).getUserObject();
                    }
                }
            }
            CreateEditItemForm cif = new CreateEditItemForm(false);
            for (int i = 0; i < r.getMenu().getCategories().size(); i++) {
                cif.getCbx_items().add(r.getMenu().getCategories().get(i));
            }
            if (category.equalsIgnoreCase("")) {
                cif.getCbx_item_category().setSelectedIndex(0);
            } else {
                cif.getCbx_item_category().setSelectedItem(category);
            }
            Object result;
            do {
                // Process the create item form
                cif.getD().setVisible(true);
                result = cif.getP().getValue();
                if (result == cif.getP().getOptions()[0]) {
                    String name = StringUtil.TrimSpace(cif.getTxt_item_name().getText());
                    if (name.equalsIgnoreCase("")) {
                        r.displayError(cif.getLbl_item_name_error(), Localization.getResourceBundle().getString("CreateCategoryItemController.nameRequired"));
                    } else {
                        cif.getLbl_item_name_error().setText(null);

                        String priceText = cif.getTxt_item_price().getText().trim();
                        if (priceText.equalsIgnoreCase("")) {
                            r.displayError(cif.getLbl_item_price_error(), Localization.getResourceBundle().getString("CreateCategoryItemController.priceRequired"));
                        } else {
                            cif.getLbl_item_price_error().setText(null);

                            if (r.validateItem(null, name)) {
                                cif.getLbl_item_name_error().setText(null);

                                r.createItem(name, Integer.parseInt(cif.getTxt_item_price().getText().trim().replaceAll("[, ]+", "")), (String) cif.getCbx_item_category().getSelectedItem());
                                created = true;
                                JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("CreateCategoryItemController.createItemSuccessfully.message") + "\"" + name + "\"!", Localization.getResourceBundle().getString("CreateCategoryItemController.createItemSuccessfully.title"), JOptionPane.INFORMATION_MESSAGE);
                                r.update();
                            } else {
                                r.displayError(cif.getLbl_item_name_error(), Localization.getResourceBundle().getString("CreateCategoryItemController.nameAlreadyExit"));
                            }
                        }
                    }
                }
            } while (!created && result == cif.getP().getOptions()[0]);
        }
    }
}