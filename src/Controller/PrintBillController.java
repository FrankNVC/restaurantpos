/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Main.Application;
import View.PaymentView;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Sang
 */
public class PrintBillController implements ActionListener, Printable {

    private PaymentView pv;

    /**
     *
     * @param pv
     */
    public PrintBillController(PaymentView pv) {
        this.pv = pv;
    }

    /**
     * defines events when print bill button is clicked
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (!Application.cmv.getCurrent_tables().isEmpty()) {
            File f = new File("Bill.png");
            f.setWritable(true);
            try {
                try (BufferedWriter bw = new BufferedWriter(new FileWriter(f))) {
                    bw.write(pv.getFullHTML());
                }
            } catch (IOException ex) {
                System.out.println(ex);
            }
            FileInputStream fis = null;
            try {
                fis = new FileInputStream("Bill.png");
            } catch (FileNotFoundException ex) {
                System.out.println(ex);
            }
            PrinterJob pj = PrinterJob.getPrinterJob();
            if (pj.printDialog()) {
                pj.setPrintable(this);
                if (pj.getPrintService().toString().equalsIgnoreCase("Win32 Printer : Microsoft XPS Document Writer")) {
                    JFileChooser fc = new JFileChooser();
                    fc.setAcceptAllFileFilterUsed(false);
                    fc.setFileFilter(new FileNameExtensionFilter("PNG (*.png)", "png"));
                    fc.showSaveDialog(null);
                    fc.setSelectedFile(new File(fc.getSelectedFile().getPath() + ".png"));
                    BufferedImage bi = new BufferedImage(pv.GetEditorPane().getWidth(), pv.GetEditorPane().getHeight(), BufferedImage.TYPE_INT_ARGB);
                    Graphics g = bi.createGraphics();
                    pv.GetEditorPane().paint(g);
                    g.dispose();
                    try {
                        ImageIO.write(bi, "png", new File(fc.getSelectedFile().toURI()));
                    } catch (IOException ioe) {
                        System.out.println(ioe);
                    }
                } else {
                    try {
                        pj.print();
                    } catch (PrinterException ex) {
                        System.out.println(ex);
                    }
                }
            }
            f.delete();
        }
    }

    /**
     *
     * @param graphics
     * @param pageFormat
     * @param pageIndex
     * @return
     * @throws PrinterException
     */
    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
        pageFormat.setOrientation(PageFormat.PORTRAIT);
        if (pageIndex > 0) {
            return Printable.NO_SUCH_PAGE;
        }
        Graphics2D g2 = (Graphics2D) graphics;
        g2.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
        g2.translate(0f, 0f);
        pv.GetEditorPane().paint(g2);
        return Printable.PAGE_EXISTS;
    }
}