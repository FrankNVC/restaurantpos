/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Main.Application;
import Model.Restaurant;
import Model.Table;
import Utilities.Localization;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Frank NgVietCuong
 */
public class MergeTableController implements ActionListener {

    private Restaurant r;

    /**
     *
     * @param r
     */
    public MergeTableController(Restaurant r) {
        this.r = r;
    }

    /**
     * defines events when a table group is merged
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (Application.cmv.getCurrent_tables().size() > 1) {        
            ArrayList<Table> merge_tables = new ArrayList<>();
            for (int i = 0; i < Application.cmv.getCurrent_tables().size(); i++) {
                if (Application.cmv.getCurrent_tables().get(i).getTableGroupId() == -1) {
                    merge_tables.add(Application.cmv.getCurrent_tables().get(i));
                } else {
                    for (int j = 0; j < r.getTables().size(); j++) {
                        if (Application.cmv.getCurrent_tables().get(i).getTableGroupId() == r.getTables().get(j).getTableGroupId()) {
                            merge_tables.add(r.getTables().get(j));
                        }
                    }
                }
            }
            if (merge_tables.size() <= 16) {
                int root_cell_id = Application.cmv.mergable();
                if (Application.cmv.getCurrent_tables().size() > 1) {
                    if (root_cell_id != -1) {
                        Table root_table = r.mergeTables(Application.cmv.getCurrent_tables(), root_cell_id);
                        Application.cmv.mergeTable(root_table);
                        r.update();
                    }
                }
            }
            else
            {
                JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("MergeTableController.tableOverLimit.message"), Localization.getResourceBundle().getString("MergeTableController.tableOverLimit.title"), JOptionPane.INFORMATION_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("MergeTableController.chooseAtLeast2Tables.message"), Localization.getResourceBundle().getString("MergeTableController.chooseAtLeast2Tables.title"), JOptionPane.INFORMATION_MESSAGE);
        }
    }
}