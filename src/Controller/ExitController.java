/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Utilities.Localization;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Nguyen Minh Sang
 */
public class ExitController implements ActionListener {

    private JFrame mainFrame;

    /**
     *
     * @param mainFrame
     */
    public ExitController(JFrame mainFrame) {
        this.mainFrame = mainFrame;
    }

    /**
     * defines events when the exit button is clicked
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String options[] = {Localization.getResourceBundle().getString("ExitController.Confirm"), Localization.getResourceBundle().getString("ExitController.Cancel")};
        int result = JOptionPane.showOptionDialog(null, Localization.getResourceBundle().getString("ExitController.showConfirmDialog.message"), Localization.getResourceBundle().getString("ExitController.showConfirmDialog.title"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, null);
        if (result == JOptionPane.YES_OPTION) {
            mainFrame.dispose();
            System.exit(0);
        }
    }
}
