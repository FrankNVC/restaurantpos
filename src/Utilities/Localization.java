/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilities;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 *
 * @author chi linh
 */
public class Localization {

    private static String LANGUAGE = "en";
    private static String COUNTRY = "US";
    private static Locale CURRENT_LOCALE;
    private static ResourceBundle MESSAGES;

    public static void setLANGUAGE(String LANGUAGE) {
        Localization.LANGUAGE = LANGUAGE;
    }

    public static void setCOUNTRY(String COUNTRY) {
        Localization.COUNTRY = COUNTRY;
    }

    public static String getLANGUAGE() {
        return LANGUAGE;
    }

    public static String getCOUNTRY() {
        return COUNTRY;
    }

    public static ResourceBundle getResourceBundle() {

        CURRENT_LOCALE = new Locale(LANGUAGE, COUNTRY);

        MESSAGES = ResourceBundle.getBundle("MessagesBundle", CURRENT_LOCALE);
        return MESSAGES;
    }
}