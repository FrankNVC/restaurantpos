package Controller;

import Main.Application;
import Model.Restaurant;
import Utilities.Localization;
import Utilities.StringUtil;
import View.CreateEditCategoryForm;
import View.CreateEditItemForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Nguyen Minh Sang
 */
public class EditCategoryItemController implements ActionListener {

    private Restaurant r;

    /**
     *
     * @param r
     */
    public EditCategoryItemController(Restaurant r) {
        this.r = r;
    }

    /**
     * defines events when an category is edited
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Application.cmv.deselectAllTable();
        boolean edited = false;
        JTree tree = Application.cmv.getTree();
        if (tree.getSelectionPaths() != null) {
            if (Application.cmv.getRdb_category().isSelected()) {

                CreateEditCategoryForm cecf = new CreateEditCategoryForm(true);
                if (tree.getSelectionPath().getPathCount() == 2) {
                    cecf.getTxt_old_category().setText((String) ((DefaultMutableTreeNode) tree.getSelectionPath().getLastPathComponent()).getUserObject());
                } else {
                    cecf.getTxt_old_category().setText((String) ((DefaultMutableTreeNode) tree.getSelectionPath().getParentPath().getLastPathComponent()).getUserObject());
                }
                cecf.getTxt_category().setText(cecf.getTxt_old_category().getText());
                Object result;
                do {
                    cecf.getD().setVisible(true);
                    result = cecf.getP().getValue();
                    if (result == cecf.getP().getOptions()[0]) {
                        String oldCategory = cecf.getTxt_old_category().getText();
                        String newCategory = StringUtil.TrimSpace(cecf.getTxt_category().getText());
                        if (!newCategory.equals("")) {
                            if (r.validateCategory(newCategory)) {
                                cecf.getLbl_category_error().setText(null);

                                r.editCategory(oldCategory, newCategory);
                                edited = true;
                                JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("EditCategoryItemController.editCategorySuccessfully.message") + newCategory + "!", Localization.getResourceBundle().getString("EditCategoryItemController.editCategorySuccessfully.title"), JOptionPane.INFORMATION_MESSAGE);
                                r.update();
                            } else {
                                r.displayError(cecf.getLbl_category_error(), Localization.getResourceBundle().getString("EditCategoryItemController.categoryAlreadyExit"));
                            }
                        }
                    }
                } while (!edited && result == cecf.getP().getOptions()[0]);
            } else if (Application.cmv.getRdb_item().isSelected()) {
                String oldItemName = (String) ((DefaultMutableTreeNode) tree.getSelectionPath().getLastPathComponent()).getUserObject();
                String[] parts = oldItemName.split("\\-");
                oldItemName = parts[0].trim();
                CreateEditItemForm ceif = new CreateEditItemForm(true);
                ceif.getTxt_old_item_name().setText(oldItemName);
                for (int i = 0; i < r.getMenu().getCategories().size(); i++) {
                    ceif.getCbx_items().add(r.getMenu().getCategories().get(i));
                }
                ceif.getCbx_item_category().setSelectedItem((String) ((DefaultMutableTreeNode) tree.getSelectionPath().getParentPath().getLastPathComponent()).getUserObject());
                for (int i = 0; i < r.getMenu().getItems().size(); i++) {
                    if (r.getMenu().getItems().get(i).getItemName().equalsIgnoreCase(oldItemName)) {
                        ceif.getCbx_item_category().setSelectedItem(r.getMenu().getItems().get(i).getItemCategory());
                        ceif.getTxt_item_price().setText(r.getMenu().getItems().get(i).getItemPrice() + "");
                        break;
                    }
                }
                ceif.getTxt_item_name().setText(ceif.getTxt_old_item_name().getText());
                Object result;
                do {
                    ceif.getD().setVisible(true);
                    result = ceif.getP().getValue();
                    if (result == ceif.getP().getOptions()[0]) {
                        String newItemName = StringUtil.TrimSpace(ceif.getTxt_item_name().getText().trim());
                        if (newItemName.equals("")) {
                            r.displayError(ceif.getLbl_item_name_error(), Localization.getResourceBundle().getString("EditCategoryItemController.newItemNameRequired"));
                        } else {
                            ceif.getLbl_item_name_error().setText(null);

                            if (ceif.getTxt_item_price().getText().trim().equals("")) {
                                r.displayError(ceif.getLbl_item_price_error(), Localization.getResourceBundle().getString("EditCategoryItemController.priceRequired"));
                            } else {
                                ceif.getLbl_item_price_error().setText(null);

                                if (r.validateItem(oldItemName, newItemName)) {
                                    ceif.getLbl_item_name_error().setText(null);
                                    r.editItem(oldItemName, newItemName, Integer.parseInt(ceif.getTxt_item_price().getText().trim().replaceAll("[ ,]+", "")), (String) ceif.getCbx_item_category().getSelectedItem());
                                    edited = true;
                                    Application.cmv.updateOrderItemList();
                                    Application.cmv.validate();
                                    JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("EditCategoryItemController.editItemSuccessfully.message") + newItemName + "!", Localization.getResourceBundle().getString("EditCategoryItemController.editItemSuccessfully.title"), JOptionPane.INFORMATION_MESSAGE);
                                    r.update();
                                } else {
                                    r.displayError(ceif.getLbl_item_name_error(), Localization.getResourceBundle().getString("EditCategoryItemController.itemAlreadyExit"));
                                }
                            }
                        }
                    }
                } while (!edited && result == ceif.getP().getOptions()[0]);
            }
        } else {
            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("EditCategoryItemController.chooseCategoryOrItem"));
        }
    }
}
