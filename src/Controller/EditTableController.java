package Controller;

import Main.Application;
import Model.Restaurant;
import Utilities.Localization;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author Na
 */
public class EditTableController implements ActionListener {

    private Restaurant r;

    /**
     *
     * @param r
     */
    public EditTableController(Restaurant r) {
        this.r = r;
    }

    /**
     * defines events when a table is edited
     *
     * @param ae
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        if (Application.cmv.getCurrent_tables().size() == 1 && Application.cmv.getCurrent_tables().get(0).getTableGroupId() == -1) {
            int oldTableNumber = Application.cmv.getCurrent_tables().get(0).getTableNumber();
            String newTableNumberText = Application.cmv.getTxt_table_number().getText().trim();
            int newTableNumber = 0;
            boolean valid = false;
            if (!newTableNumberText.equalsIgnoreCase("")) {
                try {
                    newTableNumber = Integer.parseInt(newTableNumberText);
                    valid = true;
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("EditTableController.tableNumberFormat_1"));
                    valid = false;
                }
                if (valid) {
                    valid = r.validateTableNumber(oldTableNumber, newTableNumber);
                }
            }
            if (valid) {
                Application.cmv.updateTable(r.editTableNumber(oldTableNumber, newTableNumber));
                r.sortTables(r.getTables());
                r.update();
            } else {
                JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("EditTableController.tableNumberFormat_2"));
            }
        } else {
            JOptionPane.showMessageDialog(null, 
                    Localization.getResourceBundle().getString("EditTableController.chooseSingleTable"));
        }
    }
}
