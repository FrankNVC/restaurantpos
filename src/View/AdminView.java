package View;

import Controller.CreateAccountController;
import Controller.DeleteAccountController;
import Controller.EditAccountController;
import Controller.SearchAccountController;
import Controller.SelectAccountController;
import Main.Application;
import Model.Restaurant;
import Utilities.JPagingPanel;
import Utilities.Localization;
import Utilities.REPOSButton;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Na
 */
public class AdminView extends JPanel implements Observer {

    private Restaurant r;
    private CurrentUserStatusPanel pnl_user_status;
    private JPanel pnl_west = new JPanel(new BorderLayout());
    private JPanel pnl_west_search = new JPanel(new BorderLayout());
    private JPanel pnl_west_search_left = new JPanel(new GridLayout(2, 1));
    private JPanel pnl_west_search_filter = new JPanel(new GridLayout(1, 3));
    private JPanel pnl_west_account_detail_view = new JPanel(new BorderLayout());
    private JPanel pnl_center = new JPanel(new BorderLayout());
    private JPanel pnl_center_tool_bar = new JPanel(new FlowLayout(FlowLayout.LEFT));
    private JPanel pnl_center_account_grid_view = new JPanel(new BorderLayout());
    private JTextField txt_search = new JTextField();
    private ImageIcon icon_search_normal = new ImageIcon(this.getClass().getResource("/Images/search_user.png"));
    private ImageIcon icon_search_clicked = new ImageIcon(this.getClass().getResource("/Images/search_user_clicked.png"));
    private REPOSButton btn_search = new REPOSButton(icon_search_normal, icon_search_clicked, Localization.getResourceBundle().getString("AdminView.btn_search.tooltip"));
    private ImageIcon icon_create_account_normal = new ImageIcon(this.getClass().getResource("/Images/add_account.png"));
    private ImageIcon icon_create_account_clicked = new ImageIcon(this.getClass().getResource("/Images/add_account_clicked.png"));
    private REPOSButton btn_create_account = new REPOSButton(icon_create_account_normal, icon_create_account_clicked, Localization.getResourceBundle().getString("AdminView.btn_create_account.tooltip"));
    private ImageIcon icon_delete_account_normal = new ImageIcon(this.getClass().getResource("/Images/delete_account.png"));
    private ImageIcon icon_delete_account_clicked = new ImageIcon(this.getClass().getResource("/Images/delete_account_clicked.png"));
    private REPOSButton btn_delete_account = new REPOSButton(icon_delete_account_normal, icon_delete_account_clicked, Localization.getResourceBundle().getString("AdminView.btn_delete_account.tooltip"));
    private ImageIcon icon_edit_account_normal = new ImageIcon(this.getClass().getResource("/Images/edit_account.png"));
    private ImageIcon icon_edit_account_clicked = new ImageIcon(this.getClass().getResource("/Images/edit_account_clicked.png"));
    private REPOSButton btn_update_account = new REPOSButton(icon_edit_account_normal, icon_edit_account_clicked, Localization.getResourceBundle().getString("AdminView.btn_update_account.tooltip"));
    private JCheckBox cb_admin = new JCheckBox(Localization.getResourceBundle().getString("AdminView.cb_admin"));
    private JCheckBox cb_manager = new JCheckBox(Localization.getResourceBundle().getString("AdminView.cb_manager"));
    private JCheckBox cb_cashier = new JCheckBox(Localization.getResourceBundle().getString("AdminView.cb_cashier"));
    private JList list_account_detail_left = new JList();
    private JList list_account_detail_right = new JList();
    private JPagingPanel pnl_list_account;
    private JScrollPane spane_west_account_detail_left = new JScrollPane(list_account_detail_left);
    private JScrollPane spane_west_account_detail_right = new JScrollPane(list_account_detail_right);
    private DeleteAccountController dac;  
    private TitledBorder border_west_search;
    private TitledBorder border_west_detail;
    private TitledBorder border_center_account;
    private DefaultListModel fields;   
    
    
    public AdminView(Restaurant r) {
        this.r = r;
        dac = new DeleteAccountController(r);

        // set window size
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        int screenWidth = (int) toolkit.getScreenSize().getWidth();
        int screenHeight = (int) toolkit.getScreenSize().getHeight();
        this.setSize(screenWidth * 9 / 10, screenHeight * 8 / 10);
        this.setLayout(new BorderLayout());

        pnl_west.setPreferredSize(new Dimension(500, this.getHeight() * 9 / 10));
        pnl_center.setPreferredSize(new Dimension(this.getWidth() - 500, this.getHeight() * 9 / 10));

        pnl_user_status = Application.av_cusp;

        this.add(pnl_user_status, BorderLayout.NORTH);
        this.add(pnl_west, BorderLayout.WEST);
        this.add(pnl_center, BorderLayout.CENTER);

        pnl_west_account_detail_view.setBackground(Color.WHITE);
        pnl_west_search.setBackground(Color.WHITE);
        spane_west_account_detail_left.setBackground(Color.WHITE);
        spane_west_account_detail_right.setBackground(Color.WHITE);
        pnl_center.setBackground(Color.WHITE);
        pnl_center_tool_bar.setBackground(Color.WHITE);
        pnl_center_account_grid_view.setBackground(Color.WHITE);
        cb_admin.setBackground(Color.WHITE);
        cb_manager.setBackground(Color.WHITE);
        cb_cashier.setBackground(Color.WHITE);

        txt_search = new JTextField();
        txt_search.setMaximumSize(new Dimension(50, 3));
        txt_search.setPreferredSize(new Dimension(50, 3));

        pnl_west_search_left.add(txt_search);
        pnl_west_search_left.add(pnl_west_search_filter);

        pnl_west_search_filter.add(cb_admin);
        pnl_west_search_filter.add(cb_manager);
        pnl_west_search_filter.add(cb_cashier);

        pnl_west_search.add(pnl_west_search_left, BorderLayout.CENTER);
        pnl_west_search.add(btn_search, BorderLayout.EAST);

        //search button
        border_west_search = new TitledBorder(Localization.getResourceBundle().getString("AdminView.border_west_search"));
        border_west_search.setTitleColor(Color.BLUE);
        pnl_west_search.setBorder(border_west_search);

        pnl_west.add(pnl_west_search, BorderLayout.NORTH);

        // detailed view of account
        border_west_detail = new TitledBorder(Localization.getResourceBundle().getString("AdminView.border_west_detail"));
        border_west_detail.setTitleColor(Color.BLUE);
        pnl_west_account_detail_view.setBorder(border_west_detail);

        spane_west_account_detail_left.setPreferredSize(new Dimension(150, this.getHeight() * 9 / 10));
        spane_west_account_detail_left.setBorder(null);

        spane_west_account_detail_right.setPreferredSize(new Dimension(350, this.getHeight() * 9 / 10));
        spane_west_account_detail_right.setBorder(null);

        pnl_west_account_detail_view.add(spane_west_account_detail_left, BorderLayout.WEST);
        pnl_west_account_detail_view.add(spane_west_account_detail_right, BorderLayout.CENTER);

        pnl_west.add(pnl_west_account_detail_view, BorderLayout.CENTER);

        //grid view of accounts
        border_center_account = new TitledBorder(Localization.getResourceBundle().getString("AdminView.border_center_account"));
        border_center_account.setTitleColor(Color.BLUE);
        pnl_center_account_grid_view.setBorder(border_center_account);

        pnl_center_tool_bar.add(btn_create_account);
        pnl_center_tool_bar.add(btn_update_account);
        pnl_center_tool_bar.add(btn_delete_account);

        pnl_center_account_grid_view.add(pnl_center_tool_bar, BorderLayout.NORTH);

        pnl_center.add(pnl_center_account_grid_view, BorderLayout.CENTER);

        fields = new DefaultListModel();
        fields.addElement(Localization.getResourceBundle().getString("AdminView.fields.Username"));
        fields.addElement(Localization.getResourceBundle().getString("AdminView.fields.Account_type"));
        fields.addElement(Localization.getResourceBundle().getString("AdminView.fields.Full_name"));
        fields.addElement(Localization.getResourceBundle().getString("AdminView.fields.Gender"));
        fields.addElement(Localization.getResourceBundle().getString("AdminView.fields.Social_security_number"));
        fields.addElement(Localization.getResourceBundle().getString("AdminView.fields.Date_of_birth"));
        fields.addElement(Localization.getResourceBundle().getString("AdminView.fields.Phone_number"));
        fields.addElement(Localization.getResourceBundle().getString("AdminView.fields.Email"));
        fields.addElement(Localization.getResourceBundle().getString("AdminView.fields.Address"));

        cb_admin.setSelected(true);
        cb_cashier.setSelected(true);
        cb_manager.setSelected(true);
        btn_update_account.setEnabled(false);
        btn_delete_account.setEnabled(false);

        list_account_detail_left.setModel(fields);

        btn_search.addActionListener(new SearchAccountController(r));
        btn_create_account.addActionListener(new CreateAccountController());
        btn_update_account.addActionListener(new EditAccountController(r));
        btn_delete_account.addActionListener(dac);
        
        txt_search.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "search");
        txt_search.getActionMap().put("search", new SearchAccountController(r));
        cb_admin.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "search");
        cb_admin.getActionMap().put("search", new SearchAccountController(r));
        cb_cashier.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "search");
        cb_cashier.getActionMap().put("search", new SearchAccountController(r));
        cb_manager.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "search");
        cb_manager.getActionMap().put("search", new SearchAccountController(r));
    }

    public JList getList_account_detail_right() {
        return list_account_detail_right;
    }

    public JPanel getPnl_center_account_grid_view() {
        return pnl_center_account_grid_view;
    }

    public JCheckBox getCb_admin() {
        return cb_admin;
    }

    public JCheckBox getCb_manager() {
        return cb_manager;
    }

    public JCheckBox getCb_cashier() {
        return cb_cashier;
    }

    public JTextField getTxt_search() {
        return txt_search;
    }

    public REPOSButton getBtn_search() {
        return btn_search;
    }

    public REPOSButton getBtn_delete_account() {
        return btn_delete_account;
    }

    public REPOSButton getBtn_update_account() {
        return btn_update_account;
    }

    public DeleteAccountController getDac() {
        return dac;
    }

    public CurrentUserStatusPanel getPnl_user_status() {
        return pnl_user_status;
    }

    public void setPnl_user_status(CurrentUserStatusPanel pnl_user_status) {
        this.pnl_user_status = pnl_user_status;
    }
       
    public void setBtn_search_tooltip(String tooltip) {
        this.btn_search.setToolTipText(tooltip);
    }

    public void setBtn_create_account_tooltip(String tooltip) {
        this.btn_create_account.setToolTipText(tooltip);
    }

    public void setBtn_delete_account_tooltip(String tooltip) {
        this.btn_delete_account.setToolTipText(tooltip);
    }

    public void setBtn_update_account_tooltip(String tooltip) {
        this.btn_update_account.setToolTipText(tooltip);
    }

    public void setCb_admin(String text) {
        this.cb_admin.setText(text);
    }

    public void setCb_manager(String text) {
        this.cb_manager.setText(text);
    }

    public void setCb_cashier(String text) {
        this.cb_cashier.setText(text);
    }

    public void addObs() {
        r.addObserver(this);
    }

    public JPagingPanel getPnl_list_account() {
        return pnl_list_account;
    }

    public void setPnl_list_account(JPagingPanel pnl_list_account) {
        this.pnl_list_account = pnl_list_account;
    }    
    
    public void setBorder_west_search(String title) {
        this.border_west_search.setTitle(title);
    }

    public void setBorder_west_detail(String title) {
        this.border_west_detail.setTitle(title);
    }

    public void setBorder_center_account(String title) {
        this.border_center_account.setTitle(title);
    }

    public void setFields(String username, String account_type, String fullname, String gender, String social_security_number, String date_of_birth, String phone_number, String email, String address) {
        this.fields.setElementAt(username, 0);
        this.fields.setElementAt(account_type, 1);
        this.fields.setElementAt(fullname,2);
        this.fields.setElementAt(gender, 3);
        this.fields.setElementAt(social_security_number, 4);
        this.fields.setElementAt(date_of_birth, 5);
        this.fields.setElementAt(phone_number, 6);
        this.fields.setElementAt(email, 7);
        this.fields.setElementAt(address, 8);
    }
    
    public void reload() {
//        String username = Localization.getResourceBundle().getString("AdminView.column_names.Username");
//        String fullname = Localization.getResourceBundle().getString("AdminView.column_names.FullName");
//        String gender = Localization.getResourceBundle().getString("AdminView.column_names.Gender");
//        String age = Localization.getResourceBundle().getString("AdminView.column_names.Age");
//        String account_type = Localization.getResourceBundle().getString("AdminView.column_names.Account_type");
//        String[] column_names = {username, fullname, gender, age, account_type};
//        String[] column_names = {"Username", "FullName", "Gender", "Age", "Account type"};
        String[] column_names = {Localization.getResourceBundle().getString("AdminView.column_names.Username"), Localization.getResourceBundle().getString("AdminView.column_names.FullName"), Localization.getResourceBundle().getString("AdminView.column_names.Gender"), Localization.getResourceBundle().getString("AdminView.column_names.Age"), Localization.getResourceBundle().getString("AdminView.column_names.Account_type")};
        ArrayList<String[]> data = r.getAccountList(null, true, true, true);
        DefaultTableModel list_account_table_model = new DefaultTableModel(null, column_names) {
            @Override
            public Class<?> getColumnClass(int column) {
                return (column == 0) ? Integer.class : Object.class;
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        
        for (int i = 0; i < data.size(); i++) {
            list_account_table_model.addRow(data.get(i));
        }
        
        if(pnl_list_account != null ) {
            pnl_center_account_grid_view.remove(pnl_list_account);
        }
        pnl_list_account = new JPagingPanel(list_account_table_model);        

        pnl_list_account.getTable().setModel(list_account_table_model);
        pnl_list_account.getTable().setCellSelectionEnabled(false);
        pnl_list_account.getTable().setRowSelectionAllowed(true);
        pnl_list_account.getTable().getSelectionModel().addListSelectionListener(new SelectAccountController(r));
        pnl_list_account.getTable().revalidate();

        pnl_center_account_grid_view.add(pnl_list_account, BorderLayout.CENTER);
        btn_delete_account.setEnabled(false);
        btn_update_account.setEnabled(false);
        
        pnl_list_account.addKeyListener(dac);

        validate();
    }

    @Override
    public void update(Observable o, Object o1) {
        reload();
    }
}