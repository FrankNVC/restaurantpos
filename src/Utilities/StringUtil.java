/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilities;

/**
 *
 * @author Frank NgVietCuong
 */
public class StringUtil {

    public static String TrimSpace(String s) {
        String note = s.trim();
        note = note.replaceAll("[ ]+", " ");
        return note;
    }

    public static boolean ValidateString(String s) {
        String tempStr[] = s.split(" ");
        for (int i = 0; i < tempStr.length; i++) {
            if (tempStr[i].length() > 15) {
                return false;
            }
        }
        return true;
    }

    public static String FinalStringNote(String s) {
        String finalNote = "";
        String row = "";
        String tempStr[];
        int rowSize = 0;
        tempStr = s.split(" ");

        for (int i = 0; i < tempStr.length; i++) {

            if (rowSize + tempStr[i].length() > 15) {
                finalNote += "<br/>";
                rowSize = 0;
            }
            finalNote += tempStr[i] + " ";
            rowSize += tempStr[i].length() + 1;
        }
        return finalNote;
    }
}
