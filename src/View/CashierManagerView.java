 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.CreateCategoryItemController;
import Controller.DeleteCategoryItemController;
import Controller.DeleteOrderItemController;
import Controller.DeleteTableController;
import Controller.EditCategoryItemController;
import Controller.EditOrderItemController;
import Controller.EditTableController;
import Controller.MergeTableController;
import Controller.PaymentController;
import Controller.PrintOrderController;
import Controller.RefreshOrderController;
import Controller.SelectOrderItemController;
import Controller.SetTableStatusController;
import Controller.SwitchTableController;
import Controller.UnmergeTableController;
import Controller.ViewOrderController;
import Main.Application;
import Model.Account.AccountType;
import Model.Item;
import Model.Order;
import Model.OrderItem;
import Model.Restaurant;
import Model.Table;
import Model.Table.TableShape;
import Model.Table.TableStatus;
import Utilities.Localization;
import Utilities.REPOSButton;
import Utilities.StringUtil;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.TableCellRenderer;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeSelectionModel;

/**
 *
 * @author Pham
 */
public class CashierManagerView extends JPanel implements Observer {

    private boolean isMultiTable = false;
    private ArrayList<Table> current_tables = null;
    private ArrayList<Table> paying_tables = new ArrayList<>();
    private ArrayList<Cell> cells = new ArrayList<>();
    private final int map_width = 25;
    private final int map_height = 25;
    private Restaurant m;
    private CurrentUserStatusPanel pnl_user_status;
    private JPanel pnl_west = new JPanel(new BorderLayout());
    private JPanel pnl_west_top = new JPanel(new BorderLayout());
    private JPanel pnl_west_top_button = new JPanel(new GridLayout(1, 3));
    private JPanel pnl_west_top_filter = new JPanel(new GridLayout(1, 2));
    private JPanel pnl_center = new JPanel(new BorderLayout());
    private JPanel pnl_center_tool_bar = new JPanel(new BorderLayout());
    private JPanel pnl_center_table_crud = new JPanel(new FlowLayout(FlowLayout.CENTER));
    private JPanel pnl_center_table_dnd_remain = new JPanel(new BorderLayout());
    private JPanel pnl_center_table_dnd = new JPanel(new GridLayout(1, 6));
    private JPanel pnl_center_table_remain = new JPanel(new FlowLayout(FlowLayout.LEFT));
    private JPanel pnl_center_table_info = new JPanel(new BorderLayout());
    private JPanel pnl_center_table_info_top = new JPanel(new BorderLayout());
    private JPanel pnl_center_table_status = new JPanel(new GridLayout(1, 3));
    private JPanel pnl_center_map = new JPanel();
    private JPanel pnl_east = new JPanel(new BorderLayout());
    private JPanel pnl_east_top = new JPanel(new GridLayout(1, 4));
    private JPanel pnl_east_bottom = new JPanel(new BorderLayout());
    private JPanel pnl_east_bottom_total = new JPanel(new BorderLayout());
    private JPanel pnl_east_bottom_button = new JPanel(new GridLayout(1, 2));
    private JLabel lbl_total = new JLabel(Localization.getResourceBundle().getString("CashierManagerView.lbl_total"));
    private JLabel lbl_current_total = new JLabel();
    private ImageIcon icon_print = new ImageIcon(this.getClass().getResource("/Images/print.png"));
    private ImageIcon icon_print_clicked = new ImageIcon(this.getClass().getResource("/Images/print_clicked.png"));
    private REPOSButton btn_print = new REPOSButton(icon_print, icon_print_clicked, Localization.getResourceBundle().getString("CashierManagerView.btn_print"));
    private ImageIcon icon_payment = new ImageIcon(this.getClass().getResource("/Images/payment.png"));
    private ImageIcon icon_payment_clicked = new ImageIcon(this.getClass().getResource("/Images/payment_clicked.png"));
    private REPOSButton btn_payment = new REPOSButton(icon_payment, icon_payment_clicked, Localization.getResourceBundle().getString("CashierManagerView.btn_payment"));
    private ImageIcon icon_edit_order = new ImageIcon(this.getClass().getResource("/Images/edit.png"));
    private ImageIcon icon_edit_order_clicked = new ImageIcon(this.getClass().getResource("/Images/edit_clicked.png"));
    private REPOSButton btn_edit_order = new REPOSButton(icon_edit_order, icon_edit_order_clicked, Localization.getResourceBundle().getString("CashierManagerView.btn_edit_order"));
    private ImageIcon icon_add_new_order = new ImageIcon(this.getClass().getResource("/Images/reload.png"));
    private ImageIcon icon_add_new_order_clicked = new ImageIcon(this.getClass().getResource("/Images/reload_clicked.png"));
    private REPOSButton btn_refresh_order = new REPOSButton(icon_add_new_order, icon_add_new_order_clicked, Localization.getResourceBundle().getString("CashierManagerView.btn_refresh_order"));
    private ImageIcon icon_delete_order = new ImageIcon(this.getClass().getResource("/Images/delete.png"));
    private ImageIcon icon_delete_order_clicked = new ImageIcon(this.getClass().getResource("/Images/delete_clicked.png"));
    private REPOSButton btn_delete_order = new REPOSButton(icon_delete_order, icon_delete_order_clicked, Localization.getResourceBundle().getString("CashierManagerView.btn_delete_order"));
    private ImageIcon icon_view_order = new ImageIcon(this.getClass().getResource("/Images/view.png"));
    private ImageIcon icon_view_order_clicked = new ImageIcon(this.getClass().getResource("/Images/view_clicked.png"));
    private REPOSButton btn_view_order = new REPOSButton(icon_view_order, icon_view_order_clicked, Localization.getResourceBundle().getString("CashierManagerView.btn_view_order"));
    private ImageIcon icon_delete_table = new ImageIcon(this.getClass().getResource("/Images/delete1.png"));
    private ImageIcon icon_delete_table_clicked = new ImageIcon(this.getClass().getResource("/Images/delete1_clicked.png"));
    private REPOSButton btn_delete_table = new REPOSButton(icon_delete_table, icon_delete_table_clicked, Localization.getResourceBundle().getString("CashierManagerView.btn_delete_table"));
    private ImageIcon icon_merge = new ImageIcon(this.getClass().getResource("/Images/merge.png"));
    private ImageIcon icon_merge_clicked = new ImageIcon(this.getClass().getResource("/Images/merge_clicked.png"));
    private REPOSButton btn_merge = new REPOSButton(icon_merge, icon_merge_clicked, Localization.getResourceBundle().getString("CashierManagerView.btn_merge"));
    private ImageIcon icon_unmerge = new ImageIcon(this.getClass().getResource("/Images/unmerge.png"));
    private ImageIcon icon_unmerge_clicked = new ImageIcon(this.getClass().getResource("/Images/unmerge_clicked.png"));
    private REPOSButton btn_unmerge = new REPOSButton(icon_unmerge, icon_unmerge_clicked, Localization.getResourceBundle().getString("CashierManagerView.btn_unmerge"));
    private ImageIcon icon_switch = new ImageIcon(this.getClass().getResource("/Images/switch.png"));
    private ImageIcon icon_switch_clicked = new ImageIcon(this.getClass().getResource("/Images/switch_clicked.png"));
    private REPOSButton btn_switch = new REPOSButton(icon_switch, icon_switch_clicked, Localization.getResourceBundle().getString("CashierManagerView.btn_switch"));
    private JTextField txt_table_number = new JTextField();
    private ImageIcon icon_edit_table = new ImageIcon(this.getClass().getResource("/Images/edit.png"));
    private ImageIcon icon_edit_table_clicked = new ImageIcon(this.getClass().getResource("/Images/edit_clicked.png"));
    private REPOSButton btn_edit_table = new REPOSButton(icon_edit_table, icon_edit_table_clicked, Localization.getResourceBundle().getString("CashierManagerView.btn_edit_table"));
    private ButtonGroup group_table_status = new ButtonGroup();
    private ButtonGroup group_edit = new ButtonGroup();
    private JRadioButton rdb_vacant = new JRadioButton(Localization.getResourceBundle().getString("CashierManagerView.rdb_vacant"));
    private JRadioButton rdb_occupied = new JRadioButton(Localization.getResourceBundle().getString("CashierManagerView.rdb_occupied"));
    private JRadioButton rdb_reserved = new JRadioButton(Localization.getResourceBundle().getString("CashierManagerView.rdb_reserved"));
    private JRadioButton rdb_category = new JRadioButton(Localization.getResourceBundle().getString("CashierManagerView.rdb_category"));
    private JRadioButton rdb_item = new JRadioButton(Localization.getResourceBundle().getString("CashierManagerView.rdb_item"));
    private ImageIcon icon_create = new ImageIcon(this.getClass().getResource("/Images/add.png"));
    private ImageIcon icon_create_clicked = new ImageIcon(this.getClass().getResource("/Images/add_clicked.png"));
    private REPOSButton btn_create = new REPOSButton(icon_create, icon_create_clicked, Localization.getResourceBundle().getString("CashierManagerView.btn_create"));
    private ImageIcon icon_delete = new ImageIcon(this.getClass().getResource("/Images/delete.png"));
    private ImageIcon icon_delete_clicked = new ImageIcon(this.getClass().getResource("/Images/delete_clicked.png"));
    private REPOSButton btn_delete = new REPOSButton(icon_delete, icon_delete_clicked, Localization.getResourceBundle().getString("CashierManagerView.btn_delete"));
    private ImageIcon icon_edit_normal = new ImageIcon(this.getClass().getResource("/Images/edit.png"));
    private ImageIcon icon_edit_clicked = new ImageIcon(this.getClass().getResource("/Images/edit_clicked.png"));
    private REPOSButton btn_edit = new REPOSButton(icon_edit_normal, icon_edit_clicked, Localization.getResourceBundle().getString("CashierManagerView.btn_edit"));
    private ImageIcon icon_oval_table = new ImageIcon(this.getClass().getResource("/Images/oval_table.png"));
    private REPOSButton btn_oval_table = new REPOSButton(icon_oval_table, icon_oval_table, Localization.getResourceBundle().getString("CashierManagerView.btn_oval_table"));
    private ImageIcon icon_rectangle_table = new ImageIcon(this.getClass().getResource("/Images/rectangle_table.png"));
    private REPOSButton btn_rectangle_table = new REPOSButton(icon_rectangle_table, icon_rectangle_table, Localization.getResourceBundle().getString("CashierManagerView.btn_rectangle_table"));
    private JLabel lbl_remaining = new JLabel(Localization.getResourceBundle().getString("CashierManagerView.lbl_remaining"));
    private JLabel lbl_remaining_table = new JLabel();
    private JCheckBox cb_multi_table = new JCheckBox(Localization.getResourceBundle().getString("CashierManagerView.cb_multi_table"));
    private JTable table_order_detail = new JTable();
    private JScrollPane spane_order_detail = new JScrollPane(table_order_detail);
    private JScrollPane spane_tree;
//    private DefaultMutableTreeNode top = new DefaultMutableTreeNode(Localization.getResourceBundle().getString("CashierManagerView.top"));
    private DefaultMutableTreeNode top = new DefaultMutableTreeNode("Menu");
    private JTree tree = new JTree(top);
    private DeleteCategoryItemController dcic;
    private SetTableStatusController stsc;
    private DeleteOrderItemController doic;
    //////////////////////////////////
    TitledBorder border_west;
    TitledBorder border_center_map;
    TitledBorder border_east;

    /**
     *
     * @param m
     */
    public CashierManagerView(Restaurant r) {
        current_tables = new ArrayList<>();
        cells = new ArrayList<>();
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        int screenWidth = (int) toolkit.getScreenSize().getWidth();
        int screenHeight = (int) toolkit.getScreenSize().getHeight();

        this.m = r;
        dcic = new DeleteCategoryItemController(r);
        stsc = new SetTableStatusController(r);
        doic = new DeleteOrderItemController(r);

        this.updateRestaurantMap(map_width, map_height, pnl_center_map);

        this.setSize(screenWidth * 9 / 10, screenHeight * 8 / 10);
        this.setLayout(new BorderLayout());
        pnl_center_table_remain = new JPanel(new FlowLayout(FlowLayout.CENTER));
        pnl_west.setPreferredSize(new Dimension(this.getWidth() * 2 / 10, this.getHeight() * 9 / 10));
        pnl_center.setPreferredSize(new Dimension(this.getWidth() * 5 / 10, this.getHeight() * 9 / 10));
        pnl_east.setPreferredSize(new Dimension(this.getWidth() * 3 / 10, this.getHeight() * 9 / 10));

        pnl_user_status = Application.cmv_cusp;

        this.add(pnl_user_status, BorderLayout.NORTH);

        this.add(pnl_west, BorderLayout.WEST);

        this.add(pnl_center, BorderLayout.CENTER);

        this.add(pnl_east, BorderLayout.EAST);

        pnl_west.setBackground(Color.WHITE);
        pnl_center.setBackground(Color.WHITE);
        pnl_west_top.setBackground(Color.WHITE);
        pnl_west_top_button.setBackground(Color.WHITE);
        pnl_center_map.setBackground(Color.WHITE);
        pnl_center_tool_bar.setBackground(Color.WHITE);
        pnl_center_table_crud.setBackground(Color.WHITE);
        pnl_center_table_dnd.setBackground(Color.WHITE);
        pnl_center_table_remain.setBackground(Color.WHITE);
        btn_delete_table.setBackground(Color.WHITE);
        pnl_center_table_info.setBackground(Color.WHITE);
        pnl_center_table_info_top.setBackground(Color.WHITE);
        pnl_center_table_status.setBackground(Color.WHITE);
        pnl_east.setBackground(Color.WHITE);
        pnl_east_top.setBackground(Color.WHITE);
        pnl_east_bottom_total.setBackground(Color.WHITE);
        pnl_east_bottom_button.setBackground(Color.WHITE);
        spane_order_detail.setBackground(Color.WHITE);
        rdb_vacant.setBackground(Color.WHITE);
        rdb_occupied.setBackground(Color.WHITE);
        rdb_reserved.setBackground(Color.WHITE);
        rdb_category.setBackground(Color.WHITE);
        rdb_item.setBackground(Color.WHITE);

        rdb_vacant.setFont(new Font("Calibri", Font.BOLD, 14));
        rdb_occupied.setFont(new Font("Calibri", Font.BOLD, 14));
        rdb_reserved.setFont(new Font("Calibri", Font.BOLD, 14));
        rdb_vacant.setForeground(Color.GREEN);
        rdb_occupied.setForeground(Color.ORANGE);
        rdb_reserved.setForeground(Color.RED);

        //West area
        pnl_west_top.setPreferredSize(new Dimension(this.getWidth() * 2 / 10, 81));
        pnl_west_top_filter.setPreferredSize(new Dimension(this.getWidth() * 2 / 10, 24));

        border_west = new TitledBorder(Localization.getResourceBundle().getString("CashierManagerView.border_west"));

        border_west.setTitleColor(Color.BLUE);

        pnl_west.setBorder(border_west);

        pnl_west_top_button.add(btn_create);

        pnl_west_top_button.add(btn_edit);

        pnl_west_top_button.add(btn_delete);

        pnl_west_top_filter.add(rdb_category);

        pnl_west_top_filter.add(rdb_item);

        group_edit.add(rdb_category);

        group_edit.add(rdb_item);

        pnl_west_top.add(pnl_west_top_button, BorderLayout.CENTER);

        pnl_west_top.add(pnl_west_top_filter, BorderLayout.SOUTH);

        pnl_west.add(pnl_west_top, BorderLayout.NORTH);

        updateMenuList(top);

        pnl_center_table_dnd_remain.setPreferredSize(new Dimension(this.getWidth() * 25 / 100, 81));
        pnl_center_table_dnd_remain.setMaximumSize(new Dimension(this.getWidth() * 25 / 100, 81));

        pnl_center_table_info.setPreferredSize(new Dimension(this.getWidth() * 21 / 100, 81));
        pnl_center_table_info.setMaximumSize(new Dimension(this.getWidth() * 21 / 100, 81));

        pnl_center_table_status.setPreferredSize(new Dimension(this.getWidth() * 21 / 100, 40));
        pnl_center_table_status.setMaximumSize(new Dimension(this.getWidth() * 21 / 100, 40));

        pnl_center_table_dnd.add(btn_oval_table);
        pnl_center_table_dnd.add(btn_rectangle_table);
        pnl_center_table_dnd.add(btn_delete_table);
        pnl_center_table_dnd.add(btn_merge);
        pnl_center_table_dnd.add(btn_unmerge);
        pnl_center_table_dnd.add(btn_switch);

        pnl_center_table_remain.add(lbl_remaining);

        lbl_remaining_table.setText(Integer.toString(20 - r.getTables().size()));
        pnl_center_table_remain.add(lbl_remaining_table);

        pnl_center_table_remain.add(cb_multi_table);

        pnl_center_table_dnd_remain.add(pnl_center_table_dnd, BorderLayout.CENTER);

        pnl_center_table_dnd_remain.add(pnl_center_table_remain, BorderLayout.SOUTH);

        pnl_center_table_crud.add(pnl_center_table_dnd_remain);

        pnl_center_tool_bar.add(pnl_center_table_crud, BorderLayout.WEST);

        pnl_center_tool_bar.add(pnl_center_table_info, BorderLayout.EAST);

        pnl_center_table_status.add(rdb_vacant);

        pnl_center_table_status.add(rdb_occupied);

        pnl_center_table_status.add(rdb_reserved);

        group_table_status.add(rdb_vacant);

        group_table_status.add(rdb_occupied);

        group_table_status.add(rdb_reserved);

        txt_table_number.setFont(new Font("Arial", Font.BOLD, 40));
        txt_table_number.setPreferredSize(new Dimension(30, 30));
        txt_table_number.setMaximumSize(new Dimension(30, 30));

        btn_edit_table.setPreferredSize(new Dimension(70, 30));
        btn_edit_table.setMaximumSize(new Dimension(70, 30));

        pnl_center_table_info_top.add(txt_table_number, BorderLayout.CENTER);

        pnl_center_table_info_top.add(btn_edit_table, BorderLayout.EAST);

        pnl_center_table_info.add(pnl_center_table_status, BorderLayout.SOUTH);

        pnl_center_table_info.add(pnl_center_table_info_top, BorderLayout.CENTER);

        pnl_center.add(pnl_center_tool_bar, BorderLayout.NORTH);

        //Restaurant map panel, to put tables on
        border_center_map = new TitledBorder(Localization.getResourceBundle().getString("CashierManagerView.border_center_map"));

        border_center_map.setTitleColor(Color.BLUE);

        pnl_center.setBorder(border_center_map);

        pnl_center.add(pnl_center_map, BorderLayout.CENTER);

        //Order detail
        pnl_east_top.add(btn_refresh_order);

        pnl_east_top.add(btn_edit_order);

        pnl_east_top.add(btn_delete_order);

        pnl_east_top.add(btn_view_order);

        pnl_east_bottom_total.add(lbl_total, BorderLayout.WEST);

        pnl_east_bottom_total.add(lbl_current_total, BorderLayout.EAST);

        pnl_east_bottom_button.add(btn_payment);

        pnl_east_bottom_button.add(btn_print);

        pnl_east_bottom.add(pnl_east_bottom_total, BorderLayout.CENTER);

        pnl_east_bottom.add(pnl_east_bottom_button, BorderLayout.SOUTH);

        pnl_east.add(pnl_east_top, BorderLayout.NORTH);

        pnl_east.add(spane_order_detail, BorderLayout.CENTER);

        pnl_east.add(pnl_east_bottom, BorderLayout.SOUTH);
        border_east = new TitledBorder(Localization.getResourceBundle().getString("CashierManagerView.border_east"));

        border_east.setTitleColor(Color.BLUE);

        pnl_east.setBorder(border_east);

        rdb_vacant.setEnabled(false);
        rdb_occupied.setEnabled(false);
        rdb_reserved.setEnabled(false);
        btn_delete_table.setEnabled(false);
        rdb_category.setSelected(true);

        if (r.getMenu().getCategories().isEmpty()) {
            rdb_item.setEnabled(false);
        }

        pnl_center_map.setFocusable(true);
        pnl_center_map.requestFocus();
        pnl_center_map.getInputMap().put(KeyStroke.getKeyStroke("DELETE"), "delete table");
        pnl_center_map.getActionMap().put("delete table", new DeleteTableController(r));
        for (int i = 0; i < cells.size(); i++) {
            cells.get(i).getInputMap().put(KeyStroke.getKeyStroke("DELETE"), "delete table");
            cells.get(i).getActionMap().put("delete table", new DeleteTableController(r));
        }

        btn_print.addActionListener(new PrintOrderController());
        btn_create.addActionListener(new CreateCategoryItemController());
        btn_delete.addActionListener(dcic);
        btn_edit.addActionListener(new EditCategoryItemController(r));
        btn_delete_table.addActionListener(new DeleteTableController(r));
        btn_edit_table.addActionListener(new EditTableController(r));
        btn_merge.addActionListener(new MergeTableController(r));
        btn_unmerge.addActionListener(new UnmergeTableController(r));
        btn_switch.addActionListener(new SwitchTableController(r));
        rdb_vacant.addActionListener(stsc);
        rdb_occupied.addActionListener(stsc);
        rdb_reserved.addActionListener(stsc);
        btn_refresh_order.addActionListener(new RefreshOrderController(r));
        btn_delete_order.addActionListener(new DeleteOrderItemController(r));
        btn_edit_order.addActionListener(new EditOrderItemController(r));
        btn_view_order.addActionListener(new ViewOrderController());
        btn_payment.addActionListener(new PaymentController(r));

        cb_multi_table.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent ie) {
                deselectAllTable();
            }
        });

        ButtonTransferHandler transferhandler_oval_table = new ButtonTransferHandler();

        btn_oval_table.setName("Oval table");
        btn_oval_table.setTransferHandler(transferhandler_oval_table);
        btn_oval_table.addMouseListener(new ButtonDragMouseAdapter());

        ButtonTransferHandler transferhandler_rectangle_table = new ButtonTransferHandler();

        btn_rectangle_table.setName("Rectangle table");
        btn_rectangle_table.setTransferHandler(transferhandler_rectangle_table);
        btn_rectangle_table.addMouseListener(new ButtonDragMouseAdapter());

        txt_table_number.setTransferHandler(null);
    }

    public ArrayList<Table> getPaying_tables() {
        return paying_tables;
    }

    public ArrayList<Table> getCurrent_tables() {
        return current_tables;
    }

    public ArrayList<Cell> getCells() {
        return cells;
    }

    public JTree getTree() {
        return tree;
    }

    public JTable getTable_order_detail() {
        return table_order_detail;
    }

    public void setTable_order_detail(JTable table_order_detail) {
        this.table_order_detail = table_order_detail;
    }

    public JPanel getPnl_east() {
        return pnl_east;
    }

    public JScrollPane getSpane_order_detail() {
        return spane_order_detail;
    }

    public void setSpane_order_detail(JScrollPane spane_order_detail) {
        this.spane_order_detail = spane_order_detail;
    }

    public JRadioButton getRdb_category() {
        return rdb_category;
    }

    public JRadioButton getRdb_vacant() {
        return rdb_vacant;
    }

    public JRadioButton getRdb_occupied() {
        return rdb_occupied;
    }

    public JRadioButton getRdb_reserved() {
        return rdb_reserved;
    }

    public JTextField getTxt_table_number() {
        return txt_table_number;
    }

    public REPOSButton getBtn_delete() {
        return btn_delete;
    }

    public REPOSButton getBtn_delete_table() {
        return btn_delete_table;
    }

    public REPOSButton getBtn_edit_order() {
        return btn_edit_order;
    }

    public REPOSButton getBtn_delete_order() {
        return btn_delete_order;
    }

    public JRadioButton getRdb_item() {
        return rdb_item;
    }

    public CurrentUserStatusPanel getPnl_user_status() {
        return pnl_user_status;
    }

    public DefaultMutableTreeNode getTop() {
        return top;
    }

    public void addObs(Restaurant r) {
        r.addObserver(this);
    }

    public void setLbl_total(String text) {
        this.lbl_total.setText(text);
    }

    public void setBtn_print(String tooltip) {
        this.btn_print.setToolTipText(tooltip);
    }

    public void setBtn_payment(String tooltip) {
        this.btn_payment.setToolTipText(tooltip);
    }

    public void setBtn_edit_order(String tooltip) {
        this.btn_edit_order.setToolTipText(tooltip);
    }

    public void setBtn_refresh_order(String tooltip) {
        this.btn_refresh_order.setToolTipText(tooltip);
    }

    public void setBtn_delete_order(String tooltip) {
        this.btn_delete_order.setToolTipText(tooltip);
    }

    public void setBtn_view_order(String tooltip) {
        this.btn_view_order.setToolTipText(tooltip);
    }

    public void setBtn_delete_table(String tooltip) {
        this.btn_delete_table.setToolTipText(tooltip);
    }

    public void setBtn_merge(String tooltip) {
        this.btn_merge.setToolTipText(tooltip);
    }

    public void setBtn_unmerge(String tooltip) {
        this.btn_unmerge.setToolTipText(tooltip);
    }

    public void setBtn_switch(String tooltip) {
        this.btn_switch.setToolTipText(tooltip);
    }

    public void setBtn_edit_table(String tooltip) {
        this.btn_edit_table.setToolTipText(tooltip);
    }

    public void setRdb_vacant(String text) {
        this.rdb_vacant.setText(text);
    }

    public void setRdb_occupied(String text) {
        this.rdb_occupied.setText(text);
    }

    public void setRdb_reserved(String text) {
        this.rdb_reserved.setText(text);
    }

    public void setRdb_category(String text) {
        this.rdb_category.setText(text);
    }

    public void setRdb_item(String text) {
        this.rdb_item.setText(text);
    }

    public void setBtn_create(String tooltip) {
        this.btn_create.setToolTipText(tooltip);
    }

    public void setBtn_delete(String tooltip) {
        this.btn_delete.setToolTipText(tooltip);
    }

    public void setBtn_edit(String tooltip) {
        this.btn_edit.setToolTipText(tooltip);
    }

    public void setBtn_oval_table(String tooltip) {
        this.btn_oval_table.setToolTipText(tooltip);
    }

    public void setBtn_rectangle_table(String tooltip) {
        this.btn_rectangle_table.setToolTipText(tooltip);
    }

    public void setLbl_remaining(String text) {
        this.lbl_remaining.setText(text);
    }

    public void setCb_multi_table(String text) {
        this.cb_multi_table.setText(text);
    }

//    public void setTop(String text) {
//        this.top = top;
//    }
    public void setBorder_west(String title) {
        this.border_west.setTitle(title);
    }

    public void setBorder_center_map(String title) {
        this.border_center_map.setTitle(title);
    }

    public void setBorder_east(String title) {
        this.border_east.setTitle(title);
    }

    /**
     * reload() method is used to reload the CashierManagerView when user logins
     * as cashier or manager. reload() method reloads category and item tree
     * list reload() method reloads restaurant map reload() method reloads order
     * item list
     */
    public void reload() {
        this.updateMenuList(top);

        this.updateRestaurantMap(map_width, map_height, pnl_center_map);

        lbl_remaining_table.setText(Integer.toString(20 - m.getTables().size()));

        updateTotal();

        if (Application.currentUser.getType() == AccountType.CASHIER) {
            pnl_west_top.setVisible(false);
        } else {
            pnl_west_top.setVisible(true);
        }

        deselectAllTable();

        btn_print.setEnabled(false);
        btn_payment.setEnabled(false);
        btn_refresh_order.setEnabled(false);
        btn_delete_order.setEnabled(false);
        btn_edit_order.setEnabled(false);
        btn_view_order.setEnabled(false);

        txt_table_number.setEnabled(false);
        btn_edit_table.setEnabled(false);

        rdb_category.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (tree.getSelectionPath() != null) {
                    btn_delete.setEnabled(true);
                    btn_edit.setEnabled(true);
                }
            }
        });

        if (m.getMenu().getCategories().isEmpty()) {
            rdb_item.setEnabled(false);
        } else {
            rdb_item.setEnabled(true);
            rdb_item.addItemListener(new ItemListener() {

                @Override
                public void itemStateChanged(ItemEvent e) {
                    btn_delete.setEnabled(true);
                    btn_edit.setEnabled(true);
                    if (rdb_item.isSelected()) {
                        if (tree.getSelectionPath() != null) {
                            if (tree.getSelectionPath().getPathCount() < 3) {
                                btn_delete.setEnabled(false);
                                btn_edit.setEnabled(false);
                            }
                        }
                    }
                }
            });
        }
    }

    public void updateTotal() {
        int total = 0;
        if (!current_tables.isEmpty()) {
            Table current_table = current_tables.get(0);
            for (int i = 0; i < current_table.getO().getOrderItems().size(); i++) {
                total += current_table.getO().getOrderItems().get(i).getQuantity() * current_table.getO().getOrderItems().get(i).getItem().getItemPrice();
            }
        } else {
            total = 0;
        }
        lbl_current_total.setText(Localization.getResourceBundle().getString("CashierManagerView.vnd") + total);

        String total_money = new DecimalFormat("###,###,###").format(total) + Localization.getResourceBundle().getString("CashierManagerView.vnd");
        if (total_money.contains(",")) {
            total_money = total_money.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
        } else {
            total_money = total_money.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
        }

        lbl_current_total.setText(total_money);

    }

    public void updateMenuList(DefaultMutableTreeNode top) {
        top.removeAllChildren();

        btn_edit.setEnabled(false);
        btn_delete.setEnabled(false);

        String category, item;
        DefaultMutableTreeNode cate_node, item_node;
        if (m.getMenu().getCategories().size() > 0) {
            rdb_item.setEnabled(true);
        } else {
            rdb_item.setEnabled(false);
        }

        for (int i = 0; i < m.getMenu().getCategories().size(); i++) {
            category = m.getMenu().getCategories().get(i);
            cate_node = new DefaultMutableTreeNode(category);
            top.add(cate_node);

            for (int j = 0; j < m.getMenu().getItems().size(); j++) {
                item = m.getMenu().getItems().get(j).getItemName() + "  -  " + new DecimalFormat("###,###,###").format(m.getMenu().getItems().get(j).getItemPrice()) + Localization.getResourceBundle().getString("CashierManagerView.vnd");
                if (item.contains(",")) {
                    item = item.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                } else {
                    item = item.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                }

                if (m.getMenu().getItems().get(j).getItemCategory().equals(category)) {
                    item_node = new DefaultMutableTreeNode(item);
                    cate_node.add(item_node);
                }
            }
        }

        tree = new JTree(top);
        tree.setFont(new Font("Calibri", Font.PLAIN, 16));
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.DISCONTIGUOUS_TREE_SELECTION);
        tree.getSelectionModel().addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent e) {
                if (tree.getSelectionPath() == null) {
                    btn_edit.setEnabled(false);
                    btn_delete.setEnabled(false);
                } else {
                    btn_edit.setEnabled(true);
                    btn_delete.setEnabled(true);
                    rdb_category.setEnabled(true);
                    rdb_item.setEnabled(true);
                    if (tree.getSelectionPath().getPathCount() < 3) {
                        rdb_category.setSelected(true);
                    } else {
                        rdb_item.setEnabled(true);
                        rdb_item.setSelected(true);
                    }
                    if (tree.getSelectionPaths().length > 1) {
                        btn_edit.setEnabled(false);
                    }
                }
            }
        });
        tree.setDragEnabled(true);
        tree.setTransferHandler(new CategoryItemTransferHandler());
        tree.setRootVisible(false);
        tree.addKeyListener(dcic);

        if (spane_tree != null) {
            pnl_west.remove(spane_tree);
        }
        spane_tree = new JScrollPane(tree);

        pnl_west.add(spane_tree, BorderLayout.CENTER);
        revalidate();
    }

    /**
     * updateRestaurantMap() reload restaurant map when CashierManagerView has
     * reloaded or restaurant map is modified.
     *
     * @param width is the width of the restaurant map
     * @param height is the height of the restaurant map
     * @param panel the JPanel that contain the restaurant map
     */
    private void updateRestaurantMap(int width, int height, JPanel panel) {

        panel.setBorder(BorderFactory.createLineBorder(Color.GRAY, 3));

        panel.removeAll();
        cells.clear();

        panel.setLayout(new GridLayout(width, height));
        int size = height * width;
        int x_axis = 0;
        int y_axis = 0;

        for (int i = 0; i < size; i++) {
            if (x_axis == width) {
                x_axis = 0;
                y_axis++;
            }
            Cell temp = new Cell(i, x_axis, y_axis);
            x_axis++;
            temp.setTransferHandler(new EmptyCellTransferHandler(TransferHandler.COPY));
            if (temp.getMouseListeners().length >= 1) {
                temp.removeMouseListener(temp.getMouseListeners()[0]);
            }
            temp.addMouseListener(new EmptyCellMouseAdapter());
            temp.setFocusable(true);
            temp.setFocusTraversalKeysEnabled(false);
            temp.addKeyListener(new CMVKeyListener());
            cells.add(temp);
            panel.add(temp);
        }

        for (int k = 0; k < m.getTables().size(); k++) {
            Table table = m.getTables().get(k);
            if (table.getTableGroupId() == -1) {
                int table_height = 3;
                int table_width = 3;
                int cell_id = m.getTables().get(k).getCell_root_id();
                int table_part = 1;

                for (int i = 0; i < table_height; i++) {

                    for (int j = 0; j < table_width; j++) {

                        cells.get(cell_id + map_width * i + j).used(table, table_part);
                        table_part++;
                        cells.get(cell_id + map_width * i + j).setTransferHandler(new UsedCellTransferHandler());
                        cells.get(cell_id + map_width * i + j).removeMouseListener(cells.get(cell_id + map_width * i + j).getMouseListeners()[0]);
                        cells.get(cell_id + map_width * i + j).addMouseListener(new UsedCellMouseAdapter());
                    }
                }
            } else {
                if (table.getTable_group_root_id() != -1) {
//                    int table_height = 6;
//                    int table_width = 6;
//                    int cell_id = m.getTables().get(k).getTable_group_root_id();

                    ArrayList<Table> merge_tables = new ArrayList<>();

                    for (int i = 0; i < m.getTables().size(); i++) {
                        if (m.getTables().get(i).getTableGroupId() == table.getTableGroupId()) {
                            merge_tables.add(m.getTables().get(i));
                        }
                    }

                    int count = 0;
                    int table_part = 1;
                    for (int j = 0; j < 6; j++) {

                        for (int i = 0; i < 6; i++) {

                            if (count < merge_tables.size()) {
                                boolean table_number_shown = false;
                                for (int x = 0; x < 4; ++x) {
                                    for (int y = 0; y < 4; ++y) {
                                        if (table_part == 8 + x * 6 + y) {
                                            table_number_shown = true;
                                            cells.get(table.getTable_group_root_id() + 25 * j + i).grouped(table, table_part, merge_tables.get(count).getTableNumber());
                                            count++;
                                            break;
                                        }

                                    }
                                    if (table_number_shown) {
                                        break;
                                    }
                                }
                                if (!table_number_shown) {
                                    cells.get(table.getTable_group_root_id() + 25 * j + i).grouped(table, table_part, -1);
                                }

                                table_part++;
                            } else {
                                cells.get(table.getTable_group_root_id() + 25 * j + i).grouped(table, table_part, -1);
                                table_part++;
                            }
                            cells.get(table.getTable_group_root_id() + 25 * j + i).removeMouseListener(cells.get(table.getTable_group_root_id() + 25 * j + i).getMouseListeners()[0]);
                            cells.get(table.getTable_group_root_id() + 25 * j + i).addMouseListener(new UsedCellMouseAdapter());
                            cells.get(table.getTable_group_root_id() + 25 * j + i).setTransferHandler(new UsedCellTransferHandler());

                        }
                    }
                }

            }

        }
        this.revalidate();
    }

    /**
     * setRectangle(double[] rectangle, Cell targetedCell, Cell currentCell)
     * specifies a square around the selected cell
     *
     * @param rectangle is the array containing information of the squares
     * @param targetedCell is the cell that is selected
     * @param currentCell is the current cell
     */
    private void setRectangle(double[] rectangle, Cell targetedCell, Cell currentCell) {
        int half_width = 0;
        for (int i = 3; i <= map_width - 2; ++i) {
            if (i * i >= rectangle[4]) {
                half_width = i;
                break;
            }
        }
        rectangle[0] = targetedCell.getColumn() - half_width;
        if (targetedCell.getColumn() + half_width > map_width - 2) {
            rectangle[2] = half_width + map_width - 2 - targetedCell.getColumn();
        } else if (targetedCell.getColumn() - half_width < 0) {
            rectangle[2] = half_width + targetedCell.getColumn();
            rectangle[0] = 0;
        } else {
            rectangle[2] = half_width * 2;
        }
        if (targetedCell.getRow() + half_width > map_height - 2) {
            rectangle[1] = map_height - 2 - currentCell.getRow();
        } else {
            rectangle[1] = half_width + targetedCell.getRow();
        }
    }

    /**
     * returns the ID of the available cell of adding a table according to the
     * id of the input cell
     *
     * @param cell_id is the id of the input cell
     */
    private int findTableLocation(int cell_id) {
        int result = -1;
        int table_height = 3;
        int table_width = 3;
        double[] rectangle = new double[5];
        rectangle[0] = 0;
        rectangle[1] = 0;
        rectangle[2] = map_width - 2;
        rectangle[3] = map_height - 2;
        rectangle[4] = map_width * map_width + map_height * map_height;
        boolean valid;
        for (int i = 0; i < cells.size(); i++) {
            if (rectangle[4] == 4) {
                break;
            }

            if (rectangle[0] <= cells.get(i).getColumn() && cells.get(i).getColumn() < rectangle[0] + rectangle[2] && cells.get(i).getRow() <= rectangle[1]) {
                valid = true;
                for (int j = 0; j < table_height; j++) {
                    for (int k = 0; k < table_width; k++) {
                        if (cells.get(i + j * map_width + k).getTable() != null) {
                            valid = false;
                            break;
                        }
                    }
                    if (!valid) {
                        //i += 2;
                        break;
                    }
                }

                if (valid) {
                    if (result == -1) {
                        result = i;
                        rectangle[4] = Math.pow(cells.get(cell_id).getColumn() - cells.get(i + map_width + 1).getColumn(), 2) + Math.pow(cells.get(cell_id).getRow() - cells.get(i + map_width + 1).getRow(), 2);
                        setRectangle(rectangle, cells.get(cell_id), cells.get(result));
                    } else {
                        double new_distance = Math.pow(cells.get(cell_id).getColumn() - cells.get(i + map_width + 1).getColumn(), 2) + Math.pow(cells.get(cell_id).getRow() - cells.get(i + map_width + 1).getRow(), 2);
                        if (rectangle[4] > new_distance) {
                            rectangle[4] = new_distance;
                            result = i;
                            setRectangle(rectangle, cells.get(cell_id), cells.get(result));
                        }
                    }
                }
            }

            if (cells.get(i).getColumn() == rectangle[2] - 2 && cells.get(i).getRow() == rectangle[3] - 2) {
                break;
            }
        }
        return result;
    }

    /**
     * clear the image of the table on the restaurant map
     *
     * @param delete_table is the table to be deleted
     */
    public void deleteTable(Table delete_table) {
        if (delete_table.getTableGroupId() == -1) {
            for (int i = 0; i < cells.size(); i++) {
                if (cells.get(i).getTable() != null) {
                    if (cells.get(i).getTable().getTableNumber() == delete_table.getTableNumber()) {
                        cells.get(i).setTransferHandler(new EmptyCellTransferHandler(TransferHandler.COPY));
                        cells.get(i).removeMouseListener(cells.get(i).getMouseListeners()[0]);
                        cells.get(i).addMouseListener(new EmptyCellMouseAdapter());
                        cells.get(i).free();
                    }
                }

            }
        } else {
            for (int i = 0; i < cells.size(); i++) {
                if (cells.get(i).getTable() != null) {
                    if (cells.get(i).getTable().getTableGroupId() == delete_table.getTableGroupId()) {
                        cells.get(i).setTransferHandler(new EmptyCellTransferHandler(TransferHandler.COPY));
                        cells.get(i).removeMouseListener(cells.get(i).getMouseListeners()[0]);
                        cells.get(i).addMouseListener(new EmptyCellMouseAdapter());
                        cells.get(i).free();
                    }
                }

            }
        }

        lbl_remaining_table.setText(Integer.toString(20 - m.getTables().size()));
        deselectAllTable();
        this.revalidate();
    }

    /**
     * addTable() is used to update the restaurant map when a table is added to
     * the map
     *
     * @param table is the table to be added
     * @param table_width is the width of the added table
     * @param table_height is the height of the added table
     * @param cell_id is the ID of the cell where the table is added
     */
    public void addTable(Table table, int table_width, int table_height, int cell_id) {

        if (cells.get(cell_id).getColumn() + table_width > map_width || cells.get(cell_id).getRow() + table_height > map_height) {
            cell_id = findTableLocation(cell_id);
            if (cell_id == -1) {
                m.deleteTable(table);
                JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("CashierManagerView.addTable.locationOutOfMap"));
                return;
            }
        } else {
            for (int i = 0; i < table_height; i++) {
                boolean valid = true;
                for (int j = 0; j < table_width; j++) {
                    if ((cells.get(cell_id + map_width * i + j).getTable() != null)) {
                        valid = false;
                        break;
                    }
                }
                if (!valid) {
                    cell_id = findTableLocation(cell_id);
                    break;
                }
            }
        }

        table.setCell_root_id(cell_id);

        int table_part = 1;
        for (int i = 0; i < table_height; i++) {

            for (int j = 0; j < table_width; j++) {

                cells.get(cell_id + map_width * i + j).used(table, table_part);
                table_part++;

                cells.get(cell_id + map_width * i + j).setTransferHandler(new UsedCellTransferHandler());
                cells.get(cell_id + map_width * i + j).removeMouseListener(cells.get(cell_id + map_width * i + j).getMouseListeners()[0]);
                cells.get(cell_id + map_width * i + j).addMouseListener(new UsedCellMouseAdapter());

            }
        }

        deselectAllTable();
        lbl_remaining_table.setText(Integer.toString(20 - m.getTables().size()));
        this.revalidate();
    }

    /**
     * a sub method of moveTable() used to move a single table
     *
     * @param table is the table to be added
     * @param table_width is the width of the moving table
     * @param table_height is the height of the moving table
     * @param new_cell_id is the ID of the cell where the table is moved to
     */
    private void moveSingleTable(Table table, int table_width, int table_height, int new_cell_id) {
        if (new_cell_id < 0 || new_cell_id >= cells.size()) {
            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("CashierManagerView.moveSingleTable.locationNotAvailable_1"));
            return;
        }
        if (cells.get(new_cell_id).getColumn() + table_width > map_width || cells.get(new_cell_id).getRow() + table_height > map_height) {
            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("CashierManagerView.moveSingleTable.locationNotAvailable_2"));
            return;
        }

        for (int i = 0; i < table_height; i++) {
            for (int j = 0; j < table_width; j++) {
                if (cells.get(new_cell_id + map_width * i + j).getTable() != null && cells.get(new_cell_id + map_width * i + j).getTable_id() != table.getTableNumber()) {
                    JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("CashierManagerView.moveSingleTable.locationNotAvailable_3"));
                    return;
                }
            }
        }

        for (int i = 0; i < cells.size(); i++) {
            if (cells.get(i).getTable_id() == table.getTableNumber()) {
                cells.get(i).setTransferHandler(new EmptyCellTransferHandler(TransferHandler.COPY));
                cells.get(i).removeMouseListener(cells.get(i).getMouseListeners()[0]);
                cells.get(i).addMouseListener(new EmptyCellMouseAdapter());
                cells.get(i).free();
            }
        }

        table.setCell_root_id(new_cell_id);
        int table_part = 1;
        for (int i = 0; i < table_height; i++) {

            for (int j = 0; j < table_width; j++) {

                cells.get(new_cell_id + map_width * i + j).used(table, table_part);
                table_part++;
                cells.get(new_cell_id + map_width * i + j).setTransferHandler(new UsedCellTransferHandler());
                cells.get(new_cell_id + map_width * i + j).removeMouseListener(cells.get(new_cell_id + map_width * i + j).getMouseListeners()[0]);
                cells.get(new_cell_id + map_width * i + j).addMouseListener(new UsedCellMouseAdapter());

            }
        }
    }

    /**
     * a sub method of moveTable() used to move a merged table
     *
     * @param table is the table to be added
     * @param table_width is the width of the moving table
     * @param table_height is the height of the moving table
     * @param new_cell_id is the ID of the cell where the table is moved to
     */
    private void moveMergedTable(Table table, int table_width, int table_height, int new_cell_id) {
        if (new_cell_id < 0 || new_cell_id >= cells.size()) {
            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("CashierManagerView.moveMergedTable.locationNotAvailable_1"));
            return;
        }
        if (cells.get(new_cell_id).getColumn() + table_width > map_width || cells.get(new_cell_id).getRow() + table_height > map_height) {
            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("CashierManagerView.moveMergedTable.locationNotAvailable_2"));
            return;
        }

        for (int i = 0; i < table_height; i++) {
            for (int j = 0; j < table_width; j++) {
                if (cells.get(new_cell_id + map_width * i + j).getTable() != null && cells.get(new_cell_id + map_width * i + j).getTable_id() != table.getTableNumber()) {
                    JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("CashierManagerView.moveMergedTable.locationNotAvailable_3"));
                    return;
                }
            }
        }

        table.setTable_group_root_id(new_cell_id);
        for (int i = 0; i < cells.size(); i++) {
            if (cells.get(i).getTable_id() == table.getTableNumber()) {
                cells.get(i).setTransferHandler(new EmptyCellTransferHandler(TransferHandler.COPY));
                cells.get(i).removeMouseListener(cells.get(i).getMouseListeners()[0]);
                cells.get(i).addMouseListener(new EmptyCellMouseAdapter());
                cells.get(i).free();
            }
        }

        ArrayList<Table> merge_tables = new ArrayList<>();

        for (int i = 0; i < m.getTables().size(); i++) {
            if (m.getTables().get(i).getTableGroupId() == table.getTableGroupId()) {
                merge_tables.add(m.getTables().get(i));
            }
        }

        int count = 0;
        int table_part = 1;
        for (int j = 0; j < 6; j++) {

            for (int i = 0; i < 6; i++) {

                if (count < merge_tables.size()) {
                    boolean table_number_shown = false;
                    for (int x = 0; x < 4; ++x) {
                        for (int y = 0; y < 4; ++y) {
                            if (table_part == 8 + x * 6 + y) {
                                table_number_shown = true;
                                cells.get(table.getTable_group_root_id() + 25 * j + i).grouped(table, table_part, merge_tables.get(count).getTableNumber());
                                count++;
                                break;
                            }

                        }
                        if (table_number_shown) {
                            break;
                        }
                    }
                    if (!table_number_shown) {
                        cells.get(table.getTable_group_root_id() + 25 * j + i).grouped(table, table_part, -1);
                    }

                    table_part++;
                } else {
                    cells.get(table.getTable_group_root_id() + 25 * j + i).grouped(table, table_part, -1);
                    table_part++;
                }
                cells.get(table.getTable_group_root_id() + 25 * j + i).removeMouseListener(cells.get(table.getTable_group_root_id() + 25 * j + i).getMouseListeners()[0]);
                cells.get(table.getTable_group_root_id() + 25 * j + i).addMouseListener(new UsedCellMouseAdapter());
                cells.get(table.getTable_group_root_id() + 25 * j + i).setTransferHandler(new UsedCellTransferHandler());

            }
        }
    }

    /**
     * update restaurant map when a table is moved to new location
     *
     * @param table is the table to be added
     * @param table_width is the width of the moving table
     * @param table_height is the height of the moving table
     * @param new_cell_id is the ID of the cell where the table is moved to
     */
    private void moveTable(Table table, int new_cell_id) {

        if (table.getTableGroupId() == -1) {
            int table_width = 3;
            int table_height = 3;
            moveSingleTable(table, table_width, table_height, new_cell_id);
        } else {
            int table_width = 6;
            int table_height = 6;
            moveMergedTable(table, table_width, table_height, new_cell_id);
        }

        m.update();
        deselectAllTable();
        this.revalidate();
    }

    /**
     * a sub method of deselectTable() used to deselect a single table
     *
     * @param table is the table to be deselected
     */
    private void deselectSingleTable(Table table) {

        current_tables.remove(table);

        for (int i = 0; i < cells.size(); i++) {
            if (cells.get(i).getTable() != null) {
                if (cells.get(i).getTable().equals(table)) {
                    cells.get(i).used(cells.get(i).getTable());
                }
            }
        }
    }

    /**
     * a sub method of deselectTable() used to deselect a merged table
     *
     * @param table is the table to be deselected
     */
    private void deselectMergedTable(Table table) {
        current_tables.remove(table);
        for (int i = 0; i < cells.size(); i++) {
            if (cells.get(i).getTable() != null) {
                if (cells.get(i).getTable().equals(table)) {
                    cells.get(i).grouped(table, cells.get(i).getTable_part(), -1);
                }
            }
        }
    }

    /**
     * update restaurant map when a table is deselected
     *
     * @param table is the table to be deselected
     */
    public void deselectTable(Table table) {

        if (isMultiTable || cb_multi_table.isSelected()) {
            if (table.getTableGroupId() == -1) {
                deselectSingleTable(table);
            } else {
                deselectMergedTable(table);
            }
        } else {
            deselectAllTable();
            if (table.getTableGroupId() == -1) {
                deselectSingleTable(table);
            } else {
                deselectMergedTable(table);
            }
        }

        int group_count = 0;
        int individual_count = 0;

        for (int i = 0; i < current_tables.size(); i++) {
            if (current_tables.get(i).getTableGroupId() == -1) {
                individual_count++;
            } else {
                group_count++;
            }
        }

        if (current_tables.isEmpty()) {

            deselectAllTable();
            rdb_occupied.setEnabled(false);
            rdb_vacant.setEnabled(false);
            rdb_reserved.setEnabled(false);
            rdb_occupied.setSelected(false);
            rdb_vacant.setSelected(false);
            rdb_reserved.setSelected(false);

        } else if (current_tables.size() == 1) {
            btn_delete_table.setEnabled(true);
            btn_merge.setEnabled(false);
            btn_switch.setEnabled(false);
            rdb_occupied.setEnabled(true);
            rdb_vacant.setEnabled(true);
            rdb_reserved.setEnabled(true);

            if (individual_count == 1) {
                btn_unmerge.setEnabled(false);
                txt_table_number.setEnabled(true);
                txt_table_number.setText(Integer.toString(current_tables.get(0).getTableNumber()));
                btn_edit_table.setEnabled(true);
            } else {
                btn_unmerge.setEnabled(true);
                txt_table_number.setEnabled(false);
                txt_table_number.setText("");
                btn_edit_table.setEnabled(false);
            }

            updateOrderItemList();
            tickTableStatus();
        } else {
            btn_delete_table.setEnabled(true);
            btn_merge.setEnabled(true);
            btn_unmerge.setEnabled(false);
            txt_table_number.setEnabled(false);
            txt_table_number.setText("");
            btn_edit_table.setEnabled(false);
            rdb_occupied.setEnabled(false);
            rdb_vacant.setEnabled(false);
            rdb_reserved.setEnabled(false);
            if (current_tables.size() == 2) {
                if ((current_tables.get(0).getTableStatus() == TableStatus.VACANT && current_tables.get(1).getTableStatus() == TableStatus.OCCUPIED)
                        || (current_tables.get(1).getTableStatus() == TableStatus.VACANT && current_tables.get(0).getTableStatus() == TableStatus.OCCUPIED)) {
                    btn_switch.setEnabled(true);
                } else {
                    btn_switch.setEnabled(false);
                }
            } else {
                btn_switch.setEnabled(false);
            }
            updateOrderItemList();
        }

        this.revalidate();

    }

    /**
     * update restaurant map when a table is modified.
     *
     * @param table is the table to be updated
     */
    public void updateTable(Table table) {
        if (table != null) {
            if (table.getTableGroupId() == -1) {
                int table_height = 3;
                int table_width = 3;
                int cell_id = table.getCell_root_id();
                int table_part = 1;
                for (int i = 0; i < table_height; i++) {
                    for (int j = 0; j < table_width; j++) {
                        cells.get(cell_id + map_width * i + j).used(table, table_part);
                        table_part++;
                        cells.get(cell_id + map_width * i + j).setTransferHandler(new UsedCellTransferHandler());
                        cells.get(cell_id + map_width * i + j).removeMouseListener(cells.get(cell_id + map_width * i + j).getMouseListeners()[0]);
                        cells.get(cell_id + map_width * i + j).addMouseListener(new UsedCellMouseAdapter());
                    }
                }
            } else {
                ArrayList<Table> merge_tables = new ArrayList<>();

                for (int i = 0; i < m.getTables().size(); i++) {
                    if (m.getTables().get(i).getTableGroupId() == table.getTableGroupId()) {
                        merge_tables.add(m.getTables().get(i));
                    }
                }

                int count = 0;
                int table_part = 1;
                for (int j = 0; j < 6; j++) {

                    for (int i = 0; i < 6; i++) {

                        if (count < merge_tables.size()) {
                            boolean table_number_shown = false;
                            for (int x = 0; x < 4; ++x) {
                                for (int y = 0; y < 4; ++y) {
                                    if (table_part == 8 + x * 6 + y) {
                                        table_number_shown = true;
                                        cells.get(table.getTable_group_root_id() + 25 * j + i).grouped(table, table_part, merge_tables.get(count).getTableNumber());
                                        count++;
                                        break;
                                    }

                                }
                                if (table_number_shown) {
                                    break;
                                }
                            }
                            if (!table_number_shown) {
                                cells.get(table.getTable_group_root_id() + 25 * j + i).grouped(table, table_part, -1);
                            }

                            table_part++;
                        } else {
                            cells.get(table.getTable_group_root_id() + 25 * j + i).grouped(table, table_part, -1);
                            table_part++;
                        }
                        cells.get(table.getTable_group_root_id() + 25 * j + i).removeMouseListener(cells.get(table.getTable_group_root_id() + 25 * j + i).getMouseListeners()[0]);
                        cells.get(table.getTable_group_root_id() + 25 * j + i).addMouseListener(new UsedCellMouseAdapter());
                        cells.get(table.getTable_group_root_id() + 25 * j + i).setTransferHandler(new UsedCellTransferHandler());

                    }
                }
            }

            deselectAllTable();
            selectTable(table);

            this.revalidate();
        }
    }

    /**
     * a sub method of selectTable() used to select a single table
     *
     * @param table is the table to be selected
     */
    private void selectSingleTable(Table table) {
        current_tables.add(table);
        for (int i = 0; i < cells.size(); i++) {
            if (cells.get(i).getTable() != null) {
                if (cells.get(i).getTable().equals(table)) {
                    cells.get(i).chosen();
                }
            }
        }
    }

    /**
     * a sub method of selectTable() used to select a merged table
     *
     * @param table is the table to be selected
     */
    private void selectMergedTable(Table table) {
        current_tables.add(table);
        for (int i = 0; i < cells.size(); i++) {
            if (cells.get(i).getTable() != null) {
                if (cells.get(i).getTable().equals(table)) {
                    cells.get(i).groupedChosen();
                }
            }
        }
    }

    /**
     * update restaurant map when a table is selected
     *
     * @param table is the table to be selected
     */
    public void selectTable(Table table) {

        if (isMultiTable || cb_multi_table.isSelected()) {
            if (table.getTableGroupId() == -1) {
                selectSingleTable(table);
            } else {
                selectMergedTable(table);
            }
        } else {
            deselectAllTable();
            if (table.getTableGroupId() == -1) {
                selectSingleTable(table);
            } else {
                selectMergedTable(table);
            }
        }

        int group_count = 0;
        int individual_count = 0;

        for (int i = 0; i < current_tables.size(); i++) {
            if (current_tables.get(i).getTableGroupId() == -1) {
                individual_count++;
            } else {
                group_count++;
            }
        }

        if (current_tables.size() == 1) {

            btn_delete_table.setEnabled(true);
            btn_merge.setEnabled(false);
            btn_switch.setEnabled(false);
            rdb_occupied.setEnabled(true);
            rdb_vacant.setEnabled(true);
            rdb_reserved.setEnabled(true);

            if (individual_count == 1) {
                btn_unmerge.setEnabled(false);
                txt_table_number.setEnabled(true);
                txt_table_number.setText(Integer.toString(current_tables.get(0).getTableNumber()));
                btn_edit_table.setEnabled(true);
            } else {
                btn_unmerge.setEnabled(true);
                txt_table_number.setEnabled(false);
                txt_table_number.setText("");
                btn_edit_table.setEnabled(false);
            }

            updateOrderItemList();
            tickTableStatus();
        } else {

            btn_delete_table.setEnabled(true);
            btn_merge.setEnabled(true);
            btn_unmerge.setEnabled(false);
            txt_table_number.setEnabled(false);
            txt_table_number.setText("");
            btn_edit_table.setEnabled(false);
            rdb_occupied.setEnabled(false);
            rdb_vacant.setEnabled(false);
            rdb_reserved.setEnabled(false);
            if (current_tables.size() == 2) {
                if ((current_tables.get(0).getTableStatus() == TableStatus.VACANT && current_tables.get(1).getTableStatus() == TableStatus.OCCUPIED)
                        || (current_tables.get(1).getTableStatus() == TableStatus.VACANT && current_tables.get(0).getTableStatus() == TableStatus.OCCUPIED)) {
                    btn_switch.setEnabled(true);
                } else {
                    btn_switch.setEnabled(false);
                }
            } else {
                btn_switch.setEnabled(false);
            }
            updateOrderItemList();
        }

        if (table.getTableGroupId() == -1) {
            cells.get(table.getCell_root_id()).requestFocus();
        } else {
            cells.get(table.getTable_group_root_id()).requestFocus();
        }

        this.revalidate();

    }

    /**
     * update restaurant map when all the tables are deselected
     */
    public void deselectAllTable() {

        for (int k = 0; k < current_tables.size(); k++) {

            Table table = current_tables.get(k);

            for (int i = 0; i < cells.size(); i++) {
                if (cells.get(i).getTable() != null) {
                    if (cells.get(i).getTable().equals(table)) {
                        if (cells.get(i).getTable().getTableGroupId() == -1) {
                            cells.get(i).used(table, cells.get(i).getTable_part());
                        } else {
                            cells.get(i).grouped(table, cells.get(i).getTable_part(), -1);
                        }

                    }
                }
            }
        }
        current_tables.clear();

        btn_delete_table.setEnabled(false);
        btn_merge.setEnabled(false);
        btn_unmerge.setEnabled(false);
        btn_switch.setEnabled(false);
        txt_table_number.setEnabled(false);
        txt_table_number.setText("");
        btn_edit_table.setEnabled(false);
        rdb_occupied.setEnabled(false);
        rdb_vacant.setEnabled(false);
        rdb_reserved.setEnabled(false);

        btn_refresh_order.setEnabled(false);
        btn_edit_order.setEnabled(false);
        btn_delete_order.setEnabled(false);
        btn_view_order.setEnabled(false);
        btn_print.setEnabled(false);
        btn_payment.setEnabled(false);

        updateOrderItemList();
        this.revalidate();

    }

    /**
     *
     * @return the ID of the cell available for a new merged table. return -1 if
     * there is no available location
     */
    public int mergable() {
        int result = -1;
        if (current_tables.size() <= 1) {
            return result;
        }

        for (int i = 0; i < cells.size(); i++) {
            boolean valid = true;
            if (cells.get(i).getColumn() < map_width - 5 && cells.get(i).getRow() < map_height - 5) {
                valid = true;
                for (int j = 0; j < 6; j++) {
                    for (int k = 0; k < 6; k++) {
                        if (cells.get(i + map_width * j + k).getTable() != null) {
                            boolean valid2 = false;
                            for (int x = 0; x < current_tables.size(); x++) {
                                if (cells.get(i + map_width * j + k).getTable().equals(current_tables.get(x))) {
                                    valid2 = true;
                                    break;
                                }
                            }
                            if (!valid2) {
                                valid = false;
                                break;
                            }

                        }
                    }
                    if (!valid) {
                        break;
                    }

                }
            } else {
                valid = false;
            }

            if (valid) {
                result = i;
                break;
            }

        }
        return result;
    }

    /**
     * update restaurant map when a number of table is merged
     *
     * @param table is the root table of the table group
     */
    public void mergeTable(Table table) {

        for (int i = 0; i < cells.size(); i++) {
            if (cells.get(i).getTable() != null) {
                if (cells.get(i).getTable().getTableGroupId() == table.getTableGroupId()) {
                    cells.get(i).setTransferHandler(new EmptyCellTransferHandler(TransferHandler.COPY));
                    cells.get(i).removeMouseListener(cells.get(i).getMouseListeners()[0]);
                    cells.get(i).addMouseListener(new EmptyCellMouseAdapter());
                    cells.get(i).free();
                }
            }
        }

        ArrayList<Table> merge_tables = new ArrayList<>();

        for (int i = 0; i < m.getTables().size(); i++) {
            if (m.getTables().get(i).getTableGroupId() == table.getTableGroupId()) {
                merge_tables.add(m.getTables().get(i));
            }
        }

        m.sortTables(merge_tables);

        int count = 0;
        int table_part = 1;
        for (int j = 0; j < 6; j++) {

            for (int i = 0; i < 6; i++) {
                if (count < merge_tables.size()) {
                    boolean table_number_shown = false;
                    for (int x = 0; x < 4; ++x) {
                        for (int y = 0; y < 4; ++y) {
                            if (table_part == 8 + x * 6 + y) {
                                table_number_shown = true;
                                cells.get(table.getTable_group_root_id() + 25 * j + i).grouped(table, table_part, merge_tables.get(count).getTableNumber());
                                count++;
                                break;
                            }

                        }
                        if (table_number_shown) {
                            break;
                        }
                    }
                    if (!table_number_shown) {
                        cells.get(table.getTable_group_root_id() + 25 * j + i).grouped(table, table_part, -1);
                    }

                    table_part++;
                } else {
                    cells.get(table.getTable_group_root_id() + 25 * j + i).grouped(table, table_part, -1);
                    table_part++;
                }
                cells.get(table.getTable_group_root_id() + 25 * j + i).removeMouseListener(cells.get(table.getTable_group_root_id() + 25 * j + i).getMouseListeners()[0]);
                cells.get(table.getTable_group_root_id() + 25 * j + i).addMouseListener(new UsedCellMouseAdapter());
                cells.get(table.getTable_group_root_id() + 25 * j + i).setTransferHandler(new UsedCellTransferHandler());

            }
        }

        deselectAllTable();
        this.revalidate();
    }

    /**
     * update restaurant map when a merged table is unmerged
     *
     * @param unmerge_table is the root table of the table group
     * @param tables_in_group array list containing the group of merged tables
     */
    public void unmergeTable(Table unmerge_table, ArrayList<Table> tables_in_group) {

        for (int i = 0; i < cells.size(); i++) {
            if (cells.get(i).getTable() != null) {
                if (cells.get(i).getTable().getTableNumber() == unmerge_table.getTableNumber()) {
                    cells.get(i).setTransferHandler(new EmptyCellTransferHandler(TransferHandler.COPY));
                    cells.get(i).removeMouseListener(cells.get(i).getMouseListeners()[0]);
                    cells.get(i).addMouseListener(new EmptyCellMouseAdapter());
                    cells.get(i).free();
                }
            }

        }

        for (int i = 0; i < tables_in_group.size(); i++) {
            addTable(tables_in_group.get(i), 3, 3, tables_in_group.get(i).getCell_root_id());
        }
        deselectAllTable();
        this.revalidate();

    }

    /**
     * updates the table of ordered items when a table is selected, a new item
     * is ordered or an ordered item is modified or deleted
     */
    public void updateOrderItemList() {
        if (current_tables.size() == 1) {
            Order order = current_tables.get(0).getO();

            String[] column_names = {Localization.getResourceBundle().getString("CashierManagerView.updateOrderItemList.no"), Localization.getResourceBundle().getString("CashierManagerView.updateOrderItemList.name"), Localization.getResourceBundle().getString("CashierManagerView.updateOrderItemList.quantity"), Localization.getResourceBundle().getString("CashierManagerView.updateOrderItemList.note"), Localization.getResourceBundle().getString("CashierManagerView.updateOrderItemList.status"), Localization.getResourceBundle().getString("CashierManagerView.updateOrderItemList.price")};
            ArrayList<String[]> data = new ArrayList<>();
            String status;

            for (int i = 0; i < order.getOrderItems().size(); i++) {
                OrderItem orderItem = order.getOrderItems().get(i);
                String price = new DecimalFormat("###,###,###").format(orderItem.getItem().getItemPrice()) + Localization.getResourceBundle().getString("CashierManagerView.vnd");
                if (price.contains(",")) {
                    price = price.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                } else {
                    price = price.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                }

                if (orderItem.getIsDone()) {
                    status = "Served";
                } else {
                    status = "Waiting";
                }
                data.add(new String[]{Integer.toString(orderItem.getOrderItemId()), orderItem.getItem().getItemName(), Integer.toString(orderItem.getQuantity()), orderItem.getNote(), status, price});

            }
            String[][] table_data = new String[data.size()][column_names.length];
            for (int i = 0; i < data.size(); i++) {
                System.arraycopy(data.get(i), 0, table_data[i], 0, column_names.length);
            }

            table_order_detail = new JTable(table_data, column_names) {
                @Override
                public boolean isCellEditable(int row, int column) {
                    return false;
                }
            };

            table_order_detail.setRowHeight(85);

            table_order_detail.setFocusable(true);
            table_order_detail.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
            table_order_detail.getColumnModel().getColumn(0).setPreferredWidth(this.getWidth() * 24 / 1000);
            table_order_detail.getColumnModel().getColumn(1).setPreferredWidth(this.getWidth() * 76 / 1000);
            table_order_detail.getColumnModel().getColumn(2).setPreferredWidth(this.getWidth() * 46 / 1000);
            table_order_detail.getColumnModel().getColumn(3).setPreferredWidth(this.getWidth() * 65 / 1000);
            table_order_detail.getColumnModel().getColumn(4).setPreferredWidth(this.getWidth() * 36 / 1000);
            table_order_detail.getColumnModel().getColumn(5).setPreferredWidth(this.getWidth() * 44 / 1000);
            table_order_detail.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
            table_order_detail.getSelectionModel().addListSelectionListener(new SelectOrderItemController(m));
            table_order_detail.setAutoCreateRowSorter(true);
            table_order_detail.addKeyListener(doic);

            pnl_east.remove(spane_order_detail);

            spane_order_detail = new JScrollPane(table_order_detail);
            pnl_east.add(spane_order_detail, BorderLayout.CENTER);
            table_order_detail.setDefaultRenderer(Object.class, new MultiLineCellRenderer());
            if (order == null || order.getOrderItems().isEmpty()) {
                btn_refresh_order.setEnabled(false);
                btn_edit_order.setEnabled(false);
                btn_delete_order.setEnabled(false);
                btn_view_order.setEnabled(false);
                btn_print.setEnabled(false);
                btn_payment.setEnabled(false);

            } else {
                btn_refresh_order.setEnabled(true);
                btn_edit_order.setEnabled(true);
                btn_delete_order.setEnabled(true);
                btn_view_order.setEnabled(true);
                btn_print.setEnabled(true);
                btn_payment.setEnabled(true);
            }

            table_order_detail.setAutoCreateRowSorter(true);
            table_order_detail.getRowSorter().toggleSortOrder(0);

            if (!data.isEmpty()) {
                table_order_detail.setRowSelectionInterval(0, 0);
            }

            updateTotal();

        } else {
            table_order_detail = new JTable();
            pnl_east.remove(spane_order_detail);
            spane_order_detail = new JScrollPane(table_order_detail);
            pnl_east.add(spane_order_detail, BorderLayout.CENTER);
            btn_refresh_order.setEnabled(false);
            btn_edit_order.setEnabled(false);
            btn_delete_order.setEnabled(false);
            btn_view_order.setEnabled(false);
            btn_print.setEnabled(false);
            btn_payment.setEnabled(false);
        }
        this.revalidate();
    }

    /**
     *
     * @param o
     * @param o1
     */
    @Override
    public void update(Observable o, Object o1) {
        updateMenuList(top);
    }

    /**
     * enables dragging for btn_oval_table and btn_rectangle_table
     */
    public class ButtonDragMouseAdapter extends MouseAdapter {

        /**
         *
         * @param e
         */
        @Override
        public void mousePressed(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e)) {
                JComponent c = (JComponent) e.getSource();
                TransferHandler handler = c.getTransferHandler();
                handler.exportAsDrag(c, e, TransferHandler.COPY);
            }

        }
    }

    /**
     * specifying event when user click on an empty cell
     */
    public class EmptyCellMouseAdapter extends MouseAdapter {

        /**
         *
         * @param e
         */
        @Override
        public void mousePressed(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e)) {
                deselectAllTable();
                Cell cell = (Cell) e.getSource();
                cell.requestFocus();
            }
        }
    }

    /**
     * specifying event when user click on a table
     */
    public class UsedCellMouseAdapter extends MouseAdapter {

        /**
         *
         * @param e
         */
        @Override
        public void mousePressed(MouseEvent e) {
            if (SwingUtilities.isLeftMouseButton(e)) {
                Cell c = (Cell) e.getSource();
                TransferHandler handler = c.getTransferHandler();
                handler.exportAsDrag(c, e, TransferHandler.COPY);
                c.requestFocus();
            }
        }
    }

//this inner class specify transfer handler for categories and items in menu
    class CategoryItemTransferHandler extends TransferHandler {

        @Override
        public int getSourceActions(JComponent comp) {
            return COPY;
        }

        @Override
        public Transferable createTransferable(JComponent comp) {

            DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
            int count = tree.getSelectionPath().getPathCount();
            if (count == 2) {
                return null;
            } else {
                if (node == null) {
                    return new StringSelection("");
                }
                Object nodeinfo = node.getUserObject();
                return new StringSelection(nodeinfo.toString());
            }
        }
    }

//this inner class specify transfer handler for btn_oval_table and btn_rectangle_table
    class ButtonTransferHandler extends TransferHandler {

        @Override
        public int getSourceActions(JComponent comp) {
            return COPY;
        }
        private int index = 0;

        @Override
        public Transferable createTransferable(JComponent comp) {

            JButton btn = (JButton) comp;
            return new StringSelection(btn.getName());
        }

        @Override
        public void exportDone(JComponent comp, Transferable trans, int action) {
        }
    }

//this inner class specifies transfer handler for table
    class UsedCellTransferHandler extends TransferHandler {

        @Override
        public int getSourceActions(JComponent comp) {
            return COPY;
        }

        @Override
        public boolean canImport(TransferSupport ts) {
            return true;
        }

        @Override
        public Transferable createTransferable(JComponent comp) {

            Integer table_id = ((Cell) comp).getTable_id();
            Integer cell_id = ((Cell) comp).getId();

            String data = table_id.toString() + " " + cell_id.toString();

            return new StringSelection(data);
        }

        @Override
        public boolean importData(TransferHandler.TransferSupport support) {
            // if we can't handle the import, say so

            if (!canImport(support)) {

                return false;
            }

            Cell cell = (Cell) support.getComponent();

            String data;
            try {
                data = (String) support.getTransferable().getTransferData(DataFlavor.stringFlavor);
            } catch (UnsupportedFlavorException | java.io.IOException e) {
                return false;
            }
            switch (data) {
                case "Oval table":
                    if (m.getTables().size() < 20) {
                        Table table = m.createTable(TableShape.OVAL);
                        addTable(table, 3, 3, cell.getId());
                    } else {
                        JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("CashierManagerView.importData.tableOverLimit_1"));
                    }
                    break;
                case "Rectangle table":
                    if (m.getTables().size() < 20) {
                        Table table = m.createTable(TableShape.RECTANGLE);
                        addTable(table, 3, 3, cell.getId());
                    } else {
                        JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("CashierManagerView.importData.tableOverLimit_2"));
                    }
                    break;
                default:
                    try {
                        String[] parts = data.split(" ");
                        for (int i = 0; i < parts.length; i++) {
                            Integer.parseInt(parts[i]);
                        }
                        int old_cell_id = Integer.parseInt(parts[1]);

                        Table table = null;
                        for (int i = 0; i < m.getTables().size(); i++) {
                            if (m.getTables().get(i).getTableNumber() == Integer.parseInt(parts[0])) {
                                table = m.getTables().get(i);
                            }
                        }

                        if (old_cell_id != cell.getId()) {
                            moveTable(table, cell.getId());

                        } else {

                            if (cell.isChosen) {
                                deselectTable(cell.getTable());

                            } else {
                                selectTable(cell.getTable());
                            }
                        }

                    } catch (NumberFormatException ex) {
                        CreateEditOrderItemForm ceoif = new CreateEditOrderItemForm();

                        String[] parts = data.split("\\-");
                        ceoif.setTxt_item_name(parts[0].trim());
                        boolean created = false;
                        Object result;
                        do {
                            ceoif.getD().setVisible(true);

                            result = ceoif.getP().getValue();

                            if (result == ceoif.getP().getOptions()[0]) {
                                Item item = null;
                                for (int i = 0; i < m.getMenu().getItems().size(); i++) {
                                    if (m.getMenu().getItems().get(i).getItemName().equals(parts[0].trim())) {
                                        item = m.getMenu().getItems().get(i);
                                        break;
                                    }
                                }

                                String note = StringUtil.TrimSpace(ceoif.getTxt_note().getText());
                                if (!StringUtil.ValidateString(note)) {
                                    ceoif.getTxt_note().setText(Localization.getResourceBundle().getString("CashierManagerView.importData.manyCharacters"));
                                    continue;
                                }
                                int quantity = Integer.parseInt(ceoif.getTxt_quantity().getText());
                                if (quantity == 0) {
                                    ceoif.getLbl_quantity_error().setText(Localization.getResourceBundle().getString("CashierManagerView.importData.quantityCannotBe0"));
                                    ceoif.getLbl_quantity_error().setForeground(Color.red);
                                } else {
                                    ceoif.getLbl_quantity_error().setText(null);

                                    Table table = null;
                                    for (int i = 0; i < m.getTables().size(); i++) {
                                        if (m.getTables().get(i).getTableNumber() == cell.getTable_id()) {
                                            table = m.getTables().get(i);
                                        }
                                    }
                                    Order order = table.getO();
                                    OrderItem orderItem = new OrderItem(order, item, note, quantity);
                                    if (table.getTableGroupId() != -1) {
                                        for (int i = 0; i < m.getTables().size(); i++) {
                                            Table temp_table = m.getTables().get(i);
                                            if (temp_table.getTableGroupId() == table.getTableGroupId()) {
                                                temp_table.setO(order);
                                                if (temp_table.getTableStatus() == TableStatus.VACANT) {
                                                    temp_table.setTableStatus(TableStatus.OCCUPIED);
                                                }
                                            }
                                        }
                                    } else {
                                        table.setO(order);
                                        if (table.getTableStatus() == TableStatus.VACANT) {
                                            table.setTableStatus(TableStatus.OCCUPIED);
                                        }
                                        selectTable(table);
                                    }
                                    updateTable(table);

                                    created = true;
                                }
                            }
                        } while (result == ceoif.getP().getOptions()[0] && created == false);
                    }
                    break;
            }
            return true;
        }

        @Override
        public void exportDone(JComponent comp, Transferable trans, int action) {
        }
    }

//this inner class specifies transfer handler for empty cell
    class EmptyCellTransferHandler extends TransferHandler {

        int action;

        public EmptyCellTransferHandler(int action) {
            this.action = action;

        }

        @Override
        public int getSourceActions(JComponent jc) {

            return super.getSourceActions(jc);
        }

        @Override
        public boolean canImport(TransferHandler.TransferSupport support) {
            // for the demo, we'll only support drops (not clipboard paste)
            if (!support.isDrop()) {
                return false;
            }

            // we only import Strings
            if (!support.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                return false;
            }

            boolean actionSupported = (action & support.getSourceDropActions()) == action;
            if (actionSupported) {
                support.setDropAction(action);
                return true;
            }

            return false;
        }

        @Override
        public boolean importData(TransferHandler.TransferSupport support) {
            // if we can't handle the import, say so

            if (!canImport(support)) {

                return false;
            }

            Cell cell = (Cell) support.getComponent();

            String data;
            try {
                data = (String) support.getTransferable().getTransferData(DataFlavor.stringFlavor);
            } catch (UnsupportedFlavorException | java.io.IOException e) {
                return false;
            }

            Table table = null;
            switch (data) {
                case "Oval table":
                    if (m.getTables().size() < 20) {
                        table = m.createTable(TableShape.OVAL);
                        addTable(table, 3, 3, cell.getId());
                    } else {
                        JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("CashierManagerView.importData_2.tableOverLimit_1"));
                    }
                    break;
                case "Rectangle table":
                    if (m.getTables().size() < 20) {
                        table = m.createTable(TableShape.RECTANGLE);
                        addTable(table, 3, 3, cell.getId());

                    } else {
                        JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("CashierManagerView.importData_2.tableOverLimit_2"));

                    }
                    break;
                default:
                    String[] parts = data.split(" ");
                    for (int i = 0; i < m.getTables().size(); i++) {
                        if (m.getTables().get(i).getTableNumber() == Integer.parseInt(parts[0])) {
                            table = m.getTables().get(i);
                        }
                    }
                    moveTable(table, cell.getId());
                    break;
            }

            return true;
        }
    }

    //http://www.coderanch.com/t/522452/GUI/java/JTable-multiple-lines-cell-cells
    class MultiLineCellRenderer extends JTextArea implements TableCellRenderer {

        public MultiLineCellRenderer() {
            setLineWrap(true);
            setWrapStyleWord(true);
            setOpaque(true);
            setEditable(false); //this line doesn't seem to be doing anything
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value,
                boolean isSelected, boolean hasFocus, int row, int column) {
            if (isSelected) {
                setForeground(table.getSelectionForeground());
                setBackground(table.getSelectionBackground());
            } else {
                setForeground(table.getForeground());
                setBackground(table.getBackground());
            }
            setFont(table.getFont());
            if (hasFocus) {
                setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
                if (table.isCellEditable(row, column)) {
                    setForeground(UIManager.getColor("Table.focusCellForeground"));
                    setBackground(UIManager.getColor("Table.focusCellBackground"));
                }
            } else {
                setBorder(new EmptyBorder(1, 2, 1, 2));
            }
            if (value == null) {
                setText("");
            } else {
                setText(value.toString());
            }
            setEditable(false);
            return this;
        }
    }

    class CMVKeyListener implements KeyListener {

        private final ArrayList<Character> pressed = new ArrayList<>();

        @Override
        public void keyPressed(KeyEvent ke) {
            if (ke.getKeyCode() == KeyEvent.VK_DELETE) {
                btn_delete_table.doClick();
            }

            if (ke.getKeyCode() == KeyEvent.VK_M && ke.isControlDown()) {
                btn_merge.doClick();
            }

            if (ke.getKeyCode() == KeyEvent.VK_U && ke.isControlDown()) {
                btn_unmerge.doClick();
            }

            if (ke.getKeyCode() == KeyEvent.VK_S && ke.isControlDown()) {
                btn_switch.doClick();
            }

            if (ke.getKeyCode() == KeyEvent.VK_TAB) {
                m.sortTables(m.getTables());
                int table_number;
                if (current_tables.isEmpty()) {
                    table_number = 0;
                } else {
                    table_number = current_tables.get(0).getTableNumber();
                    deselectAllTable();
                }
                for (int i = 0; i < m.getTables().size(); i++) {
                    if (m.getTables().get(i).getTableNumber() > table_number) {
                        if (m.getTables().get(i).getTableGroupId() == -1) {
                            selectTable(m.getTables().get(i));
                            break;
                        } else if (m.getTables().get(i).getTable_group_root_id() != -1) {
                            selectTable(m.getTables().get(i));
                            break;
                        }

                    }
                }
            }

            if (ke.getKeyCode() == KeyEvent.VK_A && ke.isControlDown()) {
                isMultiTable = true;
                deselectAllTable();
                for (int i = 0; i < m.getTables().size(); i++) {
                    if (m.getTables().get(i).getTableGroupId() == -1) {
                        selectTable(m.getTables().get(i));
                    } else {
                        if (m.getTables().get(i).getTable_group_root_id() != -1) {
                            selectTable(m.getTables().get(i));
                        }
                    }
                }
                isMultiTable = false;
            }
            if (ke.getKeyCode() == KeyEvent.VK_ESCAPE) {
                deselectAllTable();
            }

            if (ke.getKeyCode() == KeyEvent.VK_CONTROL) {
                isMultiTable = true;
            }

            if (ke.getKeyCode() == KeyEvent.VK_UP) {

                Cell cell = (Cell) ke.getSource();
                if (cell.isChosen) {

                    int new_cell_id = -1;
                    if (cell.getTable().getTableGroupId() == -1) {
                        new_cell_id = cell.getTable().getCell_root_id() - 25;
                    } else {
                        new_cell_id = cell.getTable().getTable_group_root_id() - 25;
                    }

                    Table table = cell.getTable();
                    moveTable(table, new_cell_id);
                    selectTable(table);
                }
            }

            if (ke.getKeyCode() == KeyEvent.VK_DOWN) {

                Cell cell = (Cell) ke.getSource();
                if (cell.isChosen) {
                    int new_cell_id = -1;
                    if (cell.getTable().getTableGroupId() == -1) {
                        new_cell_id = cell.getTable().getCell_root_id() + 25;
                    } else {
                        new_cell_id = cell.getTable().getTable_group_root_id() + 25;
                    }
                    Table table = cell.getTable();
                    moveTable(table, new_cell_id);
                    selectTable(table);
                }
            }

            if (ke.getKeyCode() == KeyEvent.VK_LEFT) {

                Cell cell = (Cell) ke.getSource();
                if (cell.isChosen) {
                    int new_cell_id = -1;
                    if (cell.getTable().getTableGroupId() == -1) {
                        new_cell_id = cell.getTable().getCell_root_id() - 1;
                    } else {
                        new_cell_id = cell.getTable().getTable_group_root_id() - 1;
                    }
                    Table table = cell.getTable();
                    moveTable(table, new_cell_id);
                    selectTable(table);
                }
            }

            if (ke.getKeyCode() == KeyEvent.VK_RIGHT) {

                Cell cell = (Cell) ke.getSource();
                if (cell.isChosen) {
                    int new_cell_id = -1;
                    if (cell.getTable().getTableGroupId() == -1) {
                        new_cell_id = cell.getTable().getCell_root_id() + 1;
                    } else {
                        new_cell_id = cell.getTable().getTable_group_root_id() + 1;
                    }
                    Table table = cell.getTable();
                    moveTable(table, new_cell_id);
                    selectTable(table);

                }
            }

            if (ke.getKeyCode() >= KeyEvent.VK_0 && ke.getKeyCode() <= KeyEvent.VK_9) {
                for (int i = 0; i < m.getTables().size(); i++) {
                    if (m.getTables().get(i).getTableNumber() == ke.getKeyCode() - 48 && m.getTables().get(i).getTableGroupId() == -1) {
                        selectTable(m.getTables().get(i));
                        break;
                    }
                }
            }
        }

        @Override
        public void keyReleased(KeyEvent ke) {
            isMultiTable = false;
        }

        @Override
        public void keyTyped(KeyEvent ke) {
        }
    }

    /**
     * update the table status
     */
    public void tickTableStatus() {
        if (current_tables.get(0).getTableStatus() == TableStatus.OCCUPIED) {
            rdb_occupied.setSelected(true);
        } else if (current_tables.get(0).getTableStatus() == TableStatus.RESERVED) {
            rdb_reserved.setSelected(true);
        } else if (current_tables.get(0).getTableStatus() == TableStatus.VACANT) {
            rdb_vacant.setSelected(true);
        }
    }
}
