/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.JDialogKeyListener;
import Utilities.JTextFieldLimit;
import Utilities.Localization;
import Utilities.SpringUtilities;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SpringLayout;

/**
 * this is the form for creating and editing order item information
 *
 * @author Pham
 */
public class CreateEditOrderItemForm extends JPanel {
    
    private JLabel lbl_item_name = new JLabel(Localization.getResourceBundle().getString("CreateEditOrderItemForm.lbl_item_name"));
    private JLabel lbl_quantity = new JLabel(Localization.getResourceBundle().getString("CreateEditOrderItemForm.lbl_quantity"));
    private JLabel lbl_item_status = new JLabel(Localization.getResourceBundle().getString("CreateEditOrderItemForm.lbl_item_status"));
    private JLabel lbl_note = new JLabel(Localization.getResourceBundle().getString("CreateEditOrderItemForm.lbl_note"));
    private JLabel lbl_quantity_error = new JLabel();
    private JTextField txt_item_name = new JTextField(15);
    private JTextField txt_quantity = new JTextField(15);
    private JTextArea txt_note = new JTextArea();
    private ButtonGroup group_item_status = new ButtonGroup();
    private JRadioButton rdb_done = new JRadioButton(Localization.getResourceBundle().getString("CreateEditOrderItemForm.rdb_done"));
    private JRadioButton rdb_not_done = new JRadioButton(Localization.getResourceBundle().getString("CreateEditOrderItemForm.rdb_not_done"));
    private JPanel pnl_main = new JPanel(new SpringLayout());
    private JPanel pnl_item_status = new JPanel(new GridLayout(1, 2));
    private JScrollPane spane_note = new JScrollPane(txt_note);
    private JOptionPane p;
    private JDialog d;
    
    public CreateEditOrderItemForm() {
        rdb_done.setBackground(Color.WHITE);
        rdb_not_done.setBackground(Color.WHITE);
        
        txt_note.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        
        this.setPreferredSize(new Dimension(350, 350));
        this.setMaximumSize(new Dimension(350, 350));
        this.setMinimumSize(new Dimension(350, 350));
        
        pnl_main.setPreferredSize(new Dimension(250, 350));
        pnl_main.setMaximumSize(new Dimension(250, 350));
        pnl_main.setMinimumSize(new Dimension(250, 350));
        
        spane_note.setPreferredSize(new Dimension(100, 100));
        spane_note.setMaximumSize(new Dimension(100, 100));
        spane_note.setMinimumSize(new Dimension(100, 100));
        txt_note.setLineWrap(true);
        
        txt_item_name.setPreferredSize(new Dimension(100, 25));
        txt_item_name.setMaximumSize(new Dimension(100, 25));
        txt_item_name.setMinimumSize(new Dimension(100, 25));
        
        txt_quantity.setPreferredSize(new Dimension(100, 25));
        txt_quantity.setMaximumSize(new Dimension(100, 25));
        txt_quantity.setMinimumSize(new Dimension(100, 25));
        
        pnl_main.add(lbl_item_name);
        pnl_main.add(txt_item_name);
        
        pnl_main.add(lbl_quantity);
        pnl_main.add(txt_quantity);
        pnl_main.add(lbl_quantity_error);
        
        pnl_item_status.setPreferredSize(new Dimension(250, 50));
        pnl_item_status.setMaximumSize(new Dimension(250, 50));
        pnl_item_status.setMinimumSize(new Dimension(250, 50));
        
        pnl_item_status.add(rdb_done);
        pnl_item_status.add(rdb_not_done);
        group_item_status.add(rdb_done);
        group_item_status.add(rdb_not_done);
        
        pnl_main.add(lbl_item_status);
        pnl_main.add(pnl_item_status);
        
        pnl_main.add(lbl_note);
        pnl_main.add(spane_note);
        
        SpringUtilities.makeCompactGrid(pnl_main, 9, 1, 0, 0, 0, 8);
        
        pnl_main.setOpaque(true);
        
        txt_item_name.setEditable(false);
        txt_item_name.setFocusable(false);
        rdb_not_done.setSelected(true);
        rdb_not_done.setEnabled(false);
        rdb_done.setEnabled(false);
        
        txt_quantity.setDocument(new JTextFieldLimit(3, "^[\\d]*$", false));
        txt_quantity.setText("1");
        txt_note.setDocument(new JTextFieldLimit(60, "", false));
        txt_note.setText("");
        
        txt_note.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                
                if (txt_note.getText().equalsIgnoreCase(Localization.getResourceBundle().getString("CreateEditOrderItemForm.wordManyCharacters"))) {
                    txt_note.setText("");
                    txt_note.validate();
                }
            }
            
            @Override
            public void focusLost(FocusEvent e) {
            }
        });
        
        this.add(pnl_main);
        
        String[] options = {Localization.getResourceBundle().getString("CreateEditOrderItemForm.options.Confirm"), Localization.getResourceBundle().getString("CreateEditOrderItemForm.options.Cancel")};
        p = new JOptionPane(this, JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION, null, options, null);
        d = p.createDialog(Localization.getResourceBundle().getString("CreateEditOrderItemForm.title"));
        this.getTxt_note().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getTxt_note().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getTxt_note().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getTxt_note().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getTxt_quantity().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getTxt_quantity().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getTxt_quantity().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getTxt_quantity().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getRdb_done().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getRdb_done().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getRdb_done().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getRdb_done().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getRdb_not_done().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getRdb_not_done().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getRdb_not_done().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getRdb_not_done().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
    }
    
    public void setTxt_item_name(String itemName) {
        txt_item_name.setText(itemName);
    }
    
    public JTextField getTxt_quantity() {
        return txt_quantity;
    }
    
    public void setTxt_quantity(String quantity) {
        txt_quantity.setText(quantity);
    }
    
    public JTextArea getTxt_note() {
        return txt_note;
    }
    
    public void setTxt_note(String note) {
        txt_note.setText(note);
    }
    
    public JRadioButton getRdb_done() {
        return rdb_done;
    }
    
    public JRadioButton getRdb_not_done() {
        return rdb_not_done;
    }
    
    public JLabel getLbl_quantity_error() {
        return lbl_quantity_error;
    }
    
    public JDialog getD() {
        return d;
    }
    
    public JOptionPane getP() {
        return p;
    }
    
}
