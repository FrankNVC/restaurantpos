/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Main.Application;
import Utilities.Localization;
import View.OrderView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author s3393322
 */
public class ViewOrderController implements ActionListener {

    /**
     * defines events when view order button is clicked
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (Application.cmv.getCurrent_tables().size() == 1) {
            JOptionPane.showMessageDialog(null, new OrderView(),Localization.getResourceBundle().getString("ViewOrderController.title"), JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("ViewOrderController.chooseTable"));
        }
    }
}
