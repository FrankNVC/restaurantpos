/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Main.Application;
import Model.Person.Gender;
import Model.Restaurant;
import Utilities.Localization;
import View.CreateEditCustomerForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author Frank NgVietCuong
 */
public class CreateCustomerController implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {

        Restaurant r = Application.r;
        CreateEditCustomerForm cecf = new CreateEditCustomerForm(false);

        boolean created = false;

        Object result;
        do {
            // Process the create category form
            cecf.getD().setVisible(true);
            result = (String) cecf.getP().getValue();

            if (result == cecf.getP().getOptions()[0]) {

                Gender g;
                if (cecf.getRdb_female().isSelected()) {
                    g = Gender.FEMALE;
                } else if (cecf.getRdb_male().isSelected()) {
                    g = Gender.MALE;
                } else {
                    r.displayError(cecf.getLbl_gender_error(), Localization.getResourceBundle().getString("CreateCustomerController.selectGender"));
                    continue;
                }
                cecf.getLbl_gender_error().setText(null);

                if (cecf.getTxt_full_name().getText().equalsIgnoreCase("")) {
                    r.displayError(cecf.getLbl_full_name_error(), Localization.getResourceBundle().getString("CreateCustomerController.fullNameRequired"));
                } else {
                    cecf.getLbl_full_name_error().setText(null);

                    if (cecf.getLbl_chosen_dob().getText().contains("Click")) {
                        r.displayError(cecf.getLbl_dob_error(), Localization.getResourceBundle().getString("CreateCustomerController.chooseDOB"));
                    } else {
                        cecf.getLbl_dob_error().setText(null);

                        String email = cecf.getTxt_email().getText().trim();
                        if (!email.matches("^[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+.[a-zA-Z]{2,4}$")) {
                            r.displayError(cecf.getLbl_email_error(), Localization.getResourceBundle().getString("CreateCustomerController.invalidEmail"));
                            cecf.getTxt_email().setSelectionStart(0);
                            cecf.getTxt_email().setSelectionEnd(cecf.getTxt_email().getText().length());
                            continue;
                        }
                        cecf.getLbl_email_error().setText(null);

                        String phoneNumber = (String) cecf.getCbx_phone().getSelectedItem() + cecf.getTxt_phone1().getText().trim() + cecf.getTxt_phone2().getText().trim();
                        if (phoneNumber.equalsIgnoreCase("null")) {
                            r.displayError(cecf.getLbl_phone_error(), Localization.getResourceBundle().getString("CreateCustomerController.phoneNumberRequired"));
                            continue;
                        }
                        if (!phoneNumber.matches("^(08[0-9]{8})|(((09[0-9]{1})|(012[0-8]{1})|(016[5-9]{1})|(01((88)|(99))))[0-9]{7})$")) {
                            r.displayError(cecf.getLbl_phone_error(), Localization.getResourceBundle().getString("CreateCustomerController.invalidPhoneNumber"));
                            continue;
                        }
                        if (!r.validatePhone(phoneNumber, null)) {
                            r.displayError(cecf.getLbl_phone_error(), "This phone number is already registered!");
                            continue;
                        }
                        cecf.getLbl_phone_error().setText(null);

                        String ssn = cecf.getTxt_ssn().getText().trim();
                        if (ssn.equalsIgnoreCase("")) {
                            r.displayError(cecf.getLbl_ssn_error(), Localization.getResourceBundle().getString("CreateCustomerController.ssnRequired"));
                        } else if (ssn.length() != 9 || ssn.equalsIgnoreCase("000000000")) {
                            r.displayError(cecf.getLbl_ssn_error(), Localization.getResourceBundle().getString("CreateCustomerController.invalidSSN"));
                            cecf.getTxt_ssn().setSelectionStart(0);
                            cecf.getTxt_ssn().setSelectionEnd(cecf.getTxt_ssn().getText().length());
                        } else if (!r.validateSSN(true, ssn, null)) {
                            r.displayError(cecf.getLbl_ssn_error(), Localization.getResourceBundle().getString("CreateCustomerController.ssnAlreadyExit"));
                            cecf.getTxt_ssn().setSelectionStart(0);
                            cecf.getTxt_ssn().setSelectionEnd(cecf.getTxt_ssn().getText().length());
                        } else {
                            cecf.getLbl_ssn_error().setText(null);
                            Date date = null;
                            if (!cecf.getLbl_chosen_dob().getText().contains("Click")) {
                                date = cecf.getCal_dob().getDate();
                            }
                            created = true;
                            r.createCustomer(g, ssn, date, cecf.getTxt_full_name().getText(), phoneNumber, email, cecf.getTxt_address().getText());
                            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("CreateCustomerController.createCustomerSuccessfully.message"), Localization.getResourceBundle().getString("CreateCustomerController.createCustomerSuccessfully.title"), JOptionPane.INFORMATION_MESSAGE);
                            r.update();
                        }
                    }
                }
            }
        } while (!created && result == cecf.getP().getOptions()[0]);
    }
}
