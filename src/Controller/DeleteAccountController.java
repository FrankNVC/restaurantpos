package Controller;

import Main.Application;
import Model.Restaurant;
import Utilities.Localization;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JTable;

/**
 *
 * @author Na
 */
public class DeleteAccountController extends KeyAdapter implements ActionListener {

    private Restaurant r;

    /**
     *
     * @param r
     */
    public DeleteAccountController(Restaurant r) {
        this.r = r;
    }

    /**
     * defines event when an account is deleted
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (Application.av.getPnl_list_account().getTable().getSelectedRow() != -1) {
            int result;
            if (Application.av.getPnl_list_account().getTable().getSelectedRows().length > 1) {
                result = r.showConfirmDialog("accounts");
            } else {
                result = r.showConfirmDialog("account");
            }
            if (result == JOptionPane.YES_OPTION) {
                JTable list_account = Application.av.getPnl_list_account().getTable();
                
                for (int i = 0; i < list_account.getSelectedRows().length; i++) {
                    String username = ((String) list_account.getValueAt(Application.av.getPnl_list_account().getTable().getSelectedRows()[i], 0));
                    String type = (String) list_account.getValueAt(Application.av.getPnl_list_account().getTable().getSelectedRows()[i], 4);
                    if (username.equalsIgnoreCase(Application.currentUser.getUsername())) {
                        JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("DeleteAccountController.showMessageDialog.delete.message"), Localization.getResourceBundle().getString("DeleteAccountController.showMessageDialog.delete.title"), JOptionPane.ERROR_MESSAGE);
                    } else {
                        r.deleteAccount(username, type);
                        r.update();
                    }
                }
                ((DefaultListModel) Application.av.getList_account_detail_right().getModel()).clear();
                Application.av.reload();
            }
        } else {
            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("DeleteAccountController.showMessageDialog.chooseAccount.message"), Localization.getResourceBundle().getString("DeleteAccountController.showMessageDialog.chooseAccount.title"), JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     *
     * @param e
     */
    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_DELETE) {
            if (Application.av.getPnl_list_account().getTable().getSelectedRow() > -1) {
                Application.av.getBtn_delete_account().doClick();
            }
        }
    }
}
