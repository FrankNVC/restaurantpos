/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Main.Application;
import Model.Order;
import Model.OrderItem;
import Utilities.Localization;
import Utilities.StringUtil;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.text.DecimalFormat;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

/**
 * this is the view for displaying order details
 * @author Pham
 */
public class OrderView extends JPanel implements Observer {

    private JTabbedPane tbpane_order_view = new JTabbedPane();
    private JScrollPane spane_order_detailed_view = new JScrollPane();
    private JScrollPane spane_order_kitchen_view = new JScrollPane();
    private JEditorPane epane_order_detailed_view = new JEditorPane();
    private JEditorPane epane_order_kitchen_view = new JEditorPane();
    
    private String restaurantName = "RePos";
    private String address = Localization.getResourceBundle().getString("OrderView.address");
    private String phoneNum = "37761300";
    private String staff = Application.currentUser.getFullName();
    private Order o = Application.cmv.getCurrent_tables().get(0).getO();
    private int tableNumber = Application.cmv.getCurrent_tables().get(0).getTableNumber();    

    public OrderView() {
        this.setLayout(new BorderLayout());

        this.setPreferredSize(new Dimension(650, 650));
        this.setMaximumSize(new Dimension(650, 650));
        this.setMinimumSize(new Dimension(650, 650));

        tbpane_order_view.addTab(Localization.getResourceBundle().getString("OrderView.tbpane_order_view.orderView"), spane_order_detailed_view);
        tbpane_order_view.addTab(Localization.getResourceBundle().getString("OrderView.tbpane_order_view.kitchenView"), spane_order_kitchen_view);        
        epane_order_detailed_view.setContentType("text/html");
        epane_order_detailed_view.setEditable(false);
        epane_order_kitchen_view.setContentType("text/html");
        epane_order_kitchen_view.setEditable(false);        
        spane_order_detailed_view.setViewportView(epane_order_detailed_view);
        spane_order_kitchen_view.setViewportView(epane_order_kitchen_view);
        
        this.add(tbpane_order_view, BorderLayout.CENTER);

        OrderDetailedView();
        OrderKitchenView();

        this.revalidate();
    }

    public void OrderDetailedView() {
        
        String localization_orderdetailedview_restaurant = Localization.getResourceBundle().getString("OrderView.localization_orderdetailedview_restaurant");
        String localization_orderdetailedview_tel = Localization.getResourceBundle().getString("OrderView.localization_orderdetailedview_tel");
        String localization_orderdetailedview_staff = Localization.getResourceBundle().getString("OrderView.localization_orderdetailedview_staff");
        String localization_orderdetailedview_table = Localization.getResourceBundle().getString("OrderView.localization_orderdetailedview_table");  
        String localization_orderdetailedview_no = Localization.getResourceBundle().getString("OrderView.localization_orderdetailedview_no");
        String localization_orderdetailedview_name = Localization.getResourceBundle().getString("OrderView.localization_orderdetailedview_name");
        String localization_orderdetailedview_note = Localization.getResourceBundle().getString("OrderView.localization_orderdetailedview_note");
        String localization_orderdetailedview_quantity = Localization.getResourceBundle().getString("OrderView.localization_orderdetailedview_quantity");
        String localization_orderdetailedview_price = Localization.getResourceBundle().getString("OrderView.localization_orderdetailedview_price");
        String localization_orderdetailedview_amount = Localization.getResourceBundle().getString("OrderView.localization_orderdetailedview_amount");
        String localization_orderdetailedview_subtotal = Localization.getResourceBundle().getString("OrderView.localization_orderdetailedview_subtotal");
        String localization_orderdetailedview_total = Localization.getResourceBundle().getString("OrderView.localization_orderdetailedview_total");

        String header = "<html><center><p style='font-size:30px; font-weight:bold;'>" + localization_orderdetailedview_restaurant + "" + restaurantName + "</p>"
                + "<p style='font-size:20px;'>" + address + "<br/>"
                + localization_orderdetailedview_tel + phoneNum + "</p></center><br/>"
                + "<p style='font-size:15px;'>" + localization_orderdetailedview_staff + "" + staff + "<br/>"
                + localization_orderdetailedview_table + tableNumber + "</p><br/>";

        String table = "<body><table width='500'>"
                + "<tr>"
                + "<td colspan='6'>-----------------------------------------------------------------------------------------------------------------------</td>"
                + "</tr>"
                + "<tr>"
                + "<td>" + localization_orderdetailedview_no + "</td>" + "<td>" + localization_orderdetailedview_name + "</td>" + "<td>" + localization_orderdetailedview_note + "</td>" + "<td>" + localization_orderdetailedview_quantity + "</td>" + "<td>" + localization_orderdetailedview_price + "</td>" + "<td>" + localization_orderdetailedview_amount + "</td>"
                + "</tr>"
                + "<tr>"
                + "<td colspan='6'>-----------------------------------------------------------------------------------------------------------------------</td>"
                + "</tr>";

        int subTotal = 0;

        for (int i = 0; i < o.getOrderItems().size(); i++) {
            OrderItem orderItem = o.getOrderItems().get(i);
            subTotal += orderItem.getItem().getItemPrice() * orderItem.getQuantity();
            String price = new DecimalFormat("###,###,###").format(orderItem.getItem().getItemPrice());
            if(price.contains(","))
            {
                price = price.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
            }
            else
            {
                price = price.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
            }
            
            String amount = new DecimalFormat("###,###,###").format(orderItem.getItem().getItemPrice() * orderItem.getQuantity());
            if(amount.contains(","))
            {
                amount = amount.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
            }
            else
            {
                amount = amount.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
            }

            table += "<tr>"
                    + "<td>" + orderItem.getOrderItemId() + "</td>"
                    + "<td>" + StringUtil.FinalStringNote(orderItem.getItem().getItemName()) + "</td>"
                    + "<td>" + StringUtil.FinalStringNote(orderItem.getNote()) + "</td>"
                    + "<td>" + orderItem.getQuantity() + "</td>"
                    + "<td>" + price + "</td>"
                    + "<td>" + amount + "</td>"
                    + "</tr>";
        }

        String sub = new DecimalFormat("###,###,###").format(subTotal);
        if(sub.contains(","))
        {
            sub = sub.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
        }
        else
        {
            sub = sub.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
        }
                
        String vat = new DecimalFormat("###,###,###").format(subTotal * 10 / 100);
        if(vat.contains(","))
        {
            vat = vat.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
        }
        else
        {
            vat = vat.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
        }
                
        String total = new DecimalFormat("###,###,###").format(subTotal + subTotal * 10 / 100);
        if(total.contains(","))
        {
            total = total.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
        }
        else
        {
            total = total.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
        }

        table +=
                "<tr>"
                + "<td colspan='6'>-----------------------------------------------------------------------------------------------------------------------</td>"
                + "</tr>"
                + "<tr>"
                + "<td></td>" + "<td></td>" + "<td></td>" + "<td></td>" + "<td>" + localization_orderdetailedview_subtotal + "</td>" + "<td>" + sub + "</td>"
                + "</tr>"
                + "<tr>"
                + "<td></td>" + "<td></td>" + "<td></td>" + "<td></td>" + "<td>VAT (10%)</td>" + "<td>" + vat + "</td>"
                + "</tr>"
                + "<tr>"
                + "<td></td>" + "<td></td>" + "<td></td>" + "<td></td>" + "<td colspan='2'>__________________________</td>"
                + "</tr>"
                + "<tr>"
                + "<td></td>" + "<td></td>" + "<td></td>" + "<td></td>" + "<td>" + localization_orderdetailedview_total + "</td>" + "<td>" + total + "</td>"
                + "</tr>";

        table += "</table></body></html>";

        String fullHTML = header + table;

        epane_order_detailed_view.setText(fullHTML);
        epane_order_detailed_view.revalidate();
    }

    public void OrderKitchenView() {
        
        String localization_orderkitchenview_restaurant = Localization.getResourceBundle().getString("OrderView.localization_orderkitchenview_restaurant");
        String localization_orderkitchenview_tel = Localization.getResourceBundle().getString("OrderView.localization_orderkitchenview_tel");
        String localization_orderkitchenview_staff = Localization.getResourceBundle().getString("OrderView.localization_orderkitchenview_staff");
        String localization_orderkitchenview_table = Localization.getResourceBundle().getString("OrderView.localization_orderkitchenview_table");
        String localization_orderkitchenview_no = Localization.getResourceBundle().getString("OrderView.localization_orderkitchenview_no");
        String localization_orderkitchenview_name = Localization.getResourceBundle().getString("OrderView.localization_orderkitchenview_name");
        String localization_orderkitchenview_note = Localization.getResourceBundle().getString("OrderView.localization_orderkitchenview_note");
        String localization_orderkitchenview_quantity = Localization.getResourceBundle().getString("OrderView.localization_orderkitchenview_quantity");
        String localization_orderkitchenview_status = Localization.getResourceBundle().getString("OrderView.localization_orderkitchenview_status");

        String header = "<html><center><p style='font-size:30px; font-weight:bold;'>" + localization_orderkitchenview_restaurant + "" + restaurantName + "</p>"
                + "<p style='font-size:20px;'>" + address + "<br/>"
                + localization_orderkitchenview_tel + phoneNum + "</p></center><br/>"
                + "<p style='font-size:15px;'>" + localization_orderkitchenview_staff + "" + staff + "<br/>"
                + localization_orderkitchenview_table + tableNumber + "</p><br/>";

        String table = "<body><table width='500'>"
                + "<tr>"
                + "<td>" + localization_orderkitchenview_no + "</td>" + "<td>" + localization_orderkitchenview_name + "</td>" + "<td>" + localization_orderkitchenview_note + "</td>" + "<td>" + localization_orderkitchenview_quantity + "</td>" + "<td>" + localization_orderkitchenview_status + "</td>"
                + "</tr>"
                + "<tr>"
                + "<td colspan='5'>-------------------------------------------------------------------------------------------------------------------------------</td>"
                + "</tr>";

        for (int i = 0; i < o.getOrderItems().size(); i++) {
            OrderItem orderItem = o.getOrderItems().get(i);
            String status;
            if (orderItem.getIsDone()) {
                status = Localization.getResourceBundle().getString("OrderView.statusDone");
            } else {
                status = Localization.getResourceBundle().getString("OrderView.statusNotDone");
            }

            table += "<tr>"
                    + "<td>" + orderItem.getOrderItemId() + "</td>"
                    + "<td>" + StringUtil.FinalStringNote(orderItem.getItem().getItemName()) + "</td>"
                    + "<td>" + StringUtil.FinalStringNote(orderItem.getNote()) + "</td>"
                    + "<td>" + orderItem.getQuantity() + "</td>"
                    + "<td>" + status + "</td>"
                    + "</tr>";
        }

        table +=
                "<tr>"
                + "<td colspan='5'>-------------------------------------------------------------------------------------------------------------------------------</td>"
                + "</tr>";

        table += "</table></body></html>";

        String fullHTML = header + table;

        epane_order_kitchen_view.setText(fullHTML);
    }

    @Override
    public void update(Observable o, Object o1) {
        this.repaint();
    }
}
