package Controller;

import Main.Application;
import Model.Restaurant;
import Model.Table;
import Utilities.Localization;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.*;

/**
 *
 * @author Nguyen Minh Sang
 */
public class DeleteTableController extends AbstractAction implements ActionListener {

    private Restaurant r;

    /**
     *
     * @param r
     */
    public DeleteTableController(Restaurant r) {
        this.r = r;
    }

    /**
     * defines events when a table is deleted
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (!Application.cmv.getCurrent_tables().isEmpty()) {
            ArrayList<Table> delete_tables = new ArrayList<>();
            for (int i = 0; i < Application.cmv.getCurrent_tables().size(); i++) {
                delete_tables.add(Application.cmv.getCurrent_tables().get(i));
            }
            if (r.showConfirmDialog("table") == JOptionPane.YES_OPTION) {
                for (int i = 0; i < delete_tables.size(); i++) {
                    r.deleteTable(delete_tables.get(i));
                    Application.cmv.deleteTable(delete_tables.get(i));
                    r.sortTables(r.getTables());
                    r.update();
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("DeleteTableController.chooseOneTable.message"), Localization.getResourceBundle().getString("DeleteTableController.chooseOneTable.title"), JOptionPane.INFORMATION_MESSAGE);
        }
    }
}
