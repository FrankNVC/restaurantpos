/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.PrintReportController;
import Controller.ReportController;
import Main.Application;
import Model.Restaurant;
import Utilities.TimeIgnoringComparator;
import com.toedter.calendar.JCalendar;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import Utilities.Localization;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 *
 * @author Frank NgVietCuong
 */
public class ReportView extends JFrame implements Observer {

    private JScrollPane spane_report = new JScrollPane();
    private JEditorPane epane_report = new JEditorPane();
    private JPanel pnl_input_date = new JPanel();
    private JPanel pnl_button = new JPanel(new GridLayout(2, 1));
    private JCalendar date_start = new JCalendar();
    private JCalendar date_end = new JCalendar();
    private JButton btn_search = new JButton();
    private JButton btn_print = new JButton();
    private String fullHTML = "";
    private static boolean opening = false;

    public ReportView() {
        this.setPreferredSize(new Dimension(550, 650));
        this.setMaximumSize(new Dimension(550, 650));
        this.setMinimumSize(new Dimension(550, 650));

        btn_search.addActionListener(new SearchButtonListener());
        btn_print.addActionListener(new PrintReportController(this));

        Date temp_date = new Date();
        date_start.setMaxSelectableDate(temp_date);
        date_end.setMaxSelectableDate(temp_date);

        pnl_button.add(btn_search);
        pnl_button.add(btn_print);

        pnl_input_date.add(date_start);
        pnl_input_date.add(date_end);
        pnl_input_date.add(pnl_button);

        epane_report.setContentType("text/html");
        epane_report.setEditable(false);
        spane_report.setViewportView(epane_report);

        btn_search.setText(Localization.getResourceBundle().getString("Report.btnSearch"));
        btn_print.setText(Localization.getResourceBundle().getString("Report.btnPrint"));

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                String options[] = {Localization.getResourceBundle().getString("Report.options.Confirm"), Localization.getResourceBundle().getString("Report.options.Cancel")};               
                if (JOptionPane.showOptionDialog(null, Localization.getResourceBundle().getString("Report.exit"), Localization.getResourceBundle().getString("Report.Confirmation"), JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE, null, options, null) == JOptionPane.YES_OPTION) {
                    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    ReportView.setOpening(false);
                    super.windowClosing(e);
                }
            }
        });

        this.add(spane_report, BorderLayout.CENTER);
        this.add(pnl_input_date, BorderLayout.NORTH);
    }

    public String getFullHTML() {
        return fullHTML;
    }

    public JEditorPane getEpane_report() {
        return epane_report;
    }

    public static boolean isOpening() {
        return opening;
    }

    public static void setOpening(boolean val) {
        opening = val;
    }

    public void CreateReport(String sale1, ArrayList<String[]> sale2, ArrayList<String[]> sale3) {
        Restaurant r = Application.r;

        String total = "<html><body><center><p style='font-size:30px; font-weight:bold;'>" + Localization.getResourceBundle().getString("Report.reportTitle") + "</p></center>"
                + "<h1>" + Localization.getResourceBundle().getString("Report.totalSale") + ": " + sale1 + Localization.getResourceBundle().getString("CashierManagerView.vnd") + "</h1><br/>";

        String table1 = "<h1>" + Localization.getResourceBundle().getString("Report.dailyShiftAmount") + "</h1><br/>"
                + "<table border='1' width='500'>"
                + "<tr>"
                + "<td>" + Localization.getResourceBundle().getString("Report.date") + "</td><td>" + Localization.getResourceBundle().getString("Report.shift1") + "</td><td>" + Localization.getResourceBundle().getString("Report.shift2") + "</td>"
                + "</tr>";

        for (int i = 0; i < sale2.size(); i++) {
            table1 += "<tr>"
                    + "<td>" + sale2.get(i)[0] + "</td>"
                    + "<td>" + sale2.get(i)[1] + Localization.getResourceBundle().getString("CashierManagerView.vnd") + "</td>"
                    + "<td>" + sale2.get(i)[2] + Localization.getResourceBundle().getString("CashierManagerView.vnd") + "</td>"
                    + "</tr>";
        }

        table1 += "</table>";

        String table2 = "<h1>" + Localization.getResourceBundle().getString("Report.itemSaleAmount") + "</h1><br/>"
                + "<table border='1' width='500'>"
                + "<tr>"
                + "<td>" + Localization.getResourceBundle().getString("Report.item") + "</td><td>" + Localization.getResourceBundle().getString("Report.quantity") + "</td><td>" + Localization.getResourceBundle().getString("Report.amount") + "</td><td>" + Localization.getResourceBundle().getString("Report.category") + "</td>"
                + "</tr>";

        for (int i = 0; i < sale3.size(); i++) {
            table2 += "<tr>"
                    + "<td>" + sale3.get(i)[0] + "</td>"
                    + "<td>" + sale3.get(i)[1] + "</td>"
                    + "<td>" + sale3.get(i)[2] + Localization.getResourceBundle().getString("CashierManagerView.vnd") + "</td>"
                    + "<td>" + sale3.get(i)[3] + "</td>"
                    + "</tr>";
        }

        table2 += "</table>";

        fullHTML += total + table1 + table2;

        epane_report.setText(fullHTML);
        epane_report.revalidate();
    }

    @Override
    public void update(Observable o, Object arg) {
        this.repaint();
    }

    class SearchButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Date start = date_start.getDate();
            Date end = date_end.getDate();
            Calendar cal_start = Calendar.getInstance();
            cal_start.setTime(start);
            Calendar cal_end = Calendar.getInstance();
            cal_end.setTime(end);
            TimeIgnoringComparator tic = new TimeIgnoringComparator();
            if (tic.compare(cal_start, cal_end) <= 0) {

                String sale1 = new DecimalFormat("###,###,###").format(Application.r.calculateTotalSaleAmount(cal_start, cal_end));
                if (sale1.contains(",")) {
                    sale1 = sale1.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                } else {
                    sale1 = sale1.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                }

                ArrayList<String[]> sale2 = Application.r.calculateDailyShiftAmount(cal_start, cal_end);
                ArrayList<String[]> sale3 = Application.r.calculateItemSaleAmount(cal_start, cal_end);

                CreateReport(sale1, sale2, sale3);
            } else {
                JOptionPane.showMessageDialog(null, "Invalid date");
            }
        }
    }
}
