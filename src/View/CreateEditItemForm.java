/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.JDialogKeyListener;
import Utilities.JTextFieldLimit;
import Utilities.Localization;
import Utilities.SpringUtilities;
import java.awt.Dimension;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SpringLayout;

/**
 * this is the form for creating and editing item
 *
 * @author Pham
 */
public class CreateEditItemForm extends JPanel {

    private JLabel lbl_item_name = new JLabel(Localization.getResourceBundle().getString("CreateEditItemForm.lbl_item_name"));
    private JLabel lbl_old_item_name = new JLabel(Localization.getResourceBundle().getString("CreateEditItemForm.lbl_old_item_name"));
    private JLabel lbl_item_category = new JLabel(Localization.getResourceBundle().getString("CreateEditItemForm.lbl_item_category"));
    private JLabel lbl_item_price = new JLabel(Localization.getResourceBundle().getString("CreateEditItemForm.lbl_item_price"));
    private JLabel lbl_item_name_error = new JLabel();
    private JLabel lbl_item_category_error = new JLabel();
    private JLabel lbl_item_price_error = new JLabel();
    private JTextField txt_item_name = new JTextField(15);
    private JTextField txt_old_item_name = new JTextField(15);
    private Vector cbx_items = new Vector();
    private DefaultComboBoxModel model = new DefaultComboBoxModel(cbx_items);
    private JComboBox cbx_item_category = new JComboBox(model);
    private JTextField txt_item_price = new JTextField(15);
    private JPanel pnl_main = new JPanel(new SpringLayout());
    private JOptionPane p;
    private JDialog d;

    public CreateEditItemForm(boolean edit) {

        this.setPreferredSize(new Dimension(200, 250));
        this.setMaximumSize(new Dimension(200, 250));
        this.setMinimumSize(new Dimension(200, 250));

        pnl_main.setPreferredSize(new Dimension(150, 240));
        pnl_main.setMaximumSize(new Dimension(150, 240));
        pnl_main.setMinimumSize(new Dimension(150, 240));

        txt_old_item_name.setPreferredSize(new Dimension(150, 25));
        txt_old_item_name.setMaximumSize(new Dimension(150, 25));
        txt_old_item_name.setMinimumSize(new Dimension(150, 25));

        txt_item_name.setPreferredSize(new Dimension(150, 25));
        txt_item_name.setMaximumSize(new Dimension(150, 25));
        txt_item_name.setMinimumSize(new Dimension(150, 25));

        cbx_item_category.setPreferredSize(new Dimension(150, 25));
        cbx_item_category.setMaximumSize(new Dimension(150, 25));
        cbx_item_category.setMinimumSize(new Dimension(150, 25));

        txt_item_price.setPreferredSize(new Dimension(150, 25));
        txt_item_price.setMaximumSize(new Dimension(150, 25));
        txt_item_price.setMinimumSize(new Dimension(150, 25));
        txt_item_price.setDocument(new JTextFieldLimit(10, "^[\\d ,]*$", true));

        txt_old_item_name.setEditable(!edit);
        txt_old_item_name.setFocusable(false);

        if (edit) {
            lbl_item_name = new JLabel(Localization.getResourceBundle().getString("CreateEditItemForm.lbl_item_name_1"));
            pnl_main.add(lbl_old_item_name);
            pnl_main.add(txt_old_item_name);
        }

        pnl_main.add(lbl_item_name);
        pnl_main.add(txt_item_name);
        pnl_main.add(lbl_item_name_error);

        pnl_main.add(lbl_item_category);
        pnl_main.add(cbx_item_category);
        pnl_main.add(lbl_item_category_error);

        pnl_main.add(lbl_item_price);
        pnl_main.add(txt_item_price);
        pnl_main.add(lbl_item_price_error);

        if (edit) {
            SpringUtilities.makeCompactGrid(pnl_main, 11, 1, 0, 0, 0, 3);
        } else {
            SpringUtilities.makeCompactGrid(pnl_main, 9, 1, 0, 30, 0, 5);
        }

        pnl_main.setOpaque(true);

        this.add(pnl_main);

        String[] options = {Localization.getResourceBundle().getString("CreateEditItemForm.options.Confirm"), Localization.getResourceBundle().getString("CreateEditItemForm.options.Cancel")};
        ImageIcon icon_create_edit_category = new ImageIcon(this.getClass().getResource("/Images/create_edit_item.jpg"));
        p = new JOptionPane(this, JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION, icon_create_edit_category, options, null);
        d = p.createDialog(Localization.getResourceBundle().getString("CreateEditItemForm.createNewItem"));
        this.getCbx_item_category().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getCbx_item_category().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getCbx_item_category().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getCbx_item_category().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getTxt_item_name().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getTxt_item_name().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getTxt_item_name().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getTxt_item_name().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getTxt_item_price().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getTxt_item_price().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getTxt_item_price().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getTxt_item_price().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
    }

    public JTextField getTxt_old_item_name() {
        return txt_old_item_name;
    }

    public JTextField getTxt_item_name() {
        return txt_item_name;
    }

    public JComboBox getCbx_item_category() {
        return cbx_item_category;
    }

    public JTextField getTxt_item_price() {
        return txt_item_price;
    }

    public JLabel getLbl_item_name_error() {
        return lbl_item_name_error;
    }

    public JLabel getLbl_item_price_error() {
        return lbl_item_price_error;
    }

    public Vector getCbx_items() {
        return cbx_items;
    }

    public JOptionPane getP() {
        return p;
    }

    public JDialog getD() {
        return d;
    }

}
