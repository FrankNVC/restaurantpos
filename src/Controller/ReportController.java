/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Main.Application;
import Utilities.Localization;
import View.ReportView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Frank NgVietCuong
 */
public class ReportController implements ActionListener {

//    Calendar date_start;
//    Calendar date_end;
    private ReportView rv;
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        if (!ReportView.isOpening()) {
            rv = new ReportView();
            ReportView.setOpening(true);
            rv.setResizable(false);
            rv.pack();
            rv.setLocationRelativeTo(null);
            rv.setVisible(true);
            rv.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            
        }
    }
}
