package Model;

import Main.Application;
import Model.Account.AccountType;
import Model.Customer.CustomerType;
import Model.Person.Gender;
import Model.Table.TableShape;
import Model.Table.TableStatus;
import Utilities.DateIgnoringComparator;
import Utilities.Localization;
import Utilities.StringUtil;
import Utilities.TimeIgnoringComparator;
import java.awt.Color;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Observable;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author Nguyen Minh Sang
 */
public class Restaurant extends Observable {

    private static final String administratorFile = "Administrator.bin";
    private static final String cashierFile = "Cashier.bin";
    private static final String managerFile = "Manager.bin";
    private static final String customerFile = "Customer.bin";
    private static final String tableFile = "Table.bin";
    private static final String menuFile = "Menu.bin";
    private static final String categoryFile = "Category.bin";
    private static final String itemFile = "Item.bin";
    private static final String discountFile = "Discount.bin";
    private static final String billFile = "Bill.bin";
    private ArrayList<Administrator> administrators = new ArrayList<>();
    private ArrayList<Cashier> cashiers = new ArrayList<>();
    private ArrayList<Manager> managers = new ArrayList<>();
    private RestaurantMenu restaurantMenu = new RestaurantMenu();
    private ArrayList<Table> tables = new ArrayList<>();
    private HashMap<String, Integer> discounts = new HashMap<>();
    private ArrayList<Bill> bills = new ArrayList<>();
    private ArrayList<Customer> customers = new ArrayList<>();

    public ArrayList<Administrator> getAdministrators() {
        return administrators;
    }

    public ArrayList<Cashier> getCashiers() {
        return cashiers;
    }

    public ArrayList<Manager> getManagers() {
        return managers;
    }

    public RestaurantMenu getMenu() {
        return restaurantMenu;
    }

    public ArrayList<Table> getTables() {
        return tables;
    }

    public HashMap<String, Integer> getDiscount() {
        return discounts;
    }

    public ArrayList<Bill> getBills() {
        return bills;
    }

    public ArrayList<Customer> getCustomers() {
        return customers;
    }

    /**
     * Sets the static variable ADMINISTRATOR_ID of class Administrator
     */
    public int getAdministartorID() {
        // Find the maximum vacant ID if there are more than 1 administrator
        if (administrators.size() > 1) {
            // start from 1, loop to the size of the administrator list
            for (int i = 1; i <= administrators.size(); i++) {
                boolean missing = true;
                // check if the current ID number is missing by looping through the administator list
                for (int j = 0; j < administrators.size(); j++) {
                    if (!administrators.get(j).getUsername().equalsIgnoreCase("admin")) {
                        if (Integer.parseInt(administrators.get(j).getUsername().substring(1)) == i) {
                            missing = false;
                            break;
                        }
                    }
                }
                if (missing) {
                    return i;
                }
            }
        }
        return 1;
    }

    /**
     * Sets the static variable CASHIER_ID of class Cashier
     */
    public int getCashierID() {
        if (!cashiers.isEmpty()) {
            // start from 1, loop to the size of the cashier list
            for (int i = 1; i <= cashiers.size(); i++) {
                boolean missing = true;
                // check if the current ID number is missing by looping through the cashier list
                for (int j = 0; j < cashiers.size(); j++) {
                    if (Integer.parseInt(cashiers.get(j).getUsername().substring(1)) == i) {
                        missing = false;
                        break;
                    }
                }
                if (missing) {
                    return i;
                } else if (i == cashiers.size()) {
                    return i + 1;
                }
            }
        }
        return 1;
    }

    /**
     * Sets the static variable MANAGER_ID of class Manager
     */
    public int getManagerID() {
        if (!managers.isEmpty()) {
            // start from 1, loop to the size of the manager list
            for (int i = 1; i <= managers.size(); i++) {
                boolean missing = true;
                // check if the current ID number is missing by looping through the manager list
                for (int j = 0; j < managers.size(); j++) {
                    if (Integer.parseInt(managers.get(j).getUsername().substring(1)) == i) {
                        missing = false;
                        break;
                    }
                }
                if (missing) {
                    return i;
                } else if (i == managers.size()) {
                    return i + 1;
                }
            }
        }
        return 1;
    }

    /**
     *
     */
    public int getTableNumber() {
        if (!tables.isEmpty()) {
            for (int i = 1; i <= tables.size(); i++) {
                boolean missing = true;
                for (int j = 0; j < tables.size(); j++) {
                    if (tables.get(j).getTableNumber() == i) {
                        missing = false;
                        break;
                    }
                }
                if (missing) {
                    return i;
                } else if (i == tables.size()) {
                    return i + 1;
                }
            }
        }
        return 1;
    }

    /**
     * sorts the Table objects in tables ArrayList in tableNumber order
     *
     * @param tables_to_sort
     */
    public void sortTables(ArrayList<Table> tables_to_sort) {
        for (int i = 1; i < tables_to_sort.size(); i++) {
            if (tables_to_sort.get(i).compareTo(tables_to_sort.get(i - 1)) < 1) {
                for (int j = i; j > 0; j--) {
                    if (tables_to_sort.get(j).compareTo(tables_to_sort.get(j - 1)) < 1) {
                        Table temp = tables_to_sort.remove(j);
                        tables_to_sort.add(j, tables_to_sort.get(j - 1));
                        tables_to_sort.remove(j - 1);
                        tables_to_sort.add(j - 1, temp);
                    } else {
                        break;
                    }
                }
            }
        }
    }

    /**
     * saves the administrators ArrayList to binary file
     *
     * @return true when successful
     * @return false when fail
     */
    public boolean saveAdministratorToFile() {
        try {
            FileOutputStream fout = new FileOutputStream(administratorFile, false);
            ObjectOutputStream oos = new ObjectOutputStream(fout);
            oos.writeObject(administrators);
        } catch (IOException ex) {
            return false;
        }
        return true;
    }

    /**
     * saves the cashiers ArrayList to binary file
     *
     * @return true when successful
     * @return false when fail
     */
    public boolean saveCashierToFile() {
        try {
            FileOutputStream fout = new FileOutputStream(cashierFile, false);
            try (ObjectOutputStream oos = new ObjectOutputStream(fout)) {
                oos.writeObject(cashiers);
            }
        } catch (IOException ex) {
            return false;
        }
        return true;
    }

    /**
     * saves the managers ArrayList to binary file
     *
     * @return true when successful
     * @return false when fail
     */
    public boolean saveManagerToFile() {
        try {
            FileOutputStream fout = new FileOutputStream(managerFile, false);
            try (ObjectOutputStream oos = new ObjectOutputStream(fout)) {
                oos.writeObject(managers);
            }
        } catch (IOException ex) {
            return false;
        }
        return true;
    }

    public boolean saveCustomerToFile() {
        try {
            FileOutputStream fout = new FileOutputStream(customerFile, false);
            try (ObjectOutputStream oos = new ObjectOutputStream(fout)) {
                oos.writeObject(customers);
            }
        } catch (IOException ex) {
            return false;
        }
        return true;
    }

    /**
     * save the tables ArrayList to binary file
     *
     * @return true when successful
     * @return false when fail
     */
    public boolean saveTableToFile() {
        try {
            FileOutputStream fout = new FileOutputStream(tableFile, false);
            try (ObjectOutputStream oos = new ObjectOutputStream(fout)) {
                oos.writeObject(tables);
            }
        } catch (IOException ex) {
            return false;
        }
        return true;
    }

    /**
     * saves discounts HashMap to binary file
     *
     * @return true when successful
     * @return false when fail
     */
    public boolean saveDiscountToFile() {
        try {
            FileOutputStream fout = new FileOutputStream(discountFile, false);
            try (ObjectOutputStream oos = new ObjectOutputStream(fout)) {
                oos.writeObject(discounts);
            }
        } catch (IOException ex) {
            return false;
        }
        return true;
    }

    /**
     * saves menu object to binary file
     *
     * @return true when successful
     * @return false when fail
     */
    public boolean saveMenuToFile() {
        try {
            FileOutputStream fout = new FileOutputStream(menuFile, false);
            try (ObjectOutputStream oos = new ObjectOutputStream(fout)) {
                oos.writeObject(restaurantMenu);
            }
        } catch (IOException ex) {
            return false;
        }
        return true;
    }

    /**
     * saves categories ArrayList to binary file
     *
     * @return true when successful
     * @return false when fail
     */
    public boolean saveCategoryToFile() {
        try {
            FileOutputStream fout = new FileOutputStream(categoryFile, false);
            try (ObjectOutputStream oos = new ObjectOutputStream(fout)) {
                oos.writeObject(restaurantMenu.getCategories());
            }
        } catch (IOException ex) {
            return false;
        }
        return true;
    }

    /**
     * saves items ArrayList to binary file
     *
     * @return true when successful
     * @return false when fail
     */
    public boolean saveItemToFile() {
        try {
            FileOutputStream fout = new FileOutputStream(itemFile, false);
            try (ObjectOutputStream oos = new ObjectOutputStream(fout)) {
                oos.writeObject(restaurantMenu.getItems());
            }
        } catch (IOException ex) {
            return false;
        }
        return true;
    }

    public boolean saveBillToFile() {
        try {
            FileOutputStream fout = new FileOutputStream(billFile, false);
            try (ObjectOutputStream oos = new ObjectOutputStream(fout)) {
                oos.writeObject(bills);
            }
        } catch (IOException ex) {
            return false;
        }
        return true;
    }

    /**
     * reads information from file to administrators ArrayList
     */
    public void readAdministratorFromFile() {
        ArrayList<Administrator> a = new ArrayList<>();
        try {
            FileInputStream fin = new FileInputStream(administratorFile);
            ObjectInputStream ois = new ObjectInputStream(fin);
            try {
                a = (ArrayList<Administrator>) ois.readObject();
                ois.close();
            } catch (ClassNotFoundException ex) {
            } finally {
                administrators = a;
            }
        } catch (IOException ex) {
        }
    }

    /**
     * reads information from file to cashiers ArrayList
     */
    public void readCashierFromFile() {
        ArrayList<Cashier> c = new ArrayList<>();
        try {
            FileInputStream fin = new FileInputStream(cashierFile);
            ObjectInputStream ois = new ObjectInputStream(fin);
            try {
                c = (ArrayList<Cashier>) ois.readObject();
                ois.close();
            } catch (ClassNotFoundException ex) {
            } finally {
                cashiers = c;
            }
        } catch (IOException ex) {
        }
    }

    /**
     * reads information from file to managers ArrayList
     */
    public void readManagerFromFile() {
        ArrayList<Manager> ma = new ArrayList<>();
        try {
            FileInputStream fin = new FileInputStream(managerFile);
            ObjectInputStream ois = new ObjectInputStream(fin);
            try {
                ma = (ArrayList<Manager>) ois.readObject();
                ois.close();
            } catch (ClassNotFoundException ex) {
            } finally {
                managers = ma;
            }
        } catch (IOException ex) {
        }
    }

    public void readCustomerFromFile() {
        ArrayList<Customer> c = new ArrayList<>();
        try {
            FileInputStream fin = new FileInputStream(customerFile);
            ObjectInputStream ois = new ObjectInputStream(fin);
            try {
                c = (ArrayList<Customer>) ois.readObject();
                ois.close();
            } catch (ClassNotFoundException ex) {
            } finally {
                customers = c;
            }
        } catch (IOException ex) {
        }
    }

    /**
     * reads information from file to tables ArrayList
     */
    public void readTableFromFile() {
        ArrayList<Table> t = new ArrayList<>();
        try {
            FileInputStream fin = new FileInputStream(tableFile);
            ObjectInputStream ois = new ObjectInputStream(fin);
            try {
                t = (ArrayList<Table>) ois.readObject();
                ois.close();
            } catch (ClassNotFoundException ex) {
            } finally {
                tables = t;
            }
        } catch (IOException ex) {
        }
    }

    /**
     * reads information from file to discount HashMap
     */
    public void readDiscountFromFile() {
        HashMap<String, Integer> d = new HashMap<>();
        try {
            FileInputStream fin = new FileInputStream(discountFile);
            ObjectInputStream ois = new ObjectInputStream(fin);
            try {
                d = (HashMap<String, Integer>) ois.readObject();
                ois.close();
            } catch (ClassNotFoundException ex) {
            } finally {
                discounts = d;
            }
        } catch (IOException ex) {
        }
    }

    /**
     * reads information from file to menu object
     */
    public void readMenuFromFile() {
        RestaurantMenu rm = new RestaurantMenu();
        try {
            FileInputStream fin = new FileInputStream(menuFile);
            ObjectInputStream ois = new ObjectInputStream(fin);
            try {
                rm = (RestaurantMenu) ois.readObject();
                ois.close();
            } catch (ClassNotFoundException ex) {
            } finally {
                restaurantMenu = rm;
            }
        } catch (IOException ex) {
        }
    }

    /**
     * reads information from file to items ArrayList
     */
    public void readItemFromFile() {
        ArrayList<Item> items = new ArrayList<>();
        try {
            FileInputStream fin = new FileInputStream(itemFile);
            ObjectInputStream ois = new ObjectInputStream(fin);
            try {
                items = (ArrayList<Item>) ois.readObject();
                ois.close();
            } catch (ClassNotFoundException ex) {
            } finally {
                restaurantMenu.setItems(items);
            }
        } catch (IOException ex) {
        }
    }

    /**
     * reads information from file to categories ArrayList
     */
    public void readCategoryFromFile() {
        ArrayList<String> i = new ArrayList<>();
        try {
            FileInputStream fin = new FileInputStream(categoryFile);
            ObjectInputStream ois = new ObjectInputStream(fin);
            try {
                i = (ArrayList<String>) ois.readObject();
                ois.close();
            } catch (ClassNotFoundException ex) {
            } finally {
                restaurantMenu.setCategories(i);
            }
        } catch (IOException ex) {
        }
    }

    public void readBillFromFile() {
        ArrayList<Bill> b = new ArrayList<>();
        try {
            FileInputStream fin = new FileInputStream(billFile);
            ObjectInputStream ois = new ObjectInputStream(fin);
            try {
                b = (ArrayList<Bill>) ois.readObject();
                ois.close();
            } catch (ClassNotFoundException ex) {
            } finally {
                bills = b;
            }
        } catch (IOException ex) {
        }
    }

    /**
     * initializes data when software starts
     */
    public void initializeData() {
        readAdministratorFromFile();
        // initializes default admin
        if (administrators.isEmpty()) {
            Administrator defaultAdmin = new Administrator(getAdministartorID(), Gender.MALE, "0", "Two Days", "RMIT Vietnam");
            defaultAdmin.setUsername("admin");
            defaultAdmin.setPassword("admin");
            defaultAdmin.setFullName("admin");
            administrators.add(defaultAdmin);
        }
        readCashierFromFile();
        readManagerFromFile();
        readCustomerFromFile();
        readTableFromFile();
        readMenuFromFile();
        readDiscountFromFile();
        readCategoryFromFile();
        readItemFromFile();
        readBillFromFile();
        // initializes default discount
        if (discounts.isEmpty()) {
            discounts.put("default", 0);
            discounts.put("Bronze", CustomerType.BRONZE.getRate());
            discounts.put("Silver", CustomerType.SILVER.getRate());
            discounts.put("Gold", CustomerType.GOLD.getRate());
            discounts.put("Platinum", CustomerType.PLATINUM.getRate());
        }

        for (int i = 0; i < bills.size(); i++) {
            System.out.println(bills.get(i).toString());
        }

        // syncing order items in tables
        for (int t = 0; t < tables.size(); ++t) {
            if (tables.get(t).getO() != null) {
                ArrayList<OrderItem> aoi = tables.get(t).getO().getOrderItems();
                for (int oi = 0; oi < aoi.size(); ++oi) {
                    ArrayList<Item> ai = restaurantMenu.getItems();
                    for (int i = 0; i < ai.size(); ++i) {
                        if (aoi.get(oi).getItem().getItemName().equalsIgnoreCase(ai.get(i).getItemName())) {
                            aoi.get(oi).setItem(ai.get(i));
                        }
                    }
                }
            }
        }
    }

    public int getBillID() {
        if (!bills.isEmpty()) {
            return bills.size() + 1;
        }
        return 1;
    }

    /**
     * saves all data to binary files
     */
    public void saveData() {
        saveAdministratorToFile();
        saveCashierToFile();
        saveManagerToFile();
        saveCustomerToFile();
        saveTableToFile();
        saveMenuToFile();
        saveDiscountToFile();
        saveCategoryToFile();
        saveItemToFile();
        saveBillToFile();
    }

    /**
     * used by LoginController validates username and password entered by user
     *
     * @param username
     * @param password
     * @return error message
     */
    public String logIn(String username, String password) {
        if (username.equalsIgnoreCase("")) {
            return Localization.getResourceBundle().getString("Restaurant.enterUsername");

        } else {
            if (password.equalsIgnoreCase("")) {
                return Localization.getResourceBundle().getString("Restaurant.enterPassword");

            } else {
                Account a = null;
                for (int i = 0; i < administrators.size(); i++) {
                    if (administrators.get(i).getUsername().equalsIgnoreCase(username)) {
                        a = administrators.get(i);
                        break;
                    }
                }
                if (a == null) {
                    for (int i = 0; i < cashiers.size(); i++) {
                        if (cashiers.get(i).getUsername().equalsIgnoreCase(username)) {
                            a = cashiers.get(i);
                            break;
                        }
                    }
                }
                if (a == null) {
                    for (int i = 0; i < managers.size(); i++) {
                        if (managers.get(i).getUsername().equalsIgnoreCase(username)) {
                            a = managers.get(i);
                            break;
                        }
                    }
                }
                if (a == null) {
                    return Localization.getResourceBundle().getString("Restaurant.usernameNotFound");

                } else {
                    if (!a.getPassword().equals(password)) {
                        return Localization.getResourceBundle().getString("Restaurant.incorrectPassword");

                    } else {
                        Application.currentUser = a;
                    }
                }
            }
        }
        return Localization.getResourceBundle().getString("Restaurant.ok");
    }

    /**
     * used by LogoutController disposes the current user
     *
     * @return true when confirm
     * @return false when cancel
     */
    public boolean logOut() {
        String options[] = {Localization.getResourceBundle().getString("Restaurant.options.Confirm"), Localization.getResourceBundle().getString("Restaurant.options.Cancel")};
        int result = JOptionPane.showConfirmDialog(null, Localization.getResourceBundle().getString("Restaurant.logoutMessage"), Localization.getResourceBundle().getString("Restaurant.logoutTooltip"), JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.YES_OPTION) {
            Application.currentUser = null;
            return true;
        }
        return false;
    }

    /**
     * shows the Confirm Dialog
     *
     * @param s
     * @return confirm option
     */
    public int showConfirmDialog(String s) {
        String options[] = {Localization.getResourceBundle().getString("Restaurant.options.Confirm"), Localization.getResourceBundle().getString("Restaurant.options.Cancel")};
        String message = Localization.getResourceBundle().getString("Restaurant.showConfirmDialog.message");
        if (s.equalsIgnoreCase("account")) {
            message += Localization.getResourceBundle().getString("Restaurant.showConfirmDialog.message.thisAccount");
        } else if (s.equalsIgnoreCase("accounts")) {
            message += Localization.getResourceBundle().getString("Restaurant.showConfirmDialog.message.theseAccounts");
        } else if (s.equalsIgnoreCase("customer")) {
            message += Localization.getResourceBundle().getString("Restaurant.showConfirmDialog.message.thisCustomer");
        } else if (s.equalsIgnoreCase("customers")) {
            message += Localization.getResourceBundle().getString("Restaurant.showConfirmDialog.message.theseCustomers");
        } else if (s.equalsIgnoreCase("category") || s.equalsIgnoreCase("Loại thức ăn")) {
            message += Localization.getResourceBundle().getString("Restaurant.showConfirmDialog.message.thisCategory");
        } else if (s.equalsIgnoreCase("categories") || s.equalsIgnoreCase("Các loại thức ăn")) {
            message += Localization.getResourceBundle().getString("Restaurant.showConfirmDialog.message.theseCategories");
        } else if (s.equalsIgnoreCase("item") || s.equalsIgnoreCase("Các món ăn")) {
            message += Localization.getResourceBundle().getString("Restaurant.showConfirmDialog.message.thisItem");
        } else if (s.equalsIgnoreCase("items") || s.equalsIgnoreCase("Món ăn")) {
            message += Localization.getResourceBundle().getString("Restaurant.showConfirmDialog.message.theseItems");
        } else if (s.equalsIgnoreCase("table")) {
            message += Localization.getResourceBundle().getString("Restaurant.showConfirmDialog.message.thisTable");
        } else if (s.equalsIgnoreCase("vacant") || s.equalsIgnoreCase("Trống")) {
            message = Localization.getResourceBundle().getString("Restaurant.showConfirmDialog.message.changeTableType");
        } else if (s.equalsIgnoreCase("order item") || s.equalsIgnoreCase("Món ăn trong hoá đơn")) {
            message += Localization.getResourceBundle().getString("Restaurant.showConfirmDialog.message.thisOrderItem");
        } else if (s.equalsIgnoreCase("order items") || s.equalsIgnoreCase("Các món ăn trong hoá đơn")) {
            message += Localization.getResourceBundle().getString("Restaurant.showConfirmDialog.message.theseOrderItems");
        } else {
            message = Localization.getResourceBundle().getString("Restaurant.showConfirmDialog.message.exit");
        }
        int result = JOptionPane.showOptionDialog(null, message, Localization.getResourceBundle().getString("Restaurant.showConfirmDialog.tooltip"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, null);
        return result;
    }

    /**
     * displays error message
     *
     * @param lbl
     * @param message
     */
    public void displayError(JLabel lbl, String message) {
        lbl.setText(message);
        lbl.setForeground(Color.red);
    }

    /**
     * creates an Account and adds to ArrayList
     *
     * @param type
     * @param g
     * @param ssn
     * @param dob
     * @param phone_number
     * @param email
     * @param full_name
     * @param address
     * @return
     */
    public Account createAccount(AccountType type, Gender g, String ssn, Date dob, String phone_number, String email, String full_name, String address) {
        Account newAccount;
        if (type == Account.AccountType.ADMINISTRATOR) {
            newAccount = new Administrator(getAdministartorID(), g, ssn, full_name, address);
            administrators.add((Administrator) newAccount);
        } else if (type == Account.AccountType.CASHIER) {
            newAccount = new Cashier(getCashierID(), g, ssn, full_name, address);
            cashiers.add((Cashier) newAccount);
        } else {
            newAccount = new Manager(getManagerID(), g, ssn, full_name, address);
            managers.add((Manager) newAccount);
        }
        newAccount.setDob(dob);
        if (phone_number.equalsIgnoreCase("null")) {
            newAccount.setPhoneNumber("");
        } else {
            newAccount.setPhoneNumber(phone_number);
        }
        newAccount.setEmail(email);
        update();
        return newAccount;
    }

    /**
     * updates an account in the ArrayList
     *
     * @param a
     * @param password
     * @param type
     * @param g
     * @param ssn
     * @param dob
     * @param full_name
     * @param phone_number
     * @param email
     * @param address
     */
    public void editAccount(Account a, String password, AccountType type, Gender g, String ssn, Date dob, String full_name, String phone_number, String email, String address) {
        a.setAddress(StringUtil.TrimSpace(address));
        a.setDob(dob);
        a.setG(g);
        a.setFullName(StringUtil.TrimSpace(full_name));
        a.setSsn(ssn);
        if (phone_number.equalsIgnoreCase("null")) {
            a.setPhoneNumber("");
        } else {
            a.setPhoneNumber(phone_number);
        }
        if (!email.equalsIgnoreCase("")) {
            a.setEmail(email);
        }
        if (a.getType() == AccountType.ADMINISTRATOR) {
            if (type == AccountType.CASHIER) {
                changeAccountType(a, AccountType.CASHIER);
            } else if (type == AccountType.MANAGER) {
                changeAccountType(a, AccountType.MANAGER);
            }
        } else if (a.getType() == AccountType.CASHIER) {
            if (type == AccountType.ADMINISTRATOR) {
                changeAccountType(a, AccountType.ADMINISTRATOR);
            } else if (type == AccountType.MANAGER) {
                changeAccountType(a, AccountType.MANAGER);
            }
        } else {
            if (type == AccountType.ADMINISTRATOR) {
                changeAccountType(a, AccountType.ADMINISTRATOR);
            } else if (type == AccountType.CASHIER) {
                changeAccountType(a, AccountType.CASHIER);
            }
        }
        update();
    }

    /**
     * changes the AccountType of an Account in the ArrayList
     *
     * @param oldAccount
     * @param type
     */
    public void changeAccountType(Account oldAccount, AccountType type) {
        Account newAccount;
        if (type == AccountType.ADMINISTRATOR) {
            newAccount = new Administrator(getAdministartorID(), oldAccount.getG(), oldAccount.getSsn(), oldAccount.getFullName(), oldAccount.getAddress());
            administrators.add((Administrator) newAccount);
        } else if (type == AccountType.CASHIER) {
            newAccount = new Cashier(getCashierID(), oldAccount.getG(), oldAccount.getSsn(), oldAccount.getFullName(), oldAccount.getAddress());
            cashiers.add((Cashier) newAccount);
        } else {
            newAccount = new Manager(getManagerID(), oldAccount.getG(), oldAccount.getSsn(), oldAccount.getFullName(), oldAccount.getAddress());
            managers.add((Manager) newAccount);
        }
        newAccount.setDob(oldAccount.getDob());
        newAccount.setEmail(oldAccount.getEmail());
        newAccount.setPhoneNumber(oldAccount.getPhoneNumber());
        deleteAccount(oldAccount.getUsername(), oldAccount.printType());
        update();
    }

    /**
     * deletes an Account in the ArrayList
     *
     * @param username
     * @param type
     * @return true when success
     * @return false when fail
     */
    public boolean deleteAccount(String username, String type) {
        if (type.equalsIgnoreCase("Administrator")) {
            for (int j = 0; j < administrators.size(); j++) {
                if (administrators.get(j).getUsername().equalsIgnoreCase(username)) {
                    administrators.remove(j);
                    return true;
                }
            }
        } else if (type.equalsIgnoreCase("Cashier")) {
            for (int j = 0; j < cashiers.size(); j++) {
                if (cashiers.get(j).getUsername().equalsIgnoreCase(username)) {
                    cashiers.remove(j);
                    return true;
                }
            }
        } else {
            for (int j = 0; j < managers.size(); j++) {
                if (managers.get(j).getUsername().equalsIgnoreCase(username)) {
                    managers.remove(j);
                    return true;
                }
            }
        }
        update();
        return false;
    }

    /**
     * finds an Account with the username and type
     *
     * @param username
     * @param accountType
     * @return the account found
     */
    public Account findAccount(String username, String accountType) {
        Account a = null;
        if (accountType.equalsIgnoreCase("administrator")) {
            for (int i = 0; i < administrators.size(); i++) {
                if (administrators.get(i).getUsername().equalsIgnoreCase(username)) {
                    a = administrators.get(i);
                    break;
                }
            }
        } else if (accountType.equalsIgnoreCase("cashier")) {
            for (int i = 0; i < cashiers.size(); i++) {
                if (cashiers.get(i).getUsername().equalsIgnoreCase(username)) {
                    a = cashiers.get(i);
                    break;
                }
            }
        } else {
            for (int i = 0; i < managers.size(); i++) {
                if (managers.get(i).getUsername().equalsIgnoreCase(username)) {
                    a = managers.get(i);
                    break;
                }
            }
        }
        return a;
    }

    /**
     * searchs for accounts which contains the searchQuery
     *
     * @param searchQuery
     * @param admin
     * @param cashier
     * @param manager
     * @return
     */
    public ArrayList<String[]> getAccountList(String searchQuery, boolean admin, boolean cashier, boolean manager) {
        ArrayList<String[]> data = new ArrayList<>();

        if (admin) {
            for (int i = 0; i < administrators.size(); i++) {
                if ((searchQuery == null || administrators.get(i).getUsername().contains(searchQuery)) && !administrators.get(i).getUsername().equalsIgnoreCase("admin")) {
                    if (administrators.get(i).getDob() != null) {
                        data.add(new String[]{administrators.get(i).getUsername(), administrators.get(i).getFullName(), administrators.get(i).printGender(), administrators.get(i).getAge() + "", administrators.get(i).printType()});
                    } else {
                        data.add(new String[]{administrators.get(i).getUsername(), administrators.get(i).getFullName(), administrators.get(i).printGender(), "N/A", administrators.get(i).printType()});
                    }
                }
            }
        }

        if (cashier) {
            for (int i = 0; i < cashiers.size(); i++) {
                if (searchQuery == null || cashiers.get(i).getUsername().contains(searchQuery)) {
                    if (cashiers.get(i).getDob() != null) {
                        data.add(new String[]{cashiers.get(i).getUsername(), cashiers.get(i).getFullName(), cashiers.get(i).printGender(), cashiers.get(i).getAge() + "", cashiers.get(i).printType()});
                    } else {
                        data.add(new String[]{cashiers.get(i).getUsername(), cashiers.get(i).getFullName(), cashiers.get(i).printGender(), "N/A", cashiers.get(i).printType()});
                    }
                }
            }
        }

        if (manager) {
            for (int i = 0; i < managers.size(); i++) {
                if (searchQuery == null || managers.get(i).getUsername().contains(searchQuery)) {
                    if (managers.get(i).getDob() != null) {
                        data.add(new String[]{managers.get(i).getUsername(), managers.get(i).getFullName(), managers.get(i).printGender(), managers.get(i).getAge() + "", managers.get(i).printType()});
                    } else {
                        data.add(new String[]{managers.get(i).getUsername(), managers.get(i).getFullName(), managers.get(i).printGender(), "N/A", managers.get(i).printType()});
                    }
                }
            }
        }
        return data;
    }

    /**
     * validates SSN input
     *
     * @param newSsn
     * @param oldSsn
     * @return true when successful
     * @return false when fail
     */
    public boolean validateSSN(boolean customer, String newSsn, String oldSsn) {
        if (customer) {
            for (int i = 0; i < customers.size(); i++) {
                Customer c = customers.get(i);
                if (newSsn.equalsIgnoreCase(c.getSsn()) && ((oldSsn == null) || !oldSsn.equalsIgnoreCase(c.getSsn()))) {
                    return false;
                }
            }
        } else {
            for (int i = 0; i < administrators.size(); i++) {
                if (newSsn.equalsIgnoreCase(administrators.get(i).getSsn()) && ((oldSsn == null) || !oldSsn.equalsIgnoreCase(administrators.get(i).getSsn()))) {
                    return false;
                }
            }

            for (int i = 0; i < cashiers.size(); i++) {
                if (newSsn.equalsIgnoreCase(cashiers.get(i).getSsn()) && ((oldSsn == null) || !oldSsn.equalsIgnoreCase(cashiers.get(i).getSsn()))) {
                    return false;
                }
            }

            for (int i = 0; i < managers.size(); i++) {
                if (newSsn.equalsIgnoreCase(managers.get(i).getSsn()) && ((oldSsn == null) || !oldSsn.equalsIgnoreCase(managers.get(i).getSsn()))) {
                    return false;
                }

            }
        }
        return true;
    }

    public void createCustomer(Gender g, String ssn, Date d, String name, String phone, String email, String address) {
        Customer c = new Customer(g, ssn, name, phone);
        c.setDob(d);
        c.setEmail(email);
        c.setAddress(address);
        customers.add(c);
        update();
    }

    public void editCustomer(Customer c, Gender g, String ssn, Date dob, String name, String phone, String email_address, String address) {
        c.setG(g);
        c.setSsn(ssn);
        c.setDob(dob);
        c.setFullName(name);
        c.setPhoneNumber(phone);
        c.setEmail(name);
        c.setAddress(address);
    }

    public void deleteCustomer(String ssn) {
        for (int i = 0; i < customers.size(); ++i) {
            if (customers.get(i).getSsn().equalsIgnoreCase(ssn)) {
                customers.remove(i);
                break;
            }
        }
    }

    public ArrayList<String[]> getCustomerList(String searchQuery, boolean phone) {
        ArrayList<String[]> data = new ArrayList<>();
        for (int i = 0; i < customers.size(); ++i) {
            Customer c = customers.get(i);
            if (phone) {
                if (searchQuery == null || c.getPhoneNumber().contains(searchQuery)) {
                    if (c.getDob() != null) {
                        data.add(new String[]{c.getFullName(), c.printGender(), c.getAge() + "", c.getSsn(), c.getPhoneNumber(), c.printType(), c.getLoyalty_points() + ""});
                    } else {
                        data.add(new String[]{c.getFullName(), c.printGender(), "N/A", c.getSsn(), c.getPhoneNumber(), c.printType(), c.getLoyalty_points() + ""});
                    }
                }
            } else {
                if (searchQuery == null || c.getSsn().contains(searchQuery)) {
                    if (c.getDob() != null) {
                        data.add(new String[]{c.getFullName(), c.printGender(), c.getAge() + "", c.getSsn(), c.getPhoneNumber(), c.printType(), c.getLoyalty_points() + ""});
                    } else {
                        data.add(new String[]{c.getFullName(), c.printGender(), "N/A", c.getSsn(), c.getPhoneNumber(), c.printType(), c.getLoyalty_points() + ""});
                    }
                }
            }
        }
        return data;
    }

    public boolean validatePhone(String new_phone, String old_phone) {
        for (int i = 0; i < customers.size(); i++) {
            String phone = customers.get(i).getPhoneNumber();
            if (new_phone.equalsIgnoreCase(phone) && ((old_phone == null) || !old_phone.equalsIgnoreCase(phone))) {
                return false;
            }
        }
        return true;
    }

    /**
     * creates new Table and adds to ArrayList
     *
     * @param shape
     * @return
     */
    public Table createTable(TableShape shape) {
        Table t = new Table(getTableNumber(), shape);
        this.tables.add(t);
        update();
        return t;
    }

    /**
     * updates the status of the tables
     *
     * @param update_tables
     * @param tableStatus
     */
    public void updateTableStatus(ArrayList<Table> update_tables, TableStatus tableStatus) {

        Table table = update_tables.get(0);
        if (table.getTableGroupId() == -1) {
            if (tableStatus == TableStatus.VACANT) {
                refreshOrder(table);
            }
            table.setTableStatus(tableStatus);

        } else {
            ArrayList<Table> tables_in_group = new ArrayList<>();
            for (int i = 0; i < tables.size(); i++) {
                if (tables.get(i).getTableGroupId() == table.getTableGroupId()) {
                    tables_in_group.add(tables.get(i));
                }
            }
            for (int i = 0; i < tables_in_group.size(); i++) {
                if (tableStatus == TableStatus.VACANT) {
                    refreshOrder(tables_in_group.get(i));
                }
                tables_in_group.get(i).setTableStatus(tableStatus);
            }
        }

    }

    /**
     * updates the table number
     *
     * @param oldTableNumber
     * @param newTableNumber
     * @return
     */
    public Table editTableNumber(int oldTableNumber, int newTableNumber) {
        for (int i = 0; i < tables.size(); i++) {
            if (tables.get(i).getTableNumber() == oldTableNumber) {
                tables.get(i).setTableNumber(newTableNumber);
                return tables.get(i);
            }
        }
        return null;
    }

    /**
     * validates the table number before update
     *
     * @param oldTableNumber
     * @param newTableNumber
     * @return
     */
    public boolean validateTableNumber(int oldTableNumber, int newTableNumber) {
        for (int i = 0; i < tables.size(); i++) {
            if (tables.get(i).getTableNumber() == newTableNumber && oldTableNumber != newTableNumber) {
                return false;
            }
            if (newTableNumber > 20 || newTableNumber < 1) {
                return false;
            }
        }
        return true;
    }

    /**
     * delete Table object from ArrayList
     *
     * @param delete_table
     */
    public void deleteTable(Table delete_table) {
        if (delete_table.getTableGroupId() == -1) {
            tables.remove(delete_table);
        } else {
            ArrayList<Table> temp_tables = new ArrayList<>();
            for (int i = 0; i < tables.size(); i++) {
                if (tables.get(i).getTableGroupId() == delete_table.getTableGroupId()) {
                    temp_tables.add(tables.get(i));
                }
            }
            for (int i = 0; i < temp_tables.size(); i++) {
                tables.remove(temp_tables.get(i));
            }
        }
    }

    /**
     * creates merged Table
     *
     * @param merge_tables
     * @param cell_id
     * @return
     */
    public Table mergeTables(ArrayList<Table> merge_tables, int cell_id) {

        Table root_table = merge_tables.get(0);
        int table_group_id = -1;
        for (int i = 0; i < merge_tables.size(); i++) {
            if (merge_tables.get(i).getTableGroupId() != -1) {
                if (table_group_id == -1) {
                    table_group_id = merge_tables.get(i).getTableGroupId();
                } else {
                    if (merge_tables.get(i).getTableGroupId() < table_group_id) {
                        table_group_id = merge_tables.get(i).getTableGroupId();
                    }
                }
            }
        }

        if (table_group_id == -1) {
            for (int i = 0; i < tables.size(); i++) {
                if (table_group_id < tables.get(i).getTableGroupId()) {
                    table_group_id = tables.get(i).getTableGroupId();
                }
            }
            table_group_id++;
        }

        Order order = new Order();
        for (int i = 0; i < merge_tables.size(); i++) {
            Table current_table = merge_tables.get(i);
            for (int j = 0; j < current_table.getO().getOrderItems().size(); j++) {
                OrderItem oi = new OrderItem(order, current_table.getO().getOrderItems().get(j).getItem(), current_table.getO().getOrderItems().get(j).getNote(), current_table.getO().getOrderItems().get(j).getQuantity());
            }
        }

        for (int i = 0; i < merge_tables.size(); i++) {
            if (merge_tables.get(i).getTableGroupId() != -1) {
                for (int j = 0; j < tables.size(); j++) {
                    if (tables.get(j).getTableGroupId() == merge_tables.get(i).getTableGroupId()) {
                        if (!merge_tables.contains(tables.get(j))) {
                            merge_tables.add(tables.get(j));
                        }
                    }
                }
            }
        }

        sortTables(merge_tables);

        for (int i = 0; i < merge_tables.size(); i++) {
            merge_tables.get(i).setTable_group_id(table_group_id);
            merge_tables.get(i).setTable_group_root_id(-1);
            merge_tables.get(i).setO(order);
        }

        root_table.setTable_group_root_id(cell_id);
        if (order.getOrderItems().isEmpty()) {
            updateTableStatus(merge_tables, TableStatus.VACANT);
        } else {
            updateTableStatus(merge_tables, TableStatus.OCCUPIED);
        }

        System.out.println(root_table.getO().getOrderItems().size());
        return root_table;
    }

    /**
     * un-merges the merged table
     *
     * @param unmerge_table
     */
    public void unmergeTables(Table unmerge_table) {

        int table_group_id = unmerge_table.getTableGroupId();
        for (int i = 0; i < tables.size(); i++) {
            if (tables.get(i).getTableGroupId() == table_group_id) {
                tables.get(i).setTable_group_id(-1);
                tables.get(i).setTable_group_root_id(-1);
                Order order = new Order();
                tables.get(i).setO(order);
            }
        }
    }

    /**
     * switch the order between 2 tables
     *
     * @param table
     */
    public void switchTable(ArrayList<Table> table) {

        Order tempOrder = table.get(0).getO();
        table.get(0).setO(table.get(1).getO());
        table.get(1).setO(tempOrder);

        String tempCustomerName = table.get(0).getCustomerName();
        table.get(0).setCustomerName(table.get(1).getCustomerName());
        table.get(1).setCustomerName(tempCustomerName);

        TableStatus tempTableStatus = table.get(0).getTableStatus();
        table.get(0).setTableStatus(table.get(1).getTableStatus());
        table.get(1).setTableStatus(tempTableStatus);

        for (int i = 0; i < tables.size(); i++) {
            if (tables.get(i).getO().getOrderItems().isEmpty() && !tables.get(i).getTableStatus().equals(TableStatus.RESERVED)) {
                tables.get(i).setTableStatus(TableStatus.VACANT);
            }
        }
    }

    /**
     * refresh the order of 1 table
     *
     * @param table
     */
    public void refreshOrder(Table table) {

        if (table.getTableGroupId() == -1) {
            for (int i = 0; i < tables.size(); i++) {
                if (tables.get(i).compareTo(table) == 0) {

                    tables.get(i).getO().getOrderItems().clear();

                    tables.get(i).setTableStatus(TableStatus.VACANT);
                    update();
                    break;
                }
            }
        } else {
            for (int i = 0; i < tables.size(); i++) {
                if (tables.get(i).getTableGroupId() == table.getTableGroupId()) {
                    tables.get(i).getO().getOrderItems().clear();

                    tables.get(i).setTableStatus(TableStatus.VACANT);
                }
            }
            update();
        }

    }

    /**
     * creates a category and adds to ArrayList
     *
     * @param category_name
     */
    public void createCategory(String category_name) {
        restaurantMenu.createCategory(category_name);
        restaurantMenu.sortCategories();
        update();
    }

    /**
     * validate category before create
     *
     * @param category_name
     * @return true when valid
     * @return false when invalid
     */
    public boolean validateCategory(String category_name) {
        for (int i = 0; i < restaurantMenu.getCategories().size(); i++) {
            if (restaurantMenu.getCategories().get(i).equalsIgnoreCase(category_name)) {
                return false;
            }
        }
        return true;
    }

    /**
     * updates category in the ArrayList
     *
     * @param oldCategory
     * @param newCategory
     */
    public void editCategory(String oldCategory, String newCategory) {
        restaurantMenu.editCategory(oldCategory, newCategory);
        restaurantMenu.sortCategories();
        update();
    }

    /**
     * deletes category in the ArrayList
     *
     * @param categoriesToDelete
     * @return message
     */
    public String deleteCategory(ArrayList<String> categoriesToDelete) {
        int count = categoriesToDelete.size();
        for (int i = 0; i < categoriesToDelete.size(); i++) {
            boolean deletable = true;
            for (int j = 0; j < tables.size(); j++) {
                for (int k = 0; k < tables.get(j).getO().getOrderItems().size(); k++) {
                    if (tables.get(j).getO().getOrderItems().get(k).getItem().getItemCategory().equalsIgnoreCase(categoriesToDelete.get(i))) {
                        deletable = false;
                        break;
                    }
                }
                if (!deletable) {
                    break;
                }
            }
            if (deletable) {
                restaurantMenu.deleteCategory(categoriesToDelete.get(i));
                count--;
            }
        }
        update();
        if (count > 0) {
            return Localization.getResourceBundle().getString("Restaurant.deleteCategory.couldNotDeleteCategories");
        } else {
            return Localization.getResourceBundle().getString("Restaurant.deleteCategory.deleteCategoriesSuccessfully");
        }
    }

    /**
     * creates new item and adds to ArrayList
     *
     * @param item_name
     * @param price
     * @param category
     */
    public void createItem(String item_name, int price, String category) {
        restaurantMenu.createItem(item_name, price, category);
        update();
    }

    /**
     * validates item before create
     *
     * @param old_item_name
     * @param new_item_name
     * @return true when valid
     * @return false when invalid
     */
    public boolean validateItem(String old_item_name, String new_item_name) {
        for (int i = 0; i < restaurantMenu.getItems().size(); i++) {
            if (restaurantMenu.getItems().get(i).getItemName().equalsIgnoreCase(new_item_name) && (old_item_name == null || !restaurantMenu.getItems().get(i).getItemName().equalsIgnoreCase(old_item_name))) {
                return false;
            }
        }
        return true;
    }

    /**
     * updates item in ArrayList
     *
     * @param oldItemName
     * @param newItemName
     * @param price
     * @param category
     */
    public void editItem(String oldItemName, String newItemName, int price, String category) {
        restaurantMenu.editItem(oldItemName, newItemName, price, category);
        restaurantMenu.sortItems();
        update();
    }

    /**
     * deletes item in ArrayList
     *
     * @param itemNames
     * @return
     */
    public String deleteItem(ArrayList<String> itemNames) {
        int count = itemNames.size();
        for (int i = 0; i < itemNames.size(); i++) {
            boolean deletable = true;
            for (int j = 0; j < tables.size(); j++) {
                for (int k = 0; k < tables.get(j).getO().getOrderItems().size(); k++) {
                    if (tables.get(j).getO().getOrderItems().get(k).getItem().getItemName().equalsIgnoreCase(itemNames.get(i))) {
                        deletable = false;
                        break;
                    }
                }
                if (!deletable) {
                    break;
                }
            }
            if (deletable) {
                restaurantMenu.deleteItem(itemNames.get(i));
                count--;
            }
        }
        update();
        if (count > 0) {
            return Localization.getResourceBundle().getString("Restaurant.deleteItem.couldNotDeleteItems");
        } else {
            return Localization.getResourceBundle().getString("Restaurant.deleteItem.deleteItemsSuccessfully");
        }
    }

    /**
     * updates order item of a table
     *
     * @param t
     * @param id
     * @param quantity
     * @param note
     * @param isDone
     */
    public void editOrderItem(Table t, int id, int quantity, String note, boolean isDone) {
        t.getO().editOrderItem(id, quantity, note, isDone);
        update();
    }

    /**
     * deletes order item of a table
     *
     * @param table
     * @param orderItemID
     */
    public void deleteOrderItem(ArrayList<Table> table, int orderItemID) {
        boolean deleted = false;
        for (int i = 0; i < table.get(0).getO().getOrderItems().size(); i++) {
            if (table.get(0).getO().getOrderItems().get(i).getOrderItemId() == orderItemID) {
                table.get(0).getO().getOrderItems().remove(i);
            }
            deleted = true;
        }
        if (deleted) {
            for (int i = 0; i < table.size(); i++) {
                if (table.get(i).getO().getOrderItems().isEmpty()) {
                    refreshOrder(table.get(i));
                }

            }
            update();
        }
    }

    /**
     * creates new discount and adds to HashMap
     *
     * @param discount_name
     * @param discount_value
     */
    public void createDiscount(String discount_name, int discount_value) {
        discounts.put(discount_name, discount_value);
        update();
    }

    /**
     * validates discount before create
     *
     * @param new_discount_name
     * @param old_discount_name
     * @return
     */
    public boolean validateDiscount(String new_discount_name, String old_discount_name) {
        return !discounts.containsKey(new_discount_name) || (old_discount_name != null && old_discount_name.equalsIgnoreCase(new_discount_name));
    }

    /**
     * updates discount in HashMap
     *
     * @param oldDiscount
     * @param newDiscount
     * @param discountValue
     */
    public void editDiscount(String oldDiscount, String newDiscount, int discountValue) {
        discounts.remove(oldDiscount);
        discounts.put(newDiscount, discountValue);
        update();
    }

    /**
     * deletes discount in HashMap
     *
     * @param discount
     */
    public void deleteDiscount(String discount) {
        discounts.remove(discount);
        update();
    }

    /**
     * creates bill and add to ArrayList
     *
     * @param date
     * @param staff
     * @param tableNumber
     * @param total
     * @param discount
     * @param discountValue
     * @param orderItems
     */
    public void createBill(Calendar date, String staff, int tableNumber, int total, String discount, int discountValue, ArrayList<String[]> orderItems) {
        bills.add(new Bill(getBillID(), date, staff, tableNumber, total, discount, discountValue, orderItems));
        update();
    }

    /**
     * calculates the total sale amount in the period
     *
     * @param date_start
     * @param date_end
     * @return
     */
    public int calculateTotalSaleAmount(Calendar date_start, Calendar date_end) {

        int total = 0;
        TimeIgnoringComparator tic = new TimeIgnoringComparator();
        for (int i = 0; i < bills.size(); i++) {
            Calendar temp = bills.get(i).getDate();
            if (tic.compare(temp, date_start) >= 0 && tic.compare(temp, date_end) <= 0) {
                total += bills.get(i).getTotal();
            }
        }
        return total;
    }

    /**
     * calculates the daily shift amount in the period
     *
     * @param date_start
     * @param date_end
     * @return
     */
    public ArrayList<String[]> calculateDailyShiftAmount(Calendar date_start, Calendar date_end) {

        String[] day;
        ArrayList<String[]> days = new ArrayList<>();
        ArrayList<Bill> process_bills = new ArrayList<>();
        int total_shift_1 = 0;
        int total_shift_2 = 0;
        Calendar current_date;
        Calendar time_only = Calendar.getInstance();
        time_only.set(2013, 12, 10, 16, 00, 00);

        TimeIgnoringComparator tic = new TimeIgnoringComparator();
        DateIgnoringComparator dic = new DateIgnoringComparator();

        for (int i = 0; i < bills.size(); i++) {
            Calendar temp = bills.get(i).getDate();
            if (tic.compare(temp, date_start) >= 0 && tic.compare(temp, date_end) <= 0) {
                process_bills.add(bills.get(i));
            }
        }

        if (!process_bills.isEmpty()) {
            current_date = process_bills.get(0).getDate();

            for (int i = 0; i < process_bills.size(); i++) {
                if (tic.compare(current_date, process_bills.get(i).getDate()) < 0) {
                    day = new String[3];
                    DateFormat dateformat = DateFormat.getDateInstance(DateFormat.FULL, new Locale("en", "US"));
                    day[0] = dateformat.format(current_date.getTime());

                    day[1] = new DecimalFormat("###,###,###").format(total_shift_1);
                    if (day[1].contains(",")) {
                        day[1] = day[1].replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                    } else {
                        day[1] = day[1].replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                    }

                    day[2] = new DecimalFormat("###,###,###").format(total_shift_2);
                    if (day[2].contains(",")) {
                        day[2] = day[2].replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                    } else {
                        day[2] = day[2].replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                    }
                    days.add(day);
                    current_date = process_bills.get(i).getDate();

                    total_shift_1 = 0;
                    total_shift_2 = 0;
                }
                if (dic.compare(process_bills.get(i).getDate(), time_only) <= 0) {
                    System.out.println("shift1");
                    total_shift_1 += process_bills.get(i).getTotal();
                } else {
                    System.out.println("shift2");
                    total_shift_2 += process_bills.get(i).getTotal();
                }
            }

            day = new String[3];
            DateFormat dateformat = DateFormat.getDateInstance(DateFormat.FULL, new Locale("en", "US"));
            day[0] = dateformat.format(current_date.getTime());
            day[1] = new DecimalFormat("###,###,###").format(total_shift_1);
            if (day[1].contains(",")) {
                day[1] = day[1].replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
            } else {
                day[1] = day[1].replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
            }

            day[2] = new DecimalFormat("###,###,###").format(total_shift_2);
            if (day[2].contains(",")) {
                day[2] = day[2].replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
            } else {
                day[2] = day[2].replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
            }

            days.add(day);
        }

        return days;
    }

    /**
     * calculates the sale amount of each item in the period
     *
     * @param date_start
     * @param date_end
     * @return
     */
    public ArrayList<String[]> calculateItemSaleAmount(Calendar date_start, Calendar date_end) {
        ArrayList<String[]> items = new ArrayList<>();
        boolean exist;
        String current_item[];
        String temp_item[];
        String name;
        String category;
        int total;
        int quantity;
        int price;
        ArrayList<Bill> process_bills = new ArrayList<>();

        TimeIgnoringComparator tic = new TimeIgnoringComparator();

        for (int i = 0; i < bills.size(); i++) {
            Calendar temp = bills.get(i).getDate();
            if (tic.compare(temp, date_start) >= 0 && tic.compare(temp, date_end) <= 0) {
                process_bills.add(bills.get(i));
            }
        }

        for (int i = 0; i < process_bills.size(); i++) {
            Bill bill = process_bills.get(i);
            for (int j = 0; j < bill.getOrderItems().size(); j++) {
                exist = false;
                current_item = bill.getOrderItems().get(j);
                name = current_item[0];
                price = Integer.parseInt(current_item[1]);
                quantity = Integer.parseInt(current_item[2]);
                category = current_item[3];
                total = price * quantity * (100 - bill.getDiscountValue()) / 100;

                for (int t = 0; t < items.size(); t++) {
                    temp_item = items.get(t);
                    if (temp_item[0].compareTo(name) == 0) {
                        quantity += Integer.parseInt(temp_item[1]);
                        total += Integer.parseInt(temp_item[2].trim().replaceAll("[, ]+", ""));
                        temp_item[1] = new DecimalFormat("###,###,###").format(quantity);
                        if (temp_item[1].contains(",")) {
                            temp_item[1] = temp_item[1].replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                        } else {
                            temp_item[1] = temp_item[1].replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                        }

                        temp_item[2] = new DecimalFormat("###,###,###").format(total);
                        if (temp_item[2].contains(",")) {
                            temp_item[2] = temp_item[2].replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                        } else {
                            temp_item[2] = temp_item[2].replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                        }

                        exist = true;
                        break;
                    }
                }
                if (!exist) {
                    String[] item = new String[4];
                    item[0] = name;
                    item[1] = new DecimalFormat("###,###,###").format(quantity);
                    if (item[1].contains(",")) {
                        item[1] = item[1].replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                    } else {
                        item[1] = item[1].replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                    }

                    item[2] = new DecimalFormat("###,###,###").format(total);
                    if (item[2].contains(",")) {
                        item[2] = item[2].replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                    } else {
                        item[2] = item[2].replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                    }

                    item[3] = category;
                    items.add(item);

                }
            }
        }

        for (int i = 0; i < items.size(); i++) {
            current_item = items.get(i);
            for (int j = i; j >= 0; j--) {
                if (current_item[3].compareTo(items.get(j)[3]) < 0) {
                    temp_item = items.get(j);
                    items.set(j, items.get(i));
                    items.set(i, temp_item);
                } else if (current_item[3].compareTo(items.get(j)[3]) > 0) {
                    break;
                }
            }
        }

        return items;
    }

    /**
     * gets all the bill generated in a certain day
     *
     * @param searchDate
     * @return
     */
    public ArrayList<String[]> getBillList(Calendar calFrom, Calendar calTo) {
        ArrayList<String[]> data = new ArrayList<>();

        TimeIgnoringComparator tic = new TimeIgnoringComparator();
        if (calFrom == null || calTo == null) {
            for (int i = 0; i < bills.size(); i++) {
                Date tempDate = bills.get(i).getDate().getTime();
                DateFormat dateformat = DateFormat.getDateInstance(DateFormat.LONG, new Locale(Localization.getLANGUAGE(), Localization.getCOUNTRY()));
                String sDate = dateformat.format(tempDate.getTime());
                int total = bills.get(i).getTotal();
                String sTotal = new DecimalFormat("###,###,###").format(total) + Localization.getResourceBundle().getString("CashierManagerView.vnd");
                if (sTotal.contains(",")) {
                    sTotal = sTotal.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                } else {
                    sTotal = sTotal.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                }

                data.add(new String[]{Integer.toString(bills.get(i).getId()), sDate, bills.get(i).getStaff(), Integer.toString(bills.get(i).getTableNumber()), sTotal});
            }
        } else {
            for (int i = 0; i < bills.size(); i++) {
                if (tic.compare(calFrom, bills.get(i).getDate()) >= 0 && tic.compare(calTo, bills.get(i).getDate()) <= 0) {
                    Date tempDate = bills.get(i).getDate().getTime();
                    DateFormat dateformat = DateFormat.getDateInstance(DateFormat.LONG, new Locale(Localization.getLANGUAGE(), Localization.getCOUNTRY()));
                    String sDate = dateformat.format(tempDate.getTime());
                    int total = bills.get(i).getTotal();
                    String sTotal = new DecimalFormat("###,###,###").format(total) + Localization.getResourceBundle().getString("CashierManagerView.vnd");
                    if (sTotal.contains(",")) {
                        sTotal = sTotal.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                    } else {
                        sTotal = sTotal.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                    }

                    data.add(new String[]{Integer.toString(bills.get(i).getId()), sDate, bills.get(i).getStaff(), Integer.toString(bills.get(i).getTableNumber()), sTotal});
                }
            }

        }
        return data;
    }

    /**
     * finds the bill using id
     *
     * @param id
     * @return
     */
    public Bill findBill(int id) {
        for (int i = 0; i < bills.size(); i++) {
            if (bills.get(i).getId() == id) {
                return bills.get(i);
            }
        }
        return null;
    }

    /**
     * finds the customer using phone number
     *
     * @param phonenumber
     * @return
     */
    public Customer findCustomer(String phonenumber) {
        for (int i = 0; i < customers.size(); ++i) {
            Customer c = customers.get(i);
            if (c.getPhoneNumber().equalsIgnoreCase(phonenumber)) {
                return c;
            }
        }
        return null;
    }

    /**
     * saves data and notify observers
     */
    public void update() {
        saveData();
        this.setChanged();
        this.notifyObservers(this);
    }
}
