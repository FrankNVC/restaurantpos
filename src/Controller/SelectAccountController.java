/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Main.Application;
import Model.Account;
import Model.Restaurant;
import Utilities.Localization;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import javax.swing.DefaultListModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Nguyen Minh Sang
 */
public class SelectAccountController implements ListSelectionListener {

    private Restaurant r;

    /**
     *
     * @param r
     */
    public SelectAccountController(Restaurant r) {
        this.r = r;
    }

    /**
     * defines events when an account is selected
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (Application.av.getPnl_list_account().getTable().getSelectedRows().length < 1) {
            Application.av.getBtn_delete_account().setEnabled(false);
            Application.av.getBtn_update_account().setEnabled(false);
            ((DefaultListModel) Application.av.getList_account_detail_right().getModel()).clear();
        } else {
            Application.av.getBtn_delete_account().setEnabled(true);
            if (Application.av.getPnl_list_account().getTable().getSelectedRows().length == 1) {
                Application.av.getBtn_update_account().setEnabled(true);
                Account a = r.findAccount((String) Application.av.getPnl_list_account().getTable().getValueAt(Application.av.getPnl_list_account().getTable().getSelectedRow(), 0), (String) Application.av.getPnl_list_account().getTable().getValueAt(Application.av.getPnl_list_account().getTable().getSelectedRow(), 4));
                DefaultListModel data = new DefaultListModel();
                data.addElement(a.getUsername());
                data.addElement(a.printType());
                if (a.getFullName().equalsIgnoreCase("")) {
                    data.addElement("N/A");
                } else {
                    data.addElement(a.getFullName());
                }
                data.addElement(a.printGender());
                data.addElement(a.getSsn() + "");
                if (a.getDob() == null) {
                    data.addElement("N/A");

                } else {
                    DateFormat dateformat = DateFormat.getDateInstance(DateFormat.MEDIUM, new Locale(Localization.getLANGUAGE(), Localization.getCOUNTRY()));
                    data.addElement(dateformat.format(a.getDob()));
                }
                if (a.getPhoneNumber().equalsIgnoreCase("")) {
                    data.addElement("N/A");
                } else {
                    data.addElement(a.getPhoneNumber() + "");
                }
                if (a.getEmail().equalsIgnoreCase("")) {
                    data.addElement("N/A");
                } else {
                    data.addElement(a.getEmail());
                }
                if (a.getAddress().equalsIgnoreCase("")) {
                    data.addElement("N/A");
                } else {
                    data.addElement(a.getAddress());
                }
                Application.av.getList_account_detail_right().setModel(data);
            } else {
                Application.av.getBtn_update_account().setEnabled(false);
                ((DefaultListModel) Application.av.getList_account_detail_right().getModel()).clear();
            }
        }
        Application.av.revalidate();
    }
}
