/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Main.Application;
import Model.Account;
import Model.Account.AccountType;
import Model.Person.Gender;
import Model.Restaurant;
import Utilities.Localization;
import View.ChangePasswordForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author Nguyen Minh Sang
 */
public class ChangePasswordController implements ActionListener {

    /**
     * defines events when a user password is modified
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        // Initialise the change password form
        Restaurant r = Application.r;
        Account a = Application.currentUser;
        ImageIcon icon_change_password;
        ChangePasswordForm cpf = new ChangePasswordForm();
        cpf.getTxt_username().setText(a.getUsername());
        cpf.getTxt_full_name().setText(a.getFullName());
        cpf.getTxt_ssn().setText(a.getSsn());
        cpf.getTxt_phone().setText(a.getPhoneNumber());
        cpf.getTxt_email().setText(a.getEmail());
        cpf.getTxt_address().setText(a.getAddress());
        if (a.getUsername().equals("admin")) {
            cpf.getPwf_old_password().setEnabled(false);
            cpf.getPwf_new_password().setEnabled(false);
            cpf.getPwf_confirm_password().setEnabled(false);
        }
        if (a.getType() == AccountType.MANAGER) {
            icon_change_password = new ImageIcon(this.getClass().getResource("/Images/manager.png"));
            cpf.getRdb_manager().setSelected(true);
        } else if (a.getType() == AccountType.CASHIER) {
            icon_change_password = new ImageIcon(this.getClass().getResource("/Images/cashier.png"));
            cpf.getRdb_cashier().setSelected(true);
        } else {
            icon_change_password = new ImageIcon(this.getClass().getResource("/Images/admin.png"));
            cpf.getRdb_admin().setSelected(true);
        }
        if (a.getG() == Gender.MALE) {
            cpf.getRdb_male().setSelected(true);
        } else {
            cpf.getRdb_female().setSelected(true);
        }
        cpf.createDialog(icon_change_password);
        boolean changed = false;

        Object result;
        do {
            // Process the create category form
            cpf.getPwf_old_password().setText(null);
            cpf.getPwf_new_password().setText(null);
            cpf.getPwf_confirm_password().setText(null);
            cpf.getD().setVisible(true);
            result = cpf.getP().getValue();

            if (result == cpf.getP().getOptions()[0]) {
                String oldPassword = new String(cpf.getPwf_old_password().getPassword());
                if (oldPassword.equalsIgnoreCase("")) {
                    r.displayError(cpf.getLbl_old_password_error(), Localization.getResourceBundle().getString("ChangePasswordController.oldPassword"));
                    cpf.getPwf_old_password().requestFocus();
                } else {
                    cpf.getLbl_old_password_error().setText(null);

                    String newPassword = new String(cpf.getPwf_new_password().getPassword());
                    if (newPassword.equalsIgnoreCase("")) {
                        r.displayError(cpf.getLbl_new_password_error(), Localization.getResourceBundle().getString("ChangePasswordController.newPassword"));
                        cpf.getPwf_new_password().requestFocus();
                    } else {
                        cpf.getLbl_new_password_error().setText(null);

                        String confirmPassword = new String(cpf.getPwf_confirm_password().getPassword());
                        if (confirmPassword.equalsIgnoreCase("")) {
                            r.displayError(cpf.getLbl_confirm_password_error(), Localization.getResourceBundle().getString("ChangePasswordController.confirmPassword"));
                            cpf.getPwf_confirm_password().requestFocus();
                        } else {
                            cpf.getLbl_confirm_password_error().setText(null);

                            if (!a.getPassword().equals(oldPassword)) {
                                r.displayError(cpf.getLbl_old_password_error(), Localization.getResourceBundle().getString("ChangePasswordController.incorrectPassword"));
                                cpf.getPwf_old_password().setText(null);
                                cpf.getPwf_old_password().requestFocus();
                            } else {
                                cpf.getLbl_old_password_error().setText(null);

                                if (newPassword.length() < 6 || !newPassword.matches("^.*[A-Z]+.*$")
                                        || !newPassword.matches("^.*[0-9]+.*$")
                                        || !newPassword.matches("^.*[^A-Za-z0-9]+.*$")) {
                                    r.displayError(cpf.getLbl_new_password_error(), Localization.getResourceBundle().getString("ChangePasswordController.passwordFormat"));
                                    cpf.getPwf_new_password().setText(null);
                                    cpf.getPwf_new_password().requestFocus();
                                } else {
                                    cpf.getLbl_new_password_error().setText(null);

                                    if (!newPassword.equals(confirmPassword)) {
                                        r.displayError(cpf.getLbl_confirm_password_error(), Localization.getResourceBundle().getString("ChangePasswordController.confirmPasswordNotMatch"));
                                        cpf.getPwf_confirm_password().setText(null);
                                        cpf.getPwf_confirm_password().requestFocus();
                                    } else {
                                        cpf.getLbl_confirm_password_error().setText(null);

                                        a.setPassword(newPassword);
                                        changed = true;
                                        JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("ChangePasswordController.changePasswordSuccessfully.message"), Localization.getResourceBundle().getString("ChangePasswordController.changePasswordSuccessfully.title"), JOptionPane.INFORMATION_MESSAGE);
                                        r.update();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } while (!changed && result == cpf.getP().getOptions()[0]);
    }
}
