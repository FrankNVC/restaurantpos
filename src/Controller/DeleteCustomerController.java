/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Main.Application;
import Model.Restaurant;
import Utilities.Localization;
import View.CustomerView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JTable;

/**
 *
 * @author Mr Giang
 */
public class DeleteCustomerController extends KeyAdapter implements ActionListener {

    private Restaurant r;
    private CustomerView cv;

    public DeleteCustomerController(CustomerView cv, Restaurant r) {
        this.cv = cv;
        this.r = r;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (cv.getPnl_list_customer().getTable().getSelectedRow() != -1) {
            int result;
            if (cv.getPnl_list_customer().getTable().getSelectedRows().length > 1) {
                result = r.showConfirmDialog("customers");
            } else {
                result = r.showConfirmDialog("customer");
            }
            if (result == JOptionPane.YES_OPTION) {
                JTable list_customer = cv.getPnl_list_customer().getTable();
                
                for (int i = 0; i < list_customer.getSelectedRows().length; i++) {
                    String ssn = ((String) list_customer.getValueAt(cv.getPnl_list_customer().getTable().getSelectedRows()[i], 3));
                    r.deleteCustomer(ssn);
                }
                ((DefaultListModel) cv.getList_customer_detail_right().getModel()).clear();
                r.update();
                cv.reload();
            }
        } else {
            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("EditCustomerController.chooseAccount"));
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_DELETE) {
            if (cv.getPnl_list_customer().getTable().getSelectedRow() > -1) {
                cv.getBtn_delete_customer().doClick();
            }
        }
    }
}
