package Model;

import java.io.Serializable;

/**
 *
 * @author Nguyen Minh Sang
 */
public class OrderItem implements Serializable {

    private int orderItemID;
    private Item item;
    private String note;
    private int quantity;
    private boolean isDone;

    /**
     * constructors for OrderItem and automatically add to the Order
     * @param order
     * @param item
     * @param note
     * @param quantity
     */
    public OrderItem(Order order, Item item, String note, int quantity) {
        order.setOrderItemID();
        this.orderItemID = order.getOrderItemID();
        this.item = item;
        this.note = note;
        this.quantity = quantity;
        this.isDone = false;
        order.getOrderItems().add(this);
    }

    public void setOrderItemID(int orderItemID) {
        this.orderItemID = orderItemID;
    }

    public int getOrderItemId() {
        return orderItemID;
    }

    public Item getItem() {
        return item;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean getIsDone() {
        return isDone;
    }

    public void setIsDone(boolean isDone) {
        this.isDone = isDone;
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
