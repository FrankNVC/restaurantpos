/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.JDialogKeyListener;
import Utilities.Localization;
import Utilities.SpringUtilities;
import java.awt.Dimension;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SpringLayout;

/**
 * this is the form for creating and editing category information
 *
 * @author Pham
 */
public class CreateEditCategoryForm extends JPanel {
    
    private JLabel lbl_old_category = new JLabel(Localization.getResourceBundle().getString("CreateEditCategoryForm.lbl_old_category"));
    private JLabel lbl_category = new JLabel(Localization.getResourceBundle().getString("CreateEditCategoryForm.lbl_category"));
    private JLabel lbl_category_error = new JLabel();
    private JTextField txt_old_category = new JTextField(15);
    private JTextField txt_category = new JTextField(15);
    private JPanel pnl_main = new JPanel(new SpringLayout());
    private JOptionPane p;
    private JDialog d;
    
    public CreateEditCategoryForm(boolean edit) {
        
        this.setPreferredSize(new Dimension(200, 130));
        this.setMaximumSize(new Dimension(200, 130));
        this.setMinimumSize(new Dimension(200, 130));
        
        pnl_main.setPreferredSize(new Dimension(150, 120));
        pnl_main.setMaximumSize(new Dimension(150, 120));
        pnl_main.setMinimumSize(new Dimension(150, 120));
        
        txt_old_category.setPreferredSize(new Dimension(150, 25));
        txt_old_category.setMaximumSize(new Dimension(150, 25));
        txt_old_category.setMinimumSize(new Dimension(150, 25));
        
        txt_category.setPreferredSize(new Dimension(150, 25));
        txt_category.setMaximumSize(new Dimension(150, 25));
        txt_category.setMinimumSize(new Dimension(150, 25));
        
        txt_old_category.setEditable(!edit);
        txt_old_category.setFocusable(false);
        
        if (edit) {
            lbl_category = new JLabel(Localization.getResourceBundle().getString("CreateEditCategoryForm.lbl_category_1"));
            pnl_main.add(lbl_old_category);
            pnl_main.add(txt_old_category);
        }
        
        pnl_main.add(lbl_category);
        pnl_main.add(txt_category);
        pnl_main.add(lbl_category_error);
        
        if (edit) {
            SpringUtilities.makeCompactGrid(pnl_main, 5, 1, 0, 0, 0, 3);
        } else {
            SpringUtilities.makeCompactGrid(pnl_main, 3, 1, 0, 20, 0, 5);
        }
        
        pnl_main.setOpaque(true);
        
        this.add(pnl_main);
        
        String[] options = {Localization.getResourceBundle().getString("CreateEditCategoryForm.options.Confirm"), Localization.getResourceBundle().getString("CreateEditCategoryForm.options.Cancel")};
        ImageIcon icon_create_edit_category = new ImageIcon(this.getClass().getResource("/Images/create_edit_category.png"));
        p = new JOptionPane(this, JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION, icon_create_edit_category, options, null);
        if (!edit) {
            d = p.createDialog(Localization.getResourceBundle().getString("CreateEditCategoryForm.createNewCategory"));
        } else {
            d = p.createDialog(Localization.getResourceBundle().getString("CreateEditCategoryForm.editCategoryName"));
        }
        this.getTxt_category().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getTxt_category().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getTxt_category().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getTxt_category().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
    }
    
    public JTextField getTxt_category() {
        return txt_category;
    }
    
    public JLabel getLbl_category_error() {
        return lbl_category_error;
    }
    
    public JTextField getTxt_old_category() {
        return txt_old_category;
    }
    
    public JOptionPane getP() {
        return p;
    }
    
    public JDialog getD() {
        return d;
    }
    
}
