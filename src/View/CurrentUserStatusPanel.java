/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Controller.ChangePasswordController;
import Controller.LogoutController;
import Main.Application;
import Model.Account.AccountType;
import Model.Restaurant;
import Utilities.DigitalClock;
import Utilities.Localization;
import Utilities.REPOSButton;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * this is the header panel of the program
 *
 * @author Pham
 */
public class CurrentUserStatusPanel extends JPanel implements Observer {
    
    private Restaurant r;
    private ImageIcon icon_banner_left = new ImageIcon(this.getClass().getResource("/Images/banner_left.png"));
    private JLabel lbl_banner_left = new JLabel(icon_banner_left, JLabel.LEFT);
    private ImageIcon icon_banner_right = new ImageIcon(this.getClass().getResource("/Images/banner_right.png"));
    private JLabel lbl_banner_right = new JLabel(icon_banner_right, JLabel.RIGHT);
    private JLabel lbl_user_status = new JLabel();
    private DigitalClock pnl_datetime = new DigitalClock();
    private ImageIcon icon_user;
    private ImageIcon icon_user_clicked;
    //private REPOSButton btn_user_image; 
    private REPOSButton btn_user_image = new REPOSButton(icon_user, icon_user_clicked, Localization.getResourceBundle().getString("CurrentUserStatusPanel.btn_user_image"));  
    private ImageIcon icon_logout = new ImageIcon(this.getClass().getResource(Localization.getResourceBundle().getString("CurrentUserStatusPanel.icon_logout")));
    private ImageIcon icon_logout_clicked = new ImageIcon(this.getClass().getResource(Localization.getResourceBundle().getString("CurrentUserStatusPanel.icon_logout_clicked")));   
    private REPOSButton btn_logout = new REPOSButton(icon_logout, icon_logout_clicked, Localization.getResourceBundle().getString("CurrentUserStatusPanel.btn_logout"));
    private JPanel pnl_user_status_center = new JPanel(new BorderLayout());
    private JPanel pnl_user_status_east = new JPanel(new BorderLayout());
    private JPanel pnl_user_status_east_button = new JPanel(new BorderLayout());
    private LogoutController lc;
    
    public CurrentUserStatusPanel(Restaurant r, JFrame mainFrame) throws Exception {
        this.r = r;
        r.addObserver(this);
        
        this.setLayout(new BorderLayout());
        
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        int screenWidth = (int) toolkit.getScreenSize().getWidth() * 9 / 10;
        this.setPreferredSize(new Dimension(screenWidth, 90));
        this.setLayout(new BorderLayout());
        
        pnl_user_status_center.setBackground(Color.WHITE);
        pnl_user_status_east.setBackground(Color.WHITE);
        pnl_user_status_east_button.setBackground(Color.WHITE);
        lbl_banner_right.setBackground(Color.WHITE);
        this.setBackground(Color.WHITE);
        
        lbl_banner_left.setPreferredSize(new Dimension(screenWidth * 35 / 100, this.getHeight()));
        lbl_banner_left.setMaximumSize(new Dimension(screenWidth * 35 / 100, this.getHeight()));
        
        lbl_banner_right.setPreferredSize(new Dimension(screenWidth * 35 / 100, this.getHeight()));
        lbl_banner_right.setMaximumSize(new Dimension(screenWidth * 35 / 100, this.getHeight()));
        
        this.add(lbl_banner_left, BorderLayout.WEST);
        
        pnl_user_status_center.setPreferredSize(new Dimension(screenWidth * 10 / 100, this.getHeight()));
        pnl_user_status_center.setMaximumSize(new Dimension(screenWidth * 10 / 100, this.getHeight()));
        
        pnl_user_status_center.add(lbl_user_status, BorderLayout.NORTH);
        lbl_user_status.setFont(new Font("Calibri", Font.BOLD, 24));
        lbl_user_status.setHorizontalAlignment(JLabel.CENTER);
        lbl_user_status.setVerticalAlignment(JLabel.NORTH);        
        
        pnl_user_status_center.add(pnl_datetime, BorderLayout.CENTER);
        pnl_datetime.setFont(new Font("Aria", Font.PLAIN, 15));
        
        
        this.add(pnl_user_status_center, BorderLayout.CENTER);
        
        btn_logout.setPreferredSize(new Dimension(75, 30));
        pnl_user_status_east_button.add(btn_logout, BorderLayout.SOUTH);
        
        pnl_user_status_east.add(lbl_banner_right, BorderLayout.CENTER);
        pnl_user_status_east.add(pnl_user_status_east_button, BorderLayout.EAST);
        
        this.add(pnl_user_status_east, BorderLayout.EAST);
        
        lc = new LogoutController(r, mainFrame);
        btn_logout.addActionListener(lc);
        
        btn_logout.setMnemonic(KeyEvent.VK_F3);
    }
    
    public void reload(JFrame mainFrame) {
        
        btn_logout.removeActionListener(btn_logout.getActionListeners()[0]);
        btn_logout.addActionListener(new LogoutController(r, mainFrame));
        btn_logout.setPreferredSize(new Dimension(75, 30));
        pnl_user_status_east_button.add(btn_logout, BorderLayout.SOUTH);
        
        lbl_user_status.setText(Localization.getResourceBundle().getString("CurrentUserStatusPanel.lbl_user_status") + Application.currentUser.getUsername());
        
        if (Application.currentUser.getType() == AccountType.ADMINISTRATOR) {
            icon_user = new ImageIcon(this.getClass().getResource("/Images/admin.png"));
            icon_user_clicked = new ImageIcon(this.getClass().getResource("/Images/admin_clicked.png"));
        } else if (Application.currentUser.getType() == AccountType.MANAGER) {
            icon_user = new ImageIcon(this.getClass().getResource("/Images/manager.png"));
            icon_user_clicked = new ImageIcon(this.getClass().getResource("/Images/manager_clicked.png"));
        } else if (Application.currentUser.getType() == AccountType.CASHIER) {
            icon_user = new ImageIcon(this.getClass().getResource("/Images/cashier.png"));
            icon_user_clicked = new ImageIcon(this.getClass().getResource("/Images/cashier_clicked.png"));
        }
        btn_user_image = new REPOSButton(icon_user, icon_user_clicked, Localization.getResourceBundle().getString("CurrentUserStatusPanel.btn_user_image"));
        btn_user_image.addActionListener(new ChangePasswordController());
        btn_user_image.setPreferredSize(new Dimension(75, 65));
        btn_user_image.setEnabled(!Application.currentUser.getUsername().equalsIgnoreCase("admin"));
        pnl_user_status_east_button.add(btn_user_image, BorderLayout.CENTER);
        
        lc.updateFrame(mainFrame);
        
        this.revalidate();
    }
    
    @Override
    public void update(Observable o, Object arg) {
        this.validate();
    }
  
    public void setIcon_logout() {
        ImageIcon image_icon = new ImageIcon(this.getClass().getResource(Localization.getResourceBundle().getString("CurrentUserStatusPanel.icon_logout")));
        this.icon_logout.setImage(image_icon.getImage());
    }

    public void setIcon_logout_clicked() {
        ImageIcon image_icon = new ImageIcon(this.getClass().getResource(Localization.getResourceBundle().getString("CurrentUserStatusPanel.icon_logout_clicked")));
        this.icon_logout_clicked.setImage(image_icon.getImage());
    }

    public void setLbl_user_status(JLabel lbl_user_status) {
        this.lbl_user_status = lbl_user_status;
    }

    public void setBtn_user_image(REPOSButton btn_user_image) {
        this.btn_user_image = btn_user_image;
    }

    public void setBtn_logout(REPOSButton btn_logout) {
        this.btn_logout = btn_logout;
    }
    
    public REPOSButton getBtn_user_image() {
        return btn_user_image;
    }
    
    public REPOSButton getBtn_logout() {
        return btn_logout;
    }
    
    public JLabel getLbl_user_status() {
        return lbl_user_status;
    }
}
