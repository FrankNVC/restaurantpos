
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Main.Application;
import Model.Restaurant;
import Utilities.JPagingPanel;
import Utilities.Localization;
import Utilities.TimeIgnoringComparator;
import View.BillView;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Frank NgVietCuong
 */
public class SearchBillController extends AbstractAction implements ActionListener {

    private Restaurant r;
    private BillView bv;

    /**
     *
     * @param r
     */
    public SearchBillController(Restaurant r, BillView bv) {
        this.r = r;
        this.bv = bv;
    }

    /**
     * defines event when search bill is clicked
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Calendar calFrom = bv.getCal_from().getCalendar();
        Calendar calTo = bv.getCal_to().getCalendar();
        TimeIgnoringComparator timeComparator = new TimeIgnoringComparator();
        System.out.println(calFrom.toString());
        System.out.println(calTo.toString());
        System.out.println(timeComparator.compare(calFrom, calTo));
        if (timeComparator.compare(calFrom, calTo) <= 0) {
            String[] column_names = {"No.", "Date", "Staff", "Table", "Total"};
            ArrayList<String[]> data = r.getBillList(calFrom, calTo);
            DefaultTableModel list_bill_table_model = new DefaultTableModel(null, column_names) {
                @Override
                public Class<?> getColumnClass(int column) {
                    return (column == 0) ? Integer.class : Object.class;
                }
            };

            for (int i = 0; i < data.size(); i++) {
                list_bill_table_model.addRow(data.get(i));
            }


            if (bv.getPnl_list_bill() != null) {
                bv.getPnl_center_bill_grid_view().remove(bv.getPnl_list_bill());
            }

            JPagingPanel pnl_list_bill = new JPagingPanel(list_bill_table_model);

            bv.getPnl_center_bill_grid_view().add(pnl_list_bill, BorderLayout.CENTER);
            bv.setPnl_list_bill(pnl_list_bill);

            pnl_list_bill.getTable().setCellSelectionEnabled(false);
            pnl_list_bill.getTable().setRowSelectionAllowed(true);
            pnl_list_bill.getTable().getSelectionModel().addListSelectionListener(new SelectBillController(r, bv));

            bv.revalidate();
            System.out.println("successful");
        } else {
            System.out.println("failed!");
            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("SearchBillController.errorMessage") , Localization.getResourceBundle().getString("SearchBillController.errorMessageTitle"), JOptionPane.ERROR_MESSAGE);
        }
    }
}