package View;

import Controller.PrintBillController;
import Main.Application;
import Model.Customer;
import Model.Item;
import Model.Order;
import Model.Restaurant;
import Model.Table;
import Utilities.JTextFieldLimit;
import Utilities.Localization;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;
import javax.swing.AbstractAction;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

/**
 * this is the view for displaying payment details
 *
 * @author Pham
 */
public class PaymentView extends JFrame implements Observer {

    private JLabel lbl_discount = new JLabel(Localization.getResourceBundle().getString("PaymentView.lbl_discount"));
    private JLabel lbl_payment = new JLabel(Localization.getResourceBundle().getString("PaymentView.lbl_payment"));
    private JLabel lbl_customer = new JLabel(Localization.getResourceBundle().getString("PaymentView.lbl_customer"));
    private JScrollPane spane_bill = new JScrollPane();
    private JPanel pnl_main = new JPanel(new BorderLayout());
    private JPanel pnl_south = new JPanel(new BorderLayout());
    private JPanel pnl_discount = new JPanel(new GridLayout(2, 1));
    private JPanel pnl_pay = new JPanel(new GridLayout(2, 2));
    private JPanel pnl_button = new JPanel(new GridLayout(2, 2));
    private JEditorPane epane_payment_view = new JEditorPane();
    private Vector vct_discount = new Vector();
    private DefaultComboBoxModel model = new DefaultComboBoxModel(vct_discount);
    private JComboBox cbx_discount = new JComboBox(model);
    private JTextField txt_payment = new JTextField();
    private JTextField txt_customer = new JTextField();
    private JButton btn_print = new JButton(Localization.getResourceBundle().getString("PaymentView.btn_print"));
    private JButton btn_pay = new JButton(Localization.getResourceBundle().getString("PaymentView.btn_pay"));
    private JButton btn_clear = new JButton(Localization.getResourceBundle().getString("PaymentView.btn_clear"));
    private JButton btn_find = new JButton(Localization.getResourceBundle().getString("PaymentView.btn_find"));
    private Restaurant r;
    private String restaurantName = "RePos";
    private String address = Localization.getResourceBundle().getString("PaymentView.address");
    private String phoneNum = "37761300";
    private String staff = Application.currentUser.getFullName();
    private Order o;
    private Customer c;
    private int tableNumber;
    private String fullHTML;
    private String discountKey;
    private String exchange = "";
    private String customer = "";
    private int total;
    private HashMap<Item, Integer> finalOrderList = new HashMap<>();
    private Table t;
    private ArrayList<Table> paying_tables;
    private ArrayList<Table> current_tables;
    private CustomerProfile cp;

    public PaymentView(Restaurant r) {
        this.r = r;

        this.setPreferredSize(new Dimension(230, 650));
        this.setMaximumSize(new Dimension(230, 650));
        this.setMinimumSize(new Dimension(230, 650));

        for (Map.Entry<String, Integer> entry : r.getDiscount().entrySet()) {
            String key = entry.getKey();
            if (!key.equalsIgnoreCase("Bronze")
                    && !key.equalsIgnoreCase("Silver") && !key.equalsIgnoreCase("Gold") && !key.equalsIgnoreCase("Platinum")) {
                vct_discount.add(key + " - " + entry.getValue() + "%");
            }
        }
        cbx_discount.setSelectedItem("default - 0%");

        current_tables = Application.cmv.getCurrent_tables();
        t = current_tables.get(0);
        paying_tables = Application.cmv.getPaying_tables();
        o = t.getO();
        SetFinalOrderList(o);

        if (Application.cmv.getCurrent_tables().size() > 1) {
            tableNumber = current_tables.get(0).getTableGroupId();
        } else {
            tableNumber = current_tables.get(0).getTableNumber();
        }

        pnl_discount.add(lbl_discount);
        pnl_discount.add(cbx_discount);

        pnl_pay.add(lbl_payment);
        pnl_pay.add(lbl_customer);
        pnl_pay.add(txt_payment);
        pnl_pay.add(txt_customer);

        pnl_button.setPreferredSize(new Dimension(370, 40));
        btn_print.setFont(new Font("Arial", Font.BOLD, 11));
        btn_pay.setFont(new Font("Arial", Font.BOLD, 11));
        btn_clear.setFont(new Font("Arial", Font.BOLD, 11));
        btn_find.setFont(new Font("Arial", Font.BOLD, 11));

        pnl_button.add(btn_pay);
        pnl_button.add(btn_find);
        pnl_button.add(btn_print);
        pnl_button.add(btn_clear);

        pnl_south.add(pnl_discount, BorderLayout.NORTH);
        pnl_south.add(pnl_pay, BorderLayout.CENTER);
        pnl_south.add(pnl_button, BorderLayout.SOUTH);

        pnl_main.add(pnl_south, BorderLayout.SOUTH);
        pnl_main.add(spane_bill, BorderLayout.CENTER);

        this.add(pnl_main, BorderLayout.CENTER);

        btn_clear.setEnabled(false);
        epane_payment_view.setContentType("text/html");
        epane_payment_view.setEditable(false);
        spane_bill.setViewportView(epane_payment_view);

        txt_customer.setDocument(new JTextFieldLimit(11, "^[\\d]+$", false));

        txt_payment.setDocument(new JTextFieldLimit(10, "[0-9]+", true));
        txt_payment.requestFocusInWindow();

        txt_payment.addActionListener(new PayButtonController());
        btn_pay.addActionListener(new PayButtonController());
        btn_print.addActionListener(new PrintBillController(this));
        cbx_discount.addItemListener(new DiscountSelectListener(this));
        btn_clear.addActionListener(new ClearTableButtonController(this, current_tables));
        btn_find.addActionListener(new FindButtonController());

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (!btn_pay.isEnabled()) {
                    btn_clear.doClick();
                } else {
                    String options[] = {Localization.getResourceBundle().getString("PaymentView.options.Confirm"), Localization.getResourceBundle().getString("PaymentView.options.Cancel")};
                    if (JOptionPane.showOptionDialog(null, Localization.getResourceBundle().getString("PaymentView.exitPaymentView.message"), Localization.getResourceBundle().getString("PaymentView.exitPaymentView.title"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, null) == JOptionPane.YES_OPTION) {
                        for (int i = 0; i < paying_tables.size(); i++) {
                            if (paying_tables.get(i).getTableNumber() == current_tables.get(0).getTableNumber()) {
                                paying_tables.remove(i);
                                break;
                            }
                        }

                        if (c != null) {
                            c.setLoyalty_points(c.getLoyalty_points() + cp.getPoints());
                        }
                        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                        super.windowClosing(e);
                    }
                }
            }
        });

        txt_payment.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "pay");
        txt_payment.getActionMap().put("pay", new PayButtonController());
        txt_customer.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "find");
        txt_customer.getActionMap().put("find", new FindButtonController());

        BillView("default", "");
        this.validate();
    }

    public JEditorPane GetEditorPane() {
        return epane_payment_view;
    }

    public String getFullHTML() {
        return fullHTML;
    }

    public Vector getVctDiscount() {
        return vct_discount;
    }

    public JComboBox getCbx_discount() {
        return cbx_discount;
    }

    public void SetFinalOrderList(Order o) {

        Item item;
        int quantity = 0;

        for (int i = 0; i < o.getOrderItems().size(); i++) {
            item = o.getOrderItems().get(i).getItem();
            quantity = o.getOrderItems().get(i).getQuantity();
            if (finalOrderList.containsKey(item)) {
                finalOrderList.put(item, finalOrderList.get(item) + quantity);
            } else {
                finalOrderList.put(item, quantity);
            }
        }
    }

    /**
     * Generates the bill panel
     *
     * @param key - the discount
     * @param cash - the payment amount
     */
    public void BillView(String key, String cash) {

        DateFormat dateformat = DateFormat.getDateInstance(DateFormat.SHORT, new Locale(Localization.getLANGUAGE(), Localization.getCOUNTRY()));
        Date date = new Date();

        String localization_billview_restaurant = Localization.getResourceBundle().getString("PaymentView.localization_billview_restaurant");
        String localization_billview_tel = Localization.getResourceBundle().getString("PaymentView.localization_billview_tel");
        String localization_billview_table = Localization.getResourceBundle().getString("PaymentView.localization_billview_table");
        String localization_billview_date = Localization.getResourceBundle().getString("PaymentView.localization_billview_date");
        String localization_billview_staff = Localization.getResourceBundle().getString("PaymentView.localization_billview_staff");
        String localization_billview_subtotal = Localization.getResourceBundle().getString("PaymentView.localization_billview_subtotal");
        String localization_billview_discount = Localization.getResourceBundle().getString("PaymentView.localization_billview_discount");
        String localization_billview_total = Localization.getResourceBundle().getString("PaymentView.localization_billview_total");
        String localization_billview_thank = Localization.getResourceBundle().getString("PaymentView.localization_billview_thank");
        String localization_billview_thank2 = Localization.getResourceBundle().getString("PaymentView.localization_billview_thank2");

        String header = "<html><center><p style='font-size:10px; font-weight:bold;'>" + localization_billview_restaurant + "" + restaurantName + "</p>"
                + "<p style='font-size:10px;'>" + address + "<br/>"
                + localization_billview_tel + phoneNum + "</p></center>"
                + "<p style='text-align:right;font-size:10px;font-weight:bold;'>" + localization_billview_table + "" + tableNumber + "</p>"
                + "<p style='font-size:10px;'>" + localization_billview_date + "" + dateformat.format(date) + "<br/>"
                + localization_billview_staff + staff + "</p>";

        String table = "<body><table width='120'>"
                + "<tr>"
                + "<td colspan='3'>__________________________</td>"
                + "</tr>";

        int subTotal = 0;

        for (int i = 0; i < finalOrderList.size(); i++) {

            Item item = (Item) finalOrderList.keySet().toArray()[i];
            String name = item.getItemName();
            int quantity = finalOrderList.get(item);
            int price = item.getItemPrice();
            subTotal += price * quantity;
            String sPrice = new DecimalFormat("###,###,###").format(price);
            if (sPrice.contains(",")) {
                sPrice = sPrice.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
            } else {
                sPrice = sPrice.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
            }

            String sAmount = new DecimalFormat("###,###,###").format(price * quantity);
            if (sAmount.contains(",")) {
                sAmount = sAmount.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
            } else {
                sAmount = sAmount.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
            }

            table += "<tr>"
                    + "<td colspan='3' style='font-size:8px;' >" + name + "</td>"
                    + "</tr>"
                    + "<tr>"
                    + "<td style='font-size:8px;'>" + quantity + " x</td>"
                    + "<td style='font-size:8px;'>" + sPrice + "</td>"
                    + "<td style='text-align:right;font-size:8px;'>= " + sAmount + "</td>"
                    + "</tr>";
        }
        int discountPercentage = 0;
        if (key != null) {
            discountPercentage = r.getDiscount().get(key);
        }
        int discountAmount = subTotal * discountPercentage / 100;
        String sub = new DecimalFormat("###,###,###").format(subTotal);
        if (sub.contains(",")) {
            sub = sub.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
        } else {
            sub = sub.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
        }

        String discount = new DecimalFormat("###,###,###").format(discountAmount);
        if (discount.contains(",")) {
            discount = discount.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
        } else {
            discount = discount.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
        }

        subTotal -= discountAmount;
        String vat = new DecimalFormat("###,###,###").format(subTotal * 10 / 100);
        if (vat.contains(",")) {
            vat = vat.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
        } else {
            vat = vat.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
        }

        total = subTotal + subTotal * 10 / 100;
        if (cp != null) {
            total -= cp.getExchangeAmount();
        }
        String sTotal = new DecimalFormat("###,###,###").format(total);
        if (sTotal.contains(",")) {
            sTotal = sTotal.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
        } else {
            sTotal = sTotal.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
        }

        table += "<tr>"
                + "<td colspan='3'>__________________________</td>"
                + "</tr>"
                + "<tr style='font-size:8px;'>"
                + "<td>" + localization_billview_subtotal + "</td>" + "<td></td>" + "<td style='text-align:right;font-weight:bold;'>" + sub + "</td>"
                + "</tr>"
                + "<tr style='font-size:8px;'>"
                + "<td>" + localization_billview_discount + "</td>" + "<td>" + discountPercentage + "%</td>" + "<td style='text-align:right;font-weight:bold;'>" + discount + "</td>"
                + "</tr>"
                + "<tr style='font-size:8px;'>"
                + "<td>VAT(10%): </td>" + "<td></td>" + "<td style='text-align:right;font-weight:bold;'>" + vat + "</td>"
                + "</tr>"
                + exchange
                + "<tr>"
                + "<td colspan='3'>------------------------------------</td>"
                + "</tr>"
                + "<tr style='font-size:8px;'>"
                + "<td>" + localization_billview_total + "</td>" + "<td></td>" + "<td style='text-align:right;font-weight:bold;'>" + sTotal + "</td>"
                + "</tr>"
                + cash
                + "<tr>"
                + "<td colspan='3'>------------------------------------</td>"
                + "</tr>"
                + customer
                + "<tr>"
                + "<td colspan='3'>------------------------------------</td>"
                + "</tr>";

        table += "</table>"
                + "<center><p style='font-size:10px; font-weight:bold;'>" + localization_billview_thank + "</p>"
                + "<p style='font-size:9px'>" + localization_billview_thank2 + "</p></center>"
                + "</body></html>";

        fullHTML = header + table;

        epane_payment_view.setText(fullHTML);
        epane_payment_view.revalidate();
    }

    @Override
    public void update(Observable o, Object arg) {
        this.repaint();
    }

    /**
     * Inner class Implements ActionListener Clears all the order items from the
     * table order
     */
    class ClearTableButtonController implements ActionListener {

        private PaymentView pv;
        private ArrayList<Table> table;

        public ClearTableButtonController(PaymentView pv, ArrayList<Table> table) {
            this.pv = pv;
            this.table = table;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            String options[] = {Localization.getResourceBundle().getString("PaymentView.options.Confirm"), Localization.getResourceBundle().getString("PaymentView.options.Cancel")};
            int result = JOptionPane.showOptionDialog(null, Localization.getResourceBundle().getString("PaymentView.clearOrder.message"), Localization.getResourceBundle().getString("PaymentView.clearOrder.title"), JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE, null, options, null);
            if (result == JOptionPane.YES_OPTION) {
                r.updateTableStatus(table, Table.TableStatus.VACANT);
                for (int i = 0; i < paying_tables.size(); i++) {
                    if (paying_tables.get(i).getTableNumber() == current_tables.get(0).getTableNumber()) {
                        paying_tables.remove(i);
                        break;
                    }
                }
                pv.dispose();
                Application.cmv.deselectAllTable();
                Application.cmv.revalidate();
            }
        }
    }

    /**
     * Inner class Implements ItemListener Updates the bill when apply discount
     */
    class DiscountSelectListener implements ItemListener {

        private PaymentView pv;

        public DiscountSelectListener(PaymentView pv) {
            this.pv = pv;
        }

        @Override
        public void itemStateChanged(ItemEvent e) {
            String[] s = e.getItem().toString().split(" - ");
            discountKey = s[0].trim();
            pv.BillView(discountKey, "");
            pv.repaint();
        }
    }

    /**
     * Inner class Implements ActionListener Calculates changes and creates bill
     * object
     */
    class PayButtonController extends AbstractAction {

        @Override
        public void actionPerformed(ActionEvent e) {
            String[] s = cbx_discount.getSelectedItem().toString().split(" - ");
            discountKey = s[0].trim();
            if (!txt_payment.getText().equalsIgnoreCase("")) {
                int inputCash = Integer.parseInt(txt_payment.getText().trim().replaceAll("[, ]+", ""));
                String sCash = new DecimalFormat("###,###,###").format(inputCash);
                if (sCash.contains(",")) {
                    sCash = sCash.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                } else {
                    sCash = sCash.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                }

                int change = inputCash - total;
                if (change >= 0) {
                    int newChange;
                    if (change % 1000 < 500) {
                        newChange = change - change % 1000;
                    } else {
                        newChange = change - change % 1000 + 1000;
                    }

                    String localization_paybuttoncontroller_cash = Localization.getResourceBundle().getString("PaymentView.localization_paybuttoncontroller_cash");
                    String localization_paybuttoncontroller_change = Localization.getResourceBundle().getString("PaymentView.localization_paybuttoncontroller_change");
                    String localization_paybuttoncontroller_customer = Localization.getResourceBundle().getString("PaymentView.localization_paybuttoncontroller_customer");
                    String localization_paybuttoncontroller_total_point = Localization.getResourceBundle().getString("PaymentView.localization_paybuttoncontroller_total_point");
                    String localization_paybuttoncontroller_current_point = Localization.getResourceBundle().getString("PaymentView.localization_paybuttoncontroller_current_point");
                    String localization_paybuttoncontroller_customer_information = Localization.getResourceBundle().getString("PaymentView.localization_paybuttoncontrolle_customer_information");

                    String sChange = new DecimalFormat("###,###,###").format(newChange);
                    if (sChange.contains(",")) {
                        sChange = sChange.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                    } else {
                        sChange = sChange.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                    }

                    String cash = "<tr style='font-size:8px;'>"
                            + "<td>" + localization_paybuttoncontroller_cash + "</td>" + "<td></td>" + "<td style='text-align:right;font-weight:bold;'>" + sCash + "</td>"
                            + "</tr>"
                            + "<tr style='font-size:8px;'>"
                            + "<td>" + localization_paybuttoncontroller_change + "</td>" + "<td></td>" + "<td style='text-align:right;font-weight:bold;'>" + sChange + "</td>"
                            + "</tr>";
                    if (c != null) {
                        customer = "<tr style='font-size:8px;'>"
                                + "<td colspan='3'>" + localization_paybuttoncontroller_customer_information + "</td>"
                                + "</tr>"
                                + "<tr style='font-size:8px;'>"
                                + "<td>" + localization_paybuttoncontroller_customer + "</td><td colspan='2' style='text-align:left;font-weight:bold;'>" + "(" + c.getCt() + ") " + c.getFullName() + "</td>"
                                + "</tr>"
                                + "<tr style='font-size:8px;'>"
                                + "<td>" + localization_paybuttoncontroller_total_point + "</td><td colspan='2'> : " + localization_paybuttoncontroller_current_point + "</td>"
                                + "</tr>"
                                + "<tr style='font-size:8px;'>"
                                + "<td style='text-align:left;font-weight:bold;'>" + c.getLoyalty_points() + "</td><td colspan='2' style='text-align:left;font-weight:bold;'> : " + c.moneyToPoint(total) + "</td>"
                                + "</tr>";
                    }
                    BillView(discountKey, cash);
                    repaint();
                    btn_print.doClick();
                    btn_pay.setEnabled(false);
                    txt_payment.setEnabled(false);
                    txt_customer.setEnabled(false);
                    cbx_discount.setEnabled(false);
                    btn_clear.setEnabled(true);

                    ArrayList<String[]> orderItems = new ArrayList<>();
                    String[] items;
                    for (int i = 0; i < o.getOrderItems().size(); i++) {
                        items = new String[4];
                        items[0] = o.getOrderItems().get(i).getItem().getItemName();
                        items[1] = Integer.toString(o.getOrderItems().get(i).getItem().getItemPrice());
                        items[2] = Integer.toString(o.getOrderItems().get(i).getQuantity());
                        items[3] = o.getOrderItems().get(i).getItem().getItemCategory();
                        orderItems.add(items);
                    }

                    int new_total = total * 100 / 110;
                    if (c != null) {
                        c.pay(total);
                    }
                    r.createBill(Calendar.getInstance(), staff, tableNumber, new_total, discountKey, r.getDiscount().get(discountKey), orderItems);
                } else {
                    JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("PaymentView.invalidPayment.message"), Localization.getResourceBundle().getString("PaymentView.invalidPayment.title"), JOptionPane.OK_OPTION);
                }
            } else {
                JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("PaymentView.invalidPayment.message"), Localization.getResourceBundle().getString("PaymentView.invalidPayment.title"), JOptionPane.OK_OPTION);
            }
        }
    }

    /**
     * Inner class Implements ActionListener Finds customer by phone number and
     * displays profile
     */
    class FindButtonController extends AbstractAction {

        PaymentView pv;

        @Override
        public void actionPerformed(ActionEvent e) {
            c = r.findCustomer(txt_customer.getText());
            if (c == null) {
                JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("PaymentView.findButtonController.notFound.message"), Localization.getResourceBundle().getString("PaymentView.findButtonController.notFound.title"), JOptionPane.INFORMATION_MESSAGE);
            } else {
                cp = new CustomerProfile(c, total);
                JOptionPane.showMessageDialog(null, cp, Localization.getResourceBundle().getString("PaymentView.findButtonController.customerprofile.title"), JOptionPane.INFORMATION_MESSAGE);

                String sExchangeAmount = "";
                if (cp != null) {
                    sExchangeAmount = new DecimalFormat("###,###,###").format(cp.getExchangeAmount());
                }
                if (sExchangeAmount.contains(",")) {
                    sExchangeAmount = sExchangeAmount.replace(",", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                } else {
                    sExchangeAmount = sExchangeAmount.replace(".", Localization.getResourceBundle().getString("CashierManagerView.seperator"));
                }

                String localization_exchange = Localization.getResourceBundle().getString("PaymentView.exchange");

                exchange = "<tr style='font-size:8px;'>"
                        + "<td>" + localization_exchange + "</td>" + "<td></td>" + "<td style='text-align:right;font-weight:bold;'>" + sExchangeAmount + "</td>"
                        + "</tr>";
                BillView(discountKey, "");
                btn_find.setEnabled(false);

                vct_discount.removeAllElements();
                int rate = c.getCt().getRate();
                vct_discount.add(c.printType() + " - " + rate + "%");
                for (Map.Entry<String, Integer> entry : r.getDiscount().entrySet()) {
                    String key = entry.getKey();
                    if (entry.getValue() > rate && !key.equalsIgnoreCase("Bronze") && !key.equalsIgnoreCase("Silver") && !key.equalsIgnoreCase("Gold") && !key.equalsIgnoreCase("Platinum")) {
                        vct_discount.add(key + " - " + entry.getValue() + "%");
                    }
                }
                cbx_discount.setSelectedItem(c.printType() + " - " + rate + "%");
                repaint();
            }
        }
    }
}
