/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Main.Application;
import Model.Restaurant;
import Model.Table;
import Utilities.Localization;
import Utilities.StringUtil;
import View.CreateEditOrderItemForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.JTable;

/**
 *
 * @author Nguyen Minh Sang
 */
public class EditOrderItemController implements ActionListener {

    private Restaurant r;

    /**
     *
     * @param r
     */
    public EditOrderItemController(Restaurant r) {
        this.r = r;
    }

    /**
     * defines events when an order item is edited
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (Application.cmv.getCurrent_tables().size() == 1) {
            Table table = Application.cmv.getCurrent_tables().get(0);
            JTable order_detail = Application.cmv.getTable_order_detail();
            if (!table.getO().getOrderItems().isEmpty()) {
                CreateEditOrderItemForm ceoif = new CreateEditOrderItemForm();
                ceoif.setTxt_item_name((String) (order_detail.getValueAt(Application.cmv.getTable_order_detail().getSelectedRow(), 1)));
                ceoif.setTxt_quantity((String) (order_detail.getValueAt(Application.cmv.getTable_order_detail().getSelectedRow(), 2)));
                ceoif.setTxt_note((String) (order_detail.getValueAt(Application.cmv.getTable_order_detail().getSelectedRow(), 3)));
                if (((String) (order_detail.getValueAt(order_detail.getSelectedRow(), 4))).equals("Waiting")) {
                    ceoif.getRdb_not_done().setSelected(true);
                } else {
                    ceoif.getRdb_done().setSelected(true);
                }
                ceoif.getRdb_done().setEnabled(true);
                ceoif.getRdb_not_done().setEnabled(true);

                boolean edited = false;
                Object result;
                do {
                    ceoif.getD().setVisible(true);
                    result = ceoif.getP().getValue();
                    if (result == ceoif.getP().getOptions()[0]) {
                        r.editOrderItem(table, Integer.parseInt((String) (order_detail.getValueAt(order_detail.getSelectedRow(), 0))), Integer.parseInt(ceoif.getTxt_quantity().getText()), StringUtil.TrimSpace(ceoif.getTxt_note().getText()), ceoif.getRdb_done().isSelected());
                        r.update();
                        edited = true;
                    }
                } while (!edited && result == ceoif.getP().getOptions()[0]);
                Application.cmv.updateOrderItemList();
            } else {
                JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("EditOrderItemController.chooseOrderItem"));
            }
        } else {
            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("EditOrderItemController.chooseTable"));
        }
    }
}
