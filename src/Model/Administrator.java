package Model;

/**
 *
 * @author Nguyen Minh Sang
 */
public class Administrator extends Account {

    /**
     * constructor for Administrator auto increment of ADMINISTRATOR_ID after
     * creating new Administrator
     *
     * @param m
     * @param g
     * @param ssn
     * @param fullName
     * @param address
     */
    public Administrator(int ID, Gender g, String ssn, String fullName, String address) {
        this.username = "a";
        this.username += ID;
        this.type = AccountType.ADMINISTRATOR;
        this.g = g;
        this.ssn = ssn;
        this.fullName = fullName;
        this.address = address;
    }
}
