package Main;

import Model.Account;
import Model.Restaurant;
import Utilities.Localization;
import View.AdminView;
import View.CashierManagerView;
import View.CreateEditDiscountForm;
import View.CurrentUserStatusPanel;
import View.LoginView;
import View.TheMenuBar;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;

/*admin
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Nguyen Minh Sang
 */
public class Application {

    public static Account currentUser;
    public static JPanel currentView;
    public static LoginView lv;
    public static AdminView av;
    public static CashierManagerView cmv;
    public static CurrentUserStatusPanel av_cusp;
    public static CurrentUserStatusPanel cmv_cusp;
    public static Restaurant r;
    private static JFrame frame;
    public static TheMenuBar menubar;

    public static void main(String[] args) throws Exception {
        r = new Restaurant();
        r.initializeData();

        //set up frame
        frame = new JFrame();
        menubar = new TheMenuBar(r, frame);
        frame.setJMenuBar(menubar);

        Application.currentView = Application.lv;

        Application.av_cusp = new CurrentUserStatusPanel(r, frame);
        Application.cmv_cusp = new CurrentUserStatusPanel(r, frame);
        lv = new LoginView(r, frame);
        av = new AdminView(r);
        cmv = new CashierManagerView(r);

        r.addObserver(lv);
        r.addObserver(av);
        r.addObserver(cmv);
        r.addObserver(menubar);

        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        UIManager.put("OptionPane.background", Color.WHITE);
        UIManager.put("Panel.background", Color.WHITE);
        UIManager.put("Button.defaultButtonFollowsFocus", Boolean.TRUE);

        CreateEditDiscountForm cedf = new CreateEditDiscountForm(true);

        frame.add(lv, BorderLayout.CENTER);
        frame.setResizable(false);
        frame.pack();
        frame.setTitle(Localization.getResourceBundle().getString("Application.frame.title"));
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent e) {
                super.windowOpened(e);
                lv.getTxtUsername().requestFocus();
            }

            @Override
            public void windowClosing(WindowEvent e) {
                String options[] = {Localization.getResourceBundle().getString("Application.options.Confirm"), Localization.getResourceBundle().getString("Application.options.Cancel")};
                if (JOptionPane.showOptionDialog(null, Localization.getResourceBundle().getString("Application.showConfirmDialog.message"), Localization.getResourceBundle().getString("Application.showConfirmDialog.title"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, null) == JOptionPane.YES_OPTION) {
                    r.saveData();
                    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    super.windowClosing(e);
                }
            }
        });
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
