/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Main.Application;
import Model.Restaurant;
import Model.Table;
import Model.Table.TableStatus;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JRadioButton;

/**
 *
 * @author Frank NgVietCuong
 */
public class SetTableStatusController implements ActionListener {

    private Restaurant r;

    /**
     *
     * @param r
     */
    public SetTableStatusController(Restaurant r) {
        this.r = r;
    }

    /**
     * defines events when the status of a table is modified
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        boolean confirm = false;
        ArrayList<Table> table = Application.cmv.getCurrent_tables();
        TableStatus tableStatus = TableStatus.VACANT;

        if (e.getSource() instanceof JRadioButton) {
            JRadioButton rdb = (JRadioButton) e.getSource();
            switch (rdb.getText()) {
                case "Vacant":
                    if (r.showConfirmDialog("vacant") == 0) {
                        confirm = true;
                    }
                    break;
                case "Trống":
                    if (r.showConfirmDialog("Trống") == 0) {
                        confirm = true;
                    }
                    break;
                case "Occupied":
                    tableStatus = TableStatus.OCCUPIED;
                    confirm = true;
                    break;
                case "Có khách":
                    tableStatus = TableStatus.OCCUPIED;
                    confirm = true;
                    break;
                case "Reserved":
                    tableStatus = TableStatus.RESERVED;
                    confirm = true;
                    break;
                case "Đặt":
                    tableStatus = TableStatus.RESERVED;
                    confirm = true;
                    break;
            }
        }

        if (confirm == true) {
            r.updateTableStatus(table, tableStatus);
        }
        Application.cmv.updateTable(Application.cmv.getCurrent_tables().get(0));
    }
}
