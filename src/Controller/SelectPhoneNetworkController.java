/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Utilities.JTextFieldLimit;
import Utilities.Localization;
import View.CreateEditAccountForm;
import View.CreateEditCustomerForm;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 *
 * @author Nguyen Minh Sang
 */
public class SelectPhoneNetworkController implements ItemListener {

    private CreateEditAccountForm ceaf;
    private CreateEditCustomerForm cecf;

    /**
     *
     * @param ceaf
     */
    public SelectPhoneNetworkController(CreateEditAccountForm ceaf, CreateEditCustomerForm cecf) {
        this.ceaf = ceaf;
        this.cecf = cecf;
    }

    /**
     * defines events when a phone network is selected
     *
     * @param e
     */
    @Override
    public void itemStateChanged(ItemEvent e) {
        if (ceaf != null) {
            JTextFieldLimit tlm1 = (JTextFieldLimit) ceaf.getTxt_phone1().getDocument();
            JTextFieldLimit tlm2 = (JTextFieldLimit) ceaf.getTxt_phone2().getDocument();
            ceaf.getTxt_phone1().setText("");
            ceaf.getTxt_phone2().setText("");
            ceaf.getCbx_phone_code().clear();
            if (((String) ceaf.getCbx_network().getSelectedItem()).contains(Localization.getResourceBundle().getString("SelectPhoneNetworkController.choose"))) {
                ceaf.getCbx_phone().setSelectedIndex(-1);
                ceaf.getCbx_phone().setEnabled(false);
                ceaf.getTxt_phone1().setEnabled(false);
                ceaf.getTxt_phone2().setEnabled(false);
            } else {
                ceaf.getCbx_phone().setEnabled(true);
                ceaf.getTxt_phone1().setEnabled(true);
                ceaf.getTxt_phone2().setEnabled(true);
                if (((String) ceaf.getCbx_network().getSelectedItem()).equalsIgnoreCase(Localization.getResourceBundle().getString("SelectPhoneNetworkController.local"))) {
                    tlm1.setLimit(4);
                    tlm2.setLimit(4);
                    ceaf.getCbx_phone_code().add("08");
                } else {
                    tlm1.setLimit(3);
                    tlm2.setLimit(4);
                    if (((String) ceaf.getCbx_network().getSelectedItem()).equalsIgnoreCase("MobiFone")) {
                        ceaf.getCbx_phone_code().add("090");
                        ceaf.getCbx_phone_code().add("093");
                        ceaf.getCbx_phone_code().add("0120");
                        ceaf.getCbx_phone_code().add("0121");
                        ceaf.getCbx_phone_code().add("0122");
                        ceaf.getCbx_phone_code().add("0126");
                        ceaf.getCbx_phone_code().add("0128");
                    } else if (((String) ceaf.getCbx_network().getSelectedItem()).equalsIgnoreCase("Viettel")) {
                        ceaf.getCbx_phone_code().add("096");
                        ceaf.getCbx_phone_code().add("097");
                        ceaf.getCbx_phone_code().add("098");
                        ceaf.getCbx_phone_code().add("0162");
                        ceaf.getCbx_phone_code().add("0163");
                        ceaf.getCbx_phone_code().add("0164");
                        ceaf.getCbx_phone_code().add("0165");
                        ceaf.getCbx_phone_code().add("0166");
                        ceaf.getCbx_phone_code().add("0167");
                        ceaf.getCbx_phone_code().add("0168");
                        ceaf.getCbx_phone_code().add("0169");
                    } else if (((String) ceaf.getCbx_network().getSelectedItem()).equalsIgnoreCase("Vinaphone")) {
                        ceaf.getCbx_phone_code().add("091");
                        ceaf.getCbx_phone_code().add("094");
                        ceaf.getCbx_phone_code().add("0123");
                        ceaf.getCbx_phone_code().add("0124");
                        ceaf.getCbx_phone_code().add("0125");
                        ceaf.getCbx_phone_code().add("0127");
                        ceaf.getCbx_phone_code().add("0129");
                    } else if (((String) ceaf.getCbx_network().getSelectedItem()).equalsIgnoreCase("Vietnam Mobile")) {
                        ceaf.getCbx_phone_code().add("092");
                        ceaf.getCbx_phone_code().add("0188");
                        ceaf.getCbx_phone_code().add("0186");
                    } else if (((String) ceaf.getCbx_network().getSelectedItem()).equalsIgnoreCase("Gmobile")) {
                        ceaf.getCbx_phone_code().add("099");
                        ceaf.getCbx_phone_code().add("0199");
                    } else {
                        ceaf.getCbx_phone_code().add("095");
                    }
                }
                ceaf.getCbx_phone().setSelectedIndex(0);
            }
            if (ceaf.getCbx_network().getSelectedIndex() != 0) {
                ceaf.getTxt_phone1().requestFocus();
                ceaf.getTxt_phone1().setText("");
                ceaf.getTxt_phone2().setText("");
            }
        } else {
            JTextFieldLimit tlm1 = (JTextFieldLimit) cecf.getTxt_phone1().getDocument();
            JTextFieldLimit tlm2 = (JTextFieldLimit) cecf.getTxt_phone2().getDocument();
            cecf.getTxt_phone1().setText("");
            cecf.getTxt_phone2().setText("");
            cecf.getCbx_phone_code().clear();
            if (((String) cecf.getCbx_network().getSelectedItem()).contains(Localization.getResourceBundle().getString("SelectPhoneNetworkController.choose"))) {
                cecf.getCbx_phone().setSelectedIndex(-1);
                cecf.getCbx_phone().setEnabled(false);
                cecf.getTxt_phone1().setEnabled(false);
                cecf.getTxt_phone2().setEnabled(false);
            } else {
                cecf.getCbx_phone().setEnabled(true);
                cecf.getTxt_phone1().setEnabled(true);
                cecf.getTxt_phone2().setEnabled(true);
                if (((String) cecf.getCbx_network().getSelectedItem()).equalsIgnoreCase(Localization.getResourceBundle().getString("SelectPhoneNetworkController.local"))) {
                    tlm1.setLimit(4);
                    tlm2.setLimit(4);
                    cecf.getCbx_phone_code().add("08");
                } else {
                    tlm1.setLimit(3);
                    tlm2.setLimit(4);
                    if (((String) cecf.getCbx_network().getSelectedItem()).equalsIgnoreCase("MobiFone")) {
                        cecf.getCbx_phone_code().add("090");
                        cecf.getCbx_phone_code().add("093");
                        cecf.getCbx_phone_code().add("0120");
                        cecf.getCbx_phone_code().add("0121");
                        cecf.getCbx_phone_code().add("0122");
                        cecf.getCbx_phone_code().add("0126");
                        cecf.getCbx_phone_code().add("0128");
                    } else if (((String) cecf.getCbx_network().getSelectedItem()).equalsIgnoreCase("Viettel")) {
                        cecf.getCbx_phone_code().add("096");
                        cecf.getCbx_phone_code().add("097");
                        cecf.getCbx_phone_code().add("098");
                        cecf.getCbx_phone_code().add("0162");
                        cecf.getCbx_phone_code().add("0163");
                        cecf.getCbx_phone_code().add("0164");
                        cecf.getCbx_phone_code().add("0165");
                        cecf.getCbx_phone_code().add("0166");
                        cecf.getCbx_phone_code().add("0167");
                        cecf.getCbx_phone_code().add("0168");
                        cecf.getCbx_phone_code().add("0169");
                    } else if (((String) cecf.getCbx_network().getSelectedItem()).equalsIgnoreCase("Vinaphone")) {
                        cecf.getCbx_phone_code().add("091");
                        cecf.getCbx_phone_code().add("094");
                        cecf.getCbx_phone_code().add("0123");
                        cecf.getCbx_phone_code().add("0124");
                        cecf.getCbx_phone_code().add("0125");
                        cecf.getCbx_phone_code().add("0127");
                        cecf.getCbx_phone_code().add("0129");
                    } else if (((String) cecf.getCbx_network().getSelectedItem()).equalsIgnoreCase("Vietnam Mobile")) {
                        cecf.getCbx_phone_code().add("092");
                        cecf.getCbx_phone_code().add("0188");
                        cecf.getCbx_phone_code().add("0186");
                    } else if (((String) cecf.getCbx_network().getSelectedItem()).equalsIgnoreCase("Gmobile")) {
                        cecf.getCbx_phone_code().add("099");
                        cecf.getCbx_phone_code().add("0199");
                    } else {
                        cecf.getCbx_phone_code().add("095");
                    }
                }
                cecf.getCbx_phone().setSelectedIndex(0);
            }
            if (cecf.getCbx_network().getSelectedIndex() != 0) {
                cecf.getTxt_phone1().requestFocus();
                cecf.getTxt_phone1().setText("");
                cecf.getTxt_phone2().setText("");
            }
        }
    }
}
