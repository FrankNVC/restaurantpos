/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Restaurant;
import Utilities.Localization;
import Utilities.StringUtil;
import View.CreateEditDiscountForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import javax.swing.JOptionPane;

/**
 *
 * @author s3393322
 */
public class EditDiscountController implements ActionListener {

    private Restaurant r;

    /**
     *
     * @param r
     */
    public EditDiscountController(Restaurant r) {
        this.r = r;
    }

    /**
     * defines events when discount is edited
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (r.getDiscount().size() > 1) {
            // Initialise edit discount form
            CreateEditDiscountForm cedf = new CreateEditDiscountForm(true);
            for (Map.Entry<String, Integer> entry : r.getDiscount().entrySet()) {
                String key = entry.getKey();
                if (!key.equalsIgnoreCase("default") && !key.equalsIgnoreCase("Bronze") && !key.equalsIgnoreCase("Silver") && !key.equalsIgnoreCase("Gold") && !key.equalsIgnoreCase("Platinum")) {
                    cedf.getVct_discount().add(key);
                }
            }
            cedf.getCbx_discount().setSelectedIndex(0);
            cedf.getTxt_discount_value().setText(Integer.toString(r.getDiscount().get(cedf.getCbx_discount().getItemAt(0).toString())));
            cedf.getTxt_discount().setText(cedf.getCbx_discount().getSelectedItem().toString());

            Object result;
            boolean edited = false;
            do {
                cedf.getD().setVisible(true);
                result = cedf.getP().getValue();
                if (result == cedf.getP().getOptions()[0]) {
                    String oldDiscount = (String) cedf.getCbx_discount().getSelectedItem();
                    String newDiscount = StringUtil.TrimSpace(cedf.getTxt_discount().getText());
                    if (cedf.getChk_delete_discount().isSelected()) {
                        int result2 = JOptionPane.showConfirmDialog(null, Localization.getResourceBundle().getString("EditDiscountController.wanaDeleteDiscount.message"), Localization.getResourceBundle().getString("EditDiscountController.wanaDeleteDiscount.title"), JOptionPane.YES_NO_OPTION);
                        if (result2 == JOptionPane.YES_OPTION) {
                            r.deleteDiscount(oldDiscount);
                            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("EditDiscountController.deleteDiscountSuccessfully.message") + oldDiscount + "!", Localization.getResourceBundle().getString("EditDiscountController.deleteDiscountSuccessfully.title"), JOptionPane.INFORMATION_MESSAGE);
                            break;
                        } else {
                            continue;
                        }
                    }
                    if (!newDiscount.equals("")) {
                        if (!r.validateDiscount(newDiscount, oldDiscount)) {
                            r.displayError(cedf.getLbl_discount_error(), Localization.getResourceBundle().getString("EditDiscountController.discountAlreadyExit.message"));
                        } else {
                            cedf.getLbl_discount_error().setText(null);
                            r.editDiscount(oldDiscount, newDiscount, Integer.parseInt(cedf.getTxt_discount_value().getText()));
                            edited = true;
                            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("EditDiscountController.editDiscountSuccessfully.message") + newDiscount + "!", Localization.getResourceBundle().getString("EditDiscountController.editDiscountSuccessfully.title"), JOptionPane.INFORMATION_MESSAGE);
                            r.update();
                        }
                    } else {
                        edited = true;
                    }
                }
            } while (!edited && result == cedf.getP().getOptions()[0]);
        } else {
            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("EditDiscountController.noDiscountAvailable.message"), Localization.getResourceBundle().getString("EditDiscountController.noDiscountAvailable.title"), JOptionPane.INFORMATION_MESSAGE);
        }
    }
}