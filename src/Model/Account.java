package Model;

import java.io.Serializable;

/**
 *
 * @author Nguyen Minh Sang
 */
public class Account extends Person implements Serializable {

    /**
     * the default password for all user
     */
    public static final String DEFAULT_PASSWORD = "Abc123!";

    /**
     * Enumeration for Account Type
     */
    public enum AccountType {

        MANAGER,
        CASHIER,
        ADMINISTRATOR
    }

    protected String username = "";
    protected String password = DEFAULT_PASSWORD;
    protected AccountType type;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public AccountType getType() {
        return type;
    }

    /**
     *
     * @return String value of AccountType
     */
    public String printType() {
        if (type == AccountType.ADMINISTRATOR) {
            return "Administrator";
        } else if (type == AccountType.CASHIER) {
            return "Cashier";
        } else {
            return "Manager";
        }
    }
}
