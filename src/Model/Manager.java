package Model;

/**
 *
 * @author Nguyen Minh Sang
 */
public class Manager extends Cashier {

    /**
     * constructor for Manager auto increment of MANAGER_ID after creating new
     * Manager
     *
     * @param m
     * @param g
     * @param ssn
     * @param fullName
     * @param address
     */
    public Manager(int ID, Gender g, String ssn, String fullName, String address) {
        super(ID, g, ssn, fullName, address);
        this.username = "m";
        this.username += ID;
        this.type = AccountType.MANAGER;
        this.g = g;
        this.ssn = ssn;
        this.fullName = fullName;
        this.address = address;
    }
}
