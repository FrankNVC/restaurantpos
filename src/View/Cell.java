/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Model.Table;
import Model.Table.TableShape;
import Model.Table.TableStatus;
import java.awt.Color;
import java.awt.Graphics;
import java.io.Serializable;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.MatteBorder;

/**
 *
 * @author Na
 */
public class Cell extends JPanel implements Serializable {

    /**
     *
     */
    public boolean isChosen = false;
    private int id;
    private int column;
    private int row;
    private Table table;
    private int table_part = -1;
    private JLabel lbl = new JLabel();

    /**
     *
     * @param g
     */
    @Override
    public void paintComponent(Graphics g) {

        super.paintComponent(g);

        if (table == null || table.getTableShape() == TableShape.RECTANGLE) {
        } else {
            if (table.getTableGroupId() == -1) {
                if (table_part == 1) {
                    g.setColor(Color.WHITE);
                    g.fillPolygon(new int[]{scaleX(0), scaleX(1), scaleX(0)},
                            new int[]{scaleY(0), scaleY(0), scaleY(1)},
                            3);
                    g.setColor(Color.BLACK);
                    g.drawLine(scaleX(0), scaleY(1), scaleX(1), scaleY(0));

                } else if (table_part == 3) {
                    g.setColor(Color.WHITE);
                    g.fillPolygon(new int[]{scaleX(0), scaleX(1), scaleX(1)},
                            new int[]{scaleY(0), scaleY(1), scaleY(0)},
                            3);
                    g.setColor(Color.BLACK);
                    g.drawLine(scaleX(0), scaleY(0), scaleX(1), scaleY(1));
                } else if (table_part == 7) {
                    g.setColor(Color.WHITE);
                    g.fillPolygon(new int[]{scaleX(0), scaleX(1), scaleX(0)},
                            new int[]{scaleY(0), scaleY(1), scaleY(1)},
                            3);
                    g.setColor(Color.BLACK);
                    g.drawLine(scaleX(0), scaleY(0), scaleX(1), scaleY(1));
                } else if (table_part == 9) {
                    g.setColor(Color.WHITE);
                    g.fillPolygon(new int[]{scaleX(1), scaleX(1), scaleX(0)},
                            new int[]{scaleY(1), scaleY(0), scaleY(1)}, 3);
                    g.setColor(Color.BLACK);
                    g.drawLine(scaleX(0), scaleY(1), scaleX(1), scaleY(0));

                }
            }

        }
    }

    /**
     *
     * @param id
     * @param x
     * @param y
     * @param table
     */
    public Cell(int id, int x, int y, Table table) {
        this(id, x, y);
        this.table = table;
    }

    /**
     *
     * @param id
     * @param x
     * @param y
     */
    public Cell(int id, int x, int y) {
        this.id = id;
        this.column = x;
        this.row = y;

        this.setBorder(BorderFactory.createDashedBorder(Color.LIGHT_GRAY));
        this.setBackground(Color.WHITE);

        this.add(lbl);

    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public int getColumn() {
        return column;
    }

    /**
     *
     * @return
     */
    public int getRow() {
        return row;
    }

    /**
     *
     * @return
     */
    public Table getTable() {
        return table;
    }

    /**
     *
     * @return
     */
    public int getTable_id() {
        if (table == null) {
            return -1;
        } else {
            return table.getTableNumber();
        }
    }

    /**
     *
     * @return
     */
    public int getTable_part() {
        return table_part;
    }

    //this method set the status of the cell to used by an specified table
    /**
     *
     * @param table
     * @param table_part
     */
    public void used(Table table, int table_part) {
        this.table_part = table_part;
        used(table);
    }

    
    /**
     * set the status of the cell to used by an specified table
     * @param table specify the table which use this cell
     */
    public void used(Table table) {

        isChosen = false;
        lbl.setForeground(Color.BLACK);
        this.table = table;

        if (table_part == 5) {
            this.setBackground(Color.WHITE);
            String txt = (new Integer(table.getTableNumber())).toString();
            lbl.setText(txt);
        } else {
            if (table.getTableStatus() == TableStatus.OCCUPIED) {
                this.setBackground(Color.YELLOW);

            } else if (table.getTableStatus() == TableStatus.RESERVED) {
                this.setBackground(Color.RED);

            } else if (table.getTableStatus() == TableStatus.VACANT) {
                this.setBackground(Color.GREEN);
            }
        }
        MatteBorder border = null;

        switch (table_part) {
            case 1:
                if (table.getTableShape() == TableShape.OVAL) {
                    border = new MatteBorder(0, 0, 0, 0, Color.BLACK);
                } else {
                    border = new MatteBorder(1, 1, 0, 0, Color.BLACK);
                }

                break;
            case 2:
                border = new MatteBorder(1, 0, 1, 0, Color.BLACK);
                break;
            case 3:
                if (table.getTableShape() == TableShape.OVAL) {
                    border = new MatteBorder(0, 0, 0, 0, Color.BLACK);
                } else {
                    border = new MatteBorder(1, 0, 0, 1, Color.BLACK);
                }
                break;
            case 4:
                border = new MatteBorder(0, 1, 0, 1, Color.BLACK);
                break;
            case 5:
                border = new MatteBorder(0, 0, 0, 0, Color.BLACK);
                break;
            case 6:
                border = new MatteBorder(0, 1, 0, 1, Color.BLACK);
                break;
            case 7:
                if (table.getTableShape() == TableShape.OVAL) {
                    border = new MatteBorder(0, 0, 0, 0, Color.BLACK);
                } else {
                    border = new MatteBorder(0, 1, 1, 0, Color.BLACK);
                }
                break;
            case 8:
                border = new MatteBorder(1, 0, 1, 0, Color.BLACK);
                break;
            case 9:
                if (table.getTableShape() == TableShape.OVAL) {
                    border = new MatteBorder(0, 0, 0, 0, Color.BLACK);
                } else {
                    border = new MatteBorder(0, 0, 1, 1, Color.BLACK);
                }
                break;
        }
        this.setBorder(border);
    }

    
    /**
     * set the status of the cell to chosen by an specified table
     */
    public void chosen() {
        isChosen = true;

        if (table_part == 5) {
            this.setBackground(Color.WHITE);
            String txt = (new Integer(table.getTableNumber())).toString();
            lbl.setText(txt);
        } else {
            if (table.getTableStatus() == TableStatus.OCCUPIED) {
                this.setBackground(Color.YELLOW);
            } else if (table.getTableStatus() == TableStatus.RESERVED) {
                this.setBackground(Color.RED);

            } else if (table.getTableStatus() == TableStatus.VACANT) {
                this.setBackground(Color.GREEN);
            }
        }

        if (table_part == 1 || table_part == 3 || table_part == 7 || table_part == 9) {
            this.setBackground(Color.BLACK);
            lbl.setForeground(Color.WHITE);
        }
    }

    /**
     * set the status of the cell to used by an specified group of tables
     * @param table is the root table of the table group
     * @param table_part is the part number of the cell
     * @param number number to be displayed on the cell
     */
    public void grouped(Table table, int table_part, int number) {
        lbl.setForeground(Color.BLACK);
        this.table = table;
        this.table_part = table_part;
        isChosen = false;

        if (table.getTableStatus() == TableStatus.OCCUPIED) {
            this.setBackground(Color.YELLOW);
        } else if (table.getTableStatus() == TableStatus.RESERVED) {
            this.setBackground(Color.RED);
        } else if (table.getTableStatus() == TableStatus.VACANT) {
            this.setBackground(Color.GREEN);
        }

        MatteBorder border = null;
        switch (table_part) {
            case 1:
                border = new MatteBorder(1, 1, 0, 0, Color.BLACK);
                break;
            case 2:
                border = new MatteBorder(1, 0, 1, 0, Color.BLACK);
                break;
            case 3:
                border = new MatteBorder(1, 0, 1, 0, Color.BLACK);
                break;
            case 4:
                border = new MatteBorder(1, 0, 1, 0, Color.BLACK);
                break;
            case 5:
                border = new MatteBorder(1, 0, 1, 0, Color.BLACK);
                break;
            case 6:
                border = new MatteBorder(1, 0, 0, 1, Color.BLACK);
                break;
            case 7:
                border = new MatteBorder(0, 1, 0, 1, Color.BLACK);
                break;
            case 8:
                border = new MatteBorder(0, 0, 0, 0, Color.BLACK);
                break;
            case 9:
                border = new MatteBorder(0, 0, 0, 0, Color.BLACK);
                break;
            case 10:
                border = new MatteBorder(0, 0, 0, 0, Color.BLACK);
                break;
            case 11:
                border = new MatteBorder(0, 0, 0, 0, Color.BLACK);
                break;
            case 12:
                border = new MatteBorder(0, 1, 0, 1, Color.BLACK);
                break;
            case 13:
                border = new MatteBorder(0, 1, 0, 1, Color.BLACK);
                break;
            case 14:
                border = new MatteBorder(0, 0, 0, 0, Color.BLACK);
                break;
            case 15:
                border = new MatteBorder(0, 0, 0, 0, Color.BLACK);
                break;
            case 16:
                border = new MatteBorder(0, 0, 0, 0, Color.BLACK);
                break;
            case 17:
                border = new MatteBorder(0, 0, 0, 0, Color.BLACK);
                break;
            case 18:
                border = new MatteBorder(0, 1, 0, 1, Color.BLACK);
                break;
            case 19:
                border = new MatteBorder(0, 1, 0, 1, Color.BLACK);
                break;
            case 20:
                border = new MatteBorder(0, 0, 0, 0, Color.BLACK);
                break;
            case 21:
                border = new MatteBorder(0, 0, 0, 0, Color.BLACK);
                break;
            case 22:
                border = new MatteBorder(0, 0, 0, 0, Color.BLACK);
                break;
            case 23:
                border = new MatteBorder(0, 0, 0, 0, Color.BLACK);
                break;
            case 24:
                border = new MatteBorder(0, 1, 0, 1, Color.BLACK);
                break;
            case 25:
                border = new MatteBorder(0, 1, 0, 1, Color.BLACK);
                break;
            case 26:
                border = new MatteBorder(0, 0, 0, 0, Color.BLACK);
                break;
            case 27:
                border = new MatteBorder(0, 0, 0, 0, Color.BLACK);
                break;
            case 28:
                border = new MatteBorder(0, 0, 0, 0, Color.BLACK);
                break;
            case 29:
                border = new MatteBorder(0, 0, 0, 0, Color.BLACK);
                break;
            case 30:
                border = new MatteBorder(0, 1, 0, 1, Color.BLACK);
                break;
            case 31:
                border = new MatteBorder(0, 1, 1, 0, Color.BLACK);
                break;
            case 32:
                border = new MatteBorder(1, 0, 1, 0, Color.BLACK);
                break;
            case 33:
                border = new MatteBorder(1, 0, 1, 0, Color.BLACK);
                break;
            case 34:
                border = new MatteBorder(1, 0, 1, 0, Color.BLACK);
                break;
            case 35:
                border = new MatteBorder(1, 0, 1, 0, Color.BLACK);
                break;
            case 36:
                border = new MatteBorder(0, 0, 1, 1, Color.BLACK);
                break;

        }
        this.setBorder(border);

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (table_part == 8 + 6 * i + j) {
                    this.setBackground(Color.WHITE);
                    if (number != -1) {
                        String txt = (new Integer(number)).toString();
                        lbl.setText(txt);
                    }
                }
            }
        }
    }

    /**
     * set the status of the cell to chosen by an specified table
     */
    public void groupedChosen() {
        this.isChosen = true;
        lbl.setForeground(Color.BLACK);

        if (table.getTableStatus() == TableStatus.OCCUPIED) {
            this.setBackground(Color.YELLOW);
        } else if (table.getTableStatus() == TableStatus.RESERVED) {
            this.setBackground(Color.RED);
        } else if (table.getTableStatus() == TableStatus.VACANT) {
            this.setBackground(Color.GREEN);
        }

        if (table_part == 1 || table_part == 2 || table_part == 7 || table_part == 5 || table_part == 6 || table_part == 12 || table_part == 25 || table_part == 31 || table_part == 32 || table_part == 30 || table_part == 35 || table_part == 36) {
            this.setBackground(Color.BLACK);
        }
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (table_part == 8 + 6 * i + j) {
                    this.setBackground(Color.WHITE);
                }
            }
        }
    }

    private int scaleX(double x) {
        return (int) (x * getWidth());
    }

    private int scaleY(double y) {
        return (int) (y * getHeight());
    }

    
    /**
     * set the status of the cell to free
     */
    public void free() {
        isChosen = false;
        lbl.setText(null);
        this.setBackground(Color.WHITE);
        setBorder(BorderFactory.createDashedBorder(Color.LIGHT_GRAY));
        table = null;
        table_part = -1;
    }
}