package Utilities;

/**
 * http://lifexcode.blogspot.com/2011/04/create-digital-clock-in-java.html
 */
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

public class DigitalClock extends JPanel {

    private Timer timer;
    private int delay;
    private JLabel lbl_date = new JLabel();
    private JLabel lbl_time = new JLabel();

    public DigitalClock() {
        this.setLayout(new GridLayout(2, 1));
        
        this.setBackground(Color.WHITE);
        this.add(lbl_date);
        this.add(lbl_time);       
        lbl_date.setHorizontalAlignment(JLabel.CENTER);
        lbl_date.setVerticalAlignment(JLabel.NORTH);
        lbl_time.setHorizontalAlignment(JLabel.CENTER);
        lbl_time.setVerticalAlignment(JLabel.NORTH);        
        
        this.delay = 1000;
        createTimer();
        timer.start();
    }

    /**
     * Constructs a Digital Clock using the given pattern and delay.
     *
     * @param delay - the number of milliseconds between action events
     * @param pattern - the pattern describing the date and time format
     */
    public DigitalClock(int delay) {

        this.delay = delay;
        createTimer();
        timer.start();
    }

    private void createTimer() {
        timer = new Timer(delay, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                DateFormat dateformat = DateFormat.getDateInstance(DateFormat.FULL, new Locale(Localization.getLANGUAGE(), Localization.getCOUNTRY()));
                DateFormat timeformat = DateFormat.getTimeInstance(DateFormat.DEFAULT, new Locale(Localization.getLANGUAGE(), Localization.getCOUNTRY()));
                lbl_date.setText(dateformat.format(new Date()));
                lbl_time.setText(timeformat.format(new Date()));

            }
        });
    }

    public Timer getTimer() {
        return timer;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }
}