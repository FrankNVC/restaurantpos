package View;

import Controller.JDialogKeyListener;
import Controller.SelectPhoneNetworkController;
import Utilities.JTextFieldLimit;
import Utilities.Localization;
import Utilities.REPOSButton;
import Utilities.SpringUtilities;
import com.toedter.calendar.JCalendar;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SpringLayout;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 *
 * @author Frank NgVietCuong
 */
public class CreateEditCustomerForm extends JPanel {

    private JLabel lbl_full_name = new JLabel(Localization.getResourceBundle().getString("CreateEditCustomerForm.lbl_full_name"));
    private JLabel lbl_gender = new JLabel(Localization.getResourceBundle().getString("CreateEditCustomerForm.lbl_gender"));
    private JLabel lbl_ssn = new JLabel(Localization.getResourceBundle().getString("CreateEditCustomerForm.lbl_ssn"));
    private JLabel lbl_dob = new JLabel(Localization.getResourceBundle().getString("CreateEditCustomerForm.lbl_dob"));
    private JLabel lbl_chosen_dob = new JLabel(Localization.getResourceBundle().getString("CreateEditCustomerForm.lbl_chosen_dob"), JLabel.LEFT);
    private JLabel lbl_phone = new JLabel(Localization.getResourceBundle().getString("CreateEditCustomerForm.lbl_phone"));
    private JLabel lbl_email = new JLabel(Localization.getResourceBundle().getString("CreateEditCustomerForm.lbl_email"));
    private JLabel lbl_address = new JLabel(Localization.getResourceBundle().getString("CreateEditCustomerForm.lbl_address"));
    private JLabel lbl_full_name_error = new JLabel();
    private JLabel lbl_gender_error = new JLabel();
    private JLabel lbl_ssn_error = new JLabel();
    private JLabel lbl_dob_error = new JLabel();
    private JLabel lbl_phone_error = new JLabel();
    private JLabel lbl_email_error = new JLabel();
    private JLabel lbl_address_error = new JLabel();
    private ImageIcon icon_calendar = new ImageIcon(this.getClass().getResource("/Images/calendar.png"));
    private ImageIcon icon_calendar_clicked = new ImageIcon(this.getClass().getResource("/Images/calendar_clicked.png"));
    private REPOSButton btn_show_calendar = new REPOSButton(icon_calendar, icon_calendar_clicked, Localization.getResourceBundle().getString("CreateEditCustomerForm.btn_show_calendar"));
    private JTextField txt_type = new JTextField(15);
    private JTextField txt_full_name = new JTextField(15);
    private JTextField txt_ssn = new JTextField(15);
    private JTextField txt_phone1 = new JTextField();
    private JTextField txt_phone2 = new JTextField();
    private JTextField txt_email = new JTextField(15);
    private JTextArea txt_address = new JTextArea();
    private JRadioButton rdb_male = new JRadioButton(Localization.getResourceBundle().getString("CreateEditCustomerForm.rdb_male"));
    private JRadioButton rdb_female = new JRadioButton(Localization.getResourceBundle().getString("CreateEditCustomerForm.rdb_female"));
    private ButtonGroup group_gender = new ButtonGroup();
    private JCalendar cal_dob = new JCalendar();
    private JPanel pnl_main = new JPanel(new SpringLayout());
    private JPanel pnl_gender = new JPanel(new GridLayout(1, 2));
    private JPanel pnl_dob = new JPanel(new FlowLayout(FlowLayout.LEFT));
    private JPanel pnl_phone = new JPanel(new GridLayout(1, 4));
    private JScrollPane spane_address = new JScrollPane(txt_address);
    private Vector cbx_phone_code = new Vector();
    private DefaultComboBoxModel model_code = new DefaultComboBoxModel(cbx_phone_code);
    private JComboBox cbx_phone = new JComboBox(model_code);
    private Vector cbx_phone_network = new Vector();
    private DefaultComboBoxModel model_network = new DefaultComboBoxModel(cbx_phone_network);
    private JComboBox cbx_network = new JComboBox(model_network);
    private JOptionPane p;
    private JDialog d;

    public CreateEditCustomerForm(boolean edit) {
        this.setPreferredSize(new Dimension(350, 500));
        this.setMaximumSize(new Dimension(350, 500));
        this.setMinimumSize(new Dimension(350, 500));

        rdb_male.setBackground(Color.WHITE);
        rdb_female.setBackground(Color.WHITE);

        pnl_main.setPreferredSize(new Dimension(300, 500));
        pnl_main.setMaximumSize(new Dimension(300, 500));
        pnl_main.setMinimumSize(new Dimension(300, 500));

        txt_type.setPreferredSize(new Dimension(180, 20));
        txt_type.setMaximumSize(new Dimension(180, 20));
        txt_type.setMinimumSize(new Dimension(180, 20));

        txt_full_name.setPreferredSize(new Dimension(180, 20));
        txt_full_name.setMaximumSize(new Dimension(180, 20));
        txt_full_name.setMinimumSize(new Dimension(180, 20));

        pnl_gender.setPreferredSize(new Dimension(200, 30));
        pnl_gender.setMaximumSize(new Dimension(200, 30));
        pnl_gender.setMinimumSize(new Dimension(200, 30));

        txt_ssn.setPreferredSize(new Dimension(180, 20));
        txt_ssn.setMaximumSize(new Dimension(180, 20));
        txt_ssn.setMinimumSize(new Dimension(180, 20));

        pnl_phone.setPreferredSize(new Dimension(180, 30));
        pnl_phone.setMaximumSize(new Dimension(180, 30));
        pnl_phone.setMinimumSize(new Dimension(180, 30));

        cbx_network.setPreferredSize(new Dimension(100, 20));
        cbx_network.setMaximumSize(new Dimension(100, 20));
        cbx_network.setMinimumSize(new Dimension(100, 20));

        txt_email.setPreferredSize(new Dimension(180, 20));
        txt_email.setMaximumSize(new Dimension(180, 20));
        txt_email.setMinimumSize(new Dimension(180, 20));

        spane_address.setPreferredSize(new Dimension(180, 80));
        spane_address.setMaximumSize(new Dimension(180, 80));
        spane_address.setMinimumSize(new Dimension(180, 80));

        pnl_main.add(txt_type);

        pnl_main.add(lbl_full_name);
        pnl_main.add(txt_full_name);
        pnl_main.add(lbl_full_name_error);

        pnl_main.add(lbl_gender);
        pnl_main.add(pnl_gender);
        pnl_main.add(lbl_gender_error);

        pnl_gender.add(rdb_male);
        pnl_gender.add(rdb_female);
        group_gender.add(rdb_male);
        group_gender.add(rdb_female);

        pnl_main.add(lbl_ssn);
        pnl_main.add(txt_ssn);
        pnl_main.add(lbl_ssn_error);

        Calendar c = Calendar.getInstance();
        c.add(Calendar.YEAR, -18);
        cal_dob.setMaxSelectableDate(c.getTime());
        cal_dob.setDate(c.getTime());
        pnl_main.add(lbl_dob);

        btn_show_calendar.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String[] options = {Localization.getResourceBundle().getString("CreateEditCustomerForm.options.Confirm"), Localization.getResourceBundle().getString("CreateEditCustomerForm.options.Cancel")};
                int result = JOptionPane.showOptionDialog(null, cal_dob, Localization.getResourceBundle().getString("CreateEditCustomerForm.selectDate"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, null);
                if (result == 0) {
                    DateFormat dateformat = DateFormat.getDateInstance(DateFormat.MEDIUM, new Locale(Localization.getLANGUAGE(), Localization.getCOUNTRY()));
                    lbl_chosen_dob.setText(dateformat.format(cal_dob.getDate()));
                }
            }
        });

        lbl_chosen_dob.setPreferredSize(new Dimension(80, 20));

        pnl_dob.setPreferredSize(new Dimension(200, 35));
        pnl_dob.setMaximumSize(new Dimension(200, 35));

        pnl_dob.add(lbl_chosen_dob);
        pnl_dob.add(btn_show_calendar);

        pnl_main.add(pnl_dob);
        pnl_main.add(lbl_dob_error);

        String[] phone_network = {Localization.getResourceBundle().getString("CreateEditCustomerForm.chooseNetWork"), Localization.getResourceBundle().getString("CreateEditCustomerForm.local"), "MobiFone", "Viettel", "VinaPhone", "Vietnam Mobile", "Gmobile", "S-Fone"};
        cbx_phone_network.addAll(Arrays.asList(phone_network));

        pnl_main.add(lbl_phone);
        pnl_main.add(cbx_network);
        pnl_main.add(pnl_phone);
        pnl_phone.add(cbx_phone);
        pnl_phone.add(txt_phone1);
        pnl_phone.add(txt_phone2);
        pnl_main.add(lbl_phone_error);

        pnl_main.add(lbl_email);
        pnl_main.add(txt_email);
        pnl_main.add(lbl_email_error);

        pnl_main.add(lbl_address);
        txt_address.setLineWrap(true);
        txt_address.setBorder(BorderFactory.createLineBorder(Color.black));
        pnl_main.add(spane_address);
        pnl_main.add(lbl_address_error);

        SpringUtilities.makeCompactGrid(pnl_main, 23, 1, 20, 0, 130, 2);

        pnl_main.setOpaque(true);

        txt_type.setEditable(false);
        txt_type.setFocusable(false);
        txt_type.setVisible(edit);
        cbx_network.setSelectedIndex(0);
        cbx_phone.setEnabled(false);
        txt_phone1.setEnabled(false);
        txt_phone2.setEnabled(false);

        txt_full_name.setDocument(new JTextFieldLimit(50, "^[\\D]*$", false));
        txt_ssn.setDocument(new JTextFieldLimit(9, "^[\\d]*$", false));
        txt_phone1.setDocument(new JTextFieldLimit(4, "^[\\d]*$", false));
        txt_phone2.setDocument(new JTextFieldLimit(4, "^[\\d]*$", false));

        txt_phone1.getDocument().addDocumentListener(new TextBoxPhoneListener());

        cbx_network.addItemListener(new SelectPhoneNetworkController(null, this));
        cbx_phone.addItemListener(new ComboBoxPhoneListener());

        this.add(pnl_main);

        ImageIcon icon_create_edit_account = new ImageIcon(getClass().getResource("/Images/create_edit_account.png"));
        String[] options = {Localization.getResourceBundle().getString("CreateEditCustomerForm.options_1.Confirm"), Localization.getResourceBundle().getString("CreateEditCustomerForm.options_1.Cancel")};
        p = new JOptionPane(this, JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION, icon_create_edit_account, options, null);
        if (!edit) {
            d = p.createDialog(Localization.getResourceBundle().getString("CreateEditCustomerForm.createAccount"));
        } else {
            d = p.createDialog(Localization.getResourceBundle().getString("CreateEditCustomerForm.editAccount"));
        }
        this.getTxt_full_name().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getTxt_full_name().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getTxt_full_name().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getTxt_full_name().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getTxt_phone1().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getTxt_phone1().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getTxt_phone1().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getTxt_phone1().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getTxt_phone2().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getTxt_phone2().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getTxt_phone2().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getTxt_phone2().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getTxt_ssn().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getTxt_ssn().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getTxt_ssn().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getTxt_ssn().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getCbx_network().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getCbx_network().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getCbx_network().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getCbx_network().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getCbx_phone().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getCbx_phone().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getCbx_phone().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getCbx_phone().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getRdb_female().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getRdb_female().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getRdb_female().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getRdb_female().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
        this.getRdb_male().getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "OK");
        this.getRdb_male().getActionMap().put("OK", new JDialogKeyListener(p, d, true));
        this.getRdb_male().getInputMap().put(KeyStroke.getKeyStroke("ESCAPE"), "CANCEL");
        this.getRdb_male().getActionMap().put("CANCEL", new JDialogKeyListener(p, d, false));
    }

    public JTextField getTxt_full_name() {
        return txt_full_name;
    }

    public JTextField getTxt_ssn() {
        return txt_ssn;
    }

    public JLabel getLbl_ssn_error() {
        return lbl_ssn_error;
    }

    public JRadioButton getRdb_male() {
        return rdb_male;
    }

    public JRadioButton getRdb_female() {
        return rdb_female;
    }

    public JLabel getLbl_gender_error() {
        return lbl_gender_error;
    }

    public JTextField getTxt_type() {
        return txt_type;
    }

    public JLabel getLbl_phone_error() {
        return lbl_phone_error;
    }

    public JComboBox getCbx_phone() {
        return cbx_phone;
    }

    public JComboBox getCbx_network() {
        return cbx_network;
    }

    public Vector getCbx_phone_code() {
        return cbx_phone_code;
    } 
    
    public JTextField getTxt_phone1() {
        return txt_phone1;
    }

    public JTextField getTxt_phone2() {
        return txt_phone2;
    }

    public JTextField getTxt_username() {
        return txt_type;
    }

    public JLabel getLbl_full_name_error() {
        return lbl_full_name_error;
    }

    public JLabel getLbl_chosen_dob() {
        return lbl_chosen_dob;
    }

    public JLabel getLbl_dob_error() {
        return lbl_dob_error;
    }

    public JTextField getTxt_email() {
        return txt_email;
    }

    public JLabel getLbl_email_error() {
        return lbl_email_error;
    }

    public JTextArea getTxt_address() {
        return txt_address;
    }

    public JLabel getLbl_address_error() {
        return lbl_address_error;
    }

    public JCalendar getCal_dob() {
        return cal_dob;
    }

    public JOptionPane getP() {
        return p;
    }

    public JDialog getD() {
        return d;
    }

    class ComboBoxPhoneListener implements ItemListener {

        @Override
        public void itemStateChanged(ItemEvent e) {
            txt_phone1.requestFocus();
        }
    }

    class TextBoxPhoneListener implements DocumentListener {

        @Override
        public void insertUpdate(DocumentEvent e) {
            if (cbx_network.getSelectedIndex() == 1 && txt_phone1.getText().length() == 4) {
                txt_phone2.requestFocus();
            } else if (cbx_network.getSelectedIndex() > 1 && txt_phone1.getText().length() == 3) {
                txt_phone2.requestFocus();
            }
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            if (cbx_network.getSelectedIndex() == 1 && txt_phone1.getText().length() == 4) {
                txt_phone2.requestFocus();
            } else if (cbx_network.getSelectedIndex() > 1 && txt_phone1.getText().length() == 3) {
                txt_phone2.requestFocus();
            }
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            if (cbx_network.getSelectedIndex() == 1 && txt_phone1.getText().length() == 4) {
                txt_phone2.requestFocus();
            } else if (cbx_network.getSelectedIndex() > 1 && txt_phone1.getText().length() == 3) {
                txt_phone2.requestFocus();
            }
        }
    }
}
