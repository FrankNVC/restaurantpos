/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilities;

import java.util.Calendar;
import java.util.Comparator;

/**
 *
 * @author Frank NgVietCuong
 */
public class DateIgnoringComparator implements Comparator<Calendar> {

    @Override
    public int compare(Calendar c1, Calendar c2) {
        if (c1.get(Calendar.HOUR_OF_DAY) != c2.get(Calendar.HOUR_OF_DAY)) {
            return c1.get(Calendar.HOUR_OF_DAY) - c2.get(Calendar.HOUR_OF_DAY);
        }
        if (c1.get(Calendar.MINUTE) != c2.get(Calendar.MINUTE)) {
            return c1.get(Calendar.MINUTE) - c2.get(Calendar.MINUTE);
        }
        return c1.get(Calendar.SECOND) - c2.get(Calendar.SECOND);
    }
}