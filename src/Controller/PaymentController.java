/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Main.Application;
import Model.Restaurant;
import Utilities.Localization;
import View.PaymentView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Frank NgVietCuong
 */
public class PaymentController implements ActionListener {

    private Restaurant r;

    /**
     *
     * @param r
     */
    public PaymentController(Restaurant r) {
        this.r = r;
    }

    /**
     * defines events when payment button is clicked
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (Application.cmv.getCurrent_tables().size() == 1 && !Application.cmv.getCurrent_tables().get(0).getO().getOrderItems().isEmpty()) {
            if (!Application.cmv.getPaying_tables().contains(Application.cmv.getCurrent_tables().get(0))) {
                Application.cmv.getPaying_tables().add(Application.cmv.getCurrent_tables().get(0));
                PaymentView pv = new PaymentView(r);                
                pv.setResizable(false);
                pv.pack();
                pv.setLocationRelativeTo(null);
                pv.setVisible(true);
                pv.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            }
        } else {
            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("PaymentController.chooseTableWithOrder.message"), Localization.getResourceBundle().getString("PaymentController.chooseTableWithOrder.title"), JOptionPane.INFORMATION_MESSAGE);
        }
    }
}