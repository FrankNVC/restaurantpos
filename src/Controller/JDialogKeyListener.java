/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

/**
 *
 * @author Mr Giang
 */
public class JDialogKeyListener extends AbstractAction {

    JOptionPane p;
    JDialog d;
    boolean enter;

    public JDialogKeyListener(JOptionPane p, JDialog d, boolean enter) {
        this.p = p;
        this.d = d;
        this.enter = enter;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (enter) {
            p.setValue(p.getOptions()[0]);
        } else {
            p.setValue(p.getOptions()[1]);
        }
        d.setVisible(false);
    }
}
