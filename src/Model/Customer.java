/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import static Model.Customer.CustomerType.BRONZE;
import static Model.Customer.CustomerType.GOLD;
import static Model.Customer.CustomerType.PLATINUM;
import static Model.Customer.CustomerType.SILVER;
import java.util.Date;

/**
 *
 * @author Mr Giang
 */
public class Customer extends Person {

    public static final int MONEY_TO_POINT_RATE = 100000;
    public static final int POINT_TO_MONEY_RATE = 10000;

    public enum CustomerType {

        BRONZE(0, 0),
        SILVER(100, 5),
        GOLD(200, 10),
        PLATINUM(500, 15);
        private int value;
        private int rate;

        CustomerType(int value, int rate) {
            this.value = value;
            this.rate = rate;
        }

        public int getValue() {
            return this.value;
        }

        public int getRate() {
            return rate;
        }
    };
    private int loyalty_points = 0;
    private int total_paid = 0;
    private int unexchanged_money = 0;
    private Date register_date;
    private CustomerType ct = BRONZE;

    public Customer(Gender g, String ssn, String name, String phone_num) {

        this.g = g;
        this.ssn = ssn;
        this.fullName = name;
        this.phoneNumber = phone_num;
        register_date = new Date();
    }

    public int getLoyalty_points() {
        return loyalty_points;
    }

    @Override
    public String getAddress() {
        return super.getAddress();
    }

    @Override
    public int getAge() {
        return super.getAge();
    }

    @Override
    public Date getDob() {
        return super.getDob();
    }

    @Override
    public String getEmail() {
        return super.getEmail();
    }

    @Override
    public String getFullName() {
        return super.getFullName();
    }

    public void setCt(CustomerType ct) {
        this.ct = ct;
    }

    @Override
    public Gender getG() {
        return super.getG();
    }

    @Override
    public String getPhoneNumber() {
        return super.getPhoneNumber();
    }

    public CustomerType getCt() {
        return ct;
    }

    @Override
    public String getSsn() {
        return super.getSsn();
    }

    public int getTotal_paid() {
        return total_paid;
    }

    public int getUnexchanged_money() {
        return unexchanged_money;
    }

    public Date getRegister_date() {
        return register_date;
    }

    public void setLoyalty_points(int loyalty_points) {
        this.loyalty_points = loyalty_points;
    }

    public String printType() {
        if (ct == PLATINUM) {
            return "Platinum";
        } else if (ct == GOLD) {
            return "Gold";
        } else if (ct == SILVER) {
            return "Silver";
        } else {
            return "Bronze";
        }
    }

    public void pay(int amount) {
        total_paid += amount;
        loyalty_points += (amount + unexchanged_money) / MONEY_TO_POINT_RATE;
        unexchanged_money = (amount + unexchanged_money) % MONEY_TO_POINT_RATE;
        if (loyalty_points >= CustomerType.PLATINUM.value) {
            ct = PLATINUM;
        } else if (loyalty_points >= CustomerType.GOLD.value) {
            ct = GOLD;
        } else if (loyalty_points >= CustomerType.SILVER.value) {
            ct = SILVER;
        }
    }
    
    public int moneyToPoint(int amount){
        return amount/MONEY_TO_POINT_RATE;
    }

    public int pointToMoney(int point) {
        loyalty_points -= point;
        return point * POINT_TO_MONEY_RATE;
    }
}