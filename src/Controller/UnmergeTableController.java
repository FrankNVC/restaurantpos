/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Main.Application;
import Model.Restaurant;
import Model.Table;
import Model.Table.TableStatus;
import Utilities.Localization;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Frank NgVietCuong
 */
public class UnmergeTableController implements ActionListener {

    private Restaurant r;

    /**
     *
     * @param r
     */
    public UnmergeTableController(Restaurant r) {
        this.r = r;
    }

    /**
     * defines events when a table group is unmerged
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        if (Application.cmv.getCurrent_tables().size() == 1 && Application.cmv.getCurrent_tables().get(0).getTableGroupId() > -1) {
            Table root_table = Application.cmv.getCurrent_tables().get(0);
            if (root_table.getTableStatus() == TableStatus.VACANT) {
               
                ArrayList<Table> tables_in_group = new ArrayList<>();

                for (int i = 0; i < r.getTables().size(); i++) {
                    
                    if (r.getTables().get(i).getTableGroupId() == root_table.getTableGroupId()) {
                        tables_in_group.add(r.getTables().get(i));
                    }
                }
                r.unmergeTables(root_table);
                Application.cmv.unmergeTable(root_table, tables_in_group);

                r.update();
            }
            else
            {
                JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("UnmergeTableController.tableVacant.message"), Localization.getResourceBundle().getString("UnmergeTableController.tableVacant.title"), JOptionPane.INFORMATION_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("UnmergeTableController.chooseVacantMergedTable.message"), Localization.getResourceBundle().getString("UnmergeTableController.chooseVacantMergedTable.title"), JOptionPane.INFORMATION_MESSAGE);
        }
    }
}
