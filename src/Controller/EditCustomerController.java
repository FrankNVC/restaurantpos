package Controller;

import Model.Customer;
import Model.Person.Gender;
import Model.Restaurant;
import Utilities.Localization;
import View.CreateEditCustomerForm;
import View.CustomerView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.

 */

/**
 *
 * @author Nguyen Minh Sang
 */
public class EditCustomerController implements ActionListener {

    private Restaurant r;
    CustomerView cv;

    /**
     *
     * @param r
     */
    public EditCustomerController(Restaurant r, CustomerView cv) {
        this.r = r;
        this.cv = cv;
    }

    /**
     * defines events when an account is edited
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (cv.getPnl_list_customer().getTable().getSelectedRow() != -1) {
            CreateEditCustomerForm cecf = new CreateEditCustomerForm(true);
            String mobile = ((String) cv.getPnl_list_customer().getTable().getValueAt(cv.getPnl_list_customer().getTable().getSelectedRow(), 4));

            Customer c = r.findCustomer(mobile);
            initialiseForm(cecf, c);

            boolean edited = false;
            Object result;
            do {
                cecf.getD().setVisible(true);
                result = (String) cecf.getP().getValue();

                Gender g;
                if (cecf.getRdb_female().isSelected()) {
                    g = Gender.FEMALE;
                } else if (cecf.getRdb_male().isSelected()) {
                    g = Gender.MALE;
                } else {
                    r.displayError(cecf.getLbl_gender_error(), Localization.getResourceBundle().getString("EditCustomerController.selectGender"));
                    continue;
                }
                cecf.getLbl_gender_error().setText(null);

                String full_name = cecf.getTxt_full_name().getText().trim();
                if (full_name.equalsIgnoreCase("")) {
                    full_name = c.getFullName();
                }
                if (cecf.getLbl_chosen_dob().getText().contains("Click")) {
                    r.displayError(cecf.getLbl_dob_error(), Localization.getResourceBundle().getString("EditCustomerController.chooseDOB"));
                } else {
                    cecf.getLbl_dob_error().setText(null);

                    String email = cecf.getTxt_email().getText().trim();
                    if (email.equalsIgnoreCase("")) {
                        email = c.getEmail();
                    } else if (!email.matches("^[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+.[a-zA-Z]{2,4}$")) {
                        r.displayError(cecf.getLbl_email_error(), Localization.getResourceBundle().getString("EditCustomerController.invalidEmail"));
                        cecf.getTxt_email().setSelectionStart(0);
                        cecf.getTxt_email().setSelectionEnd(cecf.getTxt_email().getText().length());
                        continue;
                    }
                    cecf.getLbl_email_error().setText(null);

                    String phoneNumber = (String) cecf.getCbx_phone().getSelectedItem() + cecf.getTxt_phone1().getText().trim() + cecf.getTxt_phone2().getText().trim();
                    if (phoneNumber.equalsIgnoreCase("null")) {
                        phoneNumber = c.getPhoneNumber();
                    } else {
                        if (!phoneNumber.matches("^(08[0-9]{8})|(((09[0-9]{1})|(012[0-8]{1})|(016[5-9]{1})|(01((88)|(99))))[0-9]{7})$")) {
                            r.displayError(cecf.getLbl_phone_error(), Localization.getResourceBundle().getString("EditCustomerController.invalidPhoneNumber"));
                            continue;
                        }
                        if (!r.validatePhone(phoneNumber, c.getPhoneNumber())) {
                            r.displayError(cecf.getLbl_phone_error(), "This phone number is already registered!");
                            continue;
                        }
                    }
                    cecf.getLbl_phone_error().setText(null);

                    String new_ssn = cecf.getTxt_ssn().getText().trim();
                    if (new_ssn.equalsIgnoreCase("")) {
                        new_ssn = c.getSsn();
                    } else if (new_ssn.length() != 9 || new_ssn.equalsIgnoreCase("000000000")) {
                        r.displayError(cecf.getLbl_ssn_error(), Localization.getResourceBundle().getString("EditCustomerController.invalidSSN"));
                        cecf.getTxt_ssn().setSelectionStart(0);
                        cecf.getTxt_ssn().setSelectionEnd(cecf.getTxt_ssn().getText().length());
                    } else if (!r.validateSSN(true, new_ssn, c.getSsn())) {
                        r.displayError(cecf.getLbl_ssn_error(), Localization.getResourceBundle().getString("EditCustomerController.ssnAlreadyExit"));
                        cecf.getTxt_ssn().setSelectionStart(0);
                        cecf.getTxt_ssn().setSelectionEnd(cecf.getTxt_ssn().getText().length());
                    } else {
                        cecf.getLbl_ssn_error().setText(null);
                        Date date = null;
                        if (!cecf.getLbl_chosen_dob().getText().contains("Click")) {
                            date = cecf.getCal_dob().getDate();
                        }
                        edited = true;
                        r.editCustomer(c, g, new_ssn, date, full_name, phoneNumber, email, cecf.getTxt_address().getText());
                        JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("EditCustomerController.editCustomerSuccessfully.message"), Localization.getResourceBundle().getString("EditCustomerController.editCustomerSuccessfully.title"), JOptionPane.INFORMATION_MESSAGE);
                        r.update();
                    }
                }
            } while (!edited && result == cecf.getP().getOptions()[0]);
            updateCustomerDetailList(c);
        } else {
            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("EditCustomerController.chooseAccount"));
        }
    }

    private void initialiseForm(CreateEditCustomerForm ceaf, Customer c) {
        ceaf.getTxt_type().setText(c.printType());
        if (c.getG() == Gender.FEMALE) {
            ceaf.getRdb_female().setSelected(true);
        } else {
            ceaf.getRdb_male().setSelected(true);
        }
        ceaf.getTxt_full_name().setText(c.getFullName());
        ceaf.getTxt_email().setText(c.getEmail());
        if (c.getDob() != null) {
            ceaf.getLbl_chosen_dob().setText(new SimpleDateFormat("MM-dd-yyyy").format(c.getDob()));
        }
        ceaf.getTxt_address().setText(c.getAddress());
        ceaf.getTxt_ssn().setText(c.getSsn());
        if (!c.getPhoneNumber().equalsIgnoreCase("")) {
            if (c.getPhoneNumber().substring(0, 2).equalsIgnoreCase("08")) {
                ceaf.getCbx_network().setSelectedItem(Localization.getResourceBundle().getString("EditCustomerController.local"));
                ceaf.getCbx_phone().setSelectedIndex(0);
                ceaf.getTxt_phone1().setText(c.getPhoneNumber().substring(2, 6));
                ceaf.getTxt_phone2().setText(c.getPhoneNumber().substring(6));
            } else {
                if (c.getPhoneNumber().substring(0, 2).equalsIgnoreCase("09")) {
                    if (c.getPhoneNumber().substring(2, 3).equalsIgnoreCase("5")) {
                        ceaf.getCbx_network().setSelectedItem(Localization.getResourceBundle().getString("EditCustomerController.sfone"));
                    } else if (c.getPhoneNumber().substring(2, 3).equalsIgnoreCase("2")) {
                        ceaf.getCbx_network().setSelectedItem(Localization.getResourceBundle().getString("EditCustomerController.vietnamMobile"));
                    } else if (c.getPhoneNumber().substring(2, 3).equalsIgnoreCase("9")) {
                        ceaf.getCbx_network().setSelectedItem(Localization.getResourceBundle().getString("EditCustomerController.gmobile"));
                    } else if (c.getPhoneNumber().substring(2, 3).matches("^((0){1})|((3){1})$")) {
                        ceaf.getCbx_network().setSelectedItem(Localization.getResourceBundle().getString("EditCustomerController.mobiphone"));
                    } else if (c.getPhoneNumber().substring(2, 3).matches("^((1){1})|((4){1})$")) {
                        ceaf.getCbx_network().setSelectedItem(Localization.getResourceBundle().getString("EditCustomerController.vinaphone"));
                    } else {
                        ceaf.getCbx_network().setSelectedItem(Localization.getResourceBundle().getString("EditCustomerController.viettel"));
                    }
                    ceaf.getCbx_phone().setSelectedItem(c.getPhoneNumber().substring(0, 3));
                    ceaf.getTxt_phone1().setText(c.getPhoneNumber().substring(3, 6));
                    ceaf.getTxt_phone2().setText(c.getPhoneNumber().substring(6));
                } else {
                    if (c.getPhoneNumber().substring(0, 3).equalsIgnoreCase("012")) {
                        if (c.getPhoneNumber().substring(3, 4).matches("^((0){1})|((1){1})|((2){1})|((6){1})|((8){1})$")) {
                            ceaf.getCbx_network().setSelectedItem(Localization.getResourceBundle().getString("EditCustomerController.mobiphone"));
                        } else {
                            ceaf.getCbx_network().setSelectedItem(Localization.getResourceBundle().getString("EditCustomerController.vinaphone"));
                        }
                    } else if (c.getPhoneNumber().substring(0, 3).equalsIgnoreCase("019")) {
                        ceaf.getCbx_network().setSelectedItem(Localization.getResourceBundle().getString("EditCustomerController.gmobile"));
                    } else if (c.getPhoneNumber().substring(0, 3).equalsIgnoreCase("018")) {
                        ceaf.getCbx_network().setSelectedItem(Localization.getResourceBundle().getString("EditCustomerController.vietnamMobile"));
                    } else {
                        ceaf.getCbx_network().setSelectedItem(Localization.getResourceBundle().getString("EditCustomerController.viettel"));
                    }
                    ceaf.getCbx_phone().setSelectedItem(c.getPhoneNumber().substring(0, 4));
                    ceaf.getTxt_phone1().setText(c.getPhoneNumber().substring(4, 7));
                    ceaf.getTxt_phone2().setText(c.getPhoneNumber().substring(7));
                }
            }
        }
    }

    private void updateCustomerDetailList(Customer c) {
        DefaultListModel listModel = (DefaultListModel) cv.getList_customer_detail_right().getModel();
        if (listModel != null) {

            if (!((String) listModel.getElementAt(0)).contains(c.getFullName().trim())) {
                listModel.remove(0);
                if (c.getFullName().trim().equalsIgnoreCase("")) {
                    listModel.add(0, "N/A");
                } else {
                    listModel.add(0, c.getFullName().trim());
                }
            }
            if (!((String) listModel.getElementAt(1)).contains(c.printGender())) {
                listModel.remove(1);
                listModel.add(1, c.printGender());
            }
            if (!((String) listModel.getElementAt(2)).contains(new SimpleDateFormat("MM-dd-yyyy").format(c.getAge()))) {
                listModel.remove(2);
                listModel.add(2, new SimpleDateFormat("MM-dd-yyyy").format(c.getAge()));
            }
            if (!((String) listModel.getElementAt(3)).contains(c.getSsn())) {
                listModel.remove(3);
                listModel.add(3, c.getSsn());
            }
            if (!((String) listModel.getElementAt(4)).contains(c.getPhoneNumber())) {
                listModel.remove(4);
                listModel.add(4, c.getPhoneNumber());
            }
            if (!((String) listModel.getElementAt(5)).contains(c.printType())) {
                listModel.remove(5);
                listModel.add(5, c.printType());
            }
            if (!((String) listModel.getElementAt(6)).contains(c.getLoyalty_points() + "")) {
                listModel.remove(6);
                listModel.add(6, c.getLoyalty_points() + "");
            }
        }
    }
}
