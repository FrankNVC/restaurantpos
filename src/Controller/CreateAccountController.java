package Controller;

import Main.Application;
import Model.Account;
import Model.Account.AccountType;
import Model.Person.Gender;
import Model.Restaurant;
import Utilities.Localization;
import View.CreateEditAccountForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Nguyen Minh Sang
 */
public class CreateAccountController implements ActionListener {

    /**
     * defines event when an account is created
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        // Initialise the create acount form
        Restaurant r = Application.r;
        CreateEditAccountForm ceaf = new CreateEditAccountForm(false);

        boolean created = false;

        Object result;
        do {
            // Process the create category form
            ceaf.getD().setVisible(true);
            result = (String) ceaf.getP().getValue();

            if (result == ceaf.getP().getOptions()[0]) {
                AccountType type;
                if (ceaf.getRdb_admin().isSelected()) {
                    type = AccountType.ADMINISTRATOR;
                } else if (ceaf.getRdb_cashier().isSelected()) {
                    type = AccountType.CASHIER;
                } else if (ceaf.getRdb_manager().isSelected()) {
                    type = AccountType.MANAGER;
                } else {
                    r.displayError(ceaf.getLbl_account_type_error(), Localization.getResourceBundle().getString("CreateAccountController.chooseAccountType"));
                    continue;
                }
                ceaf.getLbl_account_type_error().setText(null);

                Gender g;
                if (ceaf.getRdb_female().isSelected()) {
                    g = Gender.FEMALE;
                } else if (ceaf.getRdb_male().isSelected()) {
                    g = Gender.MALE;
                } else {
                    r.displayError(ceaf.getLbl_gender_error(), Localization.getResourceBundle().getString("CreateAccountController.chooseGender"));
                    continue;
                }
                ceaf.getLbl_gender_error().setText(null);

                if (ceaf.getTxt_full_name().getText().equalsIgnoreCase("")) {
                    r.displayError(ceaf.getLbl_full_name_error(), Localization.getResourceBundle().getString("CreateAccountController.fullname"));
                } else {
                    ceaf.getLbl_full_name_error().setText(null);

                    if (ceaf.getTxt_address().getText().equalsIgnoreCase("")) {
                        r.displayError(ceaf.getLbl_address_error(), Localization.getResourceBundle().getString("CreateAccountController.address"));
                    } else {
                        ceaf.getLbl_address_error().setText(null);

                        if (ceaf.getLbl_chosen_dob().getText().contains("Click")) {
                            r.displayError(ceaf.getLbl_dob_error(), Localization.getResourceBundle().getString("CreateAccountController.dateofbirth"));
                        } else {
                            ceaf.getLbl_dob_error().setText(null);

                            String email = ceaf.getTxt_email().getText().trim();
                            if (email.equalsIgnoreCase("")) {
                                r.displayError(ceaf.getLbl_email_error(), Localization.getResourceBundle().getString("CreateAccountController.email"));
                                continue;
                            } else if (!email.matches("^[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+.[a-zA-Z]{2,4}$")) {
                                r.displayError(ceaf.getLbl_email_error(), Localization.getResourceBundle().getString("CreateAccountController.emailFormat"));
                                ceaf.getTxt_email().setSelectionStart(0);
                                ceaf.getTxt_email().setSelectionEnd(ceaf.getTxt_email().getText().length());
                                continue;
                            }
                            ceaf.getLbl_email_error().setText(null);

                            String phoneNumber = (String) ceaf.getCbx_phone().getSelectedItem() + ceaf.getTxt_phone1().getText().trim() + ceaf.getTxt_phone2().getText().trim();
                            if (phoneNumber.equalsIgnoreCase("null")) {
                                r.displayError(ceaf.getLbl_phone_error(), Localization.getResourceBundle().getString("CreateAccountController.phonenumber"));
                                continue;
                            }
                            if (!phoneNumber.matches("^(08[0-9]{8})|(((09[0-9]{1})|(012[0-8]{1})|(016[5-9]{1})|(01((88)|(99))))[0-9]{7})$")) {
                                r.displayError(ceaf.getLbl_phone_error(), Localization.getResourceBundle().getString("CreateAccountController.phonenumberFormat"));
                                continue;
                            }
                            ceaf.getLbl_phone_error().setText(null);

                            String ssn = ceaf.getTxt_ssn().getText().trim();
                            if (ssn.equalsIgnoreCase("")) {
                                r.displayError(ceaf.getLbl_ssn_error(), Localization.getResourceBundle().getString("CreateAccountController.ssn"));
                            } else if (ssn.length() != 9 || ssn.equalsIgnoreCase("000000000")) {
                                r.displayError(ceaf.getLbl_ssn_error(), Localization.getResourceBundle().getString("CreateAccountController.ssnFormat"));
                                ceaf.getTxt_ssn().setSelectionStart(0);
                                ceaf.getTxt_ssn().setSelectionEnd(ceaf.getTxt_ssn().getText().length());
                            } else if (!r.validateSSN(false, ssn, null)) {
                                r.displayError(ceaf.getLbl_ssn_error(), Localization.getResourceBundle().getString("CreateAccountController.ssnAlreadyExit"));
                                ceaf.getTxt_ssn().setSelectionStart(0);
                                ceaf.getTxt_ssn().setSelectionEnd(ceaf.getTxt_ssn().getText().length());
                            } else {
                                ceaf.getLbl_ssn_error().setText(null);
                                created = true;
                                Account newAccount = r.createAccount(type, g, ssn, ceaf.getCal_dob().getDate(), phoneNumber, email, ceaf.getTxt_full_name().getText(), ceaf.getTxt_address().getText());
                                JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("CreateAccountController.createAccountSuccessfully") + "\"" + newAccount.getUsername() + "\"!", Localization.getResourceBundle().getString("CreateAccountController.createAccountSuccessfullyTooltip"), JOptionPane.INFORMATION_MESSAGE);
                                r.update();
                            }
                        }
                    }
                }
            }
        } while (!created && result == ceaf.getP().getOptions()[0]);
    }

}
