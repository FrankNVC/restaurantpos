package View;

import Model.Customer;
import Model.Customer.CustomerType;
import Utilities.JTextFieldLimit;
import Utilities.Localization;
import Utilities.SpringUtilities;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

/**
 *
 * @author Frank NgVietCuong
 */
public class CustomerProfile extends JPanel {

    private JLabel lbl_type = new JLabel(Localization.getResourceBundle().getString("CustomerProfile.lbl_type"));
    private JLabel lbl_fullname = new JLabel(Localization.getResourceBundle().getString("CustomerProfile.lbl_fullname"));
    private JLabel lbl_gender = new JLabel(Localization.getResourceBundle().getString("CustomerProfile.lbl_gender"));
    private JLabel lbl_ssn = new JLabel(Localization.getResourceBundle().getString("CustomerProfile.lbl_ssn"));
    private JLabel lbl_dob = new JLabel(Localization.getResourceBundle().getString("CustomerProfile.lbl_dob"));
    private JLabel lbl_phone = new JLabel(Localization.getResourceBundle().getString("CustomerProfile.lbl_phone"));
    private JLabel lbl_email = new JLabel(Localization.getResourceBundle().getString("CustomerProfile.lbl_email"));
    private JLabel lbl_address = new JLabel(Localization.getResourceBundle().getString("CustomerProfile.lbl_address"));
    private JLabel lbl_point = new JLabel(Localization.getResourceBundle().getString("CustomerProfile.lbl_point"));
    private JLabel lbl_extra = new JLabel(Localization.getResourceBundle().getString("CustomerProfile.lbl_extra"));
    private JLabel lbl_missing = new JLabel(Localization.getResourceBundle().getString("CustomerProfile.lbl_missing"));
    private JLabel lbl_exchange = new JLabel(Localization.getResourceBundle().getString("CustomerProfile.lbl_exchange"));
    private JLabel lbl_bill_total = new JLabel(Localization.getResourceBundle().getString("CustomerProfile.lbl_bill_total"));
    private JLabel lbl_exchange_amount = new JLabel(Localization.getResourceBundle().getString("CustomerProfile.lbl_bill_exchange_amount"));
    private JLabel lbl_exchange_error = new JLabel();
    private JLabel lbl_register = new JLabel(Localization.getResourceBundle().getString("CustomerProfile.lbl_register"));
    private JTextField txt_type = new JTextField(15);
    private JTextField txt_fullname = new JTextField(15);
    private JTextField txt_gender = new JTextField(15);
    private JTextField txt_ssn = new JTextField(15);
    private JTextField txt_dob = new JTextField(15);
    private JTextField txt_phone = new JTextField(15);
    private JTextField txt_email = new JTextField(15);
    private JTextField txt_address = new JTextField(15);
    private JTextField txt_point = new JTextField(15);
    private JTextField txt_extra = new JTextField(15);
    private JTextField txt_missing = new JTextField(15);
    private JTextField txt_exchange = new JTextField(15);
    private JTextField txt_register = new JTextField(15);
    private JTextField txt_bill_total = new JTextField(15);
    private JTextField txt_exchange_amount = new JTextField(15);
    private JPanel pnl_main = new JPanel(new SpringLayout());
    private JButton btn_exchange = new JButton(Localization.getResourceBundle().getString("CustomerProfile.btn_exchange"));
    private Customer c;
    private int total;
    private int exchangeAmount = 0;
    private int exchange_points = 0;

    public CustomerProfile(Customer customer, int total) {

        this.setPreferredSize(new Dimension(300, 360));

        this.c = customer;
        this.total = total;

        pnl_main.add(lbl_type);
        pnl_main.add(txt_type);
        txt_type.setText(c.getCt().name());

        pnl_main.add(lbl_register);
        pnl_main.add(txt_register);
        DateFormat dateformat = DateFormat.getDateInstance(DateFormat.MEDIUM, new Locale(Localization.getLANGUAGE(), Localization.getCOUNTRY()));
        txt_register.setText(dateformat.format(c.getRegister_date()));

        pnl_main.add(lbl_fullname);
        pnl_main.add(txt_fullname);
        txt_fullname.setText(c.getFullName());

        pnl_main.add(lbl_gender);
        pnl_main.add(txt_gender);
        txt_gender.setText(c.getG().name());

        pnl_main.add(lbl_ssn);
        pnl_main.add(txt_ssn);
        txt_ssn.setText(c.getSsn());

        pnl_main.add(lbl_dob);
        pnl_main.add(txt_dob);
        txt_dob.setText(dateformat.format(c.getDob()));

        pnl_main.add(lbl_phone);
        pnl_main.add(txt_phone);
        txt_phone.setText(c.getPhoneNumber());

        pnl_main.add(lbl_email);
        pnl_main.add(txt_email);
        txt_email.setText(c.getEmail());

        pnl_main.add(lbl_address);
        pnl_main.add(txt_address);
        txt_address.setText(c.getAddress());

        pnl_main.add(lbl_point);
        pnl_main.add(txt_point);

        pnl_main.add(lbl_extra);
        pnl_main.add(txt_extra);

        pnl_main.add(lbl_missing);
        pnl_main.add(txt_missing);

        pnl_main.add(lbl_bill_total);
        pnl_main.add(txt_bill_total);

        pnl_main.add(lbl_exchange);
        pnl_main.add(txt_exchange);

        pnl_main.add(lbl_exchange_amount);
        pnl_main.add(txt_exchange_amount);

        pnl_main.add(btn_exchange);
        pnl_main.add(lbl_exchange_error);

        txt_exchange_amount.setText("0");
        txt_bill_total.setText(total + "");

        SpringUtilities.makeCompactGrid(pnl_main, 16, 2, 5, 0, 10, 2);

        reload();

        txt_exchange.setDocument(new JTextFieldLimit(8, "^[0-9]*$", false));
        btn_exchange.addActionListener(new ExchangePointController());

        txt_type.setEditable(false);
        txt_fullname.setEditable(false);
        txt_register.setEditable(false);
        txt_gender.setEditable(false);
        txt_ssn.setEditable(false);
        txt_dob.setEditable(false);
        txt_phone.setEditable(false);
        txt_email.setEditable(false);
        txt_address.setEditable(false);
        txt_point.setEditable(false);
        txt_extra.setEditable(false);
        txt_missing.setEditable(false);
        txt_bill_total.setEditable(false);
        txt_exchange_amount.setEditable(false);
        this.add(pnl_main);
    }

    public int getExchangeAmount() {
        return exchangeAmount;
    }

    public int getPoints() {
        return exchange_points;
    }

    public void reload() {
        txt_point.setText(Integer.toString(c.getLoyalty_points()));
        txt_extra.setText(Integer.toString(c.getLoyalty_points() - c.getCt().getValue()));
        int missing = 0;
        if (c.getCt() == CustomerType.BRONZE) {
            missing = CustomerType.SILVER.getValue() - c.getLoyalty_points();
        } else if (c.getCt() == CustomerType.SILVER) {
            missing = CustomerType.GOLD.getValue() - c.getLoyalty_points();
        } else if (c.getCt() == CustomerType.GOLD) {
            missing = CustomerType.PLATINUM.getValue() - c.getLoyalty_points();
        } else {
            missing = 0;
        }
        txt_missing.setText(Integer.toString(missing));
        txt_exchange_amount.setText(exchangeAmount + "");

        revalidate();
    }

    class ExchangePointController implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String points_text = txt_exchange.getText();
            if (!points_text.equalsIgnoreCase("")) {
                int points = Integer.parseInt(points_text);
                if (points <= Integer.parseInt(txt_extra.getText())) {
                    int temp = exchangeAmount + c.pointToMoney(points);
                    if (temp > total) {
                        JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("CustomerProfile.exchangeAmountExceedsTotal.message"), Localization.getResourceBundle().getString("CustomerProfile.exchangeAmountExceedsTotal.title"), JOptionPane.OK_OPTION);
                        c.setLoyalty_points(c.getLoyalty_points() + points);
                    } else {
                        exchangeAmount = temp;
                        exchange_points += points;
                    }
                    reload();
                } else {
                    JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("CustomerProfile.canNotExchangeExtraPoints.message"), Localization.getResourceBundle().getString("CustomerProfile.canNotExchangeExtraPoints.title"), JOptionPane.OK_OPTION);
                }
            }
            txt_exchange.setText("");
        }
    }
}
