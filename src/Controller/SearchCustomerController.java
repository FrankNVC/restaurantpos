/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Restaurant;
import Utilities.JPagingPanel;
import Utilities.Localization;
import View.CustomerView;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Mr Giang
 */
public class SearchCustomerController extends AbstractAction implements ActionListener {

    Restaurant r;
    CustomerView cv;

    public SearchCustomerController(Restaurant r, CustomerView cv) {
        this.r = r;
        this.cv = cv;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String[] column_names = {Localization.getResourceBundle().getString("SearchCustomerController.fullName"), Localization.getResourceBundle().getString("SearchCustomerController.gender"), Localization.getResourceBundle().getString("SearchCustomerController.age"), Localization.getResourceBundle().getString("SearchCustomerController.ssn"), Localization.getResourceBundle().getString("SearchCustomerController.mobile"), Localization.getResourceBundle().getString("SearchCustomerController.type"), Localization.getResourceBundle().getString("SearchCustomerController.point")};
        ArrayList<String[]> data;
        if(cv.getRdb_ssn().isSelected()) {
            data = r.getCustomerList(cv.getTxt_search().getText(), false);
        } else {
            data = r.getCustomerList(cv.getTxt_search().getText(), true);
        }
        
        DefaultTableModel list_customer_table_model = new DefaultTableModel(null, column_names) {
            @Override
            public Class<?> getColumnClass(int column) {
                return (column == 0) ? Integer.class : Object.class;
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (int i = 0; i < data.size(); i++) {
            list_customer_table_model.addRow(data.get(i));
        }

        if (cv.getPnl_list_customer() != null) {
            cv.getPnl_center_customer_grid_view().remove(cv.getPnl_list_customer());
        }

        JPagingPanel list = new JPagingPanel(list_customer_table_model);

        JTable table = list.getTable();
        table.setCellSelectionEnabled(false);
        table.setRowSelectionAllowed(true);
        table.getSelectionModel().addListSelectionListener(new SelectCustomerController(r, cv));

        cv.getPnl_center_customer_grid_view().add(list, BorderLayout.CENTER);
        cv.setPnl_list_customer(list);

        cv.getBtn_delete_customer().setEnabled(false);
        cv.getBtn_update_customer().setEnabled(false);
        cv.getTxt_search().setText(null);
        cv.getTxt_search().requestFocus();
        cv.validate();
    }
}
