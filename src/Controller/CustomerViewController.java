/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Restaurant;
import View.CustomerView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

/**
 *
 * @author Frank NgVietCuong
 */
public class CustomerViewController implements ActionListener {

    private Restaurant r;

    public CustomerViewController(Restaurant r) {
        this.r = r;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!CustomerView.isOpening()) {
            CustomerView ctv = new CustomerView(r);
            CustomerView.setOpening(true);
            ctv.setResizable(false);
            ctv.pack();
            ctv.setLocationRelativeTo(null);
            ctv.setVisible(true);
            ctv.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        }
    }
}
