/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilities;

/**
 *
 * @author s3393322
 */
//http://stackoverflow.com/questions/3519151/how-to-limit-the-number-of-characters-in-jtextfield
import java.text.DecimalFormat;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class JTextFieldLimit extends PlainDocument {

    private int limit;
    private String regularExpression;
    private boolean is_price;

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getLimit() {
        return limit;
    }
    
    public JTextFieldLimit(int limit, String regularExpression, boolean is_price) {
        super();
        this.limit = limit;
        this.regularExpression = regularExpression;
        this.is_price = is_price;
    }

    @Override
    public void insertString(int offset, String str, AttributeSet attr) throws BadLocationException {

        if (str != null) {
            if (getLength() + str.length() <= limit && (regularExpression.equalsIgnoreCase("") || str.matches(regularExpression))) {
                super.insertString(offset, str, attr);
            }
        }
        if (is_price && this.getLength() > 0) {
            String price = new DecimalFormat("###,###,###").format(Integer.parseInt(getText(0, getLength()).replaceAll("[,]+", "")));
            super.remove(0, getLength());
            super.insertString(0, price, attr);
        }
    }

    @Override
    public void replace(int offset, int length, String text, AttributeSet attrs) throws BadLocationException {

        if (text != null) {
            if (offset + text.length() <= limit && (regularExpression.equalsIgnoreCase("") || text.matches(regularExpression))) {
                super.replace(offset, length, text, attrs);
            }
        }
        if (is_price && this.getLength() > 0) {
            String price = new DecimalFormat("###,###,###").format(Integer.parseInt(getText(0, getLength()).replaceAll("[,]+", "")));
            super.remove(0, getLength());
            super.insertString(0, price, attrs);
        }
    }

    @Override
    public void remove(int offs, int len) throws BadLocationException {

        super.remove(offs, len);
        if (is_price && this.getLength() > 0) {
            String price = new DecimalFormat("###,###,###").format(Integer.parseInt(getText(0, getLength()).replaceAll("[,]+", "")));
            super.remove(0, getLength());
            super.insertString(0, price, null);
        }
    }
}
