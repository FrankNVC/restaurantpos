package Controller;

import Main.Application;
import Model.Account;
import Model.Account.AccountType;
import Model.Person.Gender;
import Model.Restaurant;
import Utilities.Localization;
import View.CreateEditAccountForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Nguyen Minh Sang
 */
public class EditAccountController implements ActionListener {

    private Restaurant r;

    /**
     *
     * @param r
     */
    public EditAccountController(Restaurant r) {
        this.r = r;
    }

    /**
     * defines events when an account is edited
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (Application.av.getPnl_list_account().getTable().getSelectedRow() != -1) {
            CreateEditAccountForm ceaf = new CreateEditAccountForm(true);
            String username = ((String) Application.av.getPnl_list_account().getTable().getValueAt(Application.av.getPnl_list_account().getTable().getSelectedRow(), 0));
            String type = (String) Application.av.getPnl_list_account().getTable().getValueAt(Application.av.getPnl_list_account().getTable().getSelectedRow(), 4);
            Account a = r.findAccount(username, type);
            initialiseForm(ceaf, a);

            boolean edited = false;
            Object result;
            do {
                ceaf.getD().setVisible(true);
                result = (String) ceaf.getP().getValue();

                if (result == ceaf.getP().getOptions()[0]) {
                    String password = new String(ceaf.getPwf_password().getPassword());
                    String confirm_password = new String(ceaf.getPwf_confirm_password().getPassword());
                    if (password.equalsIgnoreCase("") && confirm_password.equalsIgnoreCase("")) {
                        password = a.getPassword();
                        confirm_password = a.getPassword();
                    }
                    if (password.equalsIgnoreCase("") && !confirm_password.equalsIgnoreCase("")) {
                        r.displayError(ceaf.getLbl_password_error(), Localization.getResourceBundle().getString("EditAccountController.password"));
                    } else if (password.length() < 6 || !password.matches("^.*[A-Z]+.*$")
                            || !password.matches("^.*[0-9]+.*$")
                            || !password.matches("^.*[^A-Za-z0-9]+.*$")) {
                        r.displayError(ceaf.getLbl_password_error(), Localization.getResourceBundle().getString("EditAccountController.passwordFormat"));
                    } else {
                        ceaf.getLbl_password_error().setText(null);

                        if (!password.equalsIgnoreCase("") && confirm_password.equalsIgnoreCase("")) {
                            r.displayError(ceaf.getLbl_confirm_password_error(), Localization.getResourceBundle().getString("EditAccountController.confirmPassword"));
                        } else if (!password.equalsIgnoreCase(confirm_password)) {
                            r.displayError(ceaf.getLbl_confirm_password_error(), Localization.getResourceBundle().getString("EditAccountController.confirmPasswordNotMatch"));
                        } else {
                            ceaf.getLbl_confirm_password_error().setText(null);

                            if (ceaf.getChk_reset_password().isSelected()) {
                                password = Account.DEFAULT_PASSWORD;
                            }
                            String ssnText = ceaf.getTxt_ssn().getText().trim();
                            if (ssnText.equalsIgnoreCase("")) {
                                ssnText = a.getSsn();
                            } else if (ssnText.length() != 9 || ssnText.equalsIgnoreCase("000000000")) {
                                r.displayError(ceaf.getLbl_ssn_error(), Localization.getResourceBundle().getString("EditAccountController.ssnFormat"));
                                continue;
                            } else if (!r.validateSSN(false, ssnText, a.getSsn())) {
                                r.displayError(ceaf.getLbl_ssn_error(), Localization.getResourceBundle().getString("EditAccountController.ssnAlreadyExit"));
                                continue;
                            }
                            ceaf.getLbl_ssn_error().setText(null);

                            String phoneNumber = (String) ceaf.getCbx_phone().getSelectedItem() + ceaf.getTxt_phone1().getText().trim() + ceaf.getTxt_phone2().getText().trim();
                            if (!phoneNumber.equalsIgnoreCase("null")) {
                                if (!phoneNumber.matches("^(08[0-9]{8})|(((09[0-9]{1})|(012[0-8]{1})|(016[5-9]{1})|(01((88)|(99))))[0-9]{7})$")) {
                                    r.displayError(ceaf.getLbl_phone_error(), Localization.getResourceBundle().getString("EditAccountController.phonenumberFormat"));
                                    continue;
                                }
                            } else {
                                phoneNumber = a.getPhoneNumber();
                            }
                            ceaf.getLbl_phone_error().setText(null);

                            String full_name = ceaf.getTxt_full_name().getText().trim();
                            if (full_name.equalsIgnoreCase("")) {
                                full_name = a.getFullName();
                            }

                            String address = ceaf.getTxt_address().getText().trim();
                            if (address.equalsIgnoreCase("")) {
                                address = a.getAddress();
                            }

                            String email = ceaf.getTxt_email().getText().trim();
                            if (!email.equalsIgnoreCase("")) {
                                if (!email.matches("^[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+.[a-zA-Z]{2,4}$")) {
                                    r.displayError(ceaf.getLbl_email_error(), Localization.getResourceBundle().getString("EditAccountController.emailFormat"));
                                    continue;
                                }
                            } else {
                                email = a.getEmail();
                            }
                            ceaf.getLbl_email_error().setText(null);

                            Date dob = null;
                            if (!ceaf.getLbl_chosen_dob().getText().contains("Click")) {
                                dob = ceaf.getCal_dob().getDate();
                            }
                            Gender g;
                            if (ceaf.getRdb_female().isSelected()) {
                                g = Account.Gender.FEMALE;
                            } else {
                                g = Account.Gender.MALE;
                            }
                            AccountType selected_type;
                            if (ceaf.getRdb_admin().isSelected()) {
                                selected_type = AccountType.ADMINISTRATOR;
                            } else if (ceaf.getRdb_cashier().isSelected()) {
                                selected_type = AccountType.CASHIER;
                            } else {
                                selected_type = AccountType.MANAGER;
                            }

                            r.editAccount(a, password, selected_type, g, ssnText, dob, full_name, phoneNumber, email, address);
                            edited = true;
                            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("EditAccountController.editAccountSuccessfully") + "\"" + a.getUsername() + "\"!", Localization.getResourceBundle().getString("EditAccountController.editAccountTooltip"), JOptionPane.INFORMATION_MESSAGE);
                            r.update();
                        }
                    }
                }
            } while (!edited && result == ceaf.getP().getOptions()[0]);
            updateAccountDetailList(a);
        } else {
            JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("EditAccountController.chooseAccount"), Localization.getResourceBundle().getString("EditAccountController.chooseAccountTitle"), JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private void initialiseForm(CreateEditAccountForm ceaf, Account a) {
        if (a.getType() == Account.AccountType.ADMINISTRATOR) {
            ceaf.getRdb_admin().setSelected(true);
        } else if (a.getType() == Account.AccountType.CASHIER) {
            ceaf.getRdb_cashier().setSelected(true);
        } else {
            ceaf.getRdb_manager().setSelected(true);
        }
        if (a.getUsername().equalsIgnoreCase(Application.currentUser.getUsername())) {
            ceaf.getRdb_cashier().setEnabled(false);
            ceaf.getRdb_manager().setEnabled(false);
        }
        if (a.getG() == Gender.FEMALE) {
            ceaf.getRdb_female().setSelected(true);
        } else {
            ceaf.getRdb_male().setSelected(true);
        }
        ceaf.getTxt_username().setText(a.getUsername());
        ceaf.getTxt_full_name().setText(a.getFullName());
        ceaf.getTxt_email().setText(a.getEmail());
        if (a.getDob() != null) {
            ceaf.getLbl_chosen_dob().setText(new SimpleDateFormat("MM-dd-yyyy").format(a.getDob()));
        }
        ceaf.getTxt_address().setText(a.getAddress());
        ceaf.getTxt_ssn().setText(a.getSsn());
        if (!a.getPhoneNumber().equalsIgnoreCase("")) {
            if (a.getPhoneNumber().substring(0, 2).equalsIgnoreCase("08")) {
                ceaf.getCbx_network().setSelectedItem(Localization.getResourceBundle().getString("EditAccountController.phonenumberLocal"));
                ceaf.getCbx_phone().setSelectedIndex(0);
                ceaf.getTxt_phone1().setText(a.getPhoneNumber().substring(2, 6));
                ceaf.getTxt_phone2().setText(a.getPhoneNumber().substring(6));
            } else {
                if (a.getPhoneNumber().substring(0, 2).equalsIgnoreCase("09")) {
                    if (a.getPhoneNumber().substring(2, 3).equalsIgnoreCase("5")) {
                        ceaf.getCbx_network().setSelectedItem("S-Fone");
                    } else if (a.getPhoneNumber().substring(2, 3).equalsIgnoreCase("2")) {
                        ceaf.getCbx_network().setSelectedItem("Vietnam Mobile");
                    } else if (a.getPhoneNumber().substring(2, 3).equalsIgnoreCase("9")) {
                        ceaf.getCbx_network().setSelectedItem("Gmobile");
                    } else if (a.getPhoneNumber().substring(2, 3).matches("^((0){1})|((3){1})$")) {
                        ceaf.getCbx_network().setSelectedItem("MobiFone");
                    } else if (a.getPhoneNumber().substring(2, 3).matches("^((1){1})|((4){1})$")) {
                        ceaf.getCbx_network().setSelectedItem("VinaPhone");
                    } else {
                        ceaf.getCbx_network().setSelectedItem("Viettel");
                    }
                    ceaf.getCbx_phone().setSelectedItem(a.getPhoneNumber().substring(0, 3));
                    ceaf.getTxt_phone1().setText(a.getPhoneNumber().substring(3, 6));
                    ceaf.getTxt_phone2().setText(a.getPhoneNumber().substring(6));
                } else {
                    if (a.getPhoneNumber().substring(0, 3).equalsIgnoreCase("012")) {
                        if (a.getPhoneNumber().substring(3, 4).matches("^((0){1})|((1){1})|((2){1})|((6){1})|((8){1})$")) {
                            ceaf.getCbx_network().setSelectedItem("MobiFone");
                        } else {
                            ceaf.getCbx_network().setSelectedItem("VinaPhone");
                        }
                    } else if (a.getPhoneNumber().substring(0, 3).equalsIgnoreCase("019")) {
                        ceaf.getCbx_network().setSelectedItem("Gmobile");
                    } else if (a.getPhoneNumber().substring(0, 3).equalsIgnoreCase("018")) {
                        ceaf.getCbx_network().setSelectedItem("Vietnam Mobile");
                    } else {
                        ceaf.getCbx_network().setSelectedItem("Viettel");
                    }
                    ceaf.getCbx_phone().setSelectedItem(a.getPhoneNumber().substring(0, 4));
                    ceaf.getTxt_phone1().setText(a.getPhoneNumber().substring(4, 7));
                    ceaf.getTxt_phone2().setText(a.getPhoneNumber().substring(7));
                }
            }
        }
    }

    private void updateAccountDetailList(Account a) {
        DefaultListModel listModel = (DefaultListModel) Application.av.getList_account_detail_right().getModel();
        if (listModel != null) {
            if (!((String) listModel.getElementAt(1)).contains(a.printType())) {
                listModel.remove(1);
                listModel.add(1, a.printType());
            }
            if (!((String) listModel.getElementAt(2)).contains(a.getFullName().trim())) {
                listModel.remove(2);
                if (a.getFullName().trim().equalsIgnoreCase("")) {
                    listModel.add(2, "N/A");
                } else {
                    listModel.add(2, a.getFullName().trim());
                }
            }
            if (!((String) listModel.getElementAt(3)).contains(a.printGender())) {
                listModel.remove(3);
                listModel.add(3, a.printGender());
            }
            if (!((String) listModel.getElementAt(4)).contains(a.getSsn())) {
                listModel.remove(4);
                listModel.add(4, a.getSsn());
            }
            if (a.getDob() != null) {
                if (!((String) listModel.getElementAt(5)).contains(new SimpleDateFormat("MM-dd-yyyy").format(a.getDob()))) {
                    listModel.remove(5);
                    listModel.add(5, new SimpleDateFormat("MM-dd-yyyy").format(a.getDob()));
                }
            } else {
                listModel.remove(5);
                listModel.add(5, "N/A");
            }
            if (!((String) listModel.getElementAt(6)).contains(a.getPhoneNumber() + "")) {
                listModel.remove(7);
                if (a.getPhoneNumber().equalsIgnoreCase("")) {
                    listModel.add(7, "N/A");
                } else {
                    listModel.add(7, a.getPhoneNumber() + "");
                }
            }
            if (!((String) listModel.getElementAt(7)).contains(a.getEmail().trim())) {
                listModel.remove(8);
                if (a.getEmail().trim().equalsIgnoreCase("")) {
                    listModel.add(8, "N/A");
                } else {
                    listModel.add(8, a.getEmail().trim());
                }
            }
            if (!((String) listModel.getElementAt(8)).contains(a.getAddress().trim())) {
                listModel.remove(6);
                if (a.getAddress().trim().equalsIgnoreCase("")) {
                    listModel.add(6, "N/A");
                } else {
                    listModel.add(6, a.getAddress().trim());
                }
            }
        }
    }
}
