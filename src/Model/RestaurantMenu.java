package Model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Nguyen Minh Sang
 */
public class RestaurantMenu implements Serializable{

    private ArrayList<Item> items;
    private ArrayList<String> categories;

    /**
     * initializes the categories and items array list
     */
    public RestaurantMenu() {
        items = new ArrayList<>();
        categories = new ArrayList<>();
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    public ArrayList<String> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<String> categories) {
        this.categories = categories;
    }

    public void createCategory(String name) {
        categories.add(name);
    }

    /**
     * updates the category in the categories array list
     *
     * @param oldName
     * @param newName
     */
    public void editCategory(String oldName, String newName) {
        for (int i = 0; i < categories.size(); i++) {
            if (oldName.equalsIgnoreCase(categories.get(i))) {
                categories.remove(i);
                categories.add(i, newName);
                for (int j = 0; j < items.size(); j++) {
                    if (items.get(j).getItemCategory().equalsIgnoreCase(oldName)) {
                        items.get(j).setItemCategory(newName);
                    }
                }
                break;
            }
        }
    }

    /**
     * deletes the category in the categories array list
     *
     * @param name
     */
    public void deleteCategory(String name) {
        for (int i = 0; i < categories.size(); i++) {
            if (categories.get(i).equalsIgnoreCase(name)) {
                categories.remove(i);
                for (int j = 0; j < items.size(); j++) {
                    if (items.get(j).getItemCategory().equalsIgnoreCase(name)) {
                        items.remove(j);
                    }
                }
                sortItems();
                break;
            }
        }
        sortCategories();
    }

    /**
     * sort the categories in the categories array list in alphabetical order
     */
    public void sortCategories() {
        for (int i = 1; i < categories.size(); i++) {
            if (categories.get(i).compareTo(categories.get(i - 1)) < 1) {
                for (int j = i; j > 0; j--) {
                    if (categories.get(j).compareTo(categories.get(j - 1)) < 1) {
                        String temp = categories.remove(j);
                        categories.add(j, categories.get(j - 1));
                        categories.remove(j - 1);
                        categories.add(j - 1, temp);
                    } else {
                        break;
                    }
                }
            }
        }
    }

    /**
     * creates a new Item and add to items array list
     *
     * @param name
     * @param price
     * @param category
     */
    public void createItem(String name, int price, String category) {
        Item i = new Item(name, price, category);
        items.add(i);
        sortItems();
    }

    /**
     * updates an item in the items array list
     *
     * @param oldName
     * @param newName
     * @param newPrice
     * @param newCategory
     */
    public void editItem(String oldName, String newName, int newPrice, String newCategory) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getItemName().equalsIgnoreCase(oldName)) {
                items.get(i).setItemName(newName);
                items.get(i).setItemCategory(newCategory);
                items.get(i).setItemPrice(newPrice);
                break;
            }
        }
    }

    /**
     * deletes an item in the items array list
     *
     * @param name
     */
    public void deleteItem(String name) {
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getItemName().equalsIgnoreCase(name)) {
                items.remove(i);
                sortItems();
                break;
            }
        }
    }

    /**
     * sorts the items in the items array list in alphabetical order
     */
    public void sortItems() {
        for (int i = 1; i < items.size(); i++) {
            if (items.get(i).getItemCategory().compareTo(items.get(i - 1).getItemCategory()) < 1) {
                for (int j = i; j > 0; j--) {
                    if (items.get(j).getItemCategory().compareTo(items.get(j - 1).getItemCategory()) < 1) {
                        Item temp = items.remove(j);
                        items.add(j, items.get(j - 1));
                        items.remove(j - 1);
                        items.add(j - 1, temp);
                    } else {
                        break;
                    }
                }
            }
        }

        int j = 0;
        ArrayList<ArrayList<Item>> tempItemLists = new ArrayList<>();
        for (int i = 0; i < categories.size(); i++) {
            ArrayList<Item> tempItemList = new ArrayList<>();
            while (j < items.size() && items.get(j).getItemCategory().equalsIgnoreCase(categories.get(i))) {
                tempItemList.add(items.get(j));
                j++;
            }
            tempItemLists.add(tempItemList);
        }

        for (int i = 0; i < tempItemLists.size(); i++) {
            for (int k = 1; k < tempItemLists.get(i).size(); k++) {
                if (tempItemLists.get(i).get(k).getItemName().compareTo(tempItemLists.get(i).get(k - 1).getItemName()) < 1) {
                    for (int x = k; x > 0; x--) {
                        if (tempItemLists.get(i).get(x).getItemName().compareTo(tempItemLists.get(i).get(x - 1).getItemName()) < 1) {
                            Item temp = tempItemLists.get(i).remove(x);
                            tempItemLists.get(i).add(x, tempItemLists.get(i).get(x - 1));
                            tempItemLists.get(i).remove(x - 1);
                            tempItemLists.get(i).add(x - 1, temp);
                        } else {
                            break;
                        }
                    }
                }
            }
        }

        items.clear();
        for (int i = 0; i < tempItemLists.size(); i++) {
            for (int k = 0; k < tempItemLists.get(i).size(); k++) {
                items.add(tempItemLists.get(i).get(k));
            }
        }
    }
}
