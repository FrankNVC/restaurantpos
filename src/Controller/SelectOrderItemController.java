/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Main.Application;
import Model.Restaurant;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Nguyen Minh Sang
 */
public class SelectOrderItemController implements ListSelectionListener {

    private Restaurant r;

    /**
     *
     * @param r
     */
    public SelectOrderItemController(Restaurant r) {
        this.r = r;
    }

    /**
     * defines events when an order item is selected
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        JTable list_order_detail = Application.cmv.getTable_order_detail();

        if (list_order_detail.getSelectedRows().length != 1) {
            Application.cmv.getBtn_edit_order().setEnabled(false);
        } else {
            Application.cmv.getBtn_edit_order().setEnabled(true);
        }
        if (list_order_detail.getSelectedRows().length < 1) {
            Application.cmv.getBtn_delete_order().setEnabled(false);
        }
        if (list_order_detail.getSelectedRows().length > 0) {
            Application.cmv.getBtn_delete_order().setEnabled(true);
        }
    }
}
