/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Main.Application;
import Model.Restaurant;
import Utilities.Localization;
import Utilities.StringUtil;
import View.CreateEditDiscountForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author s3393322
 */
public class CreateDiscountController implements ActionListener {

    /**
     * defines event when a discount is created
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        // Initialise the create discount form
        Restaurant r = Application.r;
        CreateEditDiscountForm cedf = new CreateEditDiscountForm(false);
        cedf.setRequestFocusEnabled(true);
        boolean created = false;
        Object result;
        do {
            // Process the create category form
            cedf.getD().setVisible(true);
            result = cedf.getP().getValue();

            if (result == cedf.getP().getOptions()[0]) {
                String discount_name;
                discount_name = StringUtil.TrimSpace(cedf.getTxt_discount().getText());
                if (discount_name.equals("")) {
                    r.displayError(cedf.getLbl_discount_error(), Localization.getResourceBundle().getString("CreateDiscountController.discountNameRequired"));
                } else if (!r.validateDiscount(discount_name, null)) {
                    r.displayError(cedf.getLbl_discount_error(), Localization.getResourceBundle().getString("CreateDiscountController.discountNameAlreadyExit"));
                } else if (!cedf.getTxt_discount_value().getText().trim().equals("")) {
                    cedf.getLbl_discount_error().setText(null);
                    r.createDiscount(discount_name, Integer.parseInt(cedf.getTxt_discount_value().getText().trim()));
                    created = true;
                    JOptionPane.showMessageDialog(null, Localization.getResourceBundle().getString("CreateDiscountController.createDiscountSuccessfully.message")+ "\"" + discount_name + "\"!", Localization.getResourceBundle().getString("CreateDiscountController.createDiscountSuccessfully.title"), JOptionPane.INFORMATION_MESSAGE);
                    r.update();
                } else {
                    r.displayError(cedf.getLbl_discount_value_error(), Localization.getResourceBundle().getString("CreateDiscountController.discountValueRequired"));
                }
            }
        } while (!created && result == cedf.getP().getOptions()[0]);
    }
}
