package View;

import Controller.CreateCustomerController;
import Controller.DeleteCustomerController;
import Controller.EditCustomerController;
import Controller.SearchCustomerController;
import Controller.SelectCustomerController;
import Model.Restaurant;
import Utilities.JPagingPanel;
import Utilities.Localization;
import Utilities.REPOSButton;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Frank NgVietCuong
 */
public class CustomerView extends JFrame implements Observer {

    private Restaurant r;
    private JMenuBar menuBar = new JMenuBar();
    private JMenu menu_customer = new JMenu("Customer");
    private JMenuItem menu_create = new JMenuItem("Create");
    private JMenuItem menu_edit = new JMenuItem("Edit");
    private JMenuItem menu_delete = new JMenuItem("Delete");
    private JMenuItem menu_search = new JMenuItem("Search");
    private JPanel pnl_west = new JPanel(new BorderLayout());
    private JPanel pnl_west_search = new JPanel(new BorderLayout());
    private JPanel pnl_west_search_left = new JPanel(new GridLayout(2, 1));
    private JPanel pnl_west_search_filter = new JPanel(new GridLayout(1, 2));
    private JPanel pnl_west_customer_detail_view = new JPanel(new BorderLayout());
    private JPanel pnl_center = new JPanel(new BorderLayout());
    private JPanel pnl_center_tool_bar = new JPanel(new FlowLayout(FlowLayout.LEFT));
    private JPanel pnl_center_customer_grid_view = new JPanel(new BorderLayout());
    private JPanel pnl_main = new JPanel();
    private JTextField txt_search = new JTextField(10);
    private ImageIcon icon_search_normal = new ImageIcon(this.getClass().getResource("/Images/search_user.png"));
    private ImageIcon icon_search_clicked = new ImageIcon(this.getClass().getResource("/Images/search_user_clicked.png"));
    private REPOSButton btn_search = new REPOSButton(icon_search_normal, icon_search_clicked, Localization.getResourceBundle().getString("CustomerView.btn_search"));
    private ImageIcon icon_create_customer_normal = new ImageIcon(this.getClass().getResource("/Images/add_account.png"));
    private ImageIcon icon_create_customer_clicked = new ImageIcon(this.getClass().getResource("/Images/add_account_clicked.png"));
    private REPOSButton btn_create_customer = new REPOSButton(icon_create_customer_normal, icon_create_customer_clicked, Localization.getResourceBundle().getString("CustomerView.btn_create_customer"));
    private ImageIcon icon_delete_customer_normal = new ImageIcon(this.getClass().getResource("/Images/delete_account.png"));
    private ImageIcon icon_delete_customer_clicked = new ImageIcon(this.getClass().getResource("/Images/delete_account_clicked.png"));
    private REPOSButton btn_delete_customer = new REPOSButton(icon_delete_customer_normal, icon_delete_customer_clicked, Localization.getResourceBundle().getString("CustomerView.btn_delete_customer"));
    private ImageIcon icon_edit_customer_normal = new ImageIcon(this.getClass().getResource("/Images/edit_account.png"));
    private ImageIcon icon_edit_customer_clicked = new ImageIcon(this.getClass().getResource("/Images/edit_account_clicked.png"));
    private REPOSButton btn_update_customer = new REPOSButton(icon_edit_customer_normal, icon_edit_customer_clicked, Localization.getResourceBundle().getString("CustomerView.btn_update_customer"));
    private JList list_customer_detail_left = new JList();
    private JList list_customer_detail_right = new JList();
    private JPagingPanel pnl_list_customer;
    private JScrollPane spane_west_customer_detail_left = new JScrollPane(list_customer_detail_left);
    private JScrollPane spane_west_customer_detail_right = new JScrollPane(list_customer_detail_right);
    private DeleteCustomerController dcc;
    private ButtonGroup group_filter = new ButtonGroup();
    private JRadioButton rdb_ssn = new JRadioButton(Localization.getResourceBundle().getString("CustomerView.rdb_ssn"));
    private JRadioButton rdb_phone = new JRadioButton(Localization.getResourceBundle().getString("CustomerView.rdb_phone"));

    private static boolean opening = false;

    public CustomerView(Restaurant r) {
        this.r = r;
        this.setTitle(Localization.getResourceBundle().getString("CustomerView.title"));

        menuBar.add(menu_customer);
        menu_customer.add(menu_create);
        menu_customer.add(menu_edit);
        menu_customer.add(menu_delete);
        menu_customer.add(menu_search);

        dcc = new DeleteCustomerController(this, r);

        // set window size
        Toolkit toolkit = Toolkit.getDefaultToolkit();
        int screenWidth = (int) toolkit.getScreenSize().getWidth();
        int screenHeight = (int) toolkit.getScreenSize().getHeight();

        this.setPreferredSize(new Dimension(screenWidth * 6 / 10, screenHeight * 6 / 10));
        this.setMaximumSize(new Dimension(screenWidth * 6 / 10, screenHeight * 6 / 10));
        this.setMinimumSize(new Dimension(screenWidth * 6 / 10, screenHeight * 6 / 10));

        rdb_ssn.setSelected(true);
        rdb_ssn.setBackground(Color.WHITE);
        rdb_phone.setBackground(Color.WHITE);

        pnl_main.setSize(screenWidth * 6 / 10, screenHeight * 6 / 10);
        pnl_main.setLayout(new BorderLayout());

        pnl_west.setPreferredSize(new Dimension(300, this.getHeight() * 9 / 10));
        pnl_center.setPreferredSize(new Dimension(this.getWidth() - 300, this.getHeight() * 9 / 10));

        pnl_main.add(pnl_west, BorderLayout.WEST);
        pnl_main.add(pnl_center, BorderLayout.CENTER);

        pnl_west_customer_detail_view.setBackground(Color.WHITE);
        pnl_west_search.setBackground(Color.WHITE);
        spane_west_customer_detail_left.setBackground(Color.WHITE);
        spane_west_customer_detail_right.setBackground(Color.WHITE);
        pnl_center.setBackground(Color.WHITE);
        pnl_center_tool_bar.setBackground(Color.WHITE);
        pnl_center_customer_grid_view.setBackground(Color.WHITE);

        txt_search = new JTextField();
        txt_search.setMaximumSize(new Dimension(50, 3));
        txt_search.setPreferredSize(new Dimension(50, 3));

        pnl_west_search_filter.add(rdb_ssn);
        pnl_west_search_filter.add(rdb_phone);

        group_filter.add(rdb_ssn);
        group_filter.add(rdb_phone);

        pnl_west_search_left.add(txt_search);
        pnl_west_search_left.add(pnl_west_search_filter);

        pnl_west_search.add(pnl_west_search_left, BorderLayout.CENTER);
        pnl_west_search.add(btn_search, BorderLayout.EAST);

        //search button
        TitledBorder border_west_search = new TitledBorder(Localization.getResourceBundle().getString("CustomerView.border_west_search"));
        border_west_search.setTitleColor(Color.BLUE);
        pnl_west_search.setBorder(border_west_search);

        pnl_west.add(pnl_west_search, BorderLayout.NORTH);

        // detailed view of customer
        TitledBorder border_west_detail = new TitledBorder(Localization.getResourceBundle().getString("CustomerView.border_west_detail"));
        border_west_detail.setTitleColor(Color.BLUE);
        pnl_west_customer_detail_view.setBorder(border_west_detail);

        spane_west_customer_detail_left.setPreferredSize(new Dimension(150, this.getHeight() * 9 / 10));
        spane_west_customer_detail_left.setBorder(null);

        spane_west_customer_detail_right.setPreferredSize(new Dimension(350, this.getHeight() * 9 / 10));
        spane_west_customer_detail_right.setBorder(null);

        pnl_west_customer_detail_view.add(spane_west_customer_detail_left, BorderLayout.WEST);
        pnl_west_customer_detail_view.add(spane_west_customer_detail_right, BorderLayout.CENTER);

        pnl_west.add(pnl_west_customer_detail_view, BorderLayout.CENTER);

        //grid view of customers
        TitledBorder border_center_customer = new TitledBorder(Localization.getResourceBundle().getString("CustomerView.border_center_customer"));
        border_center_customer.setTitleColor(Color.BLUE);
        pnl_center_customer_grid_view.setBorder(border_center_customer);

        pnl_center_tool_bar.add(btn_create_customer);
        pnl_center_tool_bar.add(btn_update_customer);
        pnl_center_tool_bar.add(btn_delete_customer);

        pnl_center_customer_grid_view.add(pnl_center_tool_bar, BorderLayout.NORTH);

        pnl_center.add(pnl_center_customer_grid_view, BorderLayout.CENTER);

        DefaultListModel fields = new DefaultListModel();
        fields.addElement(Localization.getResourceBundle().getString("CustomerView.customerType"));
        fields.addElement(Localization.getResourceBundle().getString("CustomerView.fullName"));
        fields.addElement(Localization.getResourceBundle().getString("CustomerView.gender"));
        fields.addElement(Localization.getResourceBundle().getString("CustomerView.socialSecurityNumber"));
        fields.addElement(Localization.getResourceBundle().getString("CustomerView.dob"));
        fields.addElement(Localization.getResourceBundle().getString("CustomerView.phoneNumber"));
        fields.addElement(Localization.getResourceBundle().getString("CustomerView.email"));
        fields.addElement(Localization.getResourceBundle().getString("CustomerView.address"));
        fields.addElement(Localization.getResourceBundle().getString("CustomerView.points"));

        btn_update_customer.setEnabled(false);
        btn_delete_customer.setEnabled(false);

        list_customer_detail_left.setModel(fields);

        btn_search.addActionListener(new SearchCustomerController(r, this));
        btn_create_customer.addActionListener(new CreateCustomerController());
        btn_update_customer.addActionListener(new EditCustomerController(r, this));
        btn_delete_customer.addActionListener(dcc);

        txt_search.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "search");
        txt_search.getActionMap().put("search", new SearchCustomerController(r, this));

        this.add(pnl_main, BorderLayout.CENTER);
        this.setJMenuBar(menuBar);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                String options[] = {Localization.getResourceBundle().getString("CustomerView.options.Confirm"), Localization.getResourceBundle().getString("CustomerView.options.Cancel")};
                if (JOptionPane.showOptionDialog(null, Localization.getResourceBundle().getString("CustomerView.exitCustomerView.message"), Localization.getResourceBundle().getString("CustomerView.exitCustomerView.title"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, null) == JOptionPane.YES_OPTION) {
                    setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    CustomerView.setOpening(false);
                    super.windowClosing(e);
                }
            }
        });

        addObs();
        reload();
    }

    public static boolean isOpening() {
        return opening;
    }

    public static void setOpening(boolean val) {
        opening = val;
    }

    public JList getList_customer_detail_right() {
        return list_customer_detail_right;
    }

    public JPanel getPnl_center_customer_grid_view() {
        return pnl_center_customer_grid_view;
    }

    public JTextField getTxt_search() {
        return txt_search;
    }

    public REPOSButton getBtn_search() {
        return btn_search;
    }

    public REPOSButton getBtn_delete_customer() {
        return btn_delete_customer;
    }

    public REPOSButton getBtn_update_customer() {
        return btn_update_customer;
    }

    public JPagingPanel getPnl_list_customer() {
        return pnl_list_customer;
    }

    public void setPnl_list_customer(JPagingPanel pnl_list_customer) {
        this.pnl_list_customer = pnl_list_customer;
    }

    public JRadioButton getRdb_phone() {
        return rdb_phone;
    }

    public JRadioButton getRdb_ssn() {
        return rdb_ssn;
    }

    private void addObs() {
        r.addObserver(this);
    }

    public void reload() {
        String[] column_names = {Localization.getResourceBundle().getString("CustomerView.fullName_1"), Localization.getResourceBundle().getString("CustomerView.gender_1"), Localization.getResourceBundle().getString("CustomerView.age_1"), Localization.getResourceBundle().getString("CustomerView.ssn_1"), Localization.getResourceBundle().getString("CustomerView.mobile_1"), Localization.getResourceBundle().getString("CustomerView.type_1"), Localization.getResourceBundle().getString("CustomerView.point_1")};
        ArrayList<String[]> data = r.getCustomerList(null, true);
        DefaultTableModel list_customer_table_model = new DefaultTableModel(null, column_names) {
            @Override
            public Class<?> getColumnClass(int column) {
                return (column == 0) ? Integer.class : Object.class;
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        for (int i = 0; i < data.size(); i++) {
            list_customer_table_model.addRow(data.get(i));
        }

        if (pnl_list_customer != null) {
            pnl_center_customer_grid_view.remove(pnl_list_customer);
        }
        pnl_list_customer = new JPagingPanel(list_customer_table_model);

        pnl_list_customer.getTable().setModel(list_customer_table_model);
        pnl_list_customer.getTable().setCellSelectionEnabled(false);
        pnl_list_customer.getTable().setRowSelectionAllowed(true);
        pnl_list_customer.getTable().getSelectionModel().addListSelectionListener(new SelectCustomerController(r, this));
        pnl_list_customer.getTable().revalidate();

        pnl_center_customer_grid_view.add(pnl_list_customer, BorderLayout.CENTER);

        btn_delete_customer.setEnabled(false);
        btn_update_customer.setEnabled(false);

        validate();
    }

    @Override
    public void update(Observable o, Object o1) {
        reload();
    }
}
